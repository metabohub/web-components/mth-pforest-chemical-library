import { Peaklist } from '@/components/MthPfHighchartsPeaklistsViewer.vue';
// peaklists
const peaklist1 = {} as Peaklist;
peaklist1.name = "D-Glucose; LC-ESI-TOF; MS; POSITIVE; <em>PFs000XXX</em>";
peaklist1.peaks = [
    { mz: 204.05606, i: 1.21, c: "C5 13C H12 Na O6", a: "[(M+Na)]+ (13C)", },
    { mz: 203.05262, i: 30.44, c: "C6 H12 Na O6", a: "[(M+Na)]+", },
    { mz: 384.1196, i: 0.13, c: "C11 13C H24 Na O12", a: "[(2M+Na)]+ (13C)", },
    { mz: 272.0447, i: 0.01, c: "C6 13C H13 Na2 O8", a: "[(M+Na)+(HCOONa)]+ (13C)", },
    { mz: 383.1162, i: 1.39, c: "C12 H24 Na O12", a: "[(2M+Na)]+", },
    { mz: 221.02551, i: 1.20, c: "C6 H12 K0 41K O6", a: "[(M+K)]+ (41K)", },
    { mz: 205.05698, i: 0.16, c: "C6 H12 Na O5 18O", a: "[(M+Na)]+ (18O)", },
    { mz: 219.02657, i: 0.74, c: "C6 H12 K O6", a: "[(M+K)]+", },
    { mz: 271.04045, i: 0.75, c: "C7 H13 Na2 O8", a: "[(M+Na)+(HCOONa)]+", },
    { mz: 220.02941, i: 0.01, c: "C5 13C H12 K O6", a: "[(M+K)]+ (13C)", },
];
const peaklist2 = {} as Peaklist;
peaklist2.name = "peaklist to test";
peaklist2.peaks = [
    { mz: 203.05, i: 40.1, },
    { mz: 384.11, i: 2.1, },
    { mz: 383.12, i: 1.9, },
    { mz: 221.03, i: 1.7, },
    { mz: 219.027, i: 0.7, },
    { mz: 220.09, i: 3.3, },
];
const peaklist3 = {} as Peaklist;
peaklist3.name = "D-Glucose; 7.0; NOESY-1D (noesygppr1d) - 500MHz; <em>PFs000XXX</em>";
peaklist3.peaks = [
    { ppm: 5.24, i: 1.57, a: 'H1a' },
    { ppm: 5.23, i: 1.56, a: 'H1a' },
    { ppm: 4.66, i: 1.24, a: 'H1b' },
    { ppm: 4.64, i: 1.30, a: 'H1b' },
    { ppm: 3.9, i: 1.36, a: 'H6b and/or H6\'b' },
    { ppm: 3.9, i: 1.26, a: 'H6b and/or H6\'b' },
    { ppm: 3.90, i: 1.77, a: 'H6b and/or H6\'b' },
    { ppm: 3.89, i: 1.59, a: 'H6b and/or H6\'b' },
    { ppm: 3.84, i: 1.34, a: 'H5a + H6a and/or H6\'a' },
    { ppm: 3.74, i: 0.44, a: 'H6 and/or H6/ + H3' },
    { ppm: 3.55, i: 1.08, a: 'H2a' },
    { ppm: 3.54, i: 1.05, a: 'H2a' },
    { ppm: 3.53, i: 0.89, a: 'H2a' },
    { ppm: 3.52, i: 0.89, a: 'H2a' },
    { ppm: 3.48, i: 0.73, a: 'H5b and H3b' },
    { ppm: 3.41, i: 0.68, a: 'H4' },
    { ppm: 3.26, i: 1.70, a: 'H2b' },
    { ppm: 3.25, i: 2.10, a: 'H2b' },
    { ppm: 3.23, i: 1.36, a: 'H2b' },
];
const peaklist4 = {} as Peaklist;
peaklist4.name = "peaklist to test";
peaklist4.peaks = [
    { ppm: 5.2, i: 1.6 },
    { ppm: 5.2, i: 1.6 },
    { ppm: 4.6, i: 1.2 },
    { ppm: 4.6, i: 1.3 },
    { ppm: 3.5, i: 1.1 },
    { ppm: 3.5, i: 1.1 },
    { ppm: 3.5, i: 0.9 },
    { ppm: 3.5, i: 0.9 },
];
export { peaklist1, peaklist2, peaklist3, peaklist4 }
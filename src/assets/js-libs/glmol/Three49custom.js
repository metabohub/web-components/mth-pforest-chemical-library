"use strict";
const logger = false;
var THREE = THREE || {
    REVISION: "49"
};
self.Int32Array || (self.Int32Array = Array,
self.Float32Array = Array);
(function() {
    for (var e = 0, d = ["ms", "moz", "webkit", "o"], f = 0; f < d.length && !window.requestAnimationFrame; ++f) {
        window.requestAnimationFrame = window[d[f] + "RequestAnimationFrame"];
        window.cancelAnimationFrame = window[d[f] + "CancelAnimationFrame"] || window[d[f] + "CancelRequestAnimationFrame"]
    }
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(a) {
            var j = Date.now()
              , i = Math.max(0, 16 - (j - e))
              , g = window.setTimeout(function() {
                a(j + i)
            }, i);
            e = j + i;
            return g
        }
    }
    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(b) {
            clearTimeout(b)
        }
    }
}
)();
THREE.Clock = function(b) {
    this.autoStart = b !== void 0 ? b : true;
    this.elapsedTime = this.oldTime = this.startTime = 0;
    this.running = false
}
;
THREE.Clock.prototype.start = function() {
    this.oldTime = this.startTime = Date.now();
    this.running = true
}
;
THREE.Clock.prototype.stop = function() {
    this.getElapsedTime();
    this.running = false
}
;
THREE.Clock.prototype.getElapsedTime = function() {
    return this.elapsedTime = this.elapsedTime + this.getDelta()
}
;
THREE.Clock.prototype.getDelta = function() {
    var d = 0;
    this.autoStart && !this.running && this.start();
    if (this.running) {
        var c = Date.now()
          , d = 0.001 * (c - this.oldTime);
        this.oldTime = c;
        this.elapsedTime = this.elapsedTime + d
    }
    return d
}
;
THREE.Color = function(b) {
    b !== void 0 && this.setHex(b);
    return this
}
;
THREE.Color.prototype = {
    constructor: THREE.Color,
    r: 1,
    g: 1,
    b: 1,
    copy: function(b) {
        this.r = b.r;
        this.g = b.g;
        this.b = b.b;
        return this
    },
    copyGammaToLinear: function(b) {
        this.r = b.r * b.r;
        this.g = b.g * b.g;
        this.b = b.b * b.b;
        return this
    },
    copyLinearToGamma: function(b) {
        this.r = Math.sqrt(b.r);
        this.g = Math.sqrt(b.g);
        this.b = Math.sqrt(b.b);
        return this
    },
    convertGammaToLinear: function() {
        var e = this.r
          , d = this.g
          , f = this.b;
        this.r = e * e;
        this.g = d * d;
        this.b = f * f;
        return this
    },
    convertLinearToGamma: function() {
        this.r = Math.sqrt(this.r);
        this.g = Math.sqrt(this.g);
        this.b = Math.sqrt(this.b);
        return this
    },
    setRGB: function(e, d, f) {
        this.r = e;
        this.g = d;
        this.b = f;
        return this
    },
    setHSV: function(h, g, l) {
        var k, j, i;
        if (l === 0) {
            this.r = this.g = this.b = 0
        } else {
            k = Math.floor(h * 6);
            j = h * 6 - k;
            h = l * (1 - g);
            i = l * (1 - g * j);
            g = l * (1 - g * (1 - j));
            switch (k) {
            case 1:
                this.r = i;
                this.g = l;
                this.b = h;
                break;
            case 2:
                this.r = h;
                this.g = l;
                this.b = g;
                break;
            case 3:
                this.r = h;
                this.g = i;
                this.b = l;
                break;
            case 4:
                this.r = g;
                this.g = h;
                this.b = l;
                break;
            case 5:
                this.r = l;
                this.g = h;
                this.b = i;
                break;
            case 6:
            case 0:
                this.r = l;
                this.g = g;
                this.b = h
            }
        }
        return this
    },
    setHex: function(b) {
        b = Math.floor(b);
        this.r = (b >> 16 & 255) / 255;
        this.g = (b >> 8 & 255) / 255;
        this.b = (b & 255) / 255;
        return this
    },
    lerpSelf: function(d, c) {
        this.r = this.r + (d.r - this.r) * c;
        this.g = this.g + (d.g - this.g) * c;
        this.b = this.b + (d.b - this.b) * c;
        return this
    },
    getHex: function() {
        return Math.floor(this.r * 255) << 16 ^ Math.floor(this.g * 255) << 8 ^ Math.floor(this.b * 255)
    },
    getContextStyle: function() {
        return "rgb(" + Math.floor(this.r * 255) + "," + Math.floor(this.g * 255) + "," + Math.floor(this.b * 255) + ")"
    },
    clone: function() {
        return (new THREE.Color).setRGB(this.r, this.g, this.b)
    }
};
THREE.Vector2 = function(d, c) {
    this.x = d || 0;
    this.y = c || 0
}
;
THREE.Vector2.prototype = {
    constructor: THREE.Vector2,
    set: function(d, c) {
        this.x = d;
        this.y = c;
        return this
    },
    copy: function(b) {
        this.x = b.x;
        this.y = b.y;
        return this
    },
    add: function(d, c) {
        this.x = d.x + c.x;
        this.y = d.y + c.y;
        return this
    },
    addSelf: function(b) {
        this.x = this.x + b.x;
        this.y = this.y + b.y;
        return this
    },
    sub: function(d, c) {
        this.x = d.x - c.x;
        this.y = d.y - c.y;
        return this
    },
    subSelf: function(b) {
        this.x = this.x - b.x;
        this.y = this.y - b.y;
        return this
    },
    multiplyScalar: function(b) {
        this.x = this.x * b;
        this.y = this.y * b;
        return this
    },
    divideScalar: function(b) {
        if (b) {
            this.x = this.x / b;
            this.y = this.y / b
        } else {
            this.set(0, 0)
        }
        return this
    },
    negate: function() {
        return this.multiplyScalar(-1)
    },
    dot: function(b) {
        return this.x * b.x + this.y * b.y
    },
    lengthSq: function() {
        return this.x * this.x + this.y * this.y
    },
    length: function() {
        return Math.sqrt(this.lengthSq())
    },
    normalize: function() {
        return this.divideScalar(this.length())
    },
    distanceTo: function(b) {
        return Math.sqrt(this.distanceToSquared(b))
    },
    distanceToSquared: function(d) {
        var c = this.x - d.x
          , d = this.y - d.y;
        return c * c + d * d
    },
    setLength: function(b) {
        return this.normalize().multiplyScalar(b)
    },
    lerpSelf: function(d, c) {
        this.x = this.x + (d.x - this.x) * c;
        this.y = this.y + (d.y - this.y) * c;
        return this
    },
    equals: function(b) {
        return b.x === this.x && b.y === this.y
    },
    isZero: function() {
        return this.lengthSq() < 0.0001
    },
    clone: function() {
        return new THREE.Vector2(this.x,this.y)
    }
};
THREE.Vector3 = function(e, d, f) {
    this.x = e || 0;
    this.y = d || 0;
    this.z = f || 0
}
;
THREE.Vector3.prototype = {
    constructor: THREE.Vector3,
    set: function(e, d, f) {
        this.x = e;
        this.y = d;
        this.z = f;
        return this
    },
    setX: function(b) {
        this.x = b;
        return this
    },
    setY: function(b) {
        this.y = b;
        return this
    },
    setZ: function(b) {
        this.z = b;
        return this
    },
    copy: function(b) {
        this.x = b.x;
        this.y = b.y;
        this.z = b.z;
        return this
    },
    add: function(d, c) {
        this.x = d.x + c.x;
        this.y = d.y + c.y;
        this.z = d.z + c.z;
        return this
    },
    addSelf: function(b) {
        this.x = this.x + b.x;
        this.y = this.y + b.y;
        this.z = this.z + b.z;
        return this
    },
    addScalar: function(b) {
        this.x = this.x + b;
        this.y = this.y + b;
        this.z = this.z + b;
        return this
    },
    sub: function(d, c) {
        this.x = d.x - c.x;
        this.y = d.y - c.y;
        this.z = d.z - c.z;
        return this
    },
    subSelf: function(b) {
        this.x = this.x - b.x;
        this.y = this.y - b.y;
        this.z = this.z - b.z;
        return this
    },
    multiply: function(d, c) {
        this.x = d.x * c.x;
        this.y = d.y * c.y;
        this.z = d.z * c.z;
        return this
    },
    multiplySelf: function(b) {
        this.x = this.x * b.x;
        this.y = this.y * b.y;
        this.z = this.z * b.z;
        return this
    },
    multiplyScalar: function(b) {
        this.x = this.x * b;
        this.y = this.y * b;
        this.z = this.z * b;
        return this
    },
    divideSelf: function(b) {
        this.x = this.x / b.x;
        this.y = this.y / b.y;
        this.z = this.z / b.z;
        return this
    },
    divideScalar: function(b) {
        if (b) {
            this.x = this.x / b;
            this.y = this.y / b;
            this.z = this.z / b
        } else {
            this.z = this.y = this.x = 0
        }
        return this
    },
    negate: function() {
        return this.multiplyScalar(-1)
    },
    dot: function(b) {
        return this.x * b.x + this.y * b.y + this.z * b.z
    },
    lengthSq: function() {
        return this.x * this.x + this.y * this.y + this.z * this.z
    },
    length: function() {
        return Math.sqrt(this.lengthSq())
    },
    lengthManhattan: function() {
        return Math.abs(this.x) + Math.abs(this.y) + Math.abs(this.z)
    },
    normalize: function() {
        return this.divideScalar(this.length())
    },
    setLength: function(b) {
        return this.normalize().multiplyScalar(b)
    },
    lerpSelf: function(d, c) {
        this.x = this.x + (d.x - this.x) * c;
        this.y = this.y + (d.y - this.y) * c;
        this.z = this.z + (d.z - this.z) * c;
        return this
    },
    cross: function(d, c) {
        this.x = d.y * c.z - d.z * c.y;
        this.y = d.z * c.x - d.x * c.z;
        this.z = d.x * c.y - d.y * c.x;
        return this
    },
    crossSelf: function(f) {
        var e = this.x
          , h = this.y
          , g = this.z;
        this.x = h * f.z - g * f.y;
        this.y = g * f.x - e * f.z;
        this.z = e * f.y - h * f.x;
        return this
    },
    distanceTo: function(b) {
        return Math.sqrt(this.distanceToSquared(b))
    },
    distanceToSquared: function(b) {
        return (new THREE.Vector3).sub(this, b).lengthSq()
    },
    getPositionFromMatrix: function(b) {
        this.x = b.elements[12];
        this.y = b.elements[13];
        this.z = b.elements[14];
        return this
    },
    getRotationFromMatrix: function(r, q) {
        var p = q ? q.x : 1
          , o = q ? q.y : 1
          , n = q ? q.z : 1
          , m = r.elements[0] / p
          , k = r.elements[4] / o
          , p = r.elements[1] / p
          , o = r.elements[5] / o
          , j = r.elements[9] / n
          , g = r.elements[10] / n;
        this.y = Math.asin(r.elements[8] / n);
        n = Math.cos(this.y);
        if (Math.abs(n) > 0.00001) {
            this.x = Math.atan2(-j / n, g / n);
            this.z = Math.atan2(-k / n, m / n)
        } else {
            this.x = 0;
            this.z = Math.atan2(p, o)
        }
        return this
    },
    getScaleFromMatrix: function(e) {
        var d = this.set(e.elements[0], e.elements[1], e.elements[2]).length()
          , f = this.set(e.elements[4], e.elements[5], e.elements[6]).length()
          , e = this.set(e.elements[8], e.elements[9], e.elements[10]).length();
        this.x = d;
        this.y = f;
        this.z = e
    },
    equals: function(b) {
        return b.x === this.x && b.y === this.y && b.z === this.z
    },
    isZero: function() {
        return this.lengthSq() < 0.0001
    },
    clone: function() {
        return new THREE.Vector3(this.x,this.y,this.z)
    }
};
THREE.Vector4 = function(f, e, h, g) {
    this.x = f || 0;
    this.y = e || 0;
    this.z = h || 0;
    this.w = g !== void 0 ? g : 1
}
;
THREE.Vector4.prototype = {
    constructor: THREE.Vector4,
    set: function(f, e, h, g) {
        this.x = f;
        this.y = e;
        this.z = h;
        this.w = g;
        return this
    },
    copy: function(b) {
        this.x = b.x;
        this.y = b.y;
        this.z = b.z;
        this.w = b.w !== void 0 ? b.w : 1;
        return this
    },
    add: function(d, c) {
        this.x = d.x + c.x;
        this.y = d.y + c.y;
        this.z = d.z + c.z;
        this.w = d.w + c.w;
        return this
    },
    addSelf: function(b) {
        this.x = this.x + b.x;
        this.y = this.y + b.y;
        this.z = this.z + b.z;
        this.w = this.w + b.w;
        return this
    },
    sub: function(d, c) {
        this.x = d.x - c.x;
        this.y = d.y - c.y;
        this.z = d.z - c.z;
        this.w = d.w - c.w;
        return this
    },
    subSelf: function(b) {
        this.x = this.x - b.x;
        this.y = this.y - b.y;
        this.z = this.z - b.z;
        this.w = this.w - b.w;
        return this
    },
    multiplyScalar: function(b) {
        this.x = this.x * b;
        this.y = this.y * b;
        this.z = this.z * b;
        this.w = this.w * b;
        return this
    },
    divideScalar: function(b) {
        if (b) {
            this.x = this.x / b;
            this.y = this.y / b;
            this.z = this.z / b;
            this.w = this.w / b
        } else {
            this.z = this.y = this.x = 0;
            this.w = 1
        }
        return this
    },
    negate: function() {
        return this.multiplyScalar(-1)
    },
    dot: function(b) {
        return this.x * b.x + this.y * b.y + this.z * b.z + this.w * b.w
    },
    lengthSq: function() {
        return this.dot(this)
    },
    length: function() {
        return Math.sqrt(this.lengthSq())
    },
    normalize: function() {
        return this.divideScalar(this.length())
    },
    setLength: function(b) {
        return this.normalize().multiplyScalar(b)
    },
    lerpSelf: function(d, c) {
        this.x = this.x + (d.x - this.x) * c;
        this.y = this.y + (d.y - this.y) * c;
        this.z = this.z + (d.z - this.z) * c;
        this.w = this.w + (d.w - this.w) * c;
        return this
    },
    clone: function() {
        return new THREE.Vector4(this.x,this.y,this.z,this.w)
    }
};
THREE.Frustum = function() {
    this.planes = [new THREE.Vector4, new THREE.Vector4, new THREE.Vector4, new THREE.Vector4, new THREE.Vector4, new THREE.Vector4]
}
;
THREE.Frustum.prototype.setFromMatrix = function(G) {
    var F, E = this.planes, D = G.elements, G = D[0];
    F = D[1];
    var C = D[2]
      , B = D[3]
      , A = D[4]
      , z = D[5]
      , w = D[6]
      , x = D[7]
      , y = D[8]
      , v = D[9]
      , u = D[10]
      , t = D[11]
      , H = D[12]
      , r = D[13]
      , g = D[14]
      , D = D[15];
    E[0].set(B - G, x - A, t - y, D - H);
    E[1].set(B + G, x + A, t + y, D + H);
    E[2].set(B + F, x + z, t + v, D + r);
    E[3].set(B - F, x - z, t - v, D - r);
    E[4].set(B - C, x - w, t - u, D - g);
    E[5].set(B + C, x + w, t + u, D + g);
    for (G = 0; G < 6; G++) {
        F = E[G];
        F.divideScalar(Math.sqrt(F.x * F.x + F.y * F.y + F.z * F.z))
    }
}
;
THREE.Frustum.prototype.contains = function(g) {
    for (var f = this.planes, j = g.matrixWorld, i = j.elements, j = -g.geometry.boundingSphere.radius * j.getMaxScaleOnAxis(), h = 0; h < 6; h++) {
        g = f[h].x * i[12] + f[h].y * i[13] + f[h].z * i[14] + f[h].w;
        if (g <= j) {
            return false
        }
    }
    return true
}
;
THREE.Frustum.__v1 = new THREE.Vector3;
THREE.Ray = function(ac, ab) {
    function aa(e, d, f) {
        E.sub(f, e);
        C = E.dot(d);
        J = L.add(e, G.copy(d).multiplyScalar(C));
        return v = f.distanceTo(J)
    }
    function Z(f, e, i, h) {
        E.sub(h, e);
        L.sub(i, e);
        G.sub(f, e);
        D = E.dot(E);
        z = E.dot(L);
        F = E.dot(G);
        t = L.dot(L);
        r = L.dot(G);
        g = 1 / (D * t - z * z);
        M = (t * F - z * r) * g;
        x = (D * r - z * F) * g;
        return M >= 0 && x >= 0 && M + x < 1
    }
    this.origin = ac || new THREE.Vector3;
    this.direction = ab || new THREE.Vector3;
    var X = 0.0001;
    this.setPrecision = function(b) {
        X = b
    }
    ;
    var W = new THREE.Vector3
      , V = new THREE.Vector3
      , U = new THREE.Vector3
      , R = new THREE.Vector3
      , S = new THREE.Vector3
      , T = new THREE.Vector3
      , Q = new THREE.Vector3
      , P = new THREE.Vector3
      , O = new THREE.Vector3;
    this.intersectObject = function(j) {
        var i, m = [];
        if (j instanceof THREE.Particle) {
            var d = aa(this.origin, this.direction, j.matrixWorld.getPosition());
            if (d > j.scale.x) {
                return []
            }
            i = {
                distance: d,
                point: j.position,
                face: null,
                object: j
            };
            m.push(i)
        } else {
            if (j instanceof THREE.Mesh) {
                var d = aa(this.origin, this.direction, j.matrixWorld.getPosition())
                  , f = THREE.Frustum.__v1.set(j.matrixWorld.getColumnX().length(), j.matrixWorld.getColumnY().length(), j.matrixWorld.getColumnZ().length());
                if (d > j.geometry.boundingSphere.radius * Math.max(f.x, Math.max(f.y, f.z))) {
                    return m
                }
                var c, l, h = j.geometry, k = h.vertices, e;
                j.matrixRotationWorld.extractRotation(j.matrixWorld);
                d = 0;
                for (f = h.faces.length; d < f; d++) {
                    i = h.faces[d];
                    S.copy(this.origin);
                    T.copy(this.direction);
                    e = j.matrixWorld;
                    Q = e.multiplyVector3(Q.copy(i.centroid)).subSelf(S);
                    P = j.matrixRotationWorld.multiplyVector3(P.copy(i.normal));
                    c = T.dot(P);
                    if (!(Math.abs(c) < X)) {
                        l = P.dot(Q) / c;
                        if (!(l < 0) && (j.doubleSided || (j.flipSided ? c > 0 : c < 0))) {
                            O.add(S, T.multiplyScalar(l));
                            if (i instanceof THREE.Face3) {
                                W = e.multiplyVector3(W.copy(k[i.a]));
                                V = e.multiplyVector3(V.copy(k[i.b]));
                                U = e.multiplyVector3(U.copy(k[i.c]));
                                if (Z(O, W, V, U)) {
                                    i = {
                                        distance: S.distanceTo(O),
                                        point: O.clone(),
                                        face: i,
                                        object: j
                                    };
                                    m.push(i)
                                }
                            } else {
                                if (i instanceof THREE.Face4) {
                                    W = e.multiplyVector3(W.copy(k[i.a]));
                                    V = e.multiplyVector3(V.copy(k[i.b]));
                                    U = e.multiplyVector3(U.copy(k[i.c]));
                                    R = e.multiplyVector3(R.copy(k[i.d]));
                                    if (Z(O, W, V, R) || Z(O, V, U, R)) {
                                        i = {
                                            distance: S.distanceTo(O),
                                            point: O.clone(),
                                            face: i,
                                            object: j
                                        };
                                        m.push(i)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return m
    }
    ;
    this.intersectObjects = function(f) {
        for (var e = [], i = 0, h = f.length; i < h; i++) {
            Array.prototype.push.apply(e, this.intersectObject(f[i]))
        }
        e.sort(function(d, c) {
            return d.distance - c.distance
        });
        return e
    }
    ;
    var E = new THREE.Vector3, L = new THREE.Vector3, G = new THREE.Vector3, C, J, v, D, z, F, t, r, g, M, x
}
;
THREE.Rectangle = function() {
    function j() {
        m = o - g;
        l = n - p
    }
    var g, p, o, n, m, l, k = true;
    this.getX = function() {
        return g
    }
    ;
    this.getY = function() {
        return p
    }
    ;
    this.getWidth = function() {
        return m
    }
    ;
    this.getHeight = function() {
        return l
    }
    ;
    this.getLeft = function() {
        return g
    }
    ;
    this.getTop = function() {
        return p
    }
    ;
    this.getRight = function() {
        return o
    }
    ;
    this.getBottom = function() {
        return n
    }
    ;
    this.set = function(d, c, b, a) {
        k = false;
        g = d;
        p = c;
        o = b;
        n = a;
        j()
    }
    ;
    this.addPoint = function(b, a) {
        if (k) {
            k = false;
            g = b;
            p = a;
            o = b;
            n = a
        } else {
            g = g < b ? g : b;
            p = p < a ? p : a;
            o = o > b ? o : b;
            n = n > a ? n : a
        }
        j()
    }
    ;
    this.add3Points = function(d, c, b, a, i, e) {
        if (k) {
            k = false;
            g = d < b ? d < i ? d : i : b < i ? b : i;
            p = c < a ? c < e ? c : e : a < e ? a : e;
            o = d > b ? d > i ? d : i : b > i ? b : i;
            n = c > a ? c > e ? c : e : a > e ? a : e
        } else {
            g = d < b ? d < i ? d < g ? d : g : i < g ? i : g : b < i ? b < g ? b : g : i < g ? i : g;
            p = c < a ? c < e ? c < p ? c : p : e < p ? e : p : a < e ? a < p ? a : p : e < p ? e : p;
            o = d > b ? d > i ? d > o ? d : o : i > o ? i : o : b > i ? b > o ? b : o : i > o ? i : o;
            n = c > a ? c > e ? c > n ? c : n : e > n ? e : n : a > e ? a > n ? a : n : e > n ? e : n
        }
        j()
    }
    ;
    this.addRectangle = function(a) {
        if (k) {
            k = false;
            g = a.getLeft();
            p = a.getTop();
            o = a.getRight();
            n = a.getBottom()
        } else {
            g = g < a.getLeft() ? g : a.getLeft();
            p = p < a.getTop() ? p : a.getTop();
            o = o > a.getRight() ? o : a.getRight();
            n = n > a.getBottom() ? n : a.getBottom()
        }
        j()
    }
    ;
    this.inflate = function(a) {
        g = g - a;
        p = p - a;
        o = o + a;
        n = n + a;
        j()
    }
    ;
    this.minSelf = function(a) {
        g = g > a.getLeft() ? g : a.getLeft();
        p = p > a.getTop() ? p : a.getTop();
        o = o < a.getRight() ? o : a.getRight();
        n = n < a.getBottom() ? n : a.getBottom();
        j()
    }
    ;
    this.intersects = function(b) {
        return o < b.getLeft() || g > b.getRight() || n < b.getTop() || p > b.getBottom() ? false : true
    }
    ;
    this.empty = function() {
        k = true;
        n = o = p = g = 0;
        j()
    }
    ;
    this.isEmpty = function() {
        return k
    }
}
;
THREE.Math = {
    clamp: function(e, d, f) {
        return e < d ? d : e > f ? f : e
    },
    clampBottom: function(d, c) {
        return d < c ? c : d
    },
    mapLinear: function(g, f, j, i, h) {
        return i + (g - f) * (h - i) / (j - f)
    },
    random16: function() {
        return (65280 * Math.random() + 255 * Math.random()) / 65535
    },
    randInt: function(d, c) {
        return d + Math.floor(Math.random() * (c - d + 1))
    },
    randFloat: function(d, c) {
        return d + Math.random() * (c - d)
    },
    randFloatSpread: function(b) {
        return b * (0.5 - Math.random())
    },
    sign: function(b) {
        return b < 0 ? -1 : b > 0 ? 1 : 0
    }
};
THREE.Matrix3 = function() {
    this.elements = new Float32Array(9)
}
;
THREE.Matrix3.prototype = {
    constructor: THREE.Matrix3,
    getInverse: function(v) {
        var u = v.elements
          , v = u[10] * u[5] - u[6] * u[9]
          , t = -u[10] * u[1] + u[2] * u[9]
          , s = u[6] * u[1] - u[2] * u[5]
          , r = -u[10] * u[4] + u[6] * u[8]
          , q = u[10] * u[0] - u[2] * u[8]
          , p = -u[6] * u[0] + u[2] * u[4]
          , o = u[9] * u[4] - u[5] * u[8]
          , g = -u[9] * u[0] + u[1] * u[8]
          , m = u[5] * u[0] - u[1] * u[4]
          , u = u[0] * v + u[1] * r + u[2] * o;
        u === 0 && console.warn("Matrix3.getInverse(): determinant == 0");
        var u = 1 / u
          , n = this.elements;
        n[0] = u * v;
        n[1] = u * t;
        n[2] = u * s;
        n[3] = u * r;
        n[4] = u * q;
        n[5] = u * p;
        n[6] = u * o;
        n[7] = u * g;
        n[8] = u * m;
        return this
    },
    transpose: function() {
        var d, c = this.elements;
        d = c[1];
        c[1] = c[3];
        c[3] = d;
        d = c[2];
        c[2] = c[6];
        c[6] = d;
        d = c[5];
        c[5] = c[7];
        c[7] = d;
        return this
    },
    transposeIntoArray: function(d) {
        var c = this.m;
        d[0] = c[0];
        d[1] = c[3];
        d[2] = c[6];
        d[3] = c[1];
        d[4] = c[4];
        d[5] = c[7];
        d[6] = c[2];
        d[7] = c[5];
        d[8] = c[8];
        return this
    }
};
THREE.Matrix4 = function(E, D, C, B, A, z, y, x, u, v, w, t, r, q, F, g) {
    this.elements = new Float32Array(16);
    this.set(E !== void 0 ? E : 1, D || 0, C || 0, B || 0, A || 0, z !== void 0 ? z : 1, y || 0, x || 0, u || 0, v || 0, w !== void 0 ? w : 1, t || 0, r || 0, q || 0, F || 0, g !== void 0 ? g : 1)
}
;
THREE.Matrix4.prototype = {
    constructor: THREE.Matrix4,
    set: function(G, F, E, D, C, B, A, z, w, x, y, v, u, t, H, r) {
        var g = this.elements;
        g[0] = G;
        g[4] = F;
        g[8] = E;
        g[12] = D;
        g[1] = C;
        g[5] = B;
        g[9] = A;
        g[13] = z;
        g[2] = w;
        g[6] = x;
        g[10] = y;
        g[14] = v;
        g[3] = u;
        g[7] = t;
        g[11] = H;
        g[15] = r;
        return this
    },
    identity: function() {
        this.set(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
        return this
    },
    copy: function(b) {
        b = b.elements;
        this.set(b[0], b[4], b[8], b[12], b[1], b[5], b[9], b[13], b[2], b[6], b[10], b[14], b[3], b[7], b[11], b[15]);
        return this
    },
    lookAt: function(i, g, n) {
        var m = this.elements
          , l = THREE.Matrix4.__v1
          , k = THREE.Matrix4.__v2
          , j = THREE.Matrix4.__v3;
        j.sub(i, g).normalize();
        if (j.length() === 0) {
            j.z = 1
        }
        l.cross(n, j).normalize();
        if (l.length() === 0) {
            j.x = j.x + 0.0001;
            l.cross(n, j).normalize()
        }
        k.cross(j, l);
        m[0] = l.x;
        m[4] = k.x;
        m[8] = j.x;
        m[1] = l.y;
        m[5] = k.y;
        m[9] = j.y;
        m[2] = l.z;
        m[6] = k.z;
        m[10] = j.z;
        return this
    },
    multiply: function(al, ak) {
        var aj = al.elements
          , ai = ak.elements
          , ah = this.elements
          , ag = aj[0]
          , ae = aj[4]
          , ad = aj[8]
          , aa = aj[12]
          , ab = aj[1]
          , ac = aj[5]
          , Z = aj[9]
          , X = aj[13]
          , W = aj[2]
          , Q = aj[6]
          , U = aj[10]
          , S = aj[14]
          , L = aj[3]
          , T = aj[7]
          , E = aj[11]
          , aj = aj[15]
          , O = ai[0]
          , J = ai[4]
          , R = ai[8]
          , C = ai[12]
          , z = ai[1]
          , t = ai[5]
          , V = ai[9]
          , F = ai[13]
          , x = ai[2]
          , r = ai[6]
          , v = ai[10]
          , P = ai[14]
          , af = ai[3]
          , M = ai[7]
          , G = ai[11]
          , ai = ai[15];
        ah[0] = ag * O + ae * z + ad * x + aa * af;
        ah[4] = ag * J + ae * t + ad * r + aa * M;
        ah[8] = ag * R + ae * V + ad * v + aa * G;
        ah[12] = ag * C + ae * F + ad * P + aa * ai;
        ah[1] = ab * O + ac * z + Z * x + X * af;
        ah[5] = ab * J + ac * t + Z * r + X * M;
        ah[9] = ab * R + ac * V + Z * v + X * G;
        ah[13] = ab * C + ac * F + Z * P + X * ai;
        ah[2] = W * O + Q * z + U * x + S * af;
        ah[6] = W * J + Q * t + U * r + S * M;
        ah[10] = W * R + Q * V + U * v + S * G;
        ah[14] = W * C + Q * F + U * P + S * ai;
        ah[3] = L * O + T * z + E * x + aj * af;
        ah[7] = L * J + T * t + E * r + aj * M;
        ah[11] = L * R + T * V + E * v + aj * G;
        ah[15] = L * C + T * F + E * P + aj * ai;
        return this
    },
    multiplySelf: function(b) {
        return this.multiply(this, b)
    },
    multiplyToArray: function(f, e, h) {
        var g = this.elements;
        this.multiply(f, e);
        h[0] = g[0];
        h[1] = g[1];
        h[2] = g[2];
        h[3] = g[3];
        h[4] = g[4];
        h[5] = g[5];
        h[6] = g[6];
        h[7] = g[7];
        h[8] = g[8];
        h[9] = g[9];
        h[10] = g[10];
        h[11] = g[11];
        h[12] = g[12];
        h[13] = g[13];
        h[14] = g[14];
        h[15] = g[15];
        return this
    },
    multiplyScalar: function(d) {
        var c = this.elements;
        c[0] = c[0] * d;
        c[4] = c[4] * d;
        c[8] = c[8] * d;
        c[12] = c[12] * d;
        c[1] = c[1] * d;
        c[5] = c[5] * d;
        c[9] = c[9] * d;
        c[13] = c[13] * d;
        c[2] = c[2] * d;
        c[6] = c[6] * d;
        c[10] = c[10] * d;
        c[14] = c[14] * d;
        c[3] = c[3] * d;
        c[7] = c[7] * d;
        c[11] = c[11] * d;
        c[15] = c[15] * d;
        return this
    },
    multiplyVector3: function(h) {
        var g = this.elements
          , l = h.x
          , k = h.y
          , j = h.z
          , i = 1 / (g[3] * l + g[7] * k + g[11] * j + g[15]);
        h.x = (g[0] * l + g[4] * k + g[8] * j + g[12]) * i;
        h.y = (g[1] * l + g[5] * k + g[9] * j + g[13]) * i;
        h.z = (g[2] * l + g[6] * k + g[10] * j + g[14]) * i;
        return h
    },
    multiplyVector4: function(h) {
        var g = this.elements
          , l = h.x
          , k = h.y
          , j = h.z
          , i = h.w;
        h.x = g[0] * l + g[4] * k + g[8] * j + g[12] * i;
        h.y = g[1] * l + g[5] * k + g[9] * j + g[13] * i;
        h.z = g[2] * l + g[6] * k + g[10] * j + g[14] * i;
        h.w = g[3] * l + g[7] * k + g[11] * j + g[15] * i;
        return h
    },
    rotateAxis: function(g) {
        var f = this.elements
          , j = g.x
          , i = g.y
          , h = g.z;
        g.x = j * f[0] + i * f[4] + h * f[8];
        g.y = j * f[1] + i * f[5] + h * f[9];
        g.z = j * f[2] + i * f[6] + h * f[10];
        g.normalize();
        return g
    },
    crossVector: function(e) {
        var d = this.elements
          , f = new THREE.Vector4;
        f.x = d[0] * e.x + d[4] * e.y + d[8] * e.z + d[12] * e.w;
        f.y = d[1] * e.x + d[5] * e.y + d[9] * e.z + d[13] * e.w;
        f.z = d[2] * e.x + d[6] * e.y + d[10] * e.z + d[14] * e.w;
        f.w = e.w ? d[3] * e.x + d[7] * e.y + d[11] * e.z + d[15] * e.w : 1;
        return f
    },
    determinant: function() {
        var E = this.elements
          , D = E[0]
          , C = E[4]
          , B = E[8]
          , A = E[12]
          , z = E[1]
          , y = E[5]
          , x = E[9]
          , u = E[13]
          , v = E[2]
          , w = E[6]
          , t = E[10]
          , r = E[14]
          , q = E[3]
          , F = E[7]
          , g = E[11]
          , E = E[15];
        return A * x * w * q - B * u * w * q - A * y * t * q + C * u * t * q + B * y * r * q - C * x * r * q - A * x * v * F + B * u * v * F + A * z * t * F - D * u * t * F - B * z * r * F + D * x * r * F + A * y * v * g - C * u * v * g - A * z * w * g + D * u * w * g + C * z * r * g - D * y * r * g - B * y * v * E + C * x * v * E + B * z * w * E - D * x * w * E - C * z * t * E + D * y * t * E
    },
    transpose: function() {
        var d = this.elements, c;
        c = d[1];
        d[1] = d[4];
        d[4] = c;
        c = d[2];
        d[2] = d[8];
        d[8] = c;
        c = d[6];
        d[6] = d[9];
        d[9] = c;
        c = d[3];
        d[3] = d[12];
        d[12] = c;
        c = d[7];
        d[7] = d[13];
        d[13] = c;
        c = d[11];
        d[11] = d[14];
        d[14] = c;
        return this
    },
    flattenToArray: function(d) {
        var c = this.elements;
        d[0] = c[0];
        d[1] = c[1];
        d[2] = c[2];
        d[3] = c[3];
        d[4] = c[4];
        d[5] = c[5];
        d[6] = c[6];
        d[7] = c[7];
        d[8] = c[8];
        d[9] = c[9];
        d[10] = c[10];
        d[11] = c[11];
        d[12] = c[12];
        d[13] = c[13];
        d[14] = c[14];
        d[15] = c[15];
        return d
    },
    flattenToArrayOffset: function(e, d) {
        var f = this.elements;
        e[d] = f[0];
        e[d + 1] = f[1];
        e[d + 2] = f[2];
        e[d + 3] = f[3];
        e[d + 4] = f[4];
        e[d + 5] = f[5];
        e[d + 6] = f[6];
        e[d + 7] = f[7];
        e[d + 8] = f[8];
        e[d + 9] = f[9];
        e[d + 10] = f[10];
        e[d + 11] = f[11];
        e[d + 12] = f[12];
        e[d + 13] = f[13];
        e[d + 14] = f[14];
        e[d + 15] = f[15];
        return e
    },
    getPosition: function() {
        var b = this.elements;
        return THREE.Matrix4.__v1.set(b[12], b[13], b[14])
    },
    setPosition: function(d) {
        var c = this.elements;
        c[12] = d.x;
        c[13] = d.y;
        c[14] = d.z;
        return this
    },
    getColumnX: function() {
        var b = this.elements;
        return THREE.Matrix4.__v1.set(b[0], b[1], b[2])
    },
    getColumnY: function() {
        var b = this.elements;
        return THREE.Matrix4.__v1.set(b[4], b[5], b[6])
    },
    getColumnZ: function() {
        var b = this.elements;
        return THREE.Matrix4.__v1.set(b[8], b[9], b[10])
    },
    getInverse: function(J) {
        var I = this.elements
          , H = J.elements
          , G = H[0]
          , F = H[4]
          , E = H[8]
          , D = H[12]
          , C = H[1]
          , z = H[5]
          , A = H[9]
          , B = H[13]
          , y = H[2]
          , x = H[6]
          , v = H[10]
          , r = H[14]
          , u = H[3]
          , t = H[7]
          , g = H[11]
          , H = H[15];
        I[0] = A * r * t - B * v * t + B * x * g - z * r * g - A * x * H + z * v * H;
        I[4] = D * v * t - E * r * t - D * x * g + F * r * g + E * x * H - F * v * H;
        I[8] = E * B * t - D * A * t + D * z * g - F * B * g - E * z * H + F * A * H;
        I[12] = D * A * x - E * B * x - D * z * v + F * B * v + E * z * r - F * A * r;
        I[1] = B * v * u - A * r * u - B * y * g + C * r * g + A * y * H - C * v * H;
        I[5] = E * r * u - D * v * u + D * y * g - G * r * g - E * y * H + G * v * H;
        I[9] = D * A * u - E * B * u - D * C * g + G * B * g + E * C * H - G * A * H;
        I[13] = E * B * y - D * A * y + D * C * v - G * B * v - E * C * r + G * A * r;
        I[2] = z * r * u - B * x * u + B * y * t - C * r * t - z * y * H + C * x * H;
        I[6] = D * x * u - F * r * u - D * y * t + G * r * t + F * y * H - G * x * H;
        I[10] = F * B * u - D * z * u + D * C * t - G * B * t - F * C * H + G * z * H;
        I[14] = D * z * y - F * B * y - D * C * x + G * B * x + F * C * r - G * z * r;
        I[3] = A * x * u - z * v * u - A * y * t + C * v * t + z * y * g - C * x * g;
        I[7] = F * v * u - E * x * u + E * y * t - G * v * t - F * y * g + G * x * g;
        I[11] = E * z * u - F * A * u - E * C * t + G * A * t + F * C * g - G * z * g;
        I[15] = F * A * y - E * z * y + E * C * x - G * A * x - F * C * v + G * z * v;
        this.multiplyScalar(1 / J.determinant());
        return this
    },
    setRotationFromEuler: function(z, y) {
        var x = this.elements
          , w = z.x
          , v = z.y
          , u = z.z
          , t = Math.cos(w)
          , w = Math.sin(w)
          , s = Math.cos(v)
          , v = Math.sin(v)
          , p = Math.cos(u)
          , u = Math.sin(u);
        switch (y) {
        case "YXZ":
            var q = s * p
              , r = s * u
              , o = v * p
              , g = v * u;
            x[0] = q + g * w;
            x[4] = o * w - r;
            x[8] = t * v;
            x[1] = t * u;
            x[5] = t * p;
            x[9] = -w;
            x[2] = r * w - o;
            x[6] = g + q * w;
            x[10] = t * s;
            break;
        case "ZXY":
            q = s * p;
            r = s * u;
            o = v * p;
            g = v * u;
            x[0] = q - g * w;
            x[4] = -t * u;
            x[8] = o + r * w;
            x[1] = r + o * w;
            x[5] = t * p;
            x[9] = g - q * w;
            x[2] = -t * v;
            x[6] = w;
            x[10] = t * s;
            break;
        case "ZYX":
            q = t * p;
            r = t * u;
            o = w * p;
            g = w * u;
            x[0] = s * p;
            x[4] = o * v - r;
            x[8] = q * v + g;
            x[1] = s * u;
            x[5] = g * v + q;
            x[9] = r * v - o;
            x[2] = -v;
            x[6] = w * s;
            x[10] = t * s;
            break;
        case "YZX":
            q = t * s;
            r = t * v;
            o = w * s;
            g = w * v;
            x[0] = s * p;
            x[4] = g - q * u;
            x[8] = o * u + r;
            x[1] = u;
            x[5] = t * p;
            x[9] = -w * p;
            x[2] = -v * p;
            x[6] = r * u + o;
            x[10] = q - g * u;
            break;
        case "XZY":
            q = t * s;
            r = t * v;
            o = w * s;
            g = w * v;
            x[0] = s * p;
            x[4] = -u;
            x[8] = v * p;
            x[1] = q * u + g;
            x[5] = t * p;
            x[9] = r * u - o;
            x[2] = o * u - r;
            x[6] = w * p;
            x[10] = g * u + q;
            break;
        default:
            q = t * p;
            r = t * u;
            o = w * p;
            g = w * u;
            x[0] = s * p;
            x[4] = -s * u;
            x[8] = v;
            x[1] = r + o * v;
            x[5] = q - g * v;
            x[9] = -w * s;
            x[2] = g - q * v;
            x[6] = o + r * v;
            x[10] = t * s
        }
        return this
    },
    setRotationFromQuaternion: function(v) {
        var u = this.elements
          , t = v.x
          , s = v.y
          , r = v.z
          , q = v.w
          , p = t + t
          , o = s + s
          , g = r + r
          , v = t * p
          , m = t * o
          , t = t * g
          , n = s * o
          , s = s * g
          , r = r * g
          , p = q * p
          , o = q * o
          , q = q * g;
        u[0] = 1 - (n + r);
        u[4] = m - q;
        u[8] = t + o;
        u[1] = m + q;
        u[5] = 1 - (v + r);
        u[9] = s - p;
        u[2] = t - o;
        u[6] = s + p;
        u[10] = 1 - (v + n);
        return this
    },
    compose: function(h, g, l) {
        var k = this.elements
          , j = THREE.Matrix4.__m1
          , i = THREE.Matrix4.__m2;
        j.identity();
        j.setRotationFromQuaternion(g);
        i.makeScale(l.x, l.y, l.z);
        this.multiply(j, i);
        k[12] = h.x;
        k[13] = h.y;
        k[14] = h.z;
        return this
    },
    decompose: function(i, g, n) {
        var m = this.elements
          , l = THREE.Matrix4.__v1
          , k = THREE.Matrix4.__v2
          , j = THREE.Matrix4.__v3;
        l.set(m[0], m[1], m[2]);
        k.set(m[4], m[5], m[6]);
        j.set(m[8], m[9], m[10]);
        i = i instanceof THREE.Vector3 ? i : new THREE.Vector3;
        g = g instanceof THREE.Quaternion ? g : new THREE.Quaternion;
        n = n instanceof THREE.Vector3 ? n : new THREE.Vector3;
        n.x = l.length();
        n.y = k.length();
        n.z = j.length();
        i.x = m[12];
        i.y = m[13];
        i.z = m[14];
        m = THREE.Matrix4.__m1;
        m.copy(this);
        m.elements[0] = m.elements[0] / n.x;
        m.elements[1] = m.elements[1] / n.x;
        m.elements[2] = m.elements[2] / n.x;
        m.elements[4] = m.elements[4] / n.y;
        m.elements[5] = m.elements[5] / n.y;
        m.elements[6] = m.elements[6] / n.y;
        m.elements[8] = m.elements[8] / n.z;
        m.elements[9] = m.elements[9] / n.z;
        m.elements[10] = m.elements[10] / n.z;
        g.setFromRotationMatrix(m);
        return [i, g, n]
    },
    extractPosition: function(d) {
        var c = this.elements
          , d = d.elements;
        c[12] = d[12];
        c[13] = d[13];
        c[14] = d[14];
        return this
    },
    extractRotation: function(g) {
        var f = this.elements
          , g = g.elements
          , j = THREE.Matrix4.__v1
          , i = 1 / j.set(g[0], g[1], g[2]).length()
          , h = 1 / j.set(g[4], g[5], g[6]).length()
          , j = 1 / j.set(g[8], g[9], g[10]).length();
        f[0] = g[0] * i;
        f[1] = g[1] * i;
        f[2] = g[2] * i;
        f[4] = g[4] * h;
        f[5] = g[5] * h;
        f[6] = g[6] * h;
        f[8] = g[8] * j;
        f[9] = g[9] * j;
        f[10] = g[10] * j;
        return this
    },
    translate: function(f) {
        var e = this.elements
          , h = f.x
          , g = f.y
          , f = f.z;
        e[12] = e[0] * h + e[4] * g + e[8] * f + e[12];
        e[13] = e[1] * h + e[5] * g + e[9] * f + e[13];
        e[14] = e[2] * h + e[6] * g + e[10] * f + e[14];
        e[15] = e[3] * h + e[7] * g + e[11] * f + e[15];
        return this
    },
    rotateX: function(v) {
        var u = this.elements
          , t = u[4]
          , s = u[5]
          , r = u[6]
          , q = u[7]
          , p = u[8]
          , o = u[9]
          , g = u[10]
          , m = u[11]
          , n = Math.cos(v)
          , v = Math.sin(v);
        u[4] = n * t + v * p;
        u[5] = n * s + v * o;
        u[6] = n * r + v * g;
        u[7] = n * q + v * m;
        u[8] = n * p - v * t;
        u[9] = n * o - v * s;
        u[10] = n * g - v * r;
        u[11] = n * m - v * q;
        return this
    },
    rotateY: function(v) {
        var u = this.elements
          , t = u[0]
          , s = u[1]
          , r = u[2]
          , q = u[3]
          , p = u[8]
          , o = u[9]
          , g = u[10]
          , m = u[11]
          , n = Math.cos(v)
          , v = Math.sin(v);
        u[0] = n * t - v * p;
        u[1] = n * s - v * o;
        u[2] = n * r - v * g;
        u[3] = n * q - v * m;
        u[8] = n * p + v * t;
        u[9] = n * o + v * s;
        u[10] = n * g + v * r;
        u[11] = n * m + v * q;
        return this
    },
    rotateZ: function(v) {
        var u = this.elements
          , t = u[0]
          , s = u[1]
          , r = u[2]
          , q = u[3]
          , p = u[4]
          , o = u[5]
          , g = u[6]
          , m = u[7]
          , n = Math.cos(v)
          , v = Math.sin(v);
        u[0] = n * t + v * p;
        u[1] = n * s + v * o;
        u[2] = n * r + v * g;
        u[3] = n * q + v * m;
        u[4] = n * p - v * t;
        u[5] = n * o - v * s;
        u[6] = n * g - v * r;
        u[7] = n * m - v * q;
        return this
    },
    rotateByAxis: function(V, U) {
        var T = this.elements;
        if (V.x === 1 && V.y === 0 && V.z === 0) {
            return this.rotateX(U)
        }
        if (V.x === 0 && V.y === 1 && V.z === 0) {
            return this.rotateY(U)
        }
        if (V.x === 0 && V.y === 0 && V.z === 1) {
            return this.rotateZ(U)
        }
        var S = V.x
          , R = V.y
          , Q = V.z
          , P = Math.sqrt(S * S + R * R + Q * Q)
          , S = S / P
          , R = R / P
          , Q = Q / P
          , P = S * S
          , O = R * R
          , L = Q * Q
          , M = Math.cos(U)
          , N = Math.sin(U)
          , J = 1 - M
          , I = S * R * J
          , G = S * Q * J
          , J = R * Q * J
          , S = S * N
          , z = R * N
          , N = Q * N
          , Q = P + (1 - P) * M
          , P = I + N
          , R = G - z
          , I = I - N
          , O = O + (1 - O) * M
          , N = J + S
          , G = G + z
          , J = J - S
          , L = L + (1 - L) * M
          , M = T[0]
          , S = T[1]
          , z = T[2]
          , F = T[3]
          , D = T[4]
          , v = T[5]
          , E = T[6]
          , r = T[7]
          , x = T[8]
          , t = T[9]
          , C = T[10]
          , g = T[11];
        T[0] = Q * M + P * D + R * x;
        T[1] = Q * S + P * v + R * t;
        T[2] = Q * z + P * E + R * C;
        T[3] = Q * F + P * r + R * g;
        T[4] = I * M + O * D + N * x;
        T[5] = I * S + O * v + N * t;
        T[6] = I * z + O * E + N * C;
        T[7] = I * F + O * r + N * g;
        T[8] = G * M + J * D + L * x;
        T[9] = G * S + J * v + L * t;
        T[10] = G * z + J * E + L * C;
        T[11] = G * F + J * r + L * g;
        return this
    },
    scale: function(f) {
        var e = this.elements
          , h = f.x
          , g = f.y
          , f = f.z;
        e[0] = e[0] * h;
        e[4] = e[4] * g;
        e[8] = e[8] * f;
        e[1] = e[1] * h;
        e[5] = e[5] * g;
        e[9] = e[9] * f;
        e[2] = e[2] * h;
        e[6] = e[6] * g;
        e[10] = e[10] * f;
        e[3] = e[3] * h;
        e[7] = e[7] * g;
        e[11] = e[11] * f;
        return this
    },
    getMaxScaleOnAxis: function() {
        var b = this.elements;
        return Math.sqrt(Math.max(b[0] * b[0] + b[1] * b[1] + b[2] * b[2], Math.max(b[4] * b[4] + b[5] * b[5] + b[6] * b[6], b[8] * b[8] + b[9] * b[9] + b[10] * b[10])))
    },
    makeTranslation: function(e, d, f) {
        this.set(1, 0, 0, e, 0, 1, 0, d, 0, 0, 1, f, 0, 0, 0, 1);
        return this
    },
    makeRotationX: function(d) {
        var c = Math.cos(d)
          , d = Math.sin(d);
        this.set(1, 0, 0, 0, 0, c, -d, 0, 0, d, c, 0, 0, 0, 0, 1);
        return this
    },
    makeRotationY: function(d) {
        var c = Math.cos(d)
          , d = Math.sin(d);
        this.set(c, 0, d, 0, 0, 1, 0, 0, -d, 0, c, 0, 0, 0, 0, 1);
        return this
    },
    makeRotationZ: function(d) {
        var c = Math.cos(d)
          , d = Math.sin(d);
        this.set(c, -d, 0, 0, d, c, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
        return this
    },
    makeRotationAxis: function(t, s) {
        var r = Math.cos(s)
          , q = Math.sin(s)
          , p = 1 - r
          , o = t.x
          , n = t.y
          , m = t.z
          , g = p * o
          , j = p * n;
        this.set(g * o + r, g * n - q * m, g * m + q * n, 0, g * n + q * m, j * n + r, j * m - q * o, 0, g * m - q * n, j * m + q * o, p * m * m + r, 0, 0, 0, 0, 1);
        return this
    },
    makeScale: function(e, d, f) {
        this.set(e, 0, 0, 0, 0, d, 0, 0, 0, 0, f, 0, 0, 0, 0, 1);
        return this
    },
    makeFrustum: function(i, g, n, m, l, k) {
        var j = this.elements;
        j[0] = 2 * l / (g - i);
        j[4] = 0;
        j[8] = (g + i) / (g - i);
        j[12] = 0;
        j[1] = 0;
        j[5] = 2 * l / (m - n);
        j[9] = (m + n) / (m - n);
        j[13] = 0;
        j[2] = 0;
        j[6] = 0;
        j[10] = -(k + l) / (k - l);
        j[14] = -2 * k * l / (k - l);
        j[3] = 0;
        j[7] = 0;
        j[11] = -1;
        j[15] = 0;
        return this
    },
    makePerspective: function(g, f, j, i) {
        var g = j * Math.tan(g * Math.PI / 360)
          , h = -g;
        return this.makeFrustum(h * f, g * f, h, g, j, i)
    },
    makeOrthographic: function(t, s, r, q, p, o) {
        var n = this.elements
          , m = s - t
          , g = r - q
          , j = o - p;
        n[0] = 2 / m;
        n[4] = 0;
        n[8] = 0;
        n[12] = -((s + t) / m);
        n[1] = 0;
        n[5] = 2 / g;
        n[9] = 0;
        n[13] = -((r + q) / g);
        n[2] = 0;
        n[6] = 0;
        n[10] = -2 / j;
        n[14] = -((o + p) / j);
        n[3] = 0;
        n[7] = 0;
        n[11] = 0;
        n[15] = 1;
        return this
    },
    clone: function() {
        var b = this.elements;
        return new THREE.Matrix4(b[0],b[4],b[8],b[12],b[1],b[5],b[9],b[13],b[2],b[6],b[10],b[14],b[3],b[7],b[11],b[15])
    }
};
THREE.Matrix4.__v1 = new THREE.Vector3;
THREE.Matrix4.__v2 = new THREE.Vector3;
THREE.Matrix4.__v3 = new THREE.Vector3;
THREE.Matrix4.__m1 = new THREE.Matrix4;
THREE.Matrix4.__m2 = new THREE.Matrix4;
THREE.Object3D = function() {
    this.id = THREE.Object3DCount++;
    this.name = "";
    this.parent = void 0;
    this.children = [];
    this.up = new THREE.Vector3(0,1,0);
    this.position = new THREE.Vector3;
    this.rotation = new THREE.Vector3;
    this.eulerOrder = "XYZ";
    this.scale = new THREE.Vector3(1,1,1);
    this.flipSided = this.doubleSided = false;
    this.renderDepth = null;
    this.rotationAutoUpdate = true;
    this.matrix = new THREE.Matrix4;
    this.matrixWorld = new THREE.Matrix4;
    this.matrixRotationWorld = new THREE.Matrix4;
    this.matrixWorldNeedsUpdate = this.matrixAutoUpdate = true;
    this.quaternion = new THREE.Quaternion;
    this.useQuaternion = false;
    this.boundRadius = 0;
    this.boundRadiusScale = 1;
    this.visible = true;
    this.receiveShadow = this.castShadow = false;
    this.frustumCulled = true;
    this._vector = new THREE.Vector3
}
;
THREE.Object3D.prototype = {
    constructor: THREE.Object3D,
    applyMatrix: function(b) {
        this.matrix.multiply(b, this.matrix);
        this.scale.getScaleFromMatrix(this.matrix);
        this.rotation.getRotationFromMatrix(this.matrix, this.scale);
        this.position.getPositionFromMatrix(this.matrix)
    },
    translate: function(d, c) {
        this.matrix.rotateAxis(c);
        this.position.addSelf(c.multiplyScalar(d))
    },
    translateX: function(b) {
        this.translate(b, this._vector.set(1, 0, 0))
    },
    translateY: function(b) {
        this.translate(b, this._vector.set(0, 1, 0))
    },
    translateZ: function(b) {
        this.translate(b, this._vector.set(0, 0, 1))
    },
    lookAt: function(b) {
        this.matrix.lookAt(b, this.position, this.up);
        this.rotationAutoUpdate && this.rotation.getRotationFromMatrix(this.matrix)
    },
    add: function(d) {
        if (d === this) {
            console.warn("THREE.Object3D.add: An object can't be added as a child of itself.")
        } else {
            if (d instanceof THREE.Object3D) {
                d.parent !== void 0 && d.parent.remove(d);
                d.parent = this;
                this.children.push(d);
                for (var c = this; c.parent !== void 0; ) {
                    c = c.parent
                }
                c !== void 0 && c instanceof THREE.Scene && c.__addObject(d)
            }
        }
    },
    remove: function(d) {
        var c = this.children.indexOf(d);
        if (c !== -1) {
            d.parent = void 0;
            this.children.splice(c, 1);
            for (c = this; c.parent !== void 0; ) {
                c = c.parent
            }
            c !== void 0 && c instanceof THREE.Scene && c.__removeObject(d)
        }
    },
    getChildByName: function(g, f) {
        var j, i, h;
        j = 0;
        for (i = this.children.length; j < i; j++) {
            h = this.children[j];
            if (h.name === g) {
                return h
            }
            if (f) {
                h = h.getChildByName(g, f);
                if (h !== void 0) {
                    return h
                }
            }
        }
    },
    updateMatrix: function() {
        this.matrix.setPosition(this.position);
        this.useQuaternion ? this.matrix.setRotationFromQuaternion(this.quaternion) : this.matrix.setRotationFromEuler(this.rotation, this.eulerOrder);
        if (this.scale.x !== 1 || this.scale.y !== 1 || this.scale.z !== 1) {
            this.matrix.scale(this.scale);
            this.boundRadiusScale = Math.max(this.scale.x, Math.max(this.scale.y, this.scale.z))
        }
        this.matrixWorldNeedsUpdate = true
    },
    updateMatrixWorld: function(e) {
        this.matrixAutoUpdate && this.updateMatrix();
        if (this.matrixWorldNeedsUpdate || e) {
            this.parent ? this.matrixWorld.multiply(this.parent.matrixWorld, this.matrix) : this.matrixWorld.copy(this.matrix);
            this.matrixWorldNeedsUpdate = false;
            e = true
        }
        for (var d = 0, f = this.children.length; d < f; d++) {
            this.children[d].updateMatrixWorld(e)
        }
    }
};
THREE.Object3DCount = 0;
THREE.Projector = function() {
    function ac() {
        var b = R[U] = R[U] || new THREE.RenderableVertex;
        U++;
        return b
    }
    function ab(d, c) {
        return c.z - d.z
    }
    function aa(j, i) {
        var p = 0
          , o = 1
          , l = j.z + j.w
          , n = i.z + i.w
          , m = -j.z + j.w
          , k = -i.z + i.w;
        if (l >= 0 && n >= 0 && m >= 0 && k >= 0) {
            return true
        }
        if (l < 0 && n < 0 || m < 0 && k < 0) {
            return false
        }
        l < 0 ? p = Math.max(p, l / (l - n)) : n < 0 && (o = Math.min(o, l / (l - n)));
        m < 0 ? p = Math.max(p, m / (m - k)) : k < 0 && (o = Math.min(o, m / (m - k)));
        if (o < p) {
            return false
        }
        j.lerpSelf(i, p);
        i.lerpSelf(j, 1 - o);
        return true
    }
    var Z, X, W = [], V, U, R = [], S, T, Q = [], P, O = [], E, L, G = [], C, J, v = [], D = {
        objects: [],
        sprites: [],
        lights: [],
        elements: []
    }, z = new THREE.Vector3, F = new THREE.Vector4, t = new THREE.Matrix4, r = new THREE.Matrix4, g = new THREE.Frustum, M = new THREE.Vector4, x = new THREE.Vector4;
    this.projectVector = function(d, c) {
        c.matrixWorldInverse.getInverse(c.matrixWorld);
        t.multiply(c.projectionMatrix, c.matrixWorldInverse);
        t.multiplyVector3(d);
        return d
    }
    ;
    this.unprojectVector = function(d, c) {
        c.projectionMatrixInverse.getInverse(c.projectionMatrix);
        t.multiply(c.matrixWorld, c.projectionMatrixInverse);
        t.multiplyVector3(d);
        return d
    }
    ;
    this.pickingRay = function(e, d) {
        var f;
        e.z = -1;
        f = new THREE.Vector3(e.x,e.y,1);
        this.unprojectVector(e, d);
        this.unprojectVector(f, d);
        f.subSelf(e).normalize();
        return new THREE.Ray(e,f)
    }
    ;
    this.projectGraph = function(b, e) {
        X = 0;
        D.objects.length = 0;
        D.sprites.length = 0;
        D.lights.length = 0;
        var d = function(h) {
            if (h.visible !== false) {
                if ((h instanceof THREE.Mesh || h instanceof THREE.Line) && (h.frustumCulled === false || g.contains(h))) {
                    z.copy(h.matrixWorld.getPosition());
                    t.multiplyVector3(z);
                    var f = W[X] = W[X] || new THREE.RenderableObject;
                    X++;
                    Z = f;
                    Z.object = h;
                    Z.z = z.z;
                    D.objects.push(Z)
                } else {
                    h instanceof THREE.Light && D.lights.push(h)
                }
                for (var f = 0, i = h.children.length; f < i; f++) {
                    d(h.children[f])
                }
            }
        };
        d(b);
        e && D.objects.sort(ab);
        return D
    }
    ;
    this.projectScene = function(K, I, B) {
        var u = I.near, y = I.far, p = false, q, c, j, k, n, h, l, a, m, b, i, o, w, A, s;
        J = L = P = T = 0;
        D.elements.length = 0;
        if (I.parent === void 0) {
            console.warn("DEPRECATED: Camera hasn't been added to a Scene. Adding it...");
            K.add(I)
        }
        K.updateMatrixWorld();
        I.matrixWorldInverse.getInverse(I.matrixWorld);
        t.multiply(I.projectionMatrix, I.matrixWorldInverse);
        g.setFromMatrix(t);
        D = this.projectGraph(K, false);
        K = 0;
        for (q = D.objects.length; K < q; K++) {
            m = D.objects[K].object;
            b = m.matrixWorld;
            U = 0;
            if (m instanceof THREE.Mesh) {
                i = m.geometry;
                o = m.geometry.materials;
                k = i.vertices;
                w = i.faces;
                A = i.faceVertexUvs;
                i = m.matrixRotationWorld.extractRotation(b);
                c = 0;
                for (j = k.length; c < j; c++) {
                    V = ac();
                    V.positionWorld.copy(k[c]);
                    b.multiplyVector3(V.positionWorld);
                    V.positionScreen.copy(V.positionWorld);
                    t.multiplyVector4(V.positionScreen);
                    V.positionScreen.x = V.positionScreen.x / V.positionScreen.w;
                    V.positionScreen.y = V.positionScreen.y / V.positionScreen.w;
                    V.visible = V.positionScreen.z > u && V.positionScreen.z < y
                }
                k = 0;
                for (c = w.length; k < c; k++) {
                    j = w[k];
                    if (j instanceof THREE.Face3) {
                        n = R[j.a];
                        h = R[j.b];
                        l = R[j.c];
                        if (n.visible && h.visible && l.visible) {
                            p = (l.positionScreen.x - n.positionScreen.x) * (h.positionScreen.y - n.positionScreen.y) - (l.positionScreen.y - n.positionScreen.y) * (h.positionScreen.x - n.positionScreen.x) < 0;
                            if (m.doubleSided || p != m.flipSided) {
                                a = Q[T] = Q[T] || new THREE.RenderableFace3;
                                T++;
                                S = a;
                                S.v1.copy(n);
                                S.v2.copy(h);
                                S.v3.copy(l)
                            } else {
                                continue
                            }
                        } else {
                            continue
                        }
                    } else {
                        if (j instanceof THREE.Face4) {
                            n = R[j.a];
                            h = R[j.b];
                            l = R[j.c];
                            a = R[j.d];
                            if (n.visible && h.visible && l.visible && a.visible) {
                                p = (a.positionScreen.x - n.positionScreen.x) * (h.positionScreen.y - n.positionScreen.y) - (a.positionScreen.y - n.positionScreen.y) * (h.positionScreen.x - n.positionScreen.x) < 0 || (h.positionScreen.x - l.positionScreen.x) * (a.positionScreen.y - l.positionScreen.y) - (h.positionScreen.y - l.positionScreen.y) * (a.positionScreen.x - l.positionScreen.x) < 0;
                                if (m.doubleSided || p != m.flipSided) {
                                    s = O[P] = O[P] || new THREE.RenderableFace4;
                                    P++;
                                    S = s;
                                    S.v1.copy(n);
                                    S.v2.copy(h);
                                    S.v3.copy(l);
                                    S.v4.copy(a)
                                } else {
                                    continue
                                }
                            } else {
                                continue
                            }
                        }
                    }
                    S.normalWorld.copy(j.normal);
                    !p && (m.flipSided || m.doubleSided) && S.normalWorld.negate();
                    i.multiplyVector3(S.normalWorld);
                    S.centroidWorld.copy(j.centroid);
                    b.multiplyVector3(S.centroidWorld);
                    S.centroidScreen.copy(S.centroidWorld);
                    t.multiplyVector3(S.centroidScreen);
                    l = j.vertexNormals;
                    n = 0;
                    for (h = l.length; n < h; n++) {
                        a = S.vertexNormalsWorld[n];
                        a.copy(l[n]);
                        !p && (m.flipSided || m.doubleSided) && a.negate();
                        i.multiplyVector3(a)
                    }
                    n = 0;
                    for (h = A.length; n < h; n++) {
                        if (s = A[n][k]) {
                            l = 0;
                            for (a = s.length; l < a; l++) {
                                S.uvs[n][l] = s[l]
                            }
                        }
                    }
                    S.material = m.material;
                    S.faceMaterial = j.materialIndex !== null ? o[j.materialIndex] : null;
                    S.z = S.centroidScreen.z;
                    D.elements.push(S)
                }
            } else {
                if (m instanceof THREE.Line) {
                    r.multiply(t, b);
                    k = m.geometry.vertices;
                    n = ac();
                    n.positionScreen.copy(k[0]);
                    r.multiplyVector4(n.positionScreen);
                    b = m.type === THREE.LinePieces ? 2 : 1;
                    c = 1;
                    for (j = k.length; c < j; c++) {
                        n = ac();
                        n.positionScreen.copy(k[c]);
                        r.multiplyVector4(n.positionScreen);
                        if (!((c + 1) % b > 0)) {
                            h = R[U - 2];
                            M.copy(n.positionScreen);
                            x.copy(h.positionScreen);
                            if (aa(M, x)) {
                                M.multiplyScalar(1 / M.w);
                                x.multiplyScalar(1 / x.w);
                                o = G[L] = G[L] || new THREE.RenderableLine;
                                L++;
                                E = o;
                                E.v1.positionScreen.copy(M);
                                E.v2.positionScreen.copy(x);
                                E.z = Math.max(M.z, x.z);
                                E.material = m.material;
                                D.elements.push(E)
                            }
                        }
                    }
                }
            }
        }
        K = 0;
        for (q = D.sprites.length; K < q; K++) {
            m = D.sprites[K].object;
            b = m.matrixWorld;
            if (m instanceof THREE.Particle) {
                F.set(b.elements[12], b.elements[13], b.elements[14], 1);
                t.multiplyVector4(F);
                F.z = F.z / F.w;
                if (F.z > 0 && F.z < 1) {
                    u = v[J] = v[J] || new THREE.RenderableParticle;
                    J++;
                    C = u;
                    C.x = F.x / F.w;
                    C.y = F.y / F.w;
                    C.z = F.z;
                    C.rotation = m.rotation.z;
                    C.scale.x = m.scale.x * Math.abs(C.x - (F.x + I.projectionMatrix.elements[0]) / (F.w + I.projectionMatrix.elements[12]));
                    C.scale.y = m.scale.y * Math.abs(C.y - (F.y + I.projectionMatrix.elements[5]) / (F.w + I.projectionMatrix.elements[13]));
                    C.material = m.material;
                    D.elements.push(C)
                }
            }
        }
        B && D.elements.sort(ab);
        return D
    }
}
;
THREE.Quaternion = function(f, e, h, g) {
    this.x = f || 0;
    this.y = e || 0;
    this.z = h || 0;
    this.w = g !== void 0 ? g : 1
}
;
THREE.Quaternion.prototype = {
    constructor: THREE.Quaternion,
    set: function(f, e, h, g) {
        this.x = f;
        this.y = e;
        this.z = h;
        this.w = g;
        return this
    },
    copy: function(b) {
        this.x = b.x;
        this.y = b.y;
        this.z = b.z;
        this.w = b.w;
        return this
    },
    setFromEuler: function(j) {
        var g = Math.PI / 360
          , p = j.x * g
          , o = j.y * g
          , n = j.z * g
          , j = Math.cos(o)
          , o = Math.sin(o)
          , g = Math.cos(-n)
          , n = Math.sin(-n)
          , m = Math.cos(p)
          , p = Math.sin(p)
          , l = j * g
          , k = o * n;
        this.w = l * m - k * p;
        this.x = l * p + k * m;
        this.y = o * g * m + j * n * p;
        this.z = j * n * m - o * g * p;
        return this
    },
    setFromAxisAngle: function(f, e) {
        var h = e / 2
          , g = Math.sin(h);
        this.x = f.x * g;
        this.y = f.y * g;
        this.z = f.z * g;
        this.w = Math.cos(h);
        return this
    },
    setFromRotationMatrix: function(d) {
        var c = Math.pow(d.determinant(), 1 / 3);
        this.w = Math.sqrt(Math.max(0, c + d.elements[0] + d.elements[5] + d.elements[10])) / 2;
        this.x = Math.sqrt(Math.max(0, c + d.elements[0] - d.elements[5] - d.elements[10])) / 2;
        this.y = Math.sqrt(Math.max(0, c - d.elements[0] + d.elements[5] - d.elements[10])) / 2;
        this.z = Math.sqrt(Math.max(0, c - d.elements[0] - d.elements[5] + d.elements[10])) / 2;
        this.x = d.elements[6] - d.elements[9] < 0 ? -Math.abs(this.x) : Math.abs(this.x);
        this.y = d.elements[8] - d.elements[2] < 0 ? -Math.abs(this.y) : Math.abs(this.y);
        this.z = d.elements[1] - d.elements[4] < 0 ? -Math.abs(this.z) : Math.abs(this.z);
        this.normalize();
        return this
    },
    calculateW: function() {
        this.w = -Math.sqrt(Math.abs(1 - this.x * this.x - this.y * this.y - this.z * this.z));
        return this
    },
    inverse: function() {
        this.x = this.x * -1;
        this.y = this.y * -1;
        this.z = this.z * -1;
        return this
    },
    length: function() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w)
    },
    normalize: function() {
        var b = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
        if (b === 0) {
            this.w = this.z = this.y = this.x = 0
        } else {
            b = 1 / b;
            this.x = this.x * b;
            this.y = this.y * b;
            this.z = this.z * b;
            this.w = this.w * b
        }
        return this
    },
    multiply: function(d, c) {
        this.x = d.x * c.w + d.y * c.z - d.z * c.y + d.w * c.x;
        this.y = -d.x * c.z + d.y * c.w + d.z * c.x + d.w * c.y;
        this.z = d.x * c.y - d.y * c.x + d.z * c.w + d.w * c.z;
        this.w = -d.x * c.x - d.y * c.y - d.z * c.z + d.w * c.w;
        return this
    },
    multiplySelf: function(j) {
        var g = this.x
          , p = this.y
          , o = this.z
          , n = this.w
          , m = j.x
          , l = j.y
          , k = j.z
          , j = j.w;
        this.x = g * j + n * m + p * k - o * l;
        this.y = p * j + n * l + o * m - g * k;
        this.z = o * j + n * k + g * l - p * m;
        this.w = n * j - g * m - p * l - o * k;
        return this
    },
    multiplyVector3: function(x, w) {
        w || (w = x);
        var v = x.x
          , u = x.y
          , t = x.z
          , s = this.x
          , r = this.y
          , q = this.z
          , n = this.w
          , o = n * v + r * t - q * u
          , p = n * u + q * v - s * t
          , g = n * t + s * u - r * v
          , v = -s * v - r * u - q * t;
        w.x = o * n + v * -s + p * -q - g * -r;
        w.y = p * n + v * -r + g * -s - o * -q;
        w.z = g * n + v * -q + o * -r - p * -s;
        return w
    },
    clone: function() {
        return new THREE.Quaternion(this.x,this.y,this.z,this.w)
    }
};
THREE.Quaternion.slerp = function(h, g, l, k) {
    var j = h.w * g.w + h.x * g.x + h.y * g.y + h.z * g.z;
    if (j < 0) {
        l.w = -g.w;
        l.x = -g.x;
        l.y = -g.y;
        l.z = -g.z;
        j = -j
    } else {
        l.copy(g)
    }
    if (Math.abs(j) >= 1) {
        l.w = h.w;
        l.x = h.x;
        l.y = h.y;
        l.z = h.z;
        return l
    }
    var i = Math.acos(j)
      , j = Math.sqrt(1 - j * j);
    if (Math.abs(j) < 0.001) {
        l.w = 0.5 * (h.w + g.w);
        l.x = 0.5 * (h.x + g.x);
        l.y = 0.5 * (h.y + g.y);
        l.z = 0.5 * (h.z + g.z);
        return l
    }
    g = Math.sin((1 - k) * i) / j;
    k = Math.sin(k * i) / j;
    l.w = h.w * g + l.w * k;
    l.x = h.x * g + l.x * k;
    l.y = h.y * g + l.y * k;
    l.z = h.z * g + l.z * k;
    return l
}
;
THREE.Vertex = function() {
    console.warn("THREE.Vertex has been DEPRECATED. Use THREE.Vector3 instead.")
}
;
THREE.Face3 = function(h, g, l, k, j, i) {
    this.a = h;
    this.b = g;
    this.c = l;
    this.normal = k instanceof THREE.Vector3 ? k : new THREE.Vector3;
    this.vertexNormals = k instanceof Array ? k : [];
    this.color = j instanceof THREE.Color ? j : new THREE.Color;
    this.vertexColors = j instanceof Array ? j : [];
    this.vertexTangents = [];
    this.materialIndex = i;
    this.centroid = new THREE.Vector3
}
;
THREE.Face3.prototype = {
    constructor: THREE.Face3,
    clone: function() {
        var e = new THREE.Face3(this.a,this.b,this.c);
        e.normal.copy(this.normal);
        e.color.copy(this.color);
        e.centroid.copy(this.centroid);
        e.materialIndex = this.materialIndex;
        var d, f;
        d = 0;
        for (f = this.vertexNormals.length; d < f; d++) {
            e.vertexNormals[d] = this.vertexNormals[d].clone()
        }
        d = 0;
        for (f = this.vertexColors.length; d < f; d++) {
            e.vertexColors[d] = this.vertexColors[d].clone()
        }
        d = 0;
        for (f = this.vertexTangents.length; d < f; d++) {
            e.vertexTangents[d] = this.vertexTangents[d].clone()
        }
        return e
    }
};
THREE.Face4 = function(i, g, n, m, l, k, j) {
    this.a = i;
    this.b = g;
    this.c = n;
    this.d = m;
    this.normal = l instanceof THREE.Vector3 ? l : new THREE.Vector3;
    this.vertexNormals = l instanceof Array ? l : [];
    this.color = k instanceof THREE.Color ? k : new THREE.Color;
    this.vertexColors = k instanceof Array ? k : [];
    this.vertexTangents = [];
    this.materialIndex = j;
    this.centroid = new THREE.Vector3
}
;
THREE.Face4.prototype = {
    constructor: THREE.Face4,
    clone: function() {
        var e = new THREE.Face4(this.a,this.b,this.c,this.d);
        e.normal.copy(this.normal);
        e.color.copy(this.color);
        e.centroid.copy(this.centroid);
        e.materialIndex = this.materialIndex;
        var d, f;
        d = 0;
        for (f = this.vertexNormals.length; d < f; d++) {
            e.vertexNormals[d] = this.vertexNormals[d].clone()
        }
        d = 0;
        for (f = this.vertexColors.length; d < f; d++) {
            e.vertexColors[d] = this.vertexColors[d].clone()
        }
        d = 0;
        for (f = this.vertexTangents.length; d < f; d++) {
            e.vertexTangents[d] = this.vertexTangents[d].clone()
        }
        return e
    }
};
THREE.UV = function(d, c) {
    this.u = d || 0;
    this.v = c || 0
}
;
THREE.UV.prototype = {
    constructor: THREE.UV,
    set: function(d, c) {
        this.u = d;
        this.v = c;
        return this
    },
    copy: function(b) {
        this.u = b.u;
        this.v = b.v;
        return this
    },
    lerpSelf: function(d, c) {
        this.u = this.u + (d.u - this.u) * c;
        this.v = this.v + (d.v - this.v) * c;
        return this
    },
    clone: function() {
        return new THREE.UV(this.u,this.v)
    }
};
THREE.Geometry = function() {
    this.id = THREE.GeometryCount++;
    this.vertices = [];
    this.colors = [];
    this.materials = [];
    this.faces = [];
    this.faceUvs = [[]];
    this.faceVertexUvs = [[]];
    this.morphTargets = [];
    this.morphColors = [];
    this.morphNormals = [];
    this.skinWeights = [];
    this.skinIndices = [];
    this.boundingSphere = this.boundingBox = null;
    this.dynamic = this.hasTangents = false
}
;
THREE.Geometry.prototype = {
    constructor: THREE.Geometry,
    applyMatrix: function(i) {
        var g = new THREE.Matrix4;
        g.extractRotation(i);
        for (var n = 0, m = this.vertices.length; n < m; n++) {
            i.multiplyVector3(this.vertices[n])
        }
        n = 0;
        for (m = this.faces.length; n < m; n++) {
            var l = this.faces[n];
            g.multiplyVector3(l.normal);
            for (var k = 0, j = l.vertexNormals.length; k < j; k++) {
                g.multiplyVector3(l.vertexNormals[k])
            }
            i.multiplyVector3(l.centroid)
        }
    },
    computeCentroids: function() {
        var e, d, f;
        e = 0;
        for (d = this.faces.length; e < d; e++) {
            f = this.faces[e];
            f.centroid.set(0, 0, 0);
            if (f instanceof THREE.Face3) {
                f.centroid.addSelf(this.vertices[f.a]);
                f.centroid.addSelf(this.vertices[f.b]);
                f.centroid.addSelf(this.vertices[f.c]);
                f.centroid.divideScalar(3)
            } else {
                if (f instanceof THREE.Face4) {
                    f.centroid.addSelf(this.vertices[f.a]);
                    f.centroid.addSelf(this.vertices[f.b]);
                    f.centroid.addSelf(this.vertices[f.c]);
                    f.centroid.addSelf(this.vertices[f.d]);
                    f.centroid.divideScalar(4)
                }
            }
        }
    },
    computeFaceNormals: function() {
        var j, g, p, o, n, m, l = new THREE.Vector3, k = new THREE.Vector3;
        j = 0;
        for (g = this.faces.length; j < g; j++) {
            p = this.faces[j];
            o = this.vertices[p.a];
            n = this.vertices[p.b];
            m = this.vertices[p.c];
            l.sub(m, n);
            k.sub(o, n);
            l.crossSelf(k);
            l.isZero() || l.normalize();
            p.normal.copy(l)
        }
    },
    computeVertexNormals: function() {
        var f, e, h, g;
        if (this.__tmpVertices === void 0) {
            g = this.__tmpVertices = Array(this.vertices.length);
            f = 0;
            for (e = this.vertices.length; f < e; f++) {
                g[f] = new THREE.Vector3
            }
            f = 0;
            for (e = this.faces.length; f < e; f++) {
                h = this.faces[f];
                if (h instanceof THREE.Face3) {
                    h.vertexNormals = [new THREE.Vector3, new THREE.Vector3, new THREE.Vector3]
                } else {
                    if (h instanceof THREE.Face4) {
                        h.vertexNormals = [new THREE.Vector3, new THREE.Vector3, new THREE.Vector3, new THREE.Vector3]
                    }
                }
            }
        } else {
            g = this.__tmpVertices;
            f = 0;
            for (e = this.vertices.length; f < e; f++) {
                g[f].set(0, 0, 0)
            }
        }
        f = 0;
        for (e = this.faces.length; f < e; f++) {
            h = this.faces[f];
            if (h instanceof THREE.Face3) {
                g[h.a].addSelf(h.normal);
                g[h.b].addSelf(h.normal);
                g[h.c].addSelf(h.normal)
            } else {
                if (h instanceof THREE.Face4) {
                    g[h.a].addSelf(h.normal);
                    g[h.b].addSelf(h.normal);
                    g[h.c].addSelf(h.normal);
                    g[h.d].addSelf(h.normal)
                }
            }
        }
        f = 0;
        for (e = this.vertices.length; f < e; f++) {
            g[f].normalize()
        }
        f = 0;
        for (e = this.faces.length; f < e; f++) {
            h = this.faces[f];
            if (h instanceof THREE.Face3) {
                h.vertexNormals[0].copy(g[h.a]);
                h.vertexNormals[1].copy(g[h.b]);
                h.vertexNormals[2].copy(g[h.c])
            } else {
                if (h instanceof THREE.Face4) {
                    h.vertexNormals[0].copy(g[h.a]);
                    h.vertexNormals[1].copy(g[h.b]);
                    h.vertexNormals[2].copy(g[h.c]);
                    h.vertexNormals[3].copy(g[h.d])
                }
            }
        }
    },
    computeMorphNormals: function() {
        var t, s, r, q, p;
        r = 0;
        for (q = this.faces.length; r < q; r++) {
            p = this.faces[r];
            p.__originalFaceNormal ? p.__originalFaceNormal.copy(p.normal) : p.__originalFaceNormal = p.normal.clone();
            if (!p.__originalVertexNormals) {
                p.__originalVertexNormals = []
            }
            t = 0;
            for (s = p.vertexNormals.length; t < s; t++) {
                p.__originalVertexNormals[t] ? p.__originalVertexNormals[t].copy(p.vertexNormals[t]) : p.__originalVertexNormals[t] = p.vertexNormals[t].clone()
            }
        }
        var o = new THREE.Geometry;
        o.faces = this.faces;
        t = 0;
        for (s = this.morphTargets.length; t < s; t++) {
            if (!this.morphNormals[t]) {
                this.morphNormals[t] = {};
                this.morphNormals[t].faceNormals = [];
                this.morphNormals[t].vertexNormals = [];
                var n = this.morphNormals[t].faceNormals, m = this.morphNormals[t].vertexNormals, g, j;
                r = 0;
                for (q = this.faces.length; r < q; r++) {
                    p = this.faces[r];
                    g = new THREE.Vector3;
                    j = p instanceof THREE.Face3 ? {
                        a: new THREE.Vector3,
                        b: new THREE.Vector3,
                        c: new THREE.Vector3
                    } : {
                        a: new THREE.Vector3,
                        b: new THREE.Vector3,
                        c: new THREE.Vector3,
                        d: new THREE.Vector3
                    };
                    n.push(g);
                    m.push(j)
                }
            }
            n = this.morphNormals[t];
            o.vertices = this.morphTargets[t].vertices;
            o.computeFaceNormals();
            o.computeVertexNormals();
            r = 0;
            for (q = this.faces.length; r < q; r++) {
                p = this.faces[r];
                g = n.faceNormals[r];
                j = n.vertexNormals[r];
                g.copy(p.normal);
                if (p instanceof THREE.Face3) {
                    j.a.copy(p.vertexNormals[0]);
                    j.b.copy(p.vertexNormals[1]);
                    j.c.copy(p.vertexNormals[2])
                } else {
                    j.a.copy(p.vertexNormals[0]);
                    j.b.copy(p.vertexNormals[1]);
                    j.c.copy(p.vertexNormals[2]);
                    j.d.copy(p.vertexNormals[3])
                }
            }
        }
        r = 0;
        for (q = this.faces.length; r < q; r++) {
            p = this.faces[r];
            p.normal = p.__originalFaceNormal;
            p.vertexNormals = p.__originalVertexNormals
        }
    },
    computeTangents: function() {
        function aj(i, h, n, m, j, l, k) {
            ab = i.vertices[h];
            X = i.vertices[n];
            Z = i.vertices[m];
            aa = ac[j];
            W = ac[l];
            V = ac[k];
            U = X.x - ab.x;
            O = Z.x - ab.x;
            S = X.y - ab.y;
            Q = Z.y - ab.y;
            J = X.z - ab.z;
            R = Z.z - ab.z;
            E = W.u - aa.u;
            L = V.u - aa.u;
            G = W.v - aa.v;
            P = V.v - aa.v;
            C = 1 / (E * P - L * G);
            F.set((P * U - G * O) * C, (P * S - G * Q) * C, (P * J - G * R) * C);
            x.set((E * O - L * U) * C, (E * Q - L * S) * C, (E * R - L * J) * C);
            t[h].addSelf(F);
            t[n].addSelf(F);
            t[m].addSelf(F);
            T[h].addSelf(x);
            T[n].addSelf(x);
            T[m].addSelf(x)
        }
        var ai, ah, ag, af, ae, ac, ab, X, Z, aa, W, V, U, O, S, Q, J, R, E, L, G, P, C, z, t = [], T = [], F = new THREE.Vector3, x = new THREE.Vector3, r = new THREE.Vector3, v = new THREE.Vector3, M = new THREE.Vector3;
        ai = 0;
        for (ah = this.vertices.length; ai < ah; ai++) {
            t[ai] = new THREE.Vector3;
            T[ai] = new THREE.Vector3
        }
        ai = 0;
        for (ah = this.faces.length; ai < ah; ai++) {
            ae = this.faces[ai];
            ac = this.faceVertexUvs[0][ai];
            if (ae instanceof THREE.Face3) {
                aj(this, ae.a, ae.b, ae.c, 0, 1, 2)
            } else {
                if (ae instanceof THREE.Face4) {
                    aj(this, ae.a, ae.b, ae.d, 0, 1, 3);
                    aj(this, ae.b, ae.c, ae.d, 1, 2, 3)
                }
            }
        }
        var ad = ["a", "b", "c", "d"];
        ai = 0;
        for (ah = this.faces.length; ai < ah; ai++) {
            ae = this.faces[ai];
            for (ag = 0; ag < ae.vertexNormals.length; ag++) {
                M.copy(ae.vertexNormals[ag]);
                af = ae[ad[ag]];
                z = t[af];
                r.copy(z);
                r.subSelf(M.multiplyScalar(M.dot(z))).normalize();
                v.cross(ae.vertexNormals[ag], z);
                af = v.dot(T[af]);
                af = af < 0 ? -1 : 1;
                ae.vertexTangents[ag] = new THREE.Vector4(r.x,r.y,r.z,af)
            }
        }
        this.hasTangents = true
    },
    computeBoundingBox: function() {
        if (!this.boundingBox) {
            this.boundingBox = {
                min: new THREE.Vector3,
                max: new THREE.Vector3
            }
        }
        if (this.vertices.length > 0) {
            var g;
            g = this.vertices[0];
            this.boundingBox.min.copy(g);
            this.boundingBox.max.copy(g);
            for (var f = this.boundingBox.min, j = this.boundingBox.max, i = 1, h = this.vertices.length; i < h; i++) {
                g = this.vertices[i];
                if (g.x < f.x) {
                    f.x = g.x
                } else {
                    if (g.x > j.x) {
                        j.x = g.x
                    }
                }
                if (g.y < f.y) {
                    f.y = g.y
                } else {
                    if (g.y > j.y) {
                        j.y = g.y
                    }
                }
                if (g.z < f.z) {
                    f.z = g.z
                } else {
                    if (g.z > j.z) {
                        j.z = g.z
                    }
                }
            }
        } else {
            this.boundingBox.min.set(0, 0, 0);
            this.boundingBox.max.set(0, 0, 0)
        }
    },
    computeBoundingSphere: function() {
        if (!this.boundingSphere) {
            this.boundingSphere = {
                radius: 0
            }
        }
        for (var f, e = 0, h = 0, g = this.vertices.length; h < g; h++) {
            f = this.vertices[h].length();
            f > e && (e = f)
        }
        this.boundingSphere.radius = e
    },
    mergeVertices: function() {
        var j = {}, g = [], p = [], o, n = Math.pow(10, 4), m, l, k;
        m = 0;
        for (l = this.vertices.length; m < l; m++) {
            o = this.vertices[m];
            o = [Math.round(o.x * n), Math.round(o.y * n), Math.round(o.z * n)].join("_");
            if (j[o] === void 0) {
                j[o] = m;
                g.push(this.vertices[m]);
                p[m] = g.length - 1
            } else {
                p[m] = p[j[o]]
            }
        }
        m = 0;
        for (l = this.faces.length; m < l; m++) {
            n = this.faces[m];
            if (n instanceof THREE.Face3) {
                n.a = p[n.a];
                n.b = p[n.b];
                n.c = p[n.c]
            } else {
                if (n instanceof THREE.Face4) {
                    n.a = p[n.a];
                    n.b = p[n.b];
                    n.c = p[n.c];
                    n.d = p[n.d];
                    o = [n.a, n.b, n.c, n.d];
                    for (j = 3; j > 0; j--) {
                        if (o.indexOf(n["abcd"[j]]) != j) {
                            o.splice(j, 1);
                            this.faces[m] = new THREE.Face3(o[0],o[1],o[2]);
                            n = 0;
                            for (o = this.faceVertexUvs.length; n < o; n++) {
                                (k = this.faceVertexUvs[n][m]) && k.splice(j, 1)
                            }
                            break
                        }
                    }
                }
            }
        }
        p = this.vertices.length - g.length;
        this.vertices = g;
        return p
    }
};
THREE.GeometryCount = 0;
THREE.Spline = function(z) {
    function y(j, i, A, n, m, l, k) {
        j = (A - j) * 0.5;
        n = (n - i) * 0.5;
        return (2 * (i - A) + j + n) * k + (-3 * (i - A) - 2 * j - n) * l + j * m + i
    }
    this.points = z;
    var x = [], w = {
        x: 0,
        y: 0,
        z: 0
    }, v, u, t, s, p, q, r, o, g;
    this.initFromArray = function(d) {
        this.points = [];
        for (var c = 0; c < d.length; c++) {
            this.points[c] = {
                x: d[c][0],
                y: d[c][1],
                z: d[c][2]
            }
        }
    }
    ;
    this.getPoint = function(b) {
        v = (this.points.length - 1) * b;
        u = Math.floor(v);
        t = v - u;
        x[0] = u === 0 ? u : u - 1;
        x[1] = u;
        x[2] = u > this.points.length - 2 ? this.points.length - 1 : u + 1;
        x[3] = u > this.points.length - 3 ? this.points.length - 1 : u + 2;
        q = this.points[x[0]];
        r = this.points[x[1]];
        o = this.points[x[2]];
        g = this.points[x[3]];
        s = t * t;
        p = t * s;
        w.x = y(q.x, r.x, o.x, g.x, t, s, p);
        w.y = y(q.y, r.y, o.y, g.y, t, s, p);
        w.z = y(q.z, r.z, o.z, g.z, t, s, p);
        return w
    }
    ;
    this.getControlPointsArray = function() {
        var f, e, i = this.points.length, h = [];
        for (f = 0; f < i; f++) {
            e = this.points[f];
            h[f] = [e.x, e.y, e.z]
        }
        return h
    }
    ;
    this.getLength = function(E) {
        var D, C, B, A = D = D = 0, n = new THREE.Vector3, m = new THREE.Vector3, l = [], j = 0;
        l[0] = 0;
        E || (E = 100);
        C = this.points.length * E;
        n.copy(this.points[0]);
        for (E = 1; E < C; E++) {
            D = E / C;
            B = this.getPoint(D);
            m.copy(B);
            j = j + m.distanceTo(n);
            n.copy(B);
            D = (this.points.length - 1) * D;
            D = Math.floor(D);
            if (D != A) {
                l[D] = j;
                A = D
            }
        }
        l[l.length] = j;
        return {
            chunks: l,
            total: j
        }
    }
    ;
    this.reparametrizeByArcLength = function(G) {
        var F, E, D, C, B, A, n = [], l = new THREE.Vector3, m = this.getLength();
        n.push(l.copy(this.points[0]).clone());
        for (F = 1; F < this.points.length; F++) {
            E = m.chunks[F] - m.chunks[F - 1];
            A = Math.ceil(G * E / m.total);
            C = (F - 1) / (this.points.length - 1);
            B = F / (this.points.length - 1);
            for (E = 1; E < A - 1; E++) {
                D = C + E * (1 / A) * (B - C);
                D = this.getPoint(D);
                n.push(l.copy(D).clone())
            }
            n.push(l.copy(this.points[F]).clone())
        }
        this.points = n
    }
}
;
THREE.Camera = function() {
    THREE.Object3D.call(this);
    this.matrixWorldInverse = new THREE.Matrix4;
    this.projectionMatrix = new THREE.Matrix4;
    this.projectionMatrixInverse = new THREE.Matrix4
}
;
THREE.Camera.prototype = new THREE.Object3D;
THREE.Camera.prototype.constructor = THREE.Camera;
THREE.Camera.prototype.lookAt = function(b) {
    this.matrix.lookAt(this.position, b, this.up);
    this.rotationAutoUpdate && this.rotation.getRotationFromMatrix(this.matrix)
}
;
THREE.OrthographicCamera = function(h, g, l, k, j, i) {
    THREE.Camera.call(this);
    this.left = h;
    this.right = g;
    this.top = l;
    this.bottom = k;
    this.near = j !== void 0 ? j : 0.1;
    this.far = i !== void 0 ? i : 2000;
    this.updateProjectionMatrix()
}
;
THREE.OrthographicCamera.prototype = new THREE.Camera;
THREE.OrthographicCamera.prototype.constructor = THREE.OrthographicCamera;
THREE.OrthographicCamera.prototype.updateProjectionMatrix = function() {
    this.projectionMatrix.makeOrthographic(this.left, this.right, this.top, this.bottom, this.near, this.far)
}
;
THREE.PerspectiveCamera = function(f, e, h, g) {
    THREE.Camera.call(this);
    this.fov = f !== void 0 ? f : 50;
    this.aspect = e !== void 0 ? e : 1;
    this.near = h !== void 0 ? h : 0.1;
    this.far = g !== void 0 ? g : 2000;
    this.updateProjectionMatrix()
}
;
THREE.PerspectiveCamera.prototype = new THREE.Camera;
THREE.PerspectiveCamera.prototype.constructor = THREE.PerspectiveCamera;
THREE.PerspectiveCamera.prototype.setLens = function(d, c) {
    this.fov = 2 * Math.atan((c !== void 0 ? c : 24) / (d * 2)) * (180 / Math.PI);
    this.updateProjectionMatrix()
}
;
THREE.PerspectiveCamera.prototype.setViewOffset = function(h, g, l, k, j, i) {
    this.fullWidth = h;
    this.fullHeight = g;
    this.x = l;
    this.y = k;
    this.width = j;
    this.height = i;
    this.updateProjectionMatrix()
}
;
THREE.PerspectiveCamera.prototype.updateProjectionMatrix = function() {
    if (this.fullWidth) {
        var f = this.fullWidth / this.fullHeight
          , e = Math.tan(this.fov * Math.PI / 360) * this.near
          , h = -e
          , g = f * h
          , f = Math.abs(f * e - g)
          , h = Math.abs(e - h);
        this.projectionMatrix.makeFrustum(g + this.x * f / this.fullWidth, g + (this.x + this.width) * f / this.fullWidth, e - (this.y + this.height) * h / this.fullHeight, e - this.y * h / this.fullHeight, this.near, this.far)
    } else {
        this.projectionMatrix.makePerspective(this.fov, this.aspect, this.near, this.far)
    }
}
;
THREE.Light = function(b) {
    THREE.Object3D.call(this);
    this.color = new THREE.Color(b)
}
;
THREE.Light.prototype = new THREE.Object3D;
THREE.Light.prototype.constructor = THREE.Light;
THREE.Light.prototype.supr = THREE.Object3D.prototype;
THREE.AmbientLight = function(b) {
    THREE.Light.call(this, b)
}
;
THREE.AmbientLight.prototype = new THREE.Light;
THREE.AmbientLight.prototype.constructor = THREE.AmbientLight;
THREE.DirectionalLight = function(e, d, f) {
    THREE.Light.call(this, e);
    this.position = new THREE.Vector3(0,1,0);
    this.target = new THREE.Object3D;
    this.intensity = d !== void 0 ? d : 1;
    this.distance = f !== void 0 ? f : 0;
    this.onlyShadow = this.castShadow = false;
    this.shadowCameraNear = 50;
    this.shadowCameraFar = 5000;
    this.shadowCameraLeft = -500;
    this.shadowCameraTop = this.shadowCameraRight = 500;
    this.shadowCameraBottom = -500;
    this.shadowCameraVisible = false;
    this.shadowBias = 0;
    this.shadowDarkness = 0.5;
    this.shadowMapHeight = this.shadowMapWidth = 512;
    this.shadowCascade = false;
    this.shadowCascadeOffset = new THREE.Vector3(0,0,-1000);
    this.shadowCascadeCount = 2;
    this.shadowCascadeBias = [0, 0, 0];
    this.shadowCascadeWidth = [512, 512, 512];
    this.shadowCascadeHeight = [512, 512, 512];
    this.shadowCascadeNearZ = [-1, 0.99, 0.998];
    this.shadowCascadeFarZ = [0.99, 0.998, 1];
    this.shadowCascadeArray = [];
    this.shadowMatrix = this.shadowCamera = this.shadowMapSize = this.shadowMap = null
}
;
THREE.DirectionalLight.prototype = new THREE.Light;
THREE.DirectionalLight.prototype.constructor = THREE.DirectionalLight;
THREE.PointLight = function(e, d, f) {
    THREE.Light.call(this, e);
    this.position = new THREE.Vector3(0,0,0);
    this.intensity = d !== void 0 ? d : 1;
    this.distance = f !== void 0 ? f : 0
}
;
THREE.PointLight.prototype = new THREE.Light;
THREE.PointLight.prototype.constructor = THREE.PointLight;
THREE.SpotLight = function(g, f, j, i, h) {
    THREE.Light.call(this, g);
    this.position = new THREE.Vector3(0,1,0);
    this.target = new THREE.Object3D;
    this.intensity = f !== void 0 ? f : 1;
    this.distance = j !== void 0 ? j : 0;
    this.angle = i !== void 0 ? i : Math.PI / 2;
    this.exponent = h !== void 0 ? h : 10;
    this.onlyShadow = this.castShadow = false;
    this.shadowCameraNear = 50;
    this.shadowCameraFar = 5000;
    this.shadowCameraFov = 50;
    this.shadowCameraVisible = false;
    this.shadowBias = 0;
    this.shadowDarkness = 0.5;
    this.shadowMapHeight = this.shadowMapWidth = 512;
    this.shadowMatrix = this.shadowCamera = this.shadowMapSize = this.shadowMap = null
}
;
THREE.SpotLight.prototype = new THREE.Light;
THREE.SpotLight.prototype.constructor = THREE.SpotLight;
THREE.Material = function(b) {
    b = b || {};
    this.id = THREE.MaterialCount++;
    this.name = "";
    this.opacity = b.opacity !== void 0 ? b.opacity : 1;
    this.transparent = b.transparent !== void 0 ? b.transparent : false;
    this.blending = b.blending !== void 0 ? b.blending : THREE.NormalBlending;
    this.blendSrc = b.blendSrc !== void 0 ? b.blendSrc : THREE.SrcAlphaFactor;
    this.blendDst = b.blendDst !== void 0 ? b.blendDst : THREE.OneMinusSrcAlphaFactor;
    this.blendEquation = b.blendEquation !== void 0 ? b.blendEquation : THREE.AddEquation;
    this.depthTest = b.depthTest !== void 0 ? b.depthTest : true;
    this.depthWrite = b.depthWrite !== void 0 ? b.depthWrite : true;
    this.polygonOffset = b.polygonOffset !== void 0 ? b.polygonOffset : false;
    this.polygonOffsetFactor = b.polygonOffsetFactor !== void 0 ? b.polygonOffsetFactor : 0;
    this.polygonOffsetUnits = b.polygonOffsetUnits !== void 0 ? b.polygonOffsetUnits : 0;
    this.alphaTest = b.alphaTest !== void 0 ? b.alphaTest : 0;
    this.overdraw = b.overdraw !== void 0 ? b.overdraw : false;
    this.needsUpdate = this.visible = true
}
;
THREE.MaterialCount = 0;
THREE.NoShading = 0;
THREE.FlatShading = 1;
THREE.SmoothShading = 2;
THREE.NoColors = 0;
THREE.FaceColors = 1;
THREE.VertexColors = 2;
THREE.NoBlending = 0;
THREE.NormalBlending = 1;
THREE.AdditiveBlending = 2;
THREE.SubtractiveBlending = 3;
THREE.MultiplyBlending = 4;
THREE.AdditiveAlphaBlending = 5;
THREE.CustomBlending = 6;
THREE.AddEquation = 100;
THREE.SubtractEquation = 101;
THREE.ReverseSubtractEquation = 102;
THREE.ZeroFactor = 200;
THREE.OneFactor = 201;
THREE.SrcColorFactor = 202;
THREE.OneMinusSrcColorFactor = 203;
THREE.SrcAlphaFactor = 204;
THREE.OneMinusSrcAlphaFactor = 205;
THREE.DstAlphaFactor = 206;
THREE.OneMinusDstAlphaFactor = 207;
THREE.DstColorFactor = 208;
THREE.OneMinusDstColorFactor = 209;
THREE.SrcAlphaSaturateFactor = 210;
THREE.LineBasicMaterial = function(b) {
    THREE.Material.call(this, b);
    b = b || {};
    this.color = b.color !== void 0 ? new THREE.Color(b.color) : new THREE.Color(16777215);
    this.linewidth = b.linewidth !== void 0 ? b.linewidth : 1;
    this.linecap = b.linecap !== void 0 ? b.linecap : "round";
    this.linejoin = b.linejoin !== void 0 ? b.linejoin : "round";
    this.vertexColors = b.vertexColors ? b.vertexColors : false;
    this.fog = b.fog !== void 0 ? b.fog : true
}
;
THREE.LineBasicMaterial.prototype = new THREE.Material;
THREE.LineBasicMaterial.prototype.constructor = THREE.LineBasicMaterial;
THREE.MeshBasicMaterial = function(b) {
    THREE.Material.call(this, b);
    b = b || {};
    this.color = b.color !== void 0 ? new THREE.Color(b.color) : new THREE.Color(16777215);
    this.map = b.map !== void 0 ? b.map : null;
    this.lightMap = b.lightMap !== void 0 ? b.lightMap : null;
    this.envMap = b.envMap !== void 0 ? b.envMap : null;
    this.combine = b.combine !== void 0 ? b.combine : THREE.MultiplyOperation;
    this.reflectivity = b.reflectivity !== void 0 ? b.reflectivity : 1;
    this.refractionRatio = b.refractionRatio !== void 0 ? b.refractionRatio : 0.98;
    this.fog = b.fog !== void 0 ? b.fog : true;
    this.shading = b.shading !== void 0 ? b.shading : THREE.SmoothShading;
    this.wireframe = b.wireframe !== void 0 ? b.wireframe : false;
    this.wireframeLinewidth = b.wireframeLinewidth !== void 0 ? b.wireframeLinewidth : 1;
    this.wireframeLinecap = b.wireframeLinecap !== void 0 ? b.wireframeLinecap : "round";
    this.wireframeLinejoin = b.wireframeLinejoin !== void 0 ? b.wireframeLinejoin : "round";
    this.vertexColors = b.vertexColors !== void 0 ? b.vertexColors : THREE.NoColors;
    this.skinning = b.skinning !== void 0 ? b.skinning : false;
    this.morphTargets = b.morphTargets !== void 0 ? b.morphTargets : false
}
;
THREE.MeshBasicMaterial.prototype = new THREE.Material;
THREE.MeshBasicMaterial.prototype.constructor = THREE.MeshBasicMaterial;
THREE.MeshLambertMaterial = function(b) {
    THREE.Material.call(this, b);
    b = b || {};
    this.color = b.color !== void 0 ? new THREE.Color(b.color) : new THREE.Color(16777215);
    this.ambient = b.ambient !== void 0 ? new THREE.Color(b.ambient) : new THREE.Color(16777215);
    this.emissive = b.emissive !== void 0 ? new THREE.Color(b.emissive) : new THREE.Color(0);
    this.wrapAround = b.wrapAround !== void 0 ? b.wrapAround : false;
    this.wrapRGB = new THREE.Vector3(1,1,1);
    this.map = b.map !== void 0 ? b.map : null;
    this.lightMap = b.lightMap !== void 0 ? b.lightMap : null;
    this.envMap = b.envMap !== void 0 ? b.envMap : null;
    this.combine = b.combine !== void 0 ? b.combine : THREE.MultiplyOperation;
    this.reflectivity = b.reflectivity !== void 0 ? b.reflectivity : 1;
    this.refractionRatio = b.refractionRatio !== void 0 ? b.refractionRatio : 0.98;
    this.fog = b.fog !== void 0 ? b.fog : true;
    this.shading = b.shading !== void 0 ? b.shading : THREE.SmoothShading;
    this.wireframe = b.wireframe !== void 0 ? b.wireframe : false;
    this.wireframeLinewidth = b.wireframeLinewidth !== void 0 ? b.wireframeLinewidth : 1;
    this.wireframeLinecap = b.wireframeLinecap !== void 0 ? b.wireframeLinecap : "round";
    this.wireframeLinejoin = b.wireframeLinejoin !== void 0 ? b.wireframeLinejoin : "round";
    this.vertexColors = b.vertexColors !== void 0 ? b.vertexColors : THREE.NoColors;
    this.skinning = b.skinning !== void 0 ? b.skinning : false;
    this.morphTargets = b.morphTargets !== void 0 ? b.morphTargets : false;
    this.morphNormals = b.morphNormals !== void 0 ? b.morphNormals : false
}
;
THREE.MeshLambertMaterial.prototype = new THREE.Material;
THREE.MeshLambertMaterial.prototype.constructor = THREE.MeshLambertMaterial;
THREE.MeshPhongMaterial = function(b) {
    THREE.Material.call(this, b);
    b = b || {};
    this.color = b.color !== void 0 ? new THREE.Color(b.color) : new THREE.Color(16777215);
    this.ambient = b.ambient !== void 0 ? new THREE.Color(b.ambient) : new THREE.Color(16777215);
    this.emissive = b.emissive !== void 0 ? new THREE.Color(b.emissive) : new THREE.Color(0);
    this.specular = b.specular !== void 0 ? new THREE.Color(b.specular) : new THREE.Color(1118481);
    this.shininess = b.shininess !== void 0 ? b.shininess : 30;
    this.metal = b.metal !== void 0 ? b.metal : false;
    this.perPixel = b.perPixel !== void 0 ? b.perPixel : false;
    this.wrapAround = b.wrapAround !== void 0 ? b.wrapAround : false;
    this.wrapRGB = new THREE.Vector3(1,1,1);
    this.map = b.map !== void 0 ? b.map : null;
    this.lightMap = b.lightMap !== void 0 ? b.lightMap : null;
    this.envMap = b.envMap !== void 0 ? b.envMap : null;
    this.combine = b.combine !== void 0 ? b.combine : THREE.MultiplyOperation;
    this.reflectivity = b.reflectivity !== void 0 ? b.reflectivity : 1;
    this.refractionRatio = b.refractionRatio !== void 0 ? b.refractionRatio : 0.98;
    this.fog = b.fog !== void 0 ? b.fog : true;
    this.shading = b.shading !== void 0 ? b.shading : THREE.SmoothShading;
    this.wireframe = b.wireframe !== void 0 ? b.wireframe : false;
    this.wireframeLinewidth = b.wireframeLinewidth !== void 0 ? b.wireframeLinewidth : 1;
    this.wireframeLinecap = b.wireframeLinecap !== void 0 ? b.wireframeLinecap : "round";
    this.wireframeLinejoin = b.wireframeLinejoin !== void 0 ? b.wireframeLinejoin : "round";
    this.vertexColors = b.vertexColors !== void 0 ? b.vertexColors : THREE.NoColors;
    this.skinning = b.skinning !== void 0 ? b.skinning : false;
    this.morphTargets = b.morphTargets !== void 0 ? b.morphTargets : false;
    this.morphNormals = b.morphNormals !== void 0 ? b.morphNormals : false
}
;
THREE.MeshPhongMaterial.prototype = new THREE.Material;
THREE.MeshPhongMaterial.prototype.constructor = THREE.MeshPhongMaterial;
THREE.MeshDepthMaterial = function(b) {
    THREE.Material.call(this, b);
    b = b || {};
    this.shading = b.shading !== void 0 ? b.shading : THREE.SmoothShading;
    this.wireframe = b.wireframe !== void 0 ? b.wireframe : false;
    this.wireframeLinewidth = b.wireframeLinewidth !== void 0 ? b.wireframeLinewidth : 1
}
;
THREE.MeshDepthMaterial.prototype = new THREE.Material;
THREE.MeshDepthMaterial.prototype.constructor = THREE.MeshDepthMaterial;
THREE.MeshNormalMaterial = function(b) {
    THREE.Material.call(this, b);
    b = b || {};
    this.shading = b.shading ? b.shading : THREE.FlatShading;
    this.wireframe = b.wireframe ? b.wireframe : false;
    this.wireframeLinewidth = b.wireframeLinewidth ? b.wireframeLinewidth : 1
}
;
THREE.MeshNormalMaterial.prototype = new THREE.Material;
THREE.MeshNormalMaterial.prototype.constructor = THREE.MeshNormalMaterial;
THREE.MeshFaceMaterial = function() {}
;
THREE.ParticleBasicMaterial = function(b) {
    THREE.Material.call(this, b);
    b = b || {};
    this.color = b.color !== void 0 ? new THREE.Color(b.color) : new THREE.Color(16777215);
    this.map = b.map !== void 0 ? b.map : null;
    this.size = b.size !== void 0 ? b.size : 1;
    this.sizeAttenuation = b.sizeAttenuation !== void 0 ? b.sizeAttenuation : true;
    this.vertexColors = b.vertexColors !== void 0 ? b.vertexColors : false;
    this.fog = b.fog !== void 0 ? b.fog : true
}
;
THREE.ParticleBasicMaterial.prototype = new THREE.Material;
THREE.ParticleBasicMaterial.prototype.constructor = THREE.ParticleBasicMaterial;
THREE.ShaderMaterial = function(b) {
    THREE.Material.call(this, b);
    b = b || {};
    this.fragmentShader = b.fragmentShader !== void 0 ? b.fragmentShader : "void main() {}";
    this.vertexShader = b.vertexShader !== void 0 ? b.vertexShader : "void main() {}";
    this.uniforms = b.uniforms !== void 0 ? b.uniforms : {};
    this.attributes = b.attributes;
    this.shading = b.shading !== void 0 ? b.shading : THREE.SmoothShading;
    this.wireframe = b.wireframe !== void 0 ? b.wireframe : false;
    this.wireframeLinewidth = b.wireframeLinewidth !== void 0 ? b.wireframeLinewidth : 1;
    this.fog = b.fog !== void 0 ? b.fog : false;
    this.lights = b.lights !== void 0 ? b.lights : false;
    this.vertexColors = b.vertexColors !== void 0 ? b.vertexColors : THREE.NoColors;
    this.skinning = b.skinning !== void 0 ? b.skinning : false;
    this.morphTargets = b.morphTargets !== void 0 ? b.morphTargets : false;
    this.morphNormals = b.morphNormals !== void 0 ? b.morphNormals : false
}
;
THREE.ShaderMaterial.prototype = new THREE.Material;
THREE.ShaderMaterial.prototype.constructor = THREE.ShaderMaterial;
THREE.Texture = function(j, g, p, o, n, m, l, k) {
    this.id = THREE.TextureCount++;
    this.image = j;
    this.mapping = g !== void 0 ? g : new THREE.UVMapping;
    this.wrapS = p !== void 0 ? p : THREE.ClampToEdgeWrapping;
    this.wrapT = o !== void 0 ? o : THREE.ClampToEdgeWrapping;
    this.magFilter = n !== void 0 ? n : THREE.LinearFilter;
    this.minFilter = m !== void 0 ? m : THREE.LinearMipMapLinearFilter;
    this.format = l !== void 0 ? l : THREE.RGBAFormat;
    this.type = k !== void 0 ? k : THREE.UnsignedByteType;
    this.offset = new THREE.Vector2(0,0);
    this.repeat = new THREE.Vector2(1,1);
    this.generateMipmaps = true;
    this.needsUpdate = this.premultiplyAlpha = false;
    this.onUpdate = null
}
;
THREE.Texture.prototype = {
    constructor: THREE.Texture,
    clone: function() {
        var b = new THREE.Texture(this.image,this.mapping,this.wrapS,this.wrapT,this.magFilter,this.minFilter,this.format,this.type);
        b.offset.copy(this.offset);
        b.repeat.copy(this.repeat);
        return b
    }
};
THREE.TextureCount = 0;
THREE.MultiplyOperation = 0;
THREE.MixOperation = 1;
THREE.UVMapping = function() {}
;
THREE.CubeReflectionMapping = function() {}
;
THREE.CubeRefractionMapping = function() {}
;
THREE.SphericalReflectionMapping = function() {}
;
THREE.SphericalRefractionMapping = function() {}
;
THREE.RepeatWrapping = 0;
THREE.ClampToEdgeWrapping = 1;
THREE.MirroredRepeatWrapping = 2;
THREE.NearestFilter = 3;
THREE.NearestMipMapNearestFilter = 4;
THREE.NearestMipMapLinearFilter = 5;
THREE.LinearFilter = 6;
THREE.LinearMipMapNearestFilter = 7;
THREE.LinearMipMapLinearFilter = 8;
THREE.ByteType = 9;
THREE.UnsignedByteType = 10;
THREE.ShortType = 11;
THREE.UnsignedShortType = 12;
THREE.IntType = 13;
THREE.UnsignedIntType = 14;
THREE.FloatType = 15;
THREE.AlphaFormat = 16;
THREE.RGBFormat = 17;
THREE.RGBAFormat = 18;
THREE.LuminanceFormat = 19;
THREE.LuminanceAlphaFormat = 20;
THREE.DataTexture = function(t, s, r, q, p, o, n, m, g, j) {
    THREE.Texture.call(this, null, o, n, m, g, j, q, p);
    this.image = {
        data: t,
        width: s,
        height: r
    }
}
;
THREE.DataTexture.prototype = new THREE.Texture;
THREE.DataTexture.prototype.constructor = THREE.DataTexture;
THREE.DataTexture.prototype.clone = function() {
    var b = new THREE.DataTexture(this.image.data,this.image.width,this.image.height,this.format,this.type,this.mapping,this.wrapS,this.wrapT,this.magFilter,this.minFilter);
    b.offset.copy(this.offset);
    b.repeat.copy(this.repeat);
    return b
}
;
THREE.Particle = function(b) {
    THREE.Object3D.call(this);
    this.material = b
}
;
THREE.Particle.prototype = new THREE.Object3D;
THREE.Particle.prototype.constructor = THREE.Particle;
THREE.ParticleSystem = function(d, c) {
    THREE.Object3D.call(this);
    this.geometry = d;
    this.material = c !== void 0 ? c : new THREE.ParticleBasicMaterial({
        color: Math.random() * 16777215
    });
    this.sortParticles = false;
    if (this.geometry) {
        this.geometry.boundingSphere || this.geometry.computeBoundingSphere();
        this.boundRadius = d.boundingSphere.radius
    }
    this.frustumCulled = false
}
;
THREE.ParticleSystem.prototype = new THREE.Object3D;
THREE.ParticleSystem.prototype.constructor = THREE.ParticleSystem;
THREE.Line = function(e, d, f) {
    THREE.Object3D.call(this);
    this.geometry = e;
    this.material = d !== void 0 ? d : new THREE.LineBasicMaterial({
        color: Math.random() * 16777215
    });
    this.type = f !== void 0 ? f : THREE.LineStrip;
    this.geometry && (this.geometry.boundingSphere || this.geometry.computeBoundingSphere())
}
;
THREE.LineStrip = 0;
THREE.LinePieces = 1;
THREE.Line.prototype = new THREE.Object3D;
THREE.Line.prototype.constructor = THREE.Line;
THREE.Mesh = function(e, d) {
    THREE.Object3D.call(this);
    this.geometry = e;
    this.material = d !== void 0 ? d : new THREE.MeshBasicMaterial({
        color: Math.random() * 16777215,
        wireframe: true
    });
    if (this.geometry) {
        this.geometry.boundingSphere || this.geometry.computeBoundingSphere();
        this.boundRadius = e.boundingSphere.radius;
        if (this.geometry.morphTargets.length) {
            this.morphTargetBase = -1;
            this.morphTargetForcedOrder = [];
            this.morphTargetInfluences = [];
            this.morphTargetDictionary = {};
            for (var f = 0; f < this.geometry.morphTargets.length; f++) {
                this.morphTargetInfluences.push(0);
                this.morphTargetDictionary[this.geometry.morphTargets[f].name] = f
            }
        }
    }
}
;
THREE.Mesh.prototype = new THREE.Object3D;
THREE.Mesh.prototype.constructor = THREE.Mesh;
THREE.Mesh.prototype.supr = THREE.Object3D.prototype;
THREE.Mesh.prototype.getMorphTargetIndexByName = function(b) {
    if (this.morphTargetDictionary[b] !== void 0) {
        return this.morphTargetDictionary[b]
    }
    if (logger) console.log("THREE.Mesh.getMorphTargetIndexByName: morph target " + b + " does not exist. Returning 0.");
    return 0
}
;
THREE.Ribbon = function(d, c) {
    THREE.Object3D.call(this);
    this.geometry = d;
    this.material = c
}
;
THREE.Ribbon.prototype = new THREE.Object3D;
THREE.Ribbon.prototype.constructor = THREE.Ribbon;
THREE.LOD = function() {
    THREE.Object3D.call(this);
    this.LODs = []
}
;
THREE.LOD.prototype = new THREE.Object3D;
THREE.LOD.prototype.constructor = THREE.LOD;
THREE.LOD.prototype.supr = THREE.Object3D.prototype;
THREE.LOD.prototype.addLevel = function(e, d) {
    d === void 0 && (d = 0);
    for (var d = Math.abs(d), f = 0; f < this.LODs.length; f++) {
        if (d < this.LODs[f].visibleAtDistance) {
            break
        }
    }
    this.LODs.splice(f, 0, {
        visibleAtDistance: d,
        object3D: e
    });
    this.add(e)
}
;
THREE.LOD.prototype.update = function(d) {
    if (this.LODs.length > 1) {
        d.matrixWorldInverse.getInverse(d.matrixWorld);
        d = d.matrixWorldInverse;
        d = -(d.elements[2] * this.matrixWorld.elements[12] + d.elements[6] * this.matrixWorld.elements[13] + d.elements[10] * this.matrixWorld.elements[14] + d.elements[14]);
        this.LODs[0].object3D.visible = true;
        for (var c = 1; c < this.LODs.length; c++) {
            if (d >= this.LODs[c].visibleAtDistance) {
                this.LODs[c - 1].object3D.visible = false;
                this.LODs[c].object3D.visible = true
            } else {
                break
            }
        }
        for (; c < this.LODs.length; c++) {
            this.LODs[c].object3D.visible = false
        }
    }
}
;
THREE.Sprite = function(b) {
    THREE.Object3D.call(this);
    this.color = b.color !== void 0 ? new THREE.Color(b.color) : new THREE.Color(16777215);
    this.map = b.map !== void 0 ? b.map : new THREE.Texture;
    this.blending = b.blending !== void 0 ? b.blending : THREE.NormalBlending;
    this.blendSrc = b.blendSrc !== void 0 ? b.blendSrc : THREE.SrcAlphaFactor;
    this.blendDst = b.blendDst !== void 0 ? b.blendDst : THREE.OneMinusSrcAlphaFactor;
    this.blendEquation = b.blendEquation !== void 0 ? b.blendEquation : THREE.AddEquation;
    this.useScreenCoordinates = b.useScreenCoordinates !== void 0 ? b.useScreenCoordinates : true;
    this.mergeWith3D = b.mergeWith3D !== void 0 ? b.mergeWith3D : !this.useScreenCoordinates;
    this.affectedByDistance = b.affectedByDistance !== void 0 ? b.affectedByDistance : !this.useScreenCoordinates;
    this.scaleByViewport = b.scaleByViewport !== void 0 ? b.scaleByViewport : !this.affectedByDistance;
    this.alignment = b.alignment instanceof THREE.Vector2 ? b.alignment : THREE.SpriteAlignment.center;
    this.rotation3d = this.rotation;
    this.rotation = 0;
    this.opacity = 1;
    this.uvOffset = new THREE.Vector2(0,0);
    this.uvScale = new THREE.Vector2(1,1)
}
;
THREE.Sprite.prototype = new THREE.Object3D;
THREE.Sprite.prototype.constructor = THREE.Sprite;
THREE.Sprite.prototype.updateMatrix = function() {
    this.matrix.setPosition(this.position);
    this.rotation3d.set(0, 0, this.rotation);
    this.matrix.setRotationFromEuler(this.rotation3d);
    if (this.scale.x !== 1 || this.scale.y !== 1) {
        this.matrix.scale(this.scale);
        this.boundRadiusScale = Math.max(this.scale.x, this.scale.y)
    }
    this.matrixWorldNeedsUpdate = true
}
;
THREE.SpriteAlignment = {};
THREE.SpriteAlignment.topLeft = new THREE.Vector2(1,-1);
THREE.SpriteAlignment.topCenter = new THREE.Vector2(0,-1);
THREE.SpriteAlignment.topRight = new THREE.Vector2(-1,-1);
THREE.SpriteAlignment.centerLeft = new THREE.Vector2(1,0);
THREE.SpriteAlignment.center = new THREE.Vector2(0,0);
THREE.SpriteAlignment.centerRight = new THREE.Vector2(-1,0);
THREE.SpriteAlignment.bottomLeft = new THREE.Vector2(1,1);
THREE.SpriteAlignment.bottomCenter = new THREE.Vector2(0,1);
THREE.SpriteAlignment.bottomRight = new THREE.Vector2(-1,1);
THREE.Scene = function() {
    THREE.Object3D.call(this);
    this.overrideMaterial = this.fog = null;
    this.matrixAutoUpdate = false;
    this.__objects = [];
    this.__lights = [];
    this.__objectsAdded = [];
    this.__objectsRemoved = []
}
;
THREE.Scene.prototype = new THREE.Object3D;
THREE.Scene.prototype.constructor = THREE.Scene;
THREE.Scene.prototype.__addObject = function(d) {
    if (d instanceof THREE.Light) {
        this.__lights.indexOf(d) === -1 && this.__lights.push(d)
    } else {
        if (!(d instanceof THREE.Camera) && this.__objects.indexOf(d) === -1) {
            this.__objects.push(d);
            this.__objectsAdded.push(d);
            var c = this.__objectsRemoved.indexOf(d);
            c !== -1 && this.__objectsRemoved.splice(c, 1)
        }
    }
    for (c = 0; c < d.children.length; c++) {
        this.__addObject(d.children[c])
    }
}
;
THREE.Scene.prototype.__removeObject = function(d) {
    if (d instanceof THREE.Light) {
        var c = this.__lights.indexOf(d);
        c !== -1 && this.__lights.splice(c, 1)
    } else {
        if (!(d instanceof THREE.Camera)) {
            c = this.__objects.indexOf(d);
            if (c !== -1) {
                this.__objects.splice(c, 1);
                this.__objectsRemoved.push(d);
                c = this.__objectsAdded.indexOf(d);
                c !== -1 && this.__objectsAdded.splice(c, 1)
            }
        }
    }
    for (c = 0; c < d.children.length; c++) {
        this.__removeObject(d.children[c])
    }
}
;
THREE.Fog = function(e, d, f) {
    this.color = new THREE.Color(e);
    this.near = d !== void 0 ? d : 1;
    this.far = f !== void 0 ? f : 1000
}
;
THREE.FogExp2 = function(d, c) {
    this.color = new THREE.Color(d);
    this.density = c !== void 0 ? c : 0.00025
}
;
THREE.ShaderChunk = {
    fog_pars_fragment: "#ifdef USE_FOG\nuniform vec3 fogColor;\n#ifdef FOG_EXP2\nuniform float fogDensity;\n#else\nuniform float fogNear;\nuniform float fogFar;\n#endif\n#endif",
    fog_fragment: "#ifdef USE_FOG\nfloat depth = gl_FragCoord.z / gl_FragCoord.w;\n#ifdef FOG_EXP2\nconst float LOG2 = 1.442695;\nfloat fogFactor = exp2( - fogDensity * fogDensity * depth * depth * LOG2 );\nfogFactor = 1.0 - clamp( fogFactor, 0.0, 1.0 );\n#else\nfloat fogFactor = smoothstep( fogNear, fogFar, depth );\n#endif\ngl_FragColor = mix( gl_FragColor, vec4( fogColor, gl_FragColor.w ), fogFactor );\n#endif",
    envmap_pars_fragment: "#ifdef USE_ENVMAP\nvarying vec3 vReflect;\nuniform float reflectivity;\nuniform samplerCube envMap;\nuniform float flipEnvMap;\nuniform int combine;\n#endif",
    envmap_fragment: "#ifdef USE_ENVMAP\n#ifdef DOUBLE_SIDED\nfloat flipNormal = ( -1.0 + 2.0 * float( gl_FrontFacing ) );\nvec4 cubeColor = textureCube( envMap, flipNormal * vec3( flipEnvMap * vReflect.x, vReflect.yz ) );\n#else\nvec4 cubeColor = textureCube( envMap, vec3( flipEnvMap * vReflect.x, vReflect.yz ) );\n#endif\n#ifdef GAMMA_INPUT\ncubeColor.xyz *= cubeColor.xyz;\n#endif\nif ( combine == 1 ) {\ngl_FragColor.xyz = mix( gl_FragColor.xyz, cubeColor.xyz, reflectivity );\n} else {\ngl_FragColor.xyz = gl_FragColor.xyz * cubeColor.xyz;\n}\n#endif",
    envmap_pars_vertex: "#ifdef USE_ENVMAP\nvarying vec3 vReflect;\nuniform float refractionRatio;\nuniform bool useRefract;\n#endif",
    envmap_vertex: "#ifdef USE_ENVMAP\nvec4 mPosition = objectMatrix * vec4( position, 1.0 );\nvec3 nWorld = mat3( objectMatrix[ 0 ].xyz, objectMatrix[ 1 ].xyz, objectMatrix[ 2 ].xyz ) * normal;\nif ( useRefract ) {\nvReflect = refract( normalize( mPosition.xyz - cameraPosition ), normalize( nWorld.xyz ), refractionRatio );\n} else {\nvReflect = reflect( normalize( mPosition.xyz - cameraPosition ), normalize( nWorld.xyz ) );\n}\n#endif",
    map_particle_pars_fragment: "#ifdef USE_MAP\nuniform sampler2D map;\n#endif",
    map_particle_fragment: "#ifdef USE_MAP\ngl_FragColor = gl_FragColor * texture2D( map, gl_PointCoord );\n#endif",
    map_pars_vertex: "#ifdef USE_MAP\nvarying vec2 vUv;\nuniform vec4 offsetRepeat;\n#endif",
    map_pars_fragment: "#ifdef USE_MAP\nvarying vec2 vUv;\nuniform sampler2D map;\n#endif",
    map_vertex: "#ifdef USE_MAP\nvUv = uv * offsetRepeat.zw + offsetRepeat.xy;\n#endif",
    map_fragment: "#ifdef USE_MAP\n#ifdef GAMMA_INPUT\nvec4 texelColor = texture2D( map, vUv );\ntexelColor.xyz *= texelColor.xyz;\ngl_FragColor = gl_FragColor * texelColor;\n#else\ngl_FragColor = gl_FragColor * texture2D( map, vUv );\n#endif\n#endif",
    lightmap_pars_fragment: "#ifdef USE_LIGHTMAP\nvarying vec2 vUv2;\nuniform sampler2D lightMap;\n#endif",
    lightmap_pars_vertex: "#ifdef USE_LIGHTMAP\nvarying vec2 vUv2;\n#endif",
    lightmap_fragment: "#ifdef USE_LIGHTMAP\ngl_FragColor = gl_FragColor * texture2D( lightMap, vUv2 );\n#endif",
    lightmap_vertex: "#ifdef USE_LIGHTMAP\nvUv2 = uv2;\n#endif",
    lights_lambert_pars_vertex: "uniform vec3 ambient;\nuniform vec3 diffuse;\nuniform vec3 emissive;\nuniform vec3 ambientLightColor;\n#if MAX_DIR_LIGHTS > 0\nuniform vec3 directionalLightColor[ MAX_DIR_LIGHTS ];\nuniform vec3 directionalLightDirection[ MAX_DIR_LIGHTS ];\n#endif\n#if MAX_POINT_LIGHTS > 0\nuniform vec3 pointLightColor[ MAX_POINT_LIGHTS ];\nuniform vec3 pointLightPosition[ MAX_POINT_LIGHTS ];\nuniform float pointLightDistance[ MAX_POINT_LIGHTS ];\n#endif\n#if MAX_SPOT_LIGHTS > 0\nuniform vec3 spotLightColor[ MAX_SPOT_LIGHTS ];\nuniform vec3 spotLightPosition[ MAX_SPOT_LIGHTS ];\nuniform vec3 spotLightDirection[ MAX_SPOT_LIGHTS ];\nuniform float spotLightDistance[ MAX_SPOT_LIGHTS ];\nuniform float spotLightAngle[ MAX_SPOT_LIGHTS ];\nuniform float spotLightExponent[ MAX_SPOT_LIGHTS ];\n#endif\n#ifdef WRAP_AROUND\nuniform vec3 wrapRGB;\n#endif",
    lights_lambert_vertex: "vLightFront = vec3( 0.0 );\n#ifdef DOUBLE_SIDED\nvLightBack = vec3( 0.0 );\n#endif\ntransformedNormal = normalize( transformedNormal );\n#if MAX_DIR_LIGHTS > 0\nfor( int i = 0; i < MAX_DIR_LIGHTS; i ++ ) {\nvec4 lDirection = viewMatrix * vec4( directionalLightDirection[ i ], 0.0 );\nvec3 dirVector = normalize( lDirection.xyz );\nfloat dotProduct = dot( transformedNormal, dirVector );\nvec3 directionalLightWeighting = vec3( max( dotProduct, 0.0 ) );\n#ifdef DOUBLE_SIDED\nvec3 directionalLightWeightingBack = vec3( max( -dotProduct, 0.0 ) );\n#ifdef WRAP_AROUND\nvec3 directionalLightWeightingHalfBack = vec3( max( -0.5 * dotProduct + 0.5, 0.0 ) );\n#endif\n#endif\n#ifdef WRAP_AROUND\nvec3 directionalLightWeightingHalf = vec3( max( 0.5 * dotProduct + 0.5, 0.0 ) );\ndirectionalLightWeighting = mix( directionalLightWeighting, directionalLightWeightingHalf, wrapRGB );\n#ifdef DOUBLE_SIDED\ndirectionalLightWeightingBack = mix( directionalLightWeightingBack, directionalLightWeightingHalfBack, wrapRGB );\n#endif\n#endif\nvLightFront += directionalLightColor[ i ] * directionalLightWeighting;\n#ifdef DOUBLE_SIDED\nvLightBack += directionalLightColor[ i ] * directionalLightWeightingBack;\n#endif\n}\n#endif\n#if MAX_POINT_LIGHTS > 0\nfor( int i = 0; i < MAX_POINT_LIGHTS; i ++ ) {\nvec4 lPosition = viewMatrix * vec4( pointLightPosition[ i ], 1.0 );\nvec3 lVector = lPosition.xyz - mvPosition.xyz;\nfloat lDistance = 1.0;\nif ( pointLightDistance[ i ] > 0.0 )\nlDistance = 1.0 - min( ( length( lVector ) / pointLightDistance[ i ] ), 1.0 );\nlVector = normalize( lVector );\nfloat dotProduct = dot( transformedNormal, lVector );\nvec3 pointLightWeighting = vec3( max( dotProduct, 0.0 ) );\n#ifdef DOUBLE_SIDED\nvec3 pointLightWeightingBack = vec3( max( -dotProduct, 0.0 ) );\n#ifdef WRAP_AROUND\nvec3 pointLightWeightingHalfBack = vec3( max( -0.5 * dotProduct + 0.5, 0.0 ) );\n#endif\n#endif\n#ifdef WRAP_AROUND\nvec3 pointLightWeightingHalf = vec3( max( 0.5 * dotProduct + 0.5, 0.0 ) );\npointLightWeighting = mix( pointLightWeighting, pointLightWeightingHalf, wrapRGB );\n#ifdef DOUBLE_SIDED\npointLightWeightingBack = mix( pointLightWeightingBack, pointLightWeightingHalfBack, wrapRGB );\n#endif\n#endif\nvLightFront += pointLightColor[ i ] * pointLightWeighting * lDistance;\n#ifdef DOUBLE_SIDED\nvLightBack += pointLightColor[ i ] * pointLightWeightingBack * lDistance;\n#endif\n}\n#endif\n#if MAX_SPOT_LIGHTS > 0\nfor( int i = 0; i < MAX_SPOT_LIGHTS; i ++ ) {\nvec4 lPosition = viewMatrix * vec4( spotLightPosition[ i ], 1.0 );\nvec3 lVector = lPosition.xyz - mvPosition.xyz;\nlVector = normalize( lVector );\nfloat spotEffect = dot( spotLightDirection[ i ], normalize( spotLightPosition[ i ] - mPosition.xyz ) );\nif ( spotEffect > spotLightAngle[ i ] ) {\nspotEffect = pow( spotEffect, spotLightExponent[ i ] );\nfloat lDistance = 1.0;\nif ( spotLightDistance[ i ] > 0.0 )\nlDistance = 1.0 - min( ( length( lVector ) / spotLightDistance[ i ] ), 1.0 );\nfloat dotProduct = dot( transformedNormal, lVector );\nvec3 spotLightWeighting = vec3( max( dotProduct, 0.0 ) );\n#ifdef DOUBLE_SIDED\nvec3 spotLightWeightingBack = vec3( max( -dotProduct, 0.0 ) );\n#ifdef WRAP_AROUND\nvec3 spotLightWeightingHalfBack = vec3( max( -0.5 * dotProduct + 0.5, 0.0 ) );\n#endif\n#endif\n#ifdef WRAP_AROUND\nvec3 spotLightWeightingHalf = vec3( max( 0.5 * dotProduct + 0.5, 0.0 ) );\nspotLightWeighting = mix( spotLightWeighting, spotLightWeightingHalf, wrapRGB );\n#ifdef DOUBLE_SIDED\nspotLightWeightingBack = mix( spotLightWeightingBack, spotLightWeightingHalfBack, wrapRGB );\n#endif\n#endif\nvLightFront += spotLightColor[ i ] * spotLightWeighting * lDistance * spotEffect;\n#ifdef DOUBLE_SIDED\nvLightBack += spotLightColor[ i ] * spotLightWeightingBack * lDistance * spotEffect;\n#endif\n}\n}\n#endif\nvLightFront = vLightFront * diffuse + ambient * ambientLightColor + emissive;\n#ifdef DOUBLE_SIDED\nvLightBack = vLightBack * diffuse + ambient * ambientLightColor + emissive;\n#endif",
    lights_phong_pars_vertex: "#ifndef PHONG_PER_PIXEL\n#if MAX_POINT_LIGHTS > 0\nuniform vec3 pointLightPosition[ MAX_POINT_LIGHTS ];\nuniform float pointLightDistance[ MAX_POINT_LIGHTS ];\nvarying vec4 vPointLight[ MAX_POINT_LIGHTS ];\n#endif\n#if MAX_SPOT_LIGHTS > 0\nuniform vec3 spotLightPosition[ MAX_SPOT_LIGHTS ];\nuniform float spotLightDistance[ MAX_SPOT_LIGHTS ];\nvarying vec4 vSpotLight[ MAX_SPOT_LIGHTS ];\n#endif\n#endif\n#if MAX_SPOT_LIGHTS > 0\nvarying vec3 vWorldPosition;\n#endif",
    lights_phong_vertex: "#ifndef PHONG_PER_PIXEL\n#if MAX_POINT_LIGHTS > 0\nfor( int i = 0; i < MAX_POINT_LIGHTS; i ++ ) {\nvec4 lPosition = viewMatrix * vec4( pointLightPosition[ i ], 1.0 );\nvec3 lVector = lPosition.xyz - mvPosition.xyz;\nfloat lDistance = 1.0;\nif ( pointLightDistance[ i ] > 0.0 )\nlDistance = 1.0 - min( ( length( lVector ) / pointLightDistance[ i ] ), 1.0 );\nvPointLight[ i ] = vec4( lVector, lDistance );\n}\n#endif\n#if MAX_SPOT_LIGHTS > 0\nfor( int i = 0; i < MAX_SPOT_LIGHTS; i ++ ) {\nvec4 lPosition = viewMatrix * vec4( spotLightPosition[ i ], 1.0 );\nvec3 lVector = lPosition.xyz - mvPosition.xyz;\nfloat lDistance = 1.0;\nif ( spotLightDistance[ i ] > 0.0 )\nlDistance = 1.0 - min( ( length( lVector ) / spotLightDistance[ i ] ), 1.0 );\nvSpotLight[ i ] = vec4( lVector, lDistance );\n}\n#endif\n#endif\n#if MAX_SPOT_LIGHTS > 0\nvWorldPosition = mPosition.xyz;\n#endif",
    lights_phong_pars_fragment: "uniform vec3 ambientLightColor;\n#if MAX_DIR_LIGHTS > 0\nuniform vec3 directionalLightColor[ MAX_DIR_LIGHTS ];\nuniform vec3 directionalLightDirection[ MAX_DIR_LIGHTS ];\n#endif\n#if MAX_POINT_LIGHTS > 0\nuniform vec3 pointLightColor[ MAX_POINT_LIGHTS ];\n#ifdef PHONG_PER_PIXEL\nuniform vec3 pointLightPosition[ MAX_POINT_LIGHTS ];\nuniform float pointLightDistance[ MAX_POINT_LIGHTS ];\n#else\nvarying vec4 vPointLight[ MAX_POINT_LIGHTS ];\n#endif\n#endif\n#if MAX_SPOT_LIGHTS > 0\nuniform vec3 spotLightColor[ MAX_SPOT_LIGHTS ];\nuniform vec3 spotLightPosition[ MAX_SPOT_LIGHTS ];\nuniform vec3 spotLightDirection[ MAX_SPOT_LIGHTS ];\nuniform float spotLightAngle[ MAX_SPOT_LIGHTS ];\nuniform float spotLightExponent[ MAX_SPOT_LIGHTS ];\n#ifdef PHONG_PER_PIXEL\nuniform float spotLightDistance[ MAX_SPOT_LIGHTS ];\n#else\nvarying vec4 vSpotLight[ MAX_SPOT_LIGHTS ];\n#endif\nvarying vec3 vWorldPosition;\n#endif\n#ifdef WRAP_AROUND\nuniform vec3 wrapRGB;\n#endif\nvarying vec3 vViewPosition;\nvarying vec3 vNormal;",
    lights_phong_fragment: "vec3 normal = normalize( vNormal );\nvec3 viewPosition = normalize( vViewPosition );\n#ifdef DOUBLE_SIDED\nnormal = normal * ( -1.0 + 2.0 * float( gl_FrontFacing ) );\n#endif\n#if MAX_POINT_LIGHTS > 0\nvec3 pointDiffuse  = vec3( 0.0 );\nvec3 pointSpecular = vec3( 0.0 );\nfor ( int i = 0; i < MAX_POINT_LIGHTS; i ++ ) {\n#ifdef PHONG_PER_PIXEL\nvec4 lPosition = viewMatrix * vec4( pointLightPosition[ i ], 1.0 );\nvec3 lVector = lPosition.xyz + vViewPosition.xyz;\nfloat lDistance = 1.0;\nif ( pointLightDistance[ i ] > 0.0 )\nlDistance = 1.0 - min( ( length( lVector ) / pointLightDistance[ i ] ), 1.0 );\nlVector = normalize( lVector );\n#else\nvec3 lVector = normalize( vPointLight[ i ].xyz );\nfloat lDistance = vPointLight[ i ].w;\n#endif\nfloat dotProduct = dot( normal, lVector );\n#ifdef WRAP_AROUND\nfloat pointDiffuseWeightFull = max( dotProduct, 0.0 );\nfloat pointDiffuseWeightHalf = max( 0.5 * dotProduct + 0.5, 0.0 );\nvec3 pointDiffuseWeight = mix( vec3 ( pointDiffuseWeightFull ), vec3( pointDiffuseWeightHalf ), wrapRGB );\n#else\nfloat pointDiffuseWeight = max( dotProduct, 0.0 );\n#endif\npointDiffuse  += diffuse * pointLightColor[ i ] * pointDiffuseWeight * lDistance;\nvec3 pointHalfVector = normalize( lVector + viewPosition );\nfloat pointDotNormalHalf = max( dot( normal, pointHalfVector ), 0.0 );\nfloat pointSpecularWeight = max( pow( pointDotNormalHalf, shininess ), 0.0 );\n#ifdef PHYSICALLY_BASED_SHADING\nfloat specularNormalization = ( shininess + 2.0001 ) / 8.0;\nvec3 schlick = specular + vec3( 1.0 - specular ) * pow( 1.0 - dot( lVector, pointHalfVector ), 5.0 );\npointSpecular += schlick * pointLightColor[ i ] * pointSpecularWeight * pointDiffuseWeight * lDistance * specularNormalization;\n#else\npointSpecular += specular * pointLightColor[ i ] * pointSpecularWeight * pointDiffuseWeight * lDistance;\n#endif\n}\n#endif\n#if MAX_SPOT_LIGHTS > 0\nvec3 spotDiffuse  = vec3( 0.0 );\nvec3 spotSpecular = vec3( 0.0 );\nfor ( int i = 0; i < MAX_SPOT_LIGHTS; i ++ ) {\n#ifdef PHONG_PER_PIXEL\nvec4 lPosition = viewMatrix * vec4( spotLightPosition[ i ], 1.0 );\nvec3 lVector = lPosition.xyz + vViewPosition.xyz;\nfloat lDistance = 1.0;\nif ( spotLightDistance[ i ] > 0.0 )\nlDistance = 1.0 - min( ( length( lVector ) / spotLightDistance[ i ] ), 1.0 );\nlVector = normalize( lVector );\n#else\nvec3 lVector = normalize( vSpotLight[ i ].xyz );\nfloat lDistance = vSpotLight[ i ].w;\n#endif\nfloat spotEffect = dot( spotLightDirection[ i ], normalize( spotLightPosition[ i ] - vWorldPosition ) );\nif ( spotEffect > spotLightAngle[ i ] ) {\nspotEffect = pow( spotEffect, spotLightExponent[ i ] );\nfloat dotProduct = dot( normal, lVector );\n#ifdef WRAP_AROUND\nfloat spotDiffuseWeightFull = max( dotProduct, 0.0 );\nfloat spotDiffuseWeightHalf = max( 0.5 * dotProduct + 0.5, 0.0 );\nvec3 spotDiffuseWeight = mix( vec3 ( spotDiffuseWeightFull ), vec3( spotDiffuseWeightHalf ), wrapRGB );\n#else\nfloat spotDiffuseWeight = max( dotProduct, 0.0 );\n#endif\nspotDiffuse += diffuse * spotLightColor[ i ] * spotDiffuseWeight * lDistance * spotEffect;\nvec3 spotHalfVector = normalize( lVector + viewPosition );\nfloat spotDotNormalHalf = max( dot( normal, spotHalfVector ), 0.0 );\nfloat spotSpecularWeight = max( pow( spotDotNormalHalf, shininess ), 0.0 );\n#ifdef PHYSICALLY_BASED_SHADING\nfloat specularNormalization = ( shininess + 2.0001 ) / 8.0;\nvec3 schlick = specular + vec3( 1.0 - specular ) * pow( 1.0 - dot( lVector, spotHalfVector ), 5.0 );\nspotSpecular += schlick * spotLightColor[ i ] * spotSpecularWeight * spotDiffuseWeight * lDistance * specularNormalization * spotEffect;\n#else\nspotSpecular += specular * spotLightColor[ i ] * spotSpecularWeight * spotDiffuseWeight * lDistance * spotEffect;\n#endif\n}\n}\n#endif\n#if MAX_DIR_LIGHTS > 0\nvec3 dirDiffuse  = vec3( 0.0 );\nvec3 dirSpecular = vec3( 0.0 );\nfor( int i = 0; i < MAX_DIR_LIGHTS; i ++ ) {\nvec4 lDirection = viewMatrix * vec4( directionalLightDirection[ i ], 0.0 );\nvec3 dirVector = normalize( lDirection.xyz );\nfloat dotProduct = dot( normal, dirVector );\n#ifdef WRAP_AROUND\nfloat dirDiffuseWeightFull = max( dotProduct, 0.0 );\nfloat dirDiffuseWeightHalf = max( 0.5 * dotProduct + 0.5, 0.0 );\nvec3 dirDiffuseWeight = mix( vec3( dirDiffuseWeightFull ), vec3( dirDiffuseWeightHalf ), wrapRGB );\n#else\nfloat dirDiffuseWeight = max( dotProduct, 0.0 );\n#endif\ndirDiffuse  += diffuse * directionalLightColor[ i ] * dirDiffuseWeight;\nvec3 dirHalfVector = normalize( dirVector + viewPosition );\nfloat dirDotNormalHalf = max( dot( normal, dirHalfVector ), 0.0 );\nfloat dirSpecularWeight = max( pow( dirDotNormalHalf, shininess ), 0.0 );\n#ifdef PHYSICALLY_BASED_SHADING\nfloat specularNormalization = ( shininess + 2.0001 ) / 8.0;\nvec3 schlick = specular + vec3( 1.0 - specular ) * pow( 1.0 - dot( dirVector, dirHalfVector ), 5.0 );\ndirSpecular += schlick * directionalLightColor[ i ] * dirSpecularWeight * dirDiffuseWeight * specularNormalization;\n#else\ndirSpecular += specular * directionalLightColor[ i ] * dirSpecularWeight * dirDiffuseWeight;\n#endif\n}\n#endif\nvec3 totalDiffuse = vec3( 0.0 );\nvec3 totalSpecular = vec3( 0.0 );\n#if MAX_DIR_LIGHTS > 0\ntotalDiffuse += dirDiffuse;\ntotalSpecular += dirSpecular;\n#endif\n#if MAX_POINT_LIGHTS > 0\ntotalDiffuse += pointDiffuse;\ntotalSpecular += pointSpecular;\n#endif\n#if MAX_SPOT_LIGHTS > 0\ntotalDiffuse += spotDiffuse;\ntotalSpecular += spotSpecular;\n#endif\n#ifdef METAL\ngl_FragColor.xyz = gl_FragColor.xyz * ( emissive + totalDiffuse + ambientLightColor * ambient + totalSpecular );\n#else\ngl_FragColor.xyz = gl_FragColor.xyz * ( emissive + totalDiffuse + ambientLightColor * ambient ) + totalSpecular;\n#endif",
    color_pars_fragment: "#ifdef USE_COLOR\nvarying vec3 vColor;\n#endif",
    color_fragment: "#ifdef USE_COLOR\ngl_FragColor = gl_FragColor * vec4( vColor, opacity );\n#endif",
    color_pars_vertex: "#ifdef USE_COLOR\nvarying vec3 vColor;\n#endif",
    color_vertex: "#ifdef USE_COLOR\n#ifdef GAMMA_INPUT\nvColor = color * color;\n#else\nvColor = color;\n#endif\n#endif",
    skinning_pars_vertex: "#ifdef USE_SKINNING\nuniform mat4 boneGlobalMatrices[ MAX_BONES ];\n#endif",
    skinning_vertex: "#ifdef USE_SKINNING\ngl_Position  = ( boneGlobalMatrices[ int( skinIndex.x ) ] * skinVertexA ) * skinWeight.x;\ngl_Position += ( boneGlobalMatrices[ int( skinIndex.y ) ] * skinVertexB ) * skinWeight.y;\ngl_Position  = projectionMatrix * modelViewMatrix * gl_Position;\n#endif",
    morphtarget_pars_vertex: "#ifdef USE_MORPHTARGETS\n#ifndef USE_MORPHNORMALS\nuniform float morphTargetInfluences[ 8 ];\n#else\nuniform float morphTargetInfluences[ 4 ];\n#endif\n#endif",
    morphtarget_vertex: "#ifdef USE_MORPHTARGETS\nvec3 morphed = vec3( 0.0 );\nmorphed += ( morphTarget0 - position ) * morphTargetInfluences[ 0 ];\nmorphed += ( morphTarget1 - position ) * morphTargetInfluences[ 1 ];\nmorphed += ( morphTarget2 - position ) * morphTargetInfluences[ 2 ];\nmorphed += ( morphTarget3 - position ) * morphTargetInfluences[ 3 ];\n#ifndef USE_MORPHNORMALS\nmorphed += ( morphTarget4 - position ) * morphTargetInfluences[ 4 ];\nmorphed += ( morphTarget5 - position ) * morphTargetInfluences[ 5 ];\nmorphed += ( morphTarget6 - position ) * morphTargetInfluences[ 6 ];\nmorphed += ( morphTarget7 - position ) * morphTargetInfluences[ 7 ];\n#endif\nmorphed += position;\ngl_Position = projectionMatrix * modelViewMatrix * vec4( morphed, 1.0 );\n#endif",
    default_vertex: "#ifndef USE_MORPHTARGETS\n#ifndef USE_SKINNING\ngl_Position = projectionMatrix * mvPosition;\n#endif\n#endif",
    morphnormal_vertex: "#ifdef USE_MORPHNORMALS\nvec3 morphedNormal = vec3( 0.0 );\nmorphedNormal +=  ( morphNormal0 - normal ) * morphTargetInfluences[ 0 ];\nmorphedNormal +=  ( morphNormal1 - normal ) * morphTargetInfluences[ 1 ];\nmorphedNormal +=  ( morphNormal2 - normal ) * morphTargetInfluences[ 2 ];\nmorphedNormal +=  ( morphNormal3 - normal ) * morphTargetInfluences[ 3 ];\nmorphedNormal += normal;\nvec3 transformedNormal = normalMatrix * morphedNormal;\n#else\nvec3 transformedNormal = normalMatrix * normal;\n#endif",
    shadowmap_pars_fragment: "#ifdef USE_SHADOWMAP\nuniform sampler2D shadowMap[ MAX_SHADOWS ];\nuniform vec2 shadowMapSize[ MAX_SHADOWS ];\nuniform float shadowDarkness[ MAX_SHADOWS ];\nuniform float shadowBias[ MAX_SHADOWS ];\nvarying vec4 vShadowCoord[ MAX_SHADOWS ];\nfloat unpackDepth( const in vec4 rgba_depth ) {\nconst vec4 bit_shift = vec4( 1.0 / ( 256.0 * 256.0 * 256.0 ), 1.0 / ( 256.0 * 256.0 ), 1.0 / 256.0, 1.0 );\nfloat depth = dot( rgba_depth, bit_shift );\nreturn depth;\n}\n#endif",
    shadowmap_fragment: "#ifdef USE_SHADOWMAP\n#ifdef SHADOWMAP_DEBUG\nvec3 frustumColors[3];\nfrustumColors[0] = vec3( 1.0, 0.5, 0.0 );\nfrustumColors[1] = vec3( 0.0, 1.0, 0.8 );\nfrustumColors[2] = vec3( 0.0, 0.5, 1.0 );\n#endif\n#ifdef SHADOWMAP_CASCADE\nint inFrustumCount = 0;\n#endif\nfloat fDepth;\nvec3 shadowColor = vec3( 1.0 );\nfor( int i = 0; i < MAX_SHADOWS; i ++ ) {\nvec3 shadowCoord = vShadowCoord[ i ].xyz / vShadowCoord[ i ].w;\nbvec4 inFrustumVec = bvec4 ( shadowCoord.x >= 0.0, shadowCoord.x <= 1.0, shadowCoord.y >= 0.0, shadowCoord.y <= 1.0 );\nbool inFrustum = all( inFrustumVec );\n#ifdef SHADOWMAP_CASCADE\ninFrustumCount += int( inFrustum );\nbvec3 frustumTestVec = bvec3( inFrustum, inFrustumCount == 1, shadowCoord.z <= 1.0 );\n#else\nbvec2 frustumTestVec = bvec2( inFrustum, shadowCoord.z <= 1.0 );\n#endif\nbool frustumTest = all( frustumTestVec );\nif ( frustumTest ) {\nshadowCoord.z += shadowBias[ i ];\n#ifdef SHADOWMAP_SOFT\nfloat shadow = 0.0;\nconst float shadowDelta = 1.0 / 9.0;\nfloat xPixelOffset = 1.0 / shadowMapSize[ i ].x;\nfloat yPixelOffset = 1.0 / shadowMapSize[ i ].y;\nfloat dx0 = -1.25 * xPixelOffset;\nfloat dy0 = -1.25 * yPixelOffset;\nfloat dx1 = 1.25 * xPixelOffset;\nfloat dy1 = 1.25 * yPixelOffset;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy + vec2( dx0, dy0 ) ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy + vec2( 0.0, dy0 ) ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy + vec2( dx1, dy0 ) ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy + vec2( dx0, 0.0 ) ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy + vec2( dx1, 0.0 ) ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy + vec2( dx0, dy1 ) ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy + vec2( 0.0, dy1 ) ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nfDepth = unpackDepth( texture2D( shadowMap[ i ], shadowCoord.xy + vec2( dx1, dy1 ) ) );\nif ( fDepth < shadowCoord.z ) shadow += shadowDelta;\nshadowColor = shadowColor * vec3( ( 1.0 - shadowDarkness[ i ] * shadow ) );\n#else\nvec4 rgbaDepth = texture2D( shadowMap[ i ], shadowCoord.xy );\nfloat fDepth = unpackDepth( rgbaDepth );\nif ( fDepth < shadowCoord.z )\nshadowColor = shadowColor * vec3( 1.0 - shadowDarkness[ i ] );\n#endif\n}\n#ifdef SHADOWMAP_DEBUG\n#ifdef SHADOWMAP_CASCADE\nif ( inFrustum && inFrustumCount == 1 ) gl_FragColor.xyz *= frustumColors[ i ];\n#else\nif ( inFrustum ) gl_FragColor.xyz *= frustumColors[ i ];\n#endif\n#endif\n}\n#ifdef GAMMA_OUTPUT\nshadowColor *= shadowColor;\n#endif\ngl_FragColor.xyz = gl_FragColor.xyz * shadowColor;\n#endif",
    shadowmap_pars_vertex: "#ifdef USE_SHADOWMAP\nvarying vec4 vShadowCoord[ MAX_SHADOWS ];\nuniform mat4 shadowMatrix[ MAX_SHADOWS ];\n#endif",
    shadowmap_vertex: "#ifdef USE_SHADOWMAP\nfor( int i = 0; i < MAX_SHADOWS; i ++ ) {\n#ifdef USE_MORPHTARGETS\nvShadowCoord[ i ] = shadowMatrix[ i ] * objectMatrix * vec4( morphed, 1.0 );\n#else\nvShadowCoord[ i ] = shadowMatrix[ i ] * objectMatrix * vec4( position, 1.0 );\n#endif\n}\n#endif",
    alphatest_fragment: "#ifdef ALPHATEST\nif ( gl_FragColor.a < ALPHATEST ) discard;\n#endif",
    linear_to_gamma_fragment: "#ifdef GAMMA_OUTPUT\ngl_FragColor.xyz = sqrt( gl_FragColor.xyz );\n#endif"
};
THREE.UniformsUtils = {
    merge: function(g) {
        var f, j, i, h = {};
        for (f = 0; f < g.length; f++) {
            i = this.clone(g[f]);
            for (j in i) {
                h[j] = i[j]
            }
        }
        return h
    },
    clone: function(g) {
        var f, j, i, h = {};
        for (f in g) {
            h[f] = {};
            for (j in g[f]) {
                i = g[f][j];
                h[f][j] = i instanceof THREE.Color || i instanceof THREE.Vector2 || i instanceof THREE.Vector3 || i instanceof THREE.Vector4 || i instanceof THREE.Matrix4 || i instanceof THREE.Texture ? i.clone() : i instanceof Array ? i.slice() : i
            }
        }
        return h
    }
};
THREE.UniformsLib = {
    common: {
        diffuse: {
            type: "c",
            value: new THREE.Color(15658734)
        },
        opacity: {
            type: "f",
            value: 1
        },
        map: {
            type: "t",
            value: 0,
            texture: null
        },
        offsetRepeat: {
            type: "v4",
            value: new THREE.Vector4(0,0,1,1)
        },
        lightMap: {
            type: "t",
            value: 2,
            texture: null
        },
        envMap: {
            type: "t",
            value: 1,
            texture: null
        },
        flipEnvMap: {
            type: "f",
            value: -1
        },
        useRefract: {
            type: "i",
            value: 0
        },
        reflectivity: {
            type: "f",
            value: 1
        },
        refractionRatio: {
            type: "f",
            value: 0.98
        },
        combine: {
            type: "i",
            value: 0
        },
        morphTargetInfluences: {
            type: "f",
            value: 0
        }
    },
    fog: {
        fogDensity: {
            type: "f",
            value: 0.00025
        },
        fogNear: {
            type: "f",
            value: 1
        },
        fogFar: {
            type: "f",
            value: 2000
        },
        fogColor: {
            type: "c",
            value: new THREE.Color(16777215)
        }
    },
    lights: {
        ambientLightColor: {
            type: "fv",
            value: []
        },
        directionalLightDirection: {
            type: "fv",
            value: []
        },
        directionalLightColor: {
            type: "fv",
            value: []
        },
        pointLightColor: {
            type: "fv",
            value: []
        },
        pointLightPosition: {
            type: "fv",
            value: []
        },
        pointLightDistance: {
            type: "fv1",
            value: []
        },
        spotLightColor: {
            type: "fv",
            value: []
        },
        spotLightPosition: {
            type: "fv",
            value: []
        },
        spotLightDirection: {
            type: "fv",
            value: []
        },
        spotLightDistance: {
            type: "fv1",
            value: []
        },
        spotLightAngle: {
            type: "fv1",
            value: []
        },
        spotLightExponent: {
            type: "fv1",
            value: []
        }
    },
    particle: {
        psColor: {
            type: "c",
            value: new THREE.Color(15658734)
        },
        opacity: {
            type: "f",
            value: 1
        },
        size: {
            type: "f",
            value: 1
        },
        scale: {
            type: "f",
            value: 1
        },
        map: {
            type: "t",
            value: 0,
            texture: null
        },
        fogDensity: {
            type: "f",
            value: 0.00025
        },
        fogNear: {
            type: "f",
            value: 1
        },
        fogFar: {
            type: "f",
            value: 2000
        },
        fogColor: {
            type: "c",
            value: new THREE.Color(16777215)
        }
    },
    shadowmap: {
        shadowMap: {
            type: "tv",
            value: 6,
            texture: []
        },
        shadowMapSize: {
            type: "v2v",
            value: []
        },
        shadowBias: {
            type: "fv1",
            value: []
        },
        shadowDarkness: {
            type: "fv1",
            value: []
        },
        shadowMatrix: {
            type: "m4v",
            value: []
        }
    }
};
THREE.ShaderLib = {
    depth: {
        uniforms: {
            mNear: {
                type: "f",
                value: 1
            },
            mFar: {
                type: "f",
                value: 2000
            },
            opacity: {
                type: "f",
                value: 1
            }
        },
        vertexShader: "void main() {\ngl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n}",
        fragmentShader: "uniform float mNear;\nuniform float mFar;\nuniform float opacity;\nvoid main() {\nfloat depth = gl_FragCoord.z / gl_FragCoord.w;\nfloat color = 1.0 - smoothstep( mNear, mFar, depth );\ngl_FragColor = vec4( vec3( color ), opacity );\n}"
    },
    normal: {
        uniforms: {
            opacity: {
                type: "f",
                value: 1
            }
        },
        vertexShader: "varying vec3 vNormal;\nvoid main() {\nvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\nvNormal = normalMatrix * normal;\ngl_Position = projectionMatrix * mvPosition;\n}",
        fragmentShader: "uniform float opacity;\nvarying vec3 vNormal;\nvoid main() {\ngl_FragColor = vec4( 0.5 * normalize( vNormal ) + 0.5, opacity );\n}"
    },
    basic: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.fog, THREE.UniformsLib.shadowmap]),
        vertexShader: [THREE.ShaderChunk.map_pars_vertex, THREE.ShaderChunk.lightmap_pars_vertex, THREE.ShaderChunk.envmap_pars_vertex, THREE.ShaderChunk.color_pars_vertex, THREE.ShaderChunk.skinning_pars_vertex, THREE.ShaderChunk.morphtarget_pars_vertex, THREE.ShaderChunk.shadowmap_pars_vertex, "void main() {\nvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );", THREE.ShaderChunk.map_vertex, THREE.ShaderChunk.lightmap_vertex, THREE.ShaderChunk.envmap_vertex, THREE.ShaderChunk.color_vertex, THREE.ShaderChunk.skinning_vertex, THREE.ShaderChunk.morphtarget_vertex, THREE.ShaderChunk.default_vertex, THREE.ShaderChunk.shadowmap_vertex, "}"].join("\n"),
        fragmentShader: ["uniform vec3 diffuse;\nuniform float opacity;", THREE.ShaderChunk.color_pars_fragment, THREE.ShaderChunk.map_pars_fragment, THREE.ShaderChunk.lightmap_pars_fragment, THREE.ShaderChunk.envmap_pars_fragment, THREE.ShaderChunk.fog_pars_fragment, THREE.ShaderChunk.shadowmap_pars_fragment, "void main() {\ngl_FragColor = vec4( diffuse, opacity );", THREE.ShaderChunk.map_fragment, THREE.ShaderChunk.alphatest_fragment, THREE.ShaderChunk.lightmap_fragment, THREE.ShaderChunk.color_fragment, THREE.ShaderChunk.envmap_fragment, THREE.ShaderChunk.shadowmap_fragment, THREE.ShaderChunk.linear_to_gamma_fragment, THREE.ShaderChunk.fog_fragment, "}"].join("\n")
    },
    lambert: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.fog, THREE.UniformsLib.lights, THREE.UniformsLib.shadowmap, {
            ambient: {
                type: "c",
                value: new THREE.Color(16777215)
            },
            emissive: {
                type: "c",
                value: new THREE.Color(0)
            },
            wrapRGB: {
                type: "v3",
                value: new THREE.Vector3(1,1,1)
            }
        }]),
        vertexShader: ["varying vec3 vLightFront;\n#ifdef DOUBLE_SIDED\nvarying vec3 vLightBack;\n#endif", THREE.ShaderChunk.map_pars_vertex, THREE.ShaderChunk.lightmap_pars_vertex, THREE.ShaderChunk.envmap_pars_vertex, THREE.ShaderChunk.lights_lambert_pars_vertex, THREE.ShaderChunk.color_pars_vertex, THREE.ShaderChunk.skinning_pars_vertex, THREE.ShaderChunk.morphtarget_pars_vertex, THREE.ShaderChunk.shadowmap_pars_vertex, "void main() {\nvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );", THREE.ShaderChunk.map_vertex, THREE.ShaderChunk.lightmap_vertex, THREE.ShaderChunk.envmap_vertex, THREE.ShaderChunk.color_vertex, THREE.ShaderChunk.morphnormal_vertex, "#ifndef USE_ENVMAP\nvec4 mPosition = objectMatrix * vec4( position, 1.0 );\n#endif", THREE.ShaderChunk.lights_lambert_vertex, THREE.ShaderChunk.skinning_vertex, THREE.ShaderChunk.morphtarget_vertex, THREE.ShaderChunk.default_vertex, THREE.ShaderChunk.shadowmap_vertex, "}"].join("\n"),
        fragmentShader: ["uniform float opacity;\nvarying vec3 vLightFront;\n#ifdef DOUBLE_SIDED\nvarying vec3 vLightBack;\n#endif", THREE.ShaderChunk.color_pars_fragment, THREE.ShaderChunk.map_pars_fragment, THREE.ShaderChunk.lightmap_pars_fragment, THREE.ShaderChunk.envmap_pars_fragment, THREE.ShaderChunk.fog_pars_fragment, THREE.ShaderChunk.shadowmap_pars_fragment, "void main() {\ngl_FragColor = vec4( vec3 ( 1.0 ), opacity );", THREE.ShaderChunk.map_fragment, THREE.ShaderChunk.alphatest_fragment, "#ifdef DOUBLE_SIDED\nif ( gl_FrontFacing )\ngl_FragColor.xyz *= vLightFront;\nelse\ngl_FragColor.xyz *= vLightBack;\n#else\ngl_FragColor.xyz *= vLightFront;\n#endif", THREE.ShaderChunk.lightmap_fragment, THREE.ShaderChunk.color_fragment, THREE.ShaderChunk.envmap_fragment, THREE.ShaderChunk.shadowmap_fragment, THREE.ShaderChunk.linear_to_gamma_fragment, THREE.ShaderChunk.fog_fragment, "}"].join("\n")
    },
    phong: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.common, THREE.UniformsLib.fog, THREE.UniformsLib.lights, THREE.UniformsLib.shadowmap, {
            ambient: {
                type: "c",
                value: new THREE.Color(16777215)
            },
            emissive: {
                type: "c",
                value: new THREE.Color(0)
            },
            specular: {
                type: "c",
                value: new THREE.Color(1118481)
            },
            shininess: {
                type: "f",
                value: 30
            },
            wrapRGB: {
                type: "v3",
                value: new THREE.Vector3(1,1,1)
            }
        }]),
        vertexShader: ["varying vec3 vViewPosition;\nvarying vec3 vNormal;", THREE.ShaderChunk.map_pars_vertex, THREE.ShaderChunk.lightmap_pars_vertex, THREE.ShaderChunk.envmap_pars_vertex, THREE.ShaderChunk.lights_phong_pars_vertex, THREE.ShaderChunk.color_pars_vertex, THREE.ShaderChunk.skinning_pars_vertex, THREE.ShaderChunk.morphtarget_pars_vertex, THREE.ShaderChunk.shadowmap_pars_vertex, "void main() {\nvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );", THREE.ShaderChunk.map_vertex, THREE.ShaderChunk.lightmap_vertex, THREE.ShaderChunk.envmap_vertex, THREE.ShaderChunk.color_vertex, "#ifndef USE_ENVMAP\nvec4 mPosition = objectMatrix * vec4( position, 1.0 );\n#endif\nvViewPosition = -mvPosition.xyz;", THREE.ShaderChunk.morphnormal_vertex, "vNormal = transformedNormal;", THREE.ShaderChunk.lights_phong_vertex, THREE.ShaderChunk.skinning_vertex, THREE.ShaderChunk.morphtarget_vertex, THREE.ShaderChunk.default_vertex, THREE.ShaderChunk.shadowmap_vertex, "}"].join("\n"),
        fragmentShader: ["uniform vec3 diffuse;\nuniform float opacity;\nuniform vec3 ambient;\nuniform vec3 emissive;\nuniform vec3 specular;\nuniform float shininess;", THREE.ShaderChunk.color_pars_fragment, THREE.ShaderChunk.map_pars_fragment, THREE.ShaderChunk.lightmap_pars_fragment, THREE.ShaderChunk.envmap_pars_fragment, THREE.ShaderChunk.fog_pars_fragment, THREE.ShaderChunk.lights_phong_pars_fragment, THREE.ShaderChunk.shadowmap_pars_fragment, "void main() {\ngl_FragColor = vec4( vec3 ( 1.0 ), opacity );", THREE.ShaderChunk.map_fragment, THREE.ShaderChunk.alphatest_fragment, THREE.ShaderChunk.lights_phong_fragment, THREE.ShaderChunk.lightmap_fragment, THREE.ShaderChunk.color_fragment, THREE.ShaderChunk.envmap_fragment, THREE.ShaderChunk.shadowmap_fragment, THREE.ShaderChunk.linear_to_gamma_fragment, THREE.ShaderChunk.fog_fragment, "}"].join("\n")
    },
    particle_basic: {
        uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.particle, THREE.UniformsLib.shadowmap]),
        vertexShader: ["uniform float size;\nuniform float scale;", THREE.ShaderChunk.color_pars_vertex, THREE.ShaderChunk.shadowmap_pars_vertex, "void main() {", THREE.ShaderChunk.color_vertex, "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n#ifdef USE_SIZEATTENUATION\ngl_PointSize = size * ( scale / length( mvPosition.xyz ) );\n#else\ngl_PointSize = size;\n#endif\ngl_Position = projectionMatrix * mvPosition;", THREE.ShaderChunk.shadowmap_vertex, "}"].join("\n"),
        fragmentShader: ["uniform vec3 psColor;\nuniform float opacity;", THREE.ShaderChunk.color_pars_fragment, THREE.ShaderChunk.map_particle_pars_fragment, THREE.ShaderChunk.fog_pars_fragment, THREE.ShaderChunk.shadowmap_pars_fragment, "void main() {\ngl_FragColor = vec4( psColor, opacity );", THREE.ShaderChunk.map_particle_fragment, THREE.ShaderChunk.alphatest_fragment, THREE.ShaderChunk.color_fragment, THREE.ShaderChunk.shadowmap_fragment, THREE.ShaderChunk.fog_fragment, "}"].join("\n")
    },
    depthRGBA: {
        uniforms: {},
        vertexShader: [THREE.ShaderChunk.morphtarget_pars_vertex, "void main() {\nvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );", THREE.ShaderChunk.morphtarget_vertex, THREE.ShaderChunk.default_vertex, "}"].join("\n"),
        fragmentShader: "vec4 pack_depth( const in float depth ) {\nconst vec4 bit_shift = vec4( 256.0 * 256.0 * 256.0, 256.0 * 256.0, 256.0, 1.0 );\nconst vec4 bit_mask  = vec4( 0.0, 1.0 / 256.0, 1.0 / 256.0, 1.0 / 256.0 );\nvec4 res = fract( depth * bit_shift );\nres -= res.xxyz * bit_mask;\nreturn res;\n}\nvoid main() {\ngl_FragData[ 0 ] = pack_depth( gl_FragCoord.z );\n}"
    }
};
THREE.WebGLRenderer = function(aW) {
    function aV(i, g) {
        var n = i.vertices.length
          , m = g.material;
        if (m.attributes) {
            if (i.__webglCustomAttributesList === void 0) {
                i.__webglCustomAttributesList = []
            }
            for (var l in m.attributes) {
                var k = m.attributes[l];
                if (!k.__webglInitialized || k.createUniqueBuffers) {
                    k.__webglInitialized = true;
                    var j = 1;
                    k.type === "v2" ? j = 2 : k.type === "v3" ? j = 3 : k.type === "v4" ? j = 4 : k.type === "c" && (j = 3);
                    k.size = j;
                    k.array = new Float32Array(n * j);
                    k.buffer = aP.createBuffer();
                    k.buffer.belongsToAttribute = l;
                    k.needsUpdate = true
                }
                i.__webglCustomAttributesList.push(k)
            }
        }
    }
    function aU(d, c) {
        if (d.material && !(d.material instanceof THREE.MeshFaceMaterial)) {
            return d.material
        }
        if (c.materialIndex >= 0) {
            return d.geometry.materials[c.materialIndex]
        }
    }
    function aT(b) {
        return b instanceof THREE.MeshBasicMaterial && !b.envMap || b instanceof THREE.MeshDepthMaterial ? false : b && b.shading !== void 0 && b.shading === THREE.SmoothShading ? THREE.SmoothShading : THREE.FlatShading
    }
    function aS(b) {
        return b.map || b.lightMap || b instanceof THREE.ShaderMaterial ? true : false
    }
    function aR(Q, O, N) {
        var K, J, I, H, D = Q.vertices;
        H = D.length;
        var A = Q.colors
          , B = A.length
          , y = Q.__vertexArray
          , u = Q.__colorArray
          , q = Q.__sortArray
          , w = Q.verticesNeedUpdate
          , g = Q.colorsNeedUpdate
          , R = Q.__webglCustomAttributesList;
        if (N.sortParticles) {
            W.copy(x);
            W.multiplySelf(N.matrixWorld);
            for (K = 0; K < H; K++) {
                J = D[K];
                U.copy(J);
                W.multiplyVector3(U);
                q[K] = [U.z, K]
            }
            q.sort(function(d, c) {
                return c[0] - d[0]
            });
            for (K = 0; K < H; K++) {
                J = D[q[K][1]];
                I = K * 3;
                y[I] = J.x;
                y[I + 1] = J.y;
                y[I + 2] = J.z
            }
            for (K = 0; K < B; K++) {
                I = K * 3;
                J = A[q[K][1]];
                u[I] = J.r;
                u[I + 1] = J.g;
                u[I + 2] = J.b
            }
            if (R) {
                A = 0;
                for (B = R.length; A < B; A++) {
                    D = R[A];
                    if (D.boundTo === void 0 || D.boundTo === "vertices") {
                        I = 0;
                        J = D.value.length;
                        if (D.size === 1) {
                            for (K = 0; K < J; K++) {
                                H = q[K][1];
                                D.array[K] = D.value[H]
                            }
                        } else {
                            if (D.size === 2) {
                                for (K = 0; K < J; K++) {
                                    H = q[K][1];
                                    H = D.value[H];
                                    D.array[I] = H.x;
                                    D.array[I + 1] = H.y;
                                    I = I + 2
                                }
                            } else {
                                if (D.size === 3) {
                                    if (D.type === "c") {
                                        for (K = 0; K < J; K++) {
                                            H = q[K][1];
                                            H = D.value[H];
                                            D.array[I] = H.r;
                                            D.array[I + 1] = H.g;
                                            D.array[I + 2] = H.b;
                                            I = I + 3
                                        }
                                    } else {
                                        for (K = 0; K < J; K++) {
                                            H = q[K][1];
                                            H = D.value[H];
                                            D.array[I] = H.x;
                                            D.array[I + 1] = H.y;
                                            D.array[I + 2] = H.z;
                                            I = I + 3
                                        }
                                    }
                                } else {
                                    if (D.size === 4) {
                                        for (K = 0; K < J; K++) {
                                            H = q[K][1];
                                            H = D.value[H];
                                            D.array[I] = H.x;
                                            D.array[I + 1] = H.y;
                                            D.array[I + 2] = H.z;
                                            D.array[I + 3] = H.w;
                                            I = I + 4
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (w) {
                for (K = 0; K < H; K++) {
                    J = D[K];
                    I = K * 3;
                    y[I] = J.x;
                    y[I + 1] = J.y;
                    y[I + 2] = J.z
                }
            }
            if (g) {
                for (K = 0; K < B; K++) {
                    J = A[K];
                    I = K * 3;
                    u[I] = J.r;
                    u[I + 1] = J.g;
                    u[I + 2] = J.b
                }
            }
            if (R) {
                A = 0;
                for (B = R.length; A < B; A++) {
                    D = R[A];
                    if (D.needsUpdate && (D.boundTo === void 0 || D.boundTo === "vertices")) {
                        J = D.value.length;
                        I = 0;
                        if (D.size === 1) {
                            for (K = 0; K < J; K++) {
                                D.array[K] = D.value[K]
                            }
                        } else {
                            if (D.size === 2) {
                                for (K = 0; K < J; K++) {
                                    H = D.value[K];
                                    D.array[I] = H.x;
                                    D.array[I + 1] = H.y;
                                    I = I + 2
                                }
                            } else {
                                if (D.size === 3) {
                                    if (D.type === "c") {
                                        for (K = 0; K < J; K++) {
                                            H = D.value[K];
                                            D.array[I] = H.r;
                                            D.array[I + 1] = H.g;
                                            D.array[I + 2] = H.b;
                                            I = I + 3
                                        }
                                    } else {
                                        for (K = 0; K < J; K++) {
                                            H = D.value[K];
                                            D.array[I] = H.x;
                                            D.array[I + 1] = H.y;
                                            D.array[I + 2] = H.z;
                                            I = I + 3
                                        }
                                    }
                                } else {
                                    if (D.size === 4) {
                                        for (K = 0; K < J; K++) {
                                            H = D.value[K];
                                            D.array[I] = H.x;
                                            D.array[I + 1] = H.y;
                                            D.array[I + 2] = H.z;
                                            D.array[I + 3] = H.w;
                                            I = I + 4
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (w || N.sortParticles) {
            aP.bindBuffer(aP.ARRAY_BUFFER, Q.__webglVertexBuffer);
            aP.bufferData(aP.ARRAY_BUFFER, y, O)
        }
        if (g || N.sortParticles) {
            aP.bindBuffer(aP.ARRAY_BUFFER, Q.__webglColorBuffer);
            aP.bufferData(aP.ARRAY_BUFFER, u, O)
        }
        if (R) {
            A = 0;
            for (B = R.length; A < B; A++) {
                D = R[A];
                if (D.needsUpdate || N.sortParticles) {
                    aP.bindBuffer(aP.ARRAY_BUFFER, D.buffer);
                    aP.bufferData(aP.ARRAY_BUFFER, D.array, O)
                }
            }
        }
    }
    function aO(d, c) {
        return c.z - d.z
    }
    function aM(g, f, j) {
        if (g.length) {
            for (var i = 0, h = g.length; i < h; i++) {
                V = L = null;
                ar = E = ah = F = S = M = G = -1;
                ax = true;
                g[i].render(f, j, af, r);
                V = L = null;
                ar = E = ah = F = S = M = G = -1;
                ax = true
            }
        }
    }
    function aJ(I, H, D, B, A, w, y, u) {
        var s, p, q, o;
        if (H) {
            p = I.length - 1;
            o = H = -1
        } else {
            p = 0;
            H = I.length;
            o = 1
        }
        for (var m = p; m !== H; m = m + o) {
            s = I[m];
            if (s.render) {
                p = s.object;
                q = s.buffer;
                if (u) {
                    s = u
                } else {
                    s = s[D];
                    if (!s) {
                        continue
                    }
                    y && ac.setBlending(s.blending, s.blendEquation, s.blendSrc, s.blendDst);
                    ac.setDepthTest(s.depthTest);
                    ac.setDepthWrite(s.depthWrite);
                    aF(s.polygonOffset, s.polygonOffsetFactor, s.polygonOffsetUnits)
                }
                ac.setObjectFaces(p);
                q instanceof THREE.BufferGeometry ? ac.renderBufferDirect(B, A, w, s, q, p) : ac.renderBuffer(B, A, w, s, q, p)
            }
        }
    }
    function aK(A, y, w, u, s, p, q) {
        for (var o, n, l = 0, m = A.length; l < m; l++) {
            o = A[l];
            n = o.object;
            if (n.visible) {
                if (q) {
                    o = q
                } else {
                    o = o[y];
                    if (!o) {
                        continue
                    }
                    p && ac.setBlending(o.blending, o.blendEquation, o.blendSrc, o.blendDst);
                    ac.setDepthTest(o.depthTest);
                    ac.setDepthWrite(o.depthWrite);
                    aF(o.polygonOffset, o.polygonOffsetFactor, o.polygonOffsetUnits)
                }
                ac.renderImmediateObject(w, u, s, o, n)
            }
        }
    }
    function aL(e, d, f) {
        e.push({
            buffer: d,
            object: f,
            opaque: null,
            transparent: null
        })
    }
    function aI(d) {
        for (var c in d.attributes) {
            if (d.attributes[c].needsUpdate) {
                return true
            }
        }
        return false
    }
    function aH(d) {
        for (var c in d.attributes) {
            d.attributes[c].needsUpdate = false
        }
    }
    function aG(bn, bm, bl, bk, bj) {
        if (!bk.program || bk.needsUpdate) {
            ac.initMaterial(bk, bm, bl, bj);
            bk.needsUpdate = false
        }
        if (bk.morphTargets && !bj.__webglMorphTargetInfluences) {
            bj.__webglMorphTargetInfluences = new Float32Array(ac.maxMorphTargets);
            for (var bi = 0, bh = ac.maxMorphTargets; bi < bh; bi++) {
                bj.__webglMorphTargetInfluences[bi] = 0
            }
        }
        var bg = false
          , bi = bk.program
          , bh = bi.uniforms
          , be = bk.uniforms;
        if (bi !== L) {
            aP.useProgram(bi);
            L = bi;
            bg = true
        }
        if (bk.id !== ar) {
            ar = bk.id;
            bg = true
        }
        if (bg || bn !== V) {
            aP.uniformMatrix4fv(bh.projectionMatrix, false, bn._projectionMatrixArray);
            bn !== V && (V = bn)
        }
        if (bg) {
            if (bl && bk.fog) {
                be.fogColor.value = bl.color;
                if (bl instanceof THREE.Fog) {
                    be.fogNear.value = bl.near;
                    be.fogFar.value = bl.far
                } else {
                    if (bl instanceof THREE.FogExp2) {
                        be.fogDensity.value = bl.density
                    }
                }
            }
            if (bk instanceof THREE.MeshPhongMaterial || bk instanceof THREE.MeshLambertMaterial || bk.lights) {
                if (ax) {
                    for (var bf, bd = 0, ba = 0, a9 = 0, bb, a7, a3, a5 = au, a1 = a5.directional.colors, a6 = a5.directional.positions, a4 = a5.point.colors, a0 = a5.point.positions, aZ = a5.point.distances, w = a5.spot.colors, aY = a5.spot.positions, J = a5.spot.distances, g = a5.spot.directions, a8 = a5.spot.angles, H = a5.spot.exponents, u = 0, a2 = 0, D = 0, bc = a3 = 0, bl = bc = 0, bg = bm.length; bl < bg; bl++) {
                        bf = bm[bl];
                        if (!bf.onlyShadow) {
                            bb = bf.color;
                            a7 = bf.intensity;
                            a3 = bf.distance;
                            if (bf instanceof THREE.AmbientLight) {
                                if (ac.gammaInput) {
                                    bd = bd + bb.r * bb.r;
                                    ba = ba + bb.g * bb.g;
                                    a9 = a9 + bb.b * bb.b
                                } else {
                                    bd = bd + bb.r;
                                    ba = ba + bb.g;
                                    a9 = a9 + bb.b
                                }
                            } else {
                                if (bf instanceof THREE.DirectionalLight) {
                                    a3 = u * 3;
                                    if (ac.gammaInput) {
                                        a1[a3] = bb.r * bb.r * a7 * a7;
                                        a1[a3 + 1] = bb.g * bb.g * a7 * a7;
                                        a1[a3 + 2] = bb.b * bb.b * a7 * a7
                                    } else {
                                        a1[a3] = bb.r * a7;
                                        a1[a3 + 1] = bb.g * a7;
                                        a1[a3 + 2] = bb.b * a7
                                    }
                                    z.copy(bf.matrixWorld.getPosition());
                                    z.subSelf(bf.target.matrixWorld.getPosition());
                                    z.normalize();
                                    a6[a3] = z.x;
                                    a6[a3 + 1] = z.y;
                                    a6[a3 + 2] = z.z;
                                    u = u + 1
                                } else {
                                    if (bf instanceof THREE.PointLight) {
                                        bc = a2 * 3;
                                        if (ac.gammaInput) {
                                            a4[bc] = bb.r * bb.r * a7 * a7;
                                            a4[bc + 1] = bb.g * bb.g * a7 * a7;
                                            a4[bc + 2] = bb.b * bb.b * a7 * a7
                                        } else {
                                            a4[bc] = bb.r * a7;
                                            a4[bc + 1] = bb.g * a7;
                                            a4[bc + 2] = bb.b * a7
                                        }
                                        bb = bf.matrixWorld.getPosition();
                                        a0[bc] = bb.x;
                                        a0[bc + 1] = bb.y;
                                        a0[bc + 2] = bb.z;
                                        aZ[a2] = a3;
                                        a2 = a2 + 1
                                    } else {
                                        if (bf instanceof THREE.SpotLight) {
                                            bc = D * 3;
                                            if (ac.gammaInput) {
                                                w[bc] = bb.r * bb.r * a7 * a7;
                                                w[bc + 1] = bb.g * bb.g * a7 * a7;
                                                w[bc + 2] = bb.b * bb.b * a7 * a7
                                            } else {
                                                w[bc] = bb.r * a7;
                                                w[bc + 1] = bb.g * a7;
                                                w[bc + 2] = bb.b * a7
                                            }
                                            bb = bf.matrixWorld.getPosition();
                                            aY[bc] = bb.x;
                                            aY[bc + 1] = bb.y;
                                            aY[bc + 2] = bb.z;
                                            J[D] = a3;
                                            z.copy(bb);
                                            z.subSelf(bf.target.matrixWorld.getPosition());
                                            z.normalize();
                                            g[bc] = z.x;
                                            g[bc + 1] = z.y;
                                            g[bc + 2] = z.z;
                                            a8[D] = Math.cos(bf.angle);
                                            H[D] = bf.exponent;
                                            D = D + 1
                                        }
                                    }
                                }
                            }
                        }
                    }
                    bl = u * 3;
                    for (bg = a1.length; bl < bg; bl++) {
                        a1[bl] = 0
                    }
                    bl = a2 * 3;
                    for (bg = a4.length; bl < bg; bl++) {
                        a4[bl] = 0
                    }
                    bl = D * 3;
                    for (bg = w.length; bl < bg; bl++) {
                        w[bl] = 0
                    }
                    a5.directional.length = u;
                    a5.point.length = a2;
                    a5.spot.length = D;
                    a5.ambient[0] = bd;
                    a5.ambient[1] = ba;
                    a5.ambient[2] = a9;
                    ax = false
                }
                bl = au;
                be.ambientLightColor.value = bl.ambient;
                be.directionalLightColor.value = bl.directional.colors;
                be.directionalLightDirection.value = bl.directional.positions;
                be.pointLightColor.value = bl.point.colors;
                be.pointLightPosition.value = bl.point.positions;
                be.pointLightDistance.value = bl.point.distances;
                be.spotLightColor.value = bl.spot.colors;
                be.spotLightPosition.value = bl.spot.positions;
                be.spotLightDistance.value = bl.spot.distances;
                be.spotLightDirection.value = bl.spot.directions;
                be.spotLightAngle.value = bl.spot.angles;
                be.spotLightExponent.value = bl.spot.exponents
            }
            if (bk instanceof THREE.MeshBasicMaterial || bk instanceof THREE.MeshLambertMaterial || bk instanceof THREE.MeshPhongMaterial) {
                be.opacity.value = bk.opacity;
                ac.gammaInput ? be.diffuse.value.copyGammaToLinear(bk.color) : be.diffuse.value = bk.color;
                (be.map.texture = bk.map) && be.offsetRepeat.value.set(bk.map.offset.x, bk.map.offset.y, bk.map.repeat.x, bk.map.repeat.y);
                be.lightMap.texture = bk.lightMap;
                be.envMap.texture = bk.envMap;
                be.flipEnvMap.value = bk.envMap instanceof THREE.WebGLRenderTargetCube ? 1 : -1;
                be.reflectivity.value = bk.reflectivity;
                be.refractionRatio.value = bk.refractionRatio;
                be.combine.value = bk.combine;
                be.useRefract.value = bk.envMap && bk.envMap.mapping instanceof THREE.CubeRefractionMapping
            }
            if (bk instanceof THREE.LineBasicMaterial) {
                be.diffuse.value = bk.color;
                be.opacity.value = bk.opacity
            } else {
                if (bk instanceof THREE.ParticleBasicMaterial) {
                    be.psColor.value = bk.color;
                    be.opacity.value = bk.opacity;
                    be.size.value = bk.size;
                    be.scale.value = aa.height / 2;
                    be.map.texture = bk.map
                } else {
                    if (bk instanceof THREE.MeshPhongMaterial) {
                        be.shininess.value = bk.shininess;
                        if (ac.gammaInput) {
                            be.ambient.value.copyGammaToLinear(bk.ambient);
                            be.emissive.value.copyGammaToLinear(bk.emissive);
                            be.specular.value.copyGammaToLinear(bk.specular)
                        } else {
                            be.ambient.value = bk.ambient;
                            be.emissive.value = bk.emissive;
                            be.specular.value = bk.specular
                        }
                        bk.wrapAround && be.wrapRGB.value.copy(bk.wrapRGB)
                    } else {
                        if (bk instanceof THREE.MeshLambertMaterial) {
                            if (ac.gammaInput) {
                                be.ambient.value.copyGammaToLinear(bk.ambient);
                                be.emissive.value.copyGammaToLinear(bk.emissive)
                            } else {
                                be.ambient.value = bk.ambient;
                                be.emissive.value = bk.emissive
                            }
                            bk.wrapAround && be.wrapRGB.value.copy(bk.wrapRGB)
                        } else {
                            if (bk instanceof THREE.MeshDepthMaterial) {
                                be.mNear.value = bn.near;
                                be.mFar.value = bn.far;
                                be.opacity.value = bk.opacity
                            } else {
                                if (bk instanceof THREE.MeshNormalMaterial) {
                                    be.opacity.value = bk.opacity
                                }
                            }
                        }
                    }
                }
            }
            if (bj.receiveShadow && !bk._shadowPass && be.shadowMatrix) {
                bg = bl = 0;
                for (bf = bm.length; bg < bf; bg++) {
                    bd = bm[bg];
                    if (bd.castShadow && (bd instanceof THREE.SpotLight || bd instanceof THREE.DirectionalLight && !bd.shadowCascade)) {
                        be.shadowMap.texture[bl] = bd.shadowMap;
                        be.shadowMapSize.value[bl] = bd.shadowMapSize;
                        be.shadowMatrix.value[bl] = bd.shadowMatrix;
                        be.shadowDarkness.value[bl] = bd.shadowDarkness;
                        be.shadowBias.value[bl] = bd.shadowBias;
                        bl++
                    }
                }
            }
            bm = bk.uniformsList;
            be = 0;
            for (bl = bm.length; be < bl; be++) {
                if (bd = bi.uniforms[bm[be][1]]) {
                    bg = bm[be][0];
                    ba = bg.type;
                    bf = bg.value;
                    switch (ba) {
                    case "i":
                        aP.uniform1i(bd, bf);
                        break;
                    case "f":
                        aP.uniform1f(bd, bf);
                        break;
                    case "v2":
                        aP.uniform2f(bd, bf.x, bf.y);
                        break;
                    case "v3":
                        aP.uniform3f(bd, bf.x, bf.y, bf.z);
                        break;
                    case "v4":
                        aP.uniform4f(bd, bf.x, bf.y, bf.z, bf.w);
                        break;
                    case "c":
                        aP.uniform3f(bd, bf.r, bf.g, bf.b);
                        break;
                    case "fv1":
                        aP.uniform1fv(bd, bf);
                        break;
                    case "fv":
                        aP.uniform3fv(bd, bf);
                        break;
                    case "v2v":
                        if (!bg._array) {
                            bg._array = new Float32Array(2 * bf.length)
                        }
                        ba = 0;
                        for (a9 = bf.length; ba < a9; ba++) {
                            a5 = ba * 2;
                            bg._array[a5] = bf[ba].x;
                            bg._array[a5 + 1] = bf[ba].y
                        }
                        aP.uniform2fv(bd, bg._array);
                        break;
                    case "v3v":
                        if (!bg._array) {
                            bg._array = new Float32Array(3 * bf.length)
                        }
                        ba = 0;
                        for (a9 = bf.length; ba < a9; ba++) {
                            a5 = ba * 3;
                            bg._array[a5] = bf[ba].x;
                            bg._array[a5 + 1] = bf[ba].y;
                            bg._array[a5 + 2] = bf[ba].z
                        }
                        aP.uniform3fv(bd, bg._array);
                        break;
                    case "v4v":
                        if (!bg._array) {
                            bg._array = new Float32Array(4 * bf.length)
                        }
                        ba = 0;
                        for (a9 = bf.length; ba < a9; ba++) {
                            a5 = ba * 4;
                            bg._array[a5] = bf[ba].x;
                            bg._array[a5 + 1] = bf[ba].y;
                            bg._array[a5 + 2] = bf[ba].z;
                            bg._array[a5 + 3] = bf[ba].w
                        }
                        aP.uniform4fv(bd, bg._array);
                        break;
                    case "m4":
                        if (!bg._array) {
                            bg._array = new Float32Array(16)
                        }
                        bf.flattenToArray(bg._array);
                        aP.uniformMatrix4fv(bd, false, bg._array);
                        break;
                    case "m4v":
                        if (!bg._array) {
                            bg._array = new Float32Array(16 * bf.length)
                        }
                        ba = 0;
                        for (a9 = bf.length; ba < a9; ba++) {
                            bf[ba].flattenToArrayOffset(bg._array, ba * 16)
                        }
                        aP.uniformMatrix4fv(bd, false, bg._array);
                        break;
                    case "t":
                        aP.uniform1i(bd, bf);
                        bd = bg.texture;
                        if (!bd) {
                            continue
                        }
                        if (bd.image instanceof Array && bd.image.length === 6) {
                            bg = bd;
                            if (bg.image.length === 6) {
                                if (bg.needsUpdate) {
                                    if (!bg.image.__webglTextureCube) {
                                        bg.image.__webglTextureCube = aP.createTexture()
                                    }
                                    aP.activeTexture(aP.TEXTURE0 + bf);
                                    aP.bindTexture(aP.TEXTURE_CUBE_MAP, bg.image.__webglTextureCube);
                                    bf = [];
                                    for (bd = 0; bd < 6; bd++) {
                                        ba = bf;
                                        a9 = bd;
                                        if (ac.autoScaleCubemaps) {
                                            a5 = bg.image[bd];
                                            a6 = ak;
                                            if (!(a5.width <= a6 && a5.height <= a6)) {
                                                a4 = Math.max(a5.width, a5.height);
                                                a1 = Math.floor(a5.width * a6 / a4);
                                                a6 = Math.floor(a5.height * a6 / a4);
                                                a4 = document.createElement("canvas");
                                                a4.width = a1;
                                                a4.height = a6;
                                                a4.getContext("2d").drawImage(a5, 0, 0, a5.width, a5.height, 0, 0, a1, a6);
                                                a5 = a4
                                            }
                                        } else {
                                            a5 = bg.image[bd]
                                        }
                                        ba[a9] = a5
                                    }
                                    bd = bf[0];
                                    ba = (bd.width & bd.width - 1) === 0 && (bd.height & bd.height - 1) === 0;
                                    a9 = ay(bg.format);
                                    a5 = ay(bg.type);
                                    aw(aP.TEXTURE_CUBE_MAP, bg, ba);
                                    for (bd = 0; bd < 6; bd++) {
                                        aP.texImage2D(aP.TEXTURE_CUBE_MAP_POSITIVE_X + bd, 0, a9, a9, a5, bf[bd])
                                    }
                                    bg.generateMipmaps && ba && aP.generateMipmap(aP.TEXTURE_CUBE_MAP);
                                    bg.needsUpdate = false;
                                    if (bg.onUpdate) {
                                        bg.onUpdate()
                                    }
                                } else {
                                    aP.activeTexture(aP.TEXTURE0 + bf);
                                    aP.bindTexture(aP.TEXTURE_CUBE_MAP, bg.image.__webglTextureCube)
                                }
                            }
                        } else {
                            if (bd instanceof THREE.WebGLRenderTargetCube) {
                                bg = bd;
                                aP.activeTexture(aP.TEXTURE0 + bf);
                                aP.bindTexture(aP.TEXTURE_CUBE_MAP, bg.__webglTexture)
                            } else {
                                ac.setTexture(bd, bf)
                            }
                        }
                        break;
                    case "tv":
                        if (!bg._array) {
                            bg._array = [];
                            ba = 0;
                            for (a9 = bg.texture.length; ba < a9; ba++) {
                                bg._array[ba] = bf + ba
                            }
                        }
                        aP.uniform1iv(bd, bg._array);
                        ba = 0;
                        for (a9 = bg.texture.length; ba < a9; ba++) {
                            (bd = bg.texture[ba]) && ac.setTexture(bd, bg._array[ba])
                        }
                    }
                }
            }
            if ((bk instanceof THREE.ShaderMaterial || bk instanceof THREE.MeshPhongMaterial || bk.envMap) && bh.cameraPosition !== null) {
                bm = bn.matrixWorld.getPosition();
                aP.uniform3f(bh.cameraPosition, bm.x, bm.y, bm.z)
            }
            (bk instanceof THREE.MeshPhongMaterial || bk instanceof THREE.MeshLambertMaterial || bk instanceof THREE.ShaderMaterial || bk.skinning) && bh.viewMatrix !== null && aP.uniformMatrix4fv(bh.viewMatrix, false, bn._viewMatrixArray);
            bk.skinning && aP.uniformMatrix4fv(bh.boneGlobalMatrices, false, bj.boneMatrices)
        }
        aP.uniformMatrix4fv(bh.modelViewMatrix, false, bj._modelViewMatrix.elements);
        bh.normalMatrix && aP.uniformMatrix3fv(bh.normalMatrix, false, bj._normalMatrix.elements);
        bh.objectMatrix !== null && aP.uniformMatrix4fv(bh.objectMatrix, false, bj.matrixWorld.elements);
        return bi
    }
    function aB(d, c) {
        d._modelViewMatrix.multiply(c.matrixWorldInverse, d.matrixWorld);
        d._normalMatrix.getInverse(d._modelViewMatrix);
        d._normalMatrix.transpose()
    }
    function aF(e, d, f) {
        if (ao !== e) {
            e ? aP.enable(aP.POLYGON_OFFSET_FILL) : aP.disable(aP.POLYGON_OFFSET_FILL);
            ao = e
        }
        if (e && (aN !== d || ap !== f)) {
            aP.polygonOffset(d, f);
            aN = d;
            ap = f
        }
    }
    function aE(e, d) {
        var f;
        e === "fragment" ? f = aP.createShader(aP.FRAGMENT_SHADER) : e === "vertex" && (f = aP.createShader(aP.VERTEX_SHADER));
        aP.shaderSource(f, d);
        aP.compileShader(f);
        if (!aP.getShaderParameter(f, aP.COMPILE_STATUS)) {
            console.error(aP.getShaderInfoLog(f));
            console.error(d);
            return null
        }
        return f
    }
    function aw(e, d, f) {
        if (f) {
            aP.texParameteri(e, aP.TEXTURE_WRAP_S, ay(d.wrapS));
            aP.texParameteri(e, aP.TEXTURE_WRAP_T, ay(d.wrapT));
            aP.texParameteri(e, aP.TEXTURE_MAG_FILTER, ay(d.magFilter));
            aP.texParameteri(e, aP.TEXTURE_MIN_FILTER, ay(d.minFilter))
        } else {
            aP.texParameteri(e, aP.TEXTURE_WRAP_S, aP.CLAMP_TO_EDGE);
            aP.texParameteri(e, aP.TEXTURE_WRAP_T, aP.CLAMP_TO_EDGE);
            aP.texParameteri(e, aP.TEXTURE_MAG_FILTER, av(d.magFilter));
            aP.texParameteri(e, aP.TEXTURE_MIN_FILTER, av(d.minFilter))
        }
    }
    function ae(d, c) {
        aP.bindRenderbuffer(aP.RENDERBUFFER, d);
        if (c.depthBuffer && !c.stencilBuffer) {
            aP.renderbufferStorage(aP.RENDERBUFFER, aP.DEPTH_COMPONENT16, c.width, c.height);
            aP.framebufferRenderbuffer(aP.FRAMEBUFFER, aP.DEPTH_ATTACHMENT, aP.RENDERBUFFER, d)
        } else {
            if (c.depthBuffer && c.stencilBuffer) {
                aP.renderbufferStorage(aP.RENDERBUFFER, aP.DEPTH_STENCIL, c.width, c.height);
                aP.framebufferRenderbuffer(aP.FRAMEBUFFER, aP.DEPTH_STENCIL_ATTACHMENT, aP.RENDERBUFFER, d)
            } else {
                aP.renderbufferStorage(aP.RENDERBUFFER, aP.RGBA4, c.width, c.height)
            }
        }
    }
    function av(b) {
        switch (b) {
        case THREE.NearestFilter:
        case THREE.NearestMipMapNearestFilter:
        case THREE.NearestMipMapLinearFilter:
            return aP.NEAREST;
        default:
            return aP.LINEAR
        }
    }
    function ay(b) {
        switch (b) {
        case THREE.RepeatWrapping:
            return aP.REPEAT;
        case THREE.ClampToEdgeWrapping:
            return aP.CLAMP_TO_EDGE;
        case THREE.MirroredRepeatWrapping:
            return aP.MIRRORED_REPEAT;
        case THREE.NearestFilter:
            return aP.NEAREST;
        case THREE.NearestMipMapNearestFilter:
            return aP.NEAREST_MIPMAP_NEAREST;
        case THREE.NearestMipMapLinearFilter:
            return aP.NEAREST_MIPMAP_LINEAR;
        case THREE.LinearFilter:
            return aP.LINEAR;
        case THREE.LinearMipMapNearestFilter:
            return aP.LINEAR_MIPMAP_NEAREST;
        case THREE.LinearMipMapLinearFilter:
            return aP.LINEAR_MIPMAP_LINEAR;
        case THREE.ByteType:
            return aP.BYTE;
        case THREE.UnsignedByteType:
            return aP.UNSIGNED_BYTE;
        case THREE.ShortType:
            return aP.SHORT;
        case THREE.UnsignedShortType:
            return aP.UNSIGNED_SHORT;
        case THREE.IntType:
            return aP.INT;
        case THREE.UnsignedIntType:
            return aP.UNSIGNED_INT;
        case THREE.FloatType:
            return aP.FLOAT;
        case THREE.AlphaFormat:
            return aP.ALPHA;
        case THREE.RGBFormat:
            return aP.RGB;
        case THREE.RGBAFormat:
            return aP.RGBA;
        case THREE.LuminanceFormat:
            return aP.LUMINANCE;
        case THREE.LuminanceAlphaFormat:
            return aP.LUMINANCE_ALPHA;
        case THREE.AddEquation:
            return aP.FUNC_ADD;
        case THREE.SubtractEquation:
            return aP.FUNC_SUBTRACT;
        case THREE.ReverseSubtractEquation:
            return aP.FUNC_REVERSE_SUBTRACT;
        case THREE.ZeroFactor:
            return aP.ZERO;
        case THREE.OneFactor:
            return aP.ONE;
        case THREE.SrcColorFactor:
            return aP.SRC_COLOR;
        case THREE.OneMinusSrcColorFactor:
            return aP.ONE_MINUS_SRC_COLOR;
        case THREE.SrcAlphaFactor:
            return aP.SRC_ALPHA;
        case THREE.OneMinusSrcAlphaFactor:
            return aP.ONE_MINUS_SRC_ALPHA;
        case THREE.DstAlphaFactor:
            return aP.DST_ALPHA;
        case THREE.OneMinusDstAlphaFactor:
            return aP.ONE_MINUS_DST_ALPHA;
        case THREE.DstColorFactor:
            return aP.DST_COLOR;
        case THREE.OneMinusDstColorFactor:
            return aP.ONE_MINUS_DST_COLOR;
        case THREE.SrcAlphaSaturateFactor:
            return aP.SRC_ALPHA_SATURATE
        }
        return 0
    }
    if (logger) console.log("THREE.WebGLRenderer", THREE.REVISION);
    var aW = aW || {}
      , aa = aW.canvas !== void 0 ? aW.canvas : document.createElement("canvas")
      , ad = aW.precision !== void 0 ? aW.precision : "highp"
      , T = aW.alpha !== void 0 ? aW.alpha : true
      , P = aW.premultipliedAlpha !== void 0 ? aW.premultipliedAlpha : true
      , v = aW.antialias !== void 0 ? aW.antialias : false
      , at = aW.stencil !== void 0 ? aW.stencil : true
      , X = aW.preserveDrawingBuffer !== void 0 ? aW.preserveDrawingBuffer : false
      , aX = aW.clearColor !== void 0 ? new THREE.Color(aW.clearColor) : new THREE.Color(0)
      , ag = aW.clearAlpha !== void 0 ? aW.clearAlpha : 0
      , am = aW.maxLights !== void 0 ? aW.maxLights : 4;
    this.domElement = aa;
    this.context = null;
    this.autoUpdateScene = this.autoUpdateObjects = this.sortObjects = this.autoClearStencil = this.autoClearDepth = this.autoClearColor = this.autoClear = true;
    this.shadowMapEnabled = this.physicallyBasedShading = this.gammaOutput = this.gammaInput = false;
    this.shadowMapCullFrontFaces = this.shadowMapSoft = this.shadowMapAutoUpdate = true;
    this.shadowMapCascade = this.shadowMapDebug = false;
    this.maxMorphTargets = 8;
    this.maxMorphNormals = 4;
    this.autoScaleCubemaps = true;
    this.renderPluginsPre = [];
    this.renderPluginsPost = [];
    this.info = {
        memory: {
            programs: 0,
            geometries: 0,
            textures: 0
        },
        render: {
            calls: 0,
            vertices: 0,
            faces: 0,
            points: 0
        }
    };
    var ac = this, aP, an = [], L = null, aA = null, ar = -1, E = null, V = null, t = 0, F = -1, ah = -1, G = -1, aj = -1, aD = -1, al = -1, M = -1, S = -1, ao = null, aN = null, ap = null, az = null, ai = 0, C = 0, ab = 0, aC = 0, af = 0, r = 0, aQ = new THREE.Frustum, x = new THREE.Matrix4, W = new THREE.Matrix4, U = new THREE.Vector4, z = new THREE.Vector3, ax = true, au = {
        ambient: [0, 0, 0],
        directional: {
            length: 0,
            colors: [],
            positions: []
        },
        point: {
            length: 0,
            colors: [],
            positions: [],
            distances: []
        },
        spot: {
            length: 0,
            colors: [],
            positions: [],
            distances: [],
            directions: [],
            angles: [],
            exponents: []
        }
    };
    aP = function() {
        var d;
        try {
            if (aa.getContext && !(d = aa.getContext("experimental-webgl", {
                alpha: T,
                premultipliedAlpha: P,
                antialias: v,
                stencil: at,
                preserveDrawingBuffer: X
            }))) {
                throw "Error creating WebGL context."
            }
        } catch (c) {
            console.error(c)
        }
        d.getExtension("OES_texture_float") || console.log("THREE.WebGLRenderer: Float textures not supported.");
        return d
    }();
    aP.clearColor(0, 0, 0, 1);
    aP.clearDepth(1);
    aP.clearStencil(0);
    aP.enable(aP.DEPTH_TEST);
    aP.depthFunc(aP.LEQUAL);
    aP.frontFace(aP.CCW);
    aP.cullFace(aP.BACK);
    aP.enable(aP.CULL_FACE);
    aP.enable(aP.BLEND);
    aP.blendEquation(aP.FUNC_ADD);
    aP.blendFunc(aP.SRC_ALPHA, aP.ONE_MINUS_SRC_ALPHA);
    aP.clearColor(aX.r, aX.g, aX.b, ag);
    this.context = aP;
    var aq = aP.getParameter(aP.MAX_VERTEX_TEXTURE_IMAGE_UNITS);
    aP.getParameter(aP.MAX_TEXTURE_SIZE);
    var ak = aP.getParameter(aP.MAX_CUBE_MAP_TEXTURE_SIZE);
    this.getContext = function() {
        return aP
    }
    ;
    this.supportsVertexTextures = function() {
        return aq > 0
    }
    ;
    this.setSize = function(d, c) {
        aa.width = d;
        aa.height = c;
        this.setViewport(0, 0, aa.width, aa.height)
    }
    ;
    this.setViewport = function(f, e, h, g) {
        ai = f;
        C = e;
        ab = h;
        aC = g;
        aP.viewport(ai, C, ab, aC)
    }
    ;
    this.setScissor = function(f, e, h, g) {
        aP.scissor(f, e, h, g)
    }
    ;
    this.enableScissorTest = function(b) {
        b ? aP.enable(aP.SCISSOR_TEST) : aP.disable(aP.SCISSOR_TEST)
    }
    ;
    this.setClearColorHex = function(d, c) {
        aX.setHex(d);
        ag = c;
        aP.clearColor(aX.r, aX.g, aX.b, ag)
    }
    ;
    this.setClearColor = function(d, c) {
        aX.copy(d);
        ag = c;
        aP.clearColor(aX.r, aX.g, aX.b, ag)
    }
    ;
    this.getClearColor = function() {
        return aX
    }
    ;
    this.getClearAlpha = function() {
        return ag
    }
    ;
    this.clear = function(f, e, h) {
        var g = 0;
        if (f === void 0 || f) {
            g = g | aP.COLOR_BUFFER_BIT
        }
        if (e === void 0 || e) {
            g = g | aP.DEPTH_BUFFER_BIT
        }
        if (h === void 0 || h) {
            g = g | aP.STENCIL_BUFFER_BIT
        }
        aP.clear(g)
    }
    ;
    this.clearTarget = function(f, e, h, g) {
        this.setRenderTarget(f);
        this.clear(e, h, g)
    }
    ;
    this.addPostPlugin = function(b) {
        b.init(this);
        this.renderPluginsPost.push(b)
    }
    ;
    this.addPrePlugin = function(b) {
        b.init(this);
        this.renderPluginsPre.push(b)
    }
    ;
    this.deallocateObject = function(g) {
        if (g.__webglInit) {
            g.__webglInit = false;
            delete g._modelViewMatrix;
            delete g._normalMatrix;
            delete g._normalMatrixArray;
            delete g._modelViewMatrixArray;
            delete g._objectMatrixArray;
            if (g instanceof THREE.Mesh) {
                for (var f in g.geometry.geometryGroups) {
                    var j = g.geometry.geometryGroups[f];
                    aP.deleteBuffer(j.__webglVertexBuffer);
                    aP.deleteBuffer(j.__webglNormalBuffer);
                    aP.deleteBuffer(j.__webglTangentBuffer);
                    aP.deleteBuffer(j.__webglColorBuffer);
                    aP.deleteBuffer(j.__webglUVBuffer);
                    aP.deleteBuffer(j.__webglUV2Buffer);
                    aP.deleteBuffer(j.__webglSkinVertexABuffer);
                    aP.deleteBuffer(j.__webglSkinVertexBBuffer);
                    aP.deleteBuffer(j.__webglSkinIndicesBuffer);
                    aP.deleteBuffer(j.__webglSkinWeightsBuffer);
                    aP.deleteBuffer(j.__webglFaceBuffer);
                    aP.deleteBuffer(j.__webglLineBuffer);
                    var i = void 0
                      , h = void 0;
                    if (j.numMorphTargets) {
                        i = 0;
                        for (h = j.numMorphTargets; i < h; i++) {
                            aP.deleteBuffer(j.__webglMorphTargetsBuffers[i])
                        }
                    }
                    if (j.numMorphNormals) {
                        i = 0;
                        for (h = j.numMorphNormals; i < h; i++) {
                            aP.deleteBuffer(j.__webglMorphNormalsBuffers[i])
                        }
                    }
                    if (j.__webglCustomAttributesList) {
                        i = void 0;
                        for (i in j.__webglCustomAttributesList) {
                            aP.deleteBuffer(j.__webglCustomAttributesList[i].buffer)
                        }
                    }
                    ac.info.memory.geometries--
                }
            } else {
                if (g instanceof THREE.Line) {
                    g = g.geometry;
                    aP.deleteBuffer(g.__webglVertexBuffer);
                    aP.deleteBuffer(g.__webglColorBuffer);
                    ac.info.memory.geometries--
                }
            }
        }
    }
    ;
    this.deallocateTexture = function(b) {
        if (b.__webglInit) {
            b.__webglInit = false;
            aP.deleteTexture(b.__webglTexture);
            ac.info.memory.textures--
        }
    }
    ;
    this.deallocateRenderTarget = function(d) {
        if (d && d.__webglTexture) {
            aP.deleteTexture(d.__webglTexture);
            if (d instanceof THREE.WebGLRenderTargetCube) {
                for (var c = 0; c < 6; c++) {
                    aP.deleteFramebuffer(d.__webglFramebuffer[c]);
                    aP.deleteRenderbuffer(d.__webglRenderbuffer[c])
                }
            } else {
                aP.deleteFramebuffer(d.__webglFramebuffer);
                aP.deleteRenderbuffer(d.__webglRenderbuffer)
            }
        }
    }
    ;
    this.updateShadowMap = function(d, c) {
        L = null;
        ar = E = S = M = G = -1;
        ax = true;
        ah = F = -1;
        this.shadowMapPlugin.update(d, c)
    }
    ;
    this.renderBufferImmediate = function(K, J, I) {
        if (!K.__webglVertexBuffer) {
            K.__webglVertexBuffer = aP.createBuffer()
        }
        if (!K.__webglNormalBuffer) {
            K.__webglNormalBuffer = aP.createBuffer()
        }
        if (K.hasPos) {
            aP.bindBuffer(aP.ARRAY_BUFFER, K.__webglVertexBuffer);
            aP.bufferData(aP.ARRAY_BUFFER, K.positionArray, aP.DYNAMIC_DRAW);
            aP.enableVertexAttribArray(J.attributes.position);
            aP.vertexAttribPointer(J.attributes.position, 3, aP.FLOAT, false, 0, 0)
        }
        if (K.hasNormal) {
            aP.bindBuffer(aP.ARRAY_BUFFER, K.__webglNormalBuffer);
            if (I === THREE.FlatShading) {
                var H, D, B, A, y, u, w, s, p, q, g = K.count * 3;
                for (q = 0; q < g; q = q + 9) {
                    I = K.normalArray;
                    H = I[q];
                    D = I[q + 1];
                    B = I[q + 2];
                    A = I[q + 3];
                    u = I[q + 4];
                    s = I[q + 5];
                    y = I[q + 6];
                    w = I[q + 7];
                    p = I[q + 8];
                    H = (H + A + y) / 3;
                    D = (D + u + w) / 3;
                    B = (B + s + p) / 3;
                    I[q] = H;
                    I[q + 1] = D;
                    I[q + 2] = B;
                    I[q + 3] = H;
                    I[q + 4] = D;
                    I[q + 5] = B;
                    I[q + 6] = H;
                    I[q + 7] = D;
                    I[q + 8] = B
                }
            }
            aP.bufferData(aP.ARRAY_BUFFER, K.normalArray, aP.DYNAMIC_DRAW);
            aP.enableVertexAttribArray(J.attributes.normal);
            aP.vertexAttribPointer(J.attributes.normal, 3, aP.FLOAT, false, 0, 0)
        }
        aP.drawArrays(aP.TRIANGLES, 0, K.count);
        K.count = 0
    }
    ;
    this.renderBufferDirect = function(h, g, l, k, j, i) {
        if (k.visible !== false) {
            l = aG(h, g, l, k, i);
            h = l.attributes;
            g = false;
            k = j.id * 16777215 + l.id * 2 + (k.wireframe ? 1 : 0);
            if (k !== E) {
                E = k;
                g = true
            }
            if (i instanceof THREE.Mesh) {
                i = j.offsets;
                k = 0;
                for (l = i.length; k < l; ++k) {
                    if (g) {
                        aP.bindBuffer(aP.ARRAY_BUFFER, j.vertexPositionBuffer);
                        aP.vertexAttribPointer(h.position, j.vertexPositionBuffer.itemSize, aP.FLOAT, false, 0, i[k].index * 12);
                        if (h.normal >= 0 && j.vertexNormalBuffer) {
                            aP.bindBuffer(aP.ARRAY_BUFFER, j.vertexNormalBuffer);
                            aP.vertexAttribPointer(h.normal, j.vertexNormalBuffer.itemSize, aP.FLOAT, false, 0, i[k].index * 12)
                        }
                        if (h.uv >= 0 && j.vertexUvBuffer) {
                            if (j.vertexUvBuffer) {
                                aP.bindBuffer(aP.ARRAY_BUFFER, j.vertexUvBuffer);
                                aP.vertexAttribPointer(h.uv, j.vertexUvBuffer.itemSize, aP.FLOAT, false, 0, i[k].index * 8);
                                aP.enableVertexAttribArray(h.uv)
                            } else {
                                aP.disableVertexAttribArray(h.uv)
                            }
                        }
                        if (h.color >= 0 && j.vertexColorBuffer) {
                            aP.bindBuffer(aP.ARRAY_BUFFER, j.vertexColorBuffer);
                            aP.vertexAttribPointer(h.color, j.vertexColorBuffer.itemSize, aP.FLOAT, false, 0, i[k].index * 16)
                        }
                        aP.bindBuffer(aP.ELEMENT_ARRAY_BUFFER, j.vertexIndexBuffer)
                    }
                    aP.drawElements(aP.TRIANGLES, i[k].count, aP.UNSIGNED_SHORT, i[k].start * 2);
                    ac.info.render.calls++;
                    ac.info.render.vertices = ac.info.render.vertices + i[k].count;
                    ac.info.render.faces = ac.info.render.faces + i[k].count / 3
                }
            }
        }
    }
    ;
    this.renderBuffer = function(I, H, D, B, A, y) {
        if (B.visible !== false) {
            var w, u, D = aG(I, H, D, B, y), H = D.attributes, I = false, D = A.id * 16777215 + D.id * 2 + (B.wireframe ? 1 : 0);
            if (D !== E) {
                E = D;
                I = true
            }
            if (!B.morphTargets && H.position >= 0) {
                if (I) {
                    aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglVertexBuffer);
                    aP.vertexAttribPointer(H.position, 3, aP.FLOAT, false, 0, 0)
                }
            } else {
                if (y.morphTargetBase) {
                    D = B.program.attributes;
                    if (y.morphTargetBase !== -1) {
                        aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglMorphTargetsBuffers[y.morphTargetBase]);
                        aP.vertexAttribPointer(D.position, 3, aP.FLOAT, false, 0, 0)
                    } else {
                        if (D.position >= 0) {
                            aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglVertexBuffer);
                            aP.vertexAttribPointer(D.position, 3, aP.FLOAT, false, 0, 0)
                        }
                    }
                    if (y.morphTargetForcedOrder.length) {
                        w = 0;
                        var q = y.morphTargetForcedOrder;
                        for (u = y.morphTargetInfluences; w < B.numSupportedMorphTargets && w < q.length; ) {
                            aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglMorphTargetsBuffers[q[w]]);
                            aP.vertexAttribPointer(D["morphTarget" + w], 3, aP.FLOAT, false, 0, 0);
                            if (B.morphNormals) {
                                aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglMorphNormalsBuffers[q[w]]);
                                aP.vertexAttribPointer(D["morphNormal" + w], 3, aP.FLOAT, false, 0, 0)
                            }
                            y.__webglMorphTargetInfluences[w] = u[q[w]];
                            w++
                        }
                    } else {
                        var q = []
                          , s = -1
                          , p = 0;
                        u = y.morphTargetInfluences;
                        var g, o = u.length;
                        w = 0;
                        for (y.morphTargetBase !== -1 && (q[y.morphTargetBase] = true); w < B.numSupportedMorphTargets; ) {
                            for (g = 0; g < o; g++) {
                                if (!q[g] && u[g] > s) {
                                    p = g;
                                    s = u[p]
                                }
                            }
                            aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglMorphTargetsBuffers[p]);
                            aP.vertexAttribPointer(D["morphTarget" + w], 3, aP.FLOAT, false, 0, 0);
                            if (B.morphNormals) {
                                aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglMorphNormalsBuffers[p]);
                                aP.vertexAttribPointer(D["morphNormal" + w], 3, aP.FLOAT, false, 0, 0)
                            }
                            y.__webglMorphTargetInfluences[w] = s;
                            q[p] = 1;
                            s = -1;
                            w++
                        }
                    }
                    B.program.uniforms.morphTargetInfluences !== null && aP.uniform1fv(B.program.uniforms.morphTargetInfluences, y.__webglMorphTargetInfluences)
                }
            }
            if (I) {
                if (A.__webglCustomAttributesList) {
                    w = 0;
                    for (u = A.__webglCustomAttributesList.length; w < u; w++) {
                        D = A.__webglCustomAttributesList[w];
                        if (H[D.buffer.belongsToAttribute] >= 0) {
                            aP.bindBuffer(aP.ARRAY_BUFFER, D.buffer);
                            aP.vertexAttribPointer(H[D.buffer.belongsToAttribute], D.size, aP.FLOAT, false, 0, 0)
                        }
                    }
                }
                if (H.color >= 0) {
                    aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglColorBuffer);
                    aP.vertexAttribPointer(H.color, 3, aP.FLOAT, false, 0, 0)
                }
                if (H.normal >= 0) {
                    aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglNormalBuffer);
                    aP.vertexAttribPointer(H.normal, 3, aP.FLOAT, false, 0, 0)
                }
                if (H.tangent >= 0) {
                    aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglTangentBuffer);
                    aP.vertexAttribPointer(H.tangent, 4, aP.FLOAT, false, 0, 0)
                }
                if (H.uv >= 0) {
                    if (A.__webglUVBuffer) {
                        aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglUVBuffer);
                        aP.vertexAttribPointer(H.uv, 2, aP.FLOAT, false, 0, 0);
                        aP.enableVertexAttribArray(H.uv)
                    } else {
                        aP.disableVertexAttribArray(H.uv)
                    }
                }
                if (H.uv2 >= 0) {
                    if (A.__webglUV2Buffer) {
                        aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglUV2Buffer);
                        aP.vertexAttribPointer(H.uv2, 2, aP.FLOAT, false, 0, 0);
                        aP.enableVertexAttribArray(H.uv2)
                    } else {
                        aP.disableVertexAttribArray(H.uv2)
                    }
                }
                if (B.skinning && H.skinVertexA >= 0 && H.skinVertexB >= 0 && H.skinIndex >= 0 && H.skinWeight >= 0) {
                    aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglSkinVertexABuffer);
                    aP.vertexAttribPointer(H.skinVertexA, 4, aP.FLOAT, false, 0, 0);
                    aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglSkinVertexBBuffer);
                    aP.vertexAttribPointer(H.skinVertexB, 4, aP.FLOAT, false, 0, 0);
                    aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglSkinIndicesBuffer);
                    aP.vertexAttribPointer(H.skinIndex, 4, aP.FLOAT, false, 0, 0);
                    aP.bindBuffer(aP.ARRAY_BUFFER, A.__webglSkinWeightsBuffer);
                    aP.vertexAttribPointer(H.skinWeight, 4, aP.FLOAT, false, 0, 0)
                }
            }
            if (y instanceof THREE.Mesh) {
                if (B.wireframe) {
                    B = B.wireframeLinewidth;
                    if (B !== az) {
                        aP.lineWidth(B);
                        az = B
                    }
                    I && aP.bindBuffer(aP.ELEMENT_ARRAY_BUFFER, A.__webglLineBuffer);
                    aP.drawElements(aP.LINES, A.__webglLineCount, aP.UNSIGNED_SHORT, 0)
                } else {
                    I && aP.bindBuffer(aP.ELEMENT_ARRAY_BUFFER, A.__webglFaceBuffer);
                    aP.drawElements(aP.TRIANGLES, A.__webglFaceCount, aP.UNSIGNED_SHORT, 0)
                }
                ac.info.render.calls++;
                ac.info.render.vertices = ac.info.render.vertices + A.__webglFaceCount;
                ac.info.render.faces = ac.info.render.faces + A.__webglFaceCount / 3
            } else {
                if (y instanceof THREE.Line) {
                    y = y.type === THREE.LineStrip ? aP.LINE_STRIP : aP.LINES;
                    B = B.linewidth;
                    if (B !== az) {
                        aP.lineWidth(B);
                        az = B
                    }
                    aP.drawArrays(y, 0, A.__webglLineCount);
                    ac.info.render.calls++
                }
            }
        }
    }
    ;
    this.render = function(I, H, B, A) {
        var s, p, l, i, k = I.__lights, h = I.fog;
        ar = -1;
        ax = true;
        if (H.parent === void 0) {
            if (logger) console.warn("DEPRECATED: Camera hasn't been added to a Scene. Adding it...");
            I.add(H)
        }
        this.autoUpdateScene && I.updateMatrixWorld();
        if (!H._viewMatrixArray) {
            H._viewMatrixArray = new Float32Array(16)
        }
        if (!H._projectionMatrixArray) {
            H._projectionMatrixArray = new Float32Array(16)
        }
        H.matrixWorldInverse.getInverse(H.matrixWorld);
        H.matrixWorldInverse.flattenToArray(H._viewMatrixArray);
        H.projectionMatrix.flattenToArray(H._projectionMatrixArray);
        x.multiply(H.projectionMatrix, H.matrixWorldInverse);
        aQ.setFromMatrix(x);
        this.autoUpdateObjects && this.initWebGLObjects(I);
        aM(this.renderPluginsPre, I, H);
        ac.info.render.calls = 0;
        ac.info.render.vertices = 0;
        ac.info.render.faces = 0;
        ac.info.render.points = 0;
        this.setRenderTarget(B);
        (this.autoClear || A) && this.clear(this.autoClearColor, this.autoClearDepth, this.autoClearStencil);
        i = I.__webglObjects;
        A = 0;
        for (s = i.length; A < s; A++) {
            p = i[A];
            l = p.object;
            p.render = false;
            if (l.visible && (!(l instanceof THREE.Mesh || l instanceof THREE.ParticleSystem) || !l.frustumCulled || aQ.contains(l))) {
                aB(l, H);
                var g = p
                  , K = g.object
                  , D = g.buffer
                  , J = void 0
                  , J = J = void 0
                  , J = K.material;
                if (J instanceof THREE.MeshFaceMaterial) {
                    J = D.materialIndex;
                    if (J >= 0) {
                        J = K.geometry.materials[J];
                        if (J.transparent) {
                            g.transparent = J;
                            g.opaque = null
                        } else {
                            g.opaque = J;
                            g.transparent = null
                        }
                    }
                } else {
                    if (J) {
                        if (J.transparent) {
                            g.transparent = J;
                            g.opaque = null
                        } else {
                            g.opaque = J;
                            g.transparent = null
                        }
                    }
                }
                p.render = true;
                if (this.sortObjects) {
                    if (l.renderDepth) {
                        p.z = l.renderDepth
                    } else {
                        U.copy(l.matrixWorld.getPosition());
                        x.multiplyVector3(U);
                        p.z = U.z
                    }
                }
            }
        }
        this.sortObjects && i.sort(aO);
        i = I.__webglObjectsImmediate;
        A = 0;
        for (s = i.length; A < s; A++) {
            p = i[A];
            l = p.object;
            if (l.visible) {
                aB(l, H);
                l = p.object.material;
                if (l.transparent) {
                    p.transparent = l;
                    p.opaque = null
                } else {
                    p.opaque = l;
                    p.transparent = null
                }
            }
        }
        if (I.overrideMaterial) {
            A = I.overrideMaterial;
            this.setBlending(A.blending, A.blendEquation, A.blendSrc, A.blendDst);
            this.setDepthTest(A.depthTest);
            this.setDepthWrite(A.depthWrite);
            aF(A.polygonOffset, A.polygonOffsetFactor, A.polygonOffsetUnits);
            aJ(I.__webglObjects, false, "", H, k, h, true, A);
            aK(I.__webglObjectsImmediate, "", H, k, h, false, A)
        } else {
            this.setBlending(THREE.NormalBlending);
            aJ(I.__webglObjects, true, "opaque", H, k, h, false);
            aK(I.__webglObjectsImmediate, "opaque", H, k, h, false);
            aJ(I.__webglObjects, false, "transparent", H, k, h, true);
            aK(I.__webglObjectsImmediate, "transparent", H, k, h, true)
        }
        aM(this.renderPluginsPost, I, H);
        if (B && B.generateMipmaps && B.minFilter !== THREE.NearestFilter && B.minFilter !== THREE.LinearFilter) {
            if (B instanceof THREE.WebGLRenderTargetCube) {
                aP.bindTexture(aP.TEXTURE_CUBE_MAP, B.__webglTexture);
                aP.generateMipmap(aP.TEXTURE_CUBE_MAP);
                aP.bindTexture(aP.TEXTURE_CUBE_MAP, null)
            } else {
                aP.bindTexture(aP.TEXTURE_2D, B.__webglTexture);
                aP.generateMipmap(aP.TEXTURE_2D);
                aP.bindTexture(aP.TEXTURE_2D, null)
            }
        }
        this.setDepthTest(true);
        this.setDepthWrite(true)
    }
    ;
    this.renderImmediateObject = function(h, g, l, k, j) {
        var i = aG(h, g, l, k, j);
        E = -1;
        ac.setObjectFaces(j);
        j.immediateRenderCallback ? j.immediateRenderCallback(i, aP, aQ) : j.render(function(b) {
            ac.renderBufferImmediate(b, i, k.shading)
        })
    }
    ;
    this.initWebGLObjects = function(cU) {
        if (!cU.__webglObjects) {
            cU.__webglObjects = [];
            cU.__webglObjectsImmediate = [];
            cU.__webglSprites = [];
            cU.__webglFlares = []
        }
        for (; cU.__objectsAdded.length; ) {
            var cQ = cU.__objectsAdded[0]
              , cP = cU
              , cO = void 0
              , cN = void 0
              , cM = void 0;
            if (!cQ.__webglInit) {
                cQ.__webglInit = true;
                cQ._modelViewMatrix = new THREE.Matrix4;
                cQ._normalMatrix = new THREE.Matrix3;
                if (cQ instanceof THREE.Mesh) {
                    cN = cQ.geometry;
                    if (cN instanceof THREE.Geometry) {
                        if (cN.geometryGroups === void 0) {
                            var cJ = cN
                              , cL = void 0
                              , cG = void 0
                              , cE = void 0
                              , cC = void 0
                              , cA = void 0
                              , dx = void 0
                              , dw = void 0
                              , dp = {}
                              , dk = cJ.morphTargets.length
                              , c1 = cJ.morphNormals.length;
                            cJ.geometryGroups = {};
                            cL = 0;
                            for (cG = cJ.faces.length; cL < cG; cL++) {
                                cE = cJ.faces[cL];
                                cC = cE.materialIndex;
                                dx = cC !== void 0 ? cC : -1;
                                dp[dx] === void 0 && (dp[dx] = {
                                    hash: dx,
                                    counter: 0
                                });
                                dw = dp[dx].hash + "_" + dp[dx].counter;
                                cJ.geometryGroups[dw] === void 0 && (cJ.geometryGroups[dw] = {
                                    faces3: [],
                                    faces4: [],
                                    materialIndex: cC,
                                    vertices: 0,
                                    numMorphTargets: dk,
                                    numMorphNormals: c1
                                });
                                cA = cE instanceof THREE.Face3 ? 3 : 4;
                                if (cJ.geometryGroups[dw].vertices + cA > 65535) {
                                    dp[dx].counter = dp[dx].counter + 1;
                                    dw = dp[dx].hash + "_" + dp[dx].counter;
                                    cJ.geometryGroups[dw] === void 0 && (cJ.geometryGroups[dw] = {
                                        faces3: [],
                                        faces4: [],
                                        materialIndex: cC,
                                        vertices: 0,
                                        numMorphTargets: dk,
                                        numMorphNormals: c1
                                    })
                                }
                                cE instanceof THREE.Face3 ? cJ.geometryGroups[dw].faces3.push(cL) : cJ.geometryGroups[dw].faces4.push(cL);
                                cJ.geometryGroups[dw].vertices = cJ.geometryGroups[dw].vertices + cA
                            }
                            cJ.geometryGroupsList = [];
                            var dl = void 0;
                            for (dl in cJ.geometryGroups) {
                                cJ.geometryGroups[dl].id = t++;
                                cJ.geometryGroupsList.push(cJ.geometryGroups[dl])
                            }
                        }
                        for (cO in cN.geometryGroups) {
                            cM = cN.geometryGroups[cO];
                            if (!cM.__webglVertexBuffer) {
                                var dh = cM;
                                dh.__webglVertexBuffer = aP.createBuffer();
                                dh.__webglNormalBuffer = aP.createBuffer();
                                dh.__webglTangentBuffer = aP.createBuffer();
                                dh.__webglColorBuffer = aP.createBuffer();
                                dh.__webglUVBuffer = aP.createBuffer();
                                dh.__webglUV2Buffer = aP.createBuffer();
                                dh.__webglSkinVertexABuffer = aP.createBuffer();
                                dh.__webglSkinVertexBBuffer = aP.createBuffer();
                                dh.__webglSkinIndicesBuffer = aP.createBuffer();
                                dh.__webglSkinWeightsBuffer = aP.createBuffer();
                                dh.__webglFaceBuffer = aP.createBuffer();
                                dh.__webglLineBuffer = aP.createBuffer();
                                var dg = void 0
                                  , c9 = void 0;
                                if (dh.numMorphTargets) {
                                    dh.__webglMorphTargetsBuffers = [];
                                    dg = 0;
                                    for (c9 = dh.numMorphTargets; dg < c9; dg++) {
                                        dh.__webglMorphTargetsBuffers.push(aP.createBuffer())
                                    }
                                }
                                if (dh.numMorphNormals) {
                                    dh.__webglMorphNormalsBuffers = [];
                                    dg = 0;
                                    for (c9 = dh.numMorphNormals; dg < c9; dg++) {
                                        dh.__webglMorphNormalsBuffers.push(aP.createBuffer())
                                    }
                                }
                                ac.info.memory.geometries++;
                                var dm = cM
                                  , ds = cQ
                                  , bf = ds.geometry
                                  , cT = dm.faces3
                                  , dO = dm.faces4
                                  , de = cT.length * 3 + dO.length * 4
                                  , dK = cT.length * 1 + dO.length * 2
                                  , cz = cT.length * 3 + dO.length * 4
                                  , d0 = aU(ds, dm)
                                  , bT = aS(d0)
                                  , c = aT(d0)
                                  , bp = d0.vertexColors ? d0.vertexColors : false;
                                dm.__vertexArray = new Float32Array(de * 3);
                                if (c) {
                                    dm.__normalArray = new Float32Array(de * 3)
                                }
                                if (bf.hasTangents) {
                                    dm.__tangentArray = new Float32Array(de * 4)
                                }
                                if (bp) {
                                    dm.__colorArray = new Float32Array(de * 3)
                                }
                                if (bT) {
                                    if (bf.faceUvs.length > 0 || bf.faceVertexUvs.length > 0) {
                                        dm.__uvArray = new Float32Array(de * 2)
                                    }
                                    if (bf.faceUvs.length > 1 || bf.faceVertexUvs.length > 1) {
                                        dm.__uv2Array = new Float32Array(de * 2)
                                    }
                                }
                                if (ds.geometry.skinWeights.length && ds.geometry.skinIndices.length) {
                                    dm.__skinVertexAArray = new Float32Array(de * 4);
                                    dm.__skinVertexBArray = new Float32Array(de * 4);
                                    dm.__skinIndexArray = new Float32Array(de * 4);
                                    dm.__skinWeightArray = new Float32Array(de * 4)
                                }
                                dm.__faceArray = new Uint16Array(dK * 3);
                                dm.__lineArray = new Uint16Array(cz * 2);
                                var bV = void 0
                                  , bB = void 0;
                                if (dm.numMorphTargets) {
                                    dm.__morphTargetsArrays = [];
                                    bV = 0;
                                    for (bB = dm.numMorphTargets; bV < bB; bV++) {
                                        dm.__morphTargetsArrays.push(new Float32Array(de * 3))
                                    }
                                }
                                if (dm.numMorphNormals) {
                                    dm.__morphNormalsArrays = [];
                                    bV = 0;
                                    for (bB = dm.numMorphNormals; bV < bB; bV++) {
                                        dm.__morphNormalsArrays.push(new Float32Array(de * 3))
                                    }
                                }
                                dm.__webglFaceCount = dK * 3;
                                dm.__webglLineCount = cz * 2;
                                if (d0.attributes) {
                                    if (dm.__webglCustomAttributesList === void 0) {
                                        dm.__webglCustomAttributesList = []
                                    }
                                    var d3 = void 0;
                                    for (d3 in d0.attributes) {
                                        var ck = d0.attributes[d3], b6 = {}, bH;
                                        for (bH in ck) {
                                            b6[bH] = ck[bH]
                                        }
                                        if (!b6.__webglInitialized || b6.createUniqueBuffers) {
                                            b6.__webglInitialized = true;
                                            var dR = 1;
                                            b6.type === "v2" ? dR = 2 : b6.type === "v3" ? dR = 3 : b6.type === "v4" ? dR = 4 : b6.type === "c" && (dR = 3);
                                            b6.size = dR;
                                            b6.array = new Float32Array(de * dR);
                                            b6.buffer = aP.createBuffer();
                                            b6.buffer.belongsToAttribute = d3;
                                            ck.needsUpdate = true;
                                            b6.__original = ck
                                        }
                                        dm.__webglCustomAttributesList.push(b6)
                                    }
                                }
                                dm.__inittedArrays = true;
                                cN.verticesNeedUpdate = true;
                                cN.morphTargetsNeedUpdate = true;
                                cN.elementsNeedUpdate = true;
                                cN.uvsNeedUpdate = true;
                                cN.normalsNeedUpdate = true;
                                cN.tangetsNeedUpdate = true;
                                cN.colorsNeedUpdate = true
                            }
                        }
                    }
                } else {
                    if (cQ instanceof THREE.Line) {
                        cN = cQ.geometry;
                        if (!cN.__webglVertexBuffer) {
                            var dY = cN;
                            dY.__webglVertexBuffer = aP.createBuffer();
                            dY.__webglColorBuffer = aP.createBuffer();
                            ac.info.memory.geometries++;
                            var a4 = cN
                              , b8 = cQ
                              , cm = a4.vertices.length;
                            a4.__vertexArray = new Float32Array(cm * 3);
                            a4.__colorArray = new Float32Array(cm * 3);
                            a4.__webglLineCount = cm;
                            aV(a4, b8);
                            cN.verticesNeedUpdate = true;
                            cN.colorsNeedUpdate = true
                        }
                    } else {
                        if (cQ instanceof THREE.ParticleSystem) {
                            cN = cQ.geometry;
                            if (!cN.__webglVertexBuffer) {
                                var bn = cN;
                                bn.__webglVertexBuffer = aP.createBuffer();
                                bn.__webglColorBuffer = aP.createBuffer();
                                ac.info.geometries++;
                                var b5 = cN
                                  , dG = cQ
                                  , a8 = b5.vertices.length;
                                b5.__vertexArray = new Float32Array(a8 * 3);
                                b5.__colorArray = new Float32Array(a8 * 3);
                                b5.__sortArray = [];
                                b5.__webglParticleCount = a8;
                                aV(b5, dG);
                                cN.verticesNeedUpdate = true;
                                cN.colorsNeedUpdate = true
                            }
                        }
                    }
                }
            }
            if (!cQ.__webglActive) {
                if (cQ instanceof THREE.Mesh) {
                    cN = cQ.geometry;
                    if (cN instanceof THREE.BufferGeometry) {
                        aL(cP.__webglObjects, cN, cQ)
                    } else {
                        for (cO in cN.geometryGroups) {
                            cM = cN.geometryGroups[cO];
                            aL(cP.__webglObjects, cM, cQ)
                        }
                    }
                } else {
                    if (cQ instanceof THREE.Line) {
                        cN = cQ.geometry;
                        aL(cP.__webglObjects, cN, cQ)
                    }
                }
                cQ.__webglActive = true
            }
            cU.__objectsAdded.splice(0, 1)
        }
        for (; cU.__objectsRemoved.length; ) {
            var dS = cU.__objectsRemoved[0];
            if (dS instanceof THREE.Mesh || dS instanceof THREE.Line) {
                for (var D = cU.__webglObjects, co = dS, dD = D.length - 1; dD >= 0; dD--) {
                    D[dD].object === co && D.splice(dD, 1)
                }
            }
            dS.__webglActive = false;
            cU.__objectsRemoved.splice(0, 1)
        }
        for (var d2 = 0, cd = cU.__webglObjects.length; d2 < cd; d2++) {
            var dd = cU.__webglObjects[d2].object
              , c4 = dd.geometry
              , dy = void 0
              , bR = void 0
              , a9 = void 0;
            if (dd instanceof THREE.Mesh) {
                if (c4 instanceof THREE.BufferGeometry) {
                    c4.verticesNeedUpdate = false;
                    c4.elementsNeedUpdate = false;
                    c4.uvsNeedUpdate = false;
                    c4.normalsNeedUpdate = false;
                    c4.colorsNeedUpdate = false
                } else {
                    for (var dI = 0, bd = c4.geometryGroupsList.length; dI < bd; dI++) {
                        dy = c4.geometryGroupsList[dI];
                        a9 = aU(dd, dy);
                        bR = a9.attributes && aI(a9);
                        if (c4.verticesNeedUpdate || c4.morphTargetsNeedUpdate || c4.elementsNeedUpdate || c4.uvsNeedUpdate || c4.normalsNeedUpdate || c4.colorsNeedUpdate || c4.tangetsNeedUpdate || bR) {
                            var di = dy
                              , aY = dd
                              , dC = aP.DYNAMIC_DRAW
                              , d4 = !c4.dynamic
                              , dQ = a9;
                            if (di.__inittedArrays) {
                                var c8 = aT(dQ)
                                  , bY = dQ.vertexColors ? dQ.vertexColors : false
                                  , cI = aS(dQ)
                                  , cY = c8 === THREE.SmoothShading
                                  , cD = void 0
                                  , dv = void 0
                                  , g = void 0
                                  , cy = void 0
                                  , bF = void 0
                                  , bU = void 0
                                  , dH = void 0
                                  , cw = void 0
                                  , b = void 0
                                  , bt = void 0
                                  , bh = void 0
                                  , du = void 0
                                  , dt = void 0
                                  , dr = void 0
                                  , c3 = void 0
                                  , cK = void 0
                                  , cp = void 0
                                  , ce = void 0
                                  , dL = void 0
                                  , bX = void 0
                                  , dz = void 0
                                  , dM = void 0
                                  , dn = void 0
                                  , dq = void 0
                                  , cS = void 0
                                  , cr = void 0
                                  , cR = void 0
                                  , cg = void 0
                                  , b2 = void 0
                                  , bO = void 0
                                  , cq = void 0
                                  , bC = void 0
                                  , bq = void 0
                                  , be = void 0
                                  , cf = void 0
                                  , bI = void 0
                                  , bw = void 0
                                  , bk = void 0
                                  , cl = void 0
                                  , a5 = void 0
                                  , j = void 0
                                  , dZ = void 0
                                  , b7 = void 0
                                  , c7 = void 0
                                  , bW = void 0
                                  , dJ = void 0
                                  , a2 = void 0
                                  , e = void 0
                                  , bJ = void 0
                                  , dA = void 0
                                  , cn = void 0
                                  , b9 = void 0
                                  , bZ = void 0
                                  , dT = void 0
                                  , a0 = 0
                                  , c0 = 0
                                  , dE = 0
                                  , cZ = 0
                                  , bv = 0
                                  , m = 0
                                  , c2 = 0
                                  , b0 = 0
                                  , dU = 0
                                  , cB = 0
                                  , dj = 0
                                  , cF = 0
                                  , cX = void 0
                                  , bx = di.__vertexArray
                                  , b1 = di.__uvArray
                                  , bN = di.__uv2Array
                                  , bj = di.__normalArray
                                  , cs = di.__tangentArray
                                  , bl = di.__colorArray
                                  , ch = di.__skinVertexAArray
                                  , b3 = di.__skinVertexBArray
                                  , bP = di.__skinIndexArray
                                  , bD = di.__skinWeightArray
                                  , bK = di.__morphTargetsArrays
                                  , by = di.__morphNormalsArrays
                                  , bm = di.__webglCustomAttributesList
                                  , cH = void 0
                                  , aZ = di.__faceArray
                                  , cv = di.__lineArray
                                  , bM = aY.geometry
                                  , dX = bM.elementsNeedUpdate
                                  , cV = bM.uvsNeedUpdate
                                  , bQ = bM.normalsNeedUpdate
                                  , bE = bM.tangetsNeedUpdate
                                  , bs = bM.colorsNeedUpdate
                                  , bg = bM.morphTargetsNeedUpdate
                                  , dB = bM.vertices
                                  , c6 = di.faces3
                                  , c5 = di.faces4
                                  , dF = bM.faces
                                  , a7 = bM.faceVertexUvs[0]
                                  , n = bM.faceVertexUvs[1]
                                  , cW = bM.skinVerticesA
                                  , cu = bM.skinVerticesB
                                  , cj = bM.skinIndices
                                  , bL = bM.skinWeights
                                  , bz = bM.morphTargets
                                  , bS = bM.morphNormals;
                                if (bM.verticesNeedUpdate) {
                                    cD = 0;
                                    for (dv = c6.length; cD < dv; cD++) {
                                        cy = dF[c6[cD]];
                                        du = dB[cy.a];
                                        dt = dB[cy.b];
                                        dr = dB[cy.c];
                                        bx[c0] = du.x;
                                        bx[c0 + 1] = du.y;
                                        bx[c0 + 2] = du.z;
                                        bx[c0 + 3] = dt.x;
                                        bx[c0 + 4] = dt.y;
                                        bx[c0 + 5] = dt.z;
                                        bx[c0 + 6] = dr.x;
                                        bx[c0 + 7] = dr.y;
                                        bx[c0 + 8] = dr.z;
                                        c0 = c0 + 9
                                    }
                                    cD = 0;
                                    for (dv = c5.length; cD < dv; cD++) {
                                        cy = dF[c5[cD]];
                                        du = dB[cy.a];
                                        dt = dB[cy.b];
                                        dr = dB[cy.c];
                                        c3 = dB[cy.d];
                                        bx[c0] = du.x;
                                        bx[c0 + 1] = du.y;
                                        bx[c0 + 2] = du.z;
                                        bx[c0 + 3] = dt.x;
                                        bx[c0 + 4] = dt.y;
                                        bx[c0 + 5] = dt.z;
                                        bx[c0 + 6] = dr.x;
                                        bx[c0 + 7] = dr.y;
                                        bx[c0 + 8] = dr.z;
                                        bx[c0 + 9] = c3.x;
                                        bx[c0 + 10] = c3.y;
                                        bx[c0 + 11] = c3.z;
                                        c0 = c0 + 12
                                    }
                                    aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglVertexBuffer);
                                    aP.bufferData(aP.ARRAY_BUFFER, bx, dC)
                                }
                                if (bg) {
                                    bJ = 0;
                                    for (dA = bz.length; bJ < dA; bJ++) {
                                        cD = dj = 0;
                                        for (dv = c6.length; cD < dv; cD++) {
                                            bZ = c6[cD];
                                            cy = dF[bZ];
                                            du = bz[bJ].vertices[cy.a];
                                            dt = bz[bJ].vertices[cy.b];
                                            dr = bz[bJ].vertices[cy.c];
                                            cn = bK[bJ];
                                            cn[dj] = du.x;
                                            cn[dj + 1] = du.y;
                                            cn[dj + 2] = du.z;
                                            cn[dj + 3] = dt.x;
                                            cn[dj + 4] = dt.y;
                                            cn[dj + 5] = dt.z;
                                            cn[dj + 6] = dr.x;
                                            cn[dj + 7] = dr.y;
                                            cn[dj + 8] = dr.z;
                                            if (dQ.morphNormals) {
                                                if (cY) {
                                                    dT = bS[bJ].vertexNormals[bZ];
                                                    bX = dT.a;
                                                    dz = dT.b;
                                                    dM = dT.c
                                                } else {
                                                    dM = dz = bX = bS[bJ].faceNormals[bZ]
                                                }
                                                b9 = by[bJ];
                                                b9[dj] = bX.x;
                                                b9[dj + 1] = bX.y;
                                                b9[dj + 2] = bX.z;
                                                b9[dj + 3] = dz.x;
                                                b9[dj + 4] = dz.y;
                                                b9[dj + 5] = dz.z;
                                                b9[dj + 6] = dM.x;
                                                b9[dj + 7] = dM.y;
                                                b9[dj + 8] = dM.z
                                            }
                                            dj = dj + 9
                                        }
                                        cD = 0;
                                        for (dv = c5.length; cD < dv; cD++) {
                                            bZ = c5[cD];
                                            cy = dF[bZ];
                                            du = bz[bJ].vertices[cy.a];
                                            dt = bz[bJ].vertices[cy.b];
                                            dr = bz[bJ].vertices[cy.c];
                                            c3 = bz[bJ].vertices[cy.d];
                                            cn = bK[bJ];
                                            cn[dj] = du.x;
                                            cn[dj + 1] = du.y;
                                            cn[dj + 2] = du.z;
                                            cn[dj + 3] = dt.x;
                                            cn[dj + 4] = dt.y;
                                            cn[dj + 5] = dt.z;
                                            cn[dj + 6] = dr.x;
                                            cn[dj + 7] = dr.y;
                                            cn[dj + 8] = dr.z;
                                            cn[dj + 9] = c3.x;
                                            cn[dj + 10] = c3.y;
                                            cn[dj + 11] = c3.z;
                                            if (dQ.morphNormals) {
                                                if (cY) {
                                                    dT = bS[bJ].vertexNormals[bZ];
                                                    bX = dT.a;
                                                    dz = dT.b;
                                                    dM = dT.c;
                                                    dn = dT.d
                                                } else {
                                                    dn = dM = dz = bX = bS[bJ].faceNormals[bZ]
                                                }
                                                b9 = by[bJ];
                                                b9[dj] = bX.x;
                                                b9[dj + 1] = bX.y;
                                                b9[dj + 2] = bX.z;
                                                b9[dj + 3] = dz.x;
                                                b9[dj + 4] = dz.y;
                                                b9[dj + 5] = dz.z;
                                                b9[dj + 6] = dM.x;
                                                b9[dj + 7] = dM.y;
                                                b9[dj + 8] = dM.z;
                                                b9[dj + 9] = dn.x;
                                                b9[dj + 10] = dn.y;
                                                b9[dj + 11] = dn.z
                                            }
                                            dj = dj + 12
                                        }
                                        aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglMorphTargetsBuffers[bJ]);
                                        aP.bufferData(aP.ARRAY_BUFFER, bK[bJ], dC);
                                        if (dQ.morphNormals) {
                                            aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglMorphNormalsBuffers[bJ]);
                                            aP.bufferData(aP.ARRAY_BUFFER, by[bJ], dC)
                                        }
                                    }
                                }
                                if (bL.length) {
                                    cD = 0;
                                    for (dv = c6.length; cD < dv; cD++) {
                                        cy = dF[c6[cD]];
                                        cg = bL[cy.a];
                                        b2 = bL[cy.b];
                                        bO = bL[cy.c];
                                        bD[cB] = cg.x;
                                        bD[cB + 1] = cg.y;
                                        bD[cB + 2] = cg.z;
                                        bD[cB + 3] = cg.w;
                                        bD[cB + 4] = b2.x;
                                        bD[cB + 5] = b2.y;
                                        bD[cB + 6] = b2.z;
                                        bD[cB + 7] = b2.w;
                                        bD[cB + 8] = bO.x;
                                        bD[cB + 9] = bO.y;
                                        bD[cB + 10] = bO.z;
                                        bD[cB + 11] = bO.w;
                                        bC = cj[cy.a];
                                        bq = cj[cy.b];
                                        be = cj[cy.c];
                                        bP[cB] = bC.x;
                                        bP[cB + 1] = bC.y;
                                        bP[cB + 2] = bC.z;
                                        bP[cB + 3] = bC.w;
                                        bP[cB + 4] = bq.x;
                                        bP[cB + 5] = bq.y;
                                        bP[cB + 6] = bq.z;
                                        bP[cB + 7] = bq.w;
                                        bP[cB + 8] = be.x;
                                        bP[cB + 9] = be.y;
                                        bP[cB + 10] = be.z;
                                        bP[cB + 11] = be.w;
                                        bI = cW[cy.a];
                                        bw = cW[cy.b];
                                        bk = cW[cy.c];
                                        ch[cB] = bI.x;
                                        ch[cB + 1] = bI.y;
                                        ch[cB + 2] = bI.z;
                                        ch[cB + 3] = 1;
                                        ch[cB + 4] = bw.x;
                                        ch[cB + 5] = bw.y;
                                        ch[cB + 6] = bw.z;
                                        ch[cB + 7] = 1;
                                        ch[cB + 8] = bk.x;
                                        ch[cB + 9] = bk.y;
                                        ch[cB + 10] = bk.z;
                                        ch[cB + 11] = 1;
                                        a5 = cu[cy.a];
                                        j = cu[cy.b];
                                        dZ = cu[cy.c];
                                        b3[cB] = a5.x;
                                        b3[cB + 1] = a5.y;
                                        b3[cB + 2] = a5.z;
                                        b3[cB + 3] = 1;
                                        b3[cB + 4] = j.x;
                                        b3[cB + 5] = j.y;
                                        b3[cB + 6] = j.z;
                                        b3[cB + 7] = 1;
                                        b3[cB + 8] = dZ.x;
                                        b3[cB + 9] = dZ.y;
                                        b3[cB + 10] = dZ.z;
                                        b3[cB + 11] = 1;
                                        cB = cB + 12
                                    }
                                    cD = 0;
                                    for (dv = c5.length; cD < dv; cD++) {
                                        cy = dF[c5[cD]];
                                        cg = bL[cy.a];
                                        b2 = bL[cy.b];
                                        bO = bL[cy.c];
                                        cq = bL[cy.d];
                                        bD[cB] = cg.x;
                                        bD[cB + 1] = cg.y;
                                        bD[cB + 2] = cg.z;
                                        bD[cB + 3] = cg.w;
                                        bD[cB + 4] = b2.x;
                                        bD[cB + 5] = b2.y;
                                        bD[cB + 6] = b2.z;
                                        bD[cB + 7] = b2.w;
                                        bD[cB + 8] = bO.x;
                                        bD[cB + 9] = bO.y;
                                        bD[cB + 10] = bO.z;
                                        bD[cB + 11] = bO.w;
                                        bD[cB + 12] = cq.x;
                                        bD[cB + 13] = cq.y;
                                        bD[cB + 14] = cq.z;
                                        bD[cB + 15] = cq.w;
                                        bC = cj[cy.a];
                                        bq = cj[cy.b];
                                        be = cj[cy.c];
                                        cf = cj[cy.d];
                                        bP[cB] = bC.x;
                                        bP[cB + 1] = bC.y;
                                        bP[cB + 2] = bC.z;
                                        bP[cB + 3] = bC.w;
                                        bP[cB + 4] = bq.x;
                                        bP[cB + 5] = bq.y;
                                        bP[cB + 6] = bq.z;
                                        bP[cB + 7] = bq.w;
                                        bP[cB + 8] = be.x;
                                        bP[cB + 9] = be.y;
                                        bP[cB + 10] = be.z;
                                        bP[cB + 11] = be.w;
                                        bP[cB + 12] = cf.x;
                                        bP[cB + 13] = cf.y;
                                        bP[cB + 14] = cf.z;
                                        bP[cB + 15] = cf.w;
                                        bI = cW[cy.a];
                                        bw = cW[cy.b];
                                        bk = cW[cy.c];
                                        cl = cW[cy.d];
                                        ch[cB] = bI.x;
                                        ch[cB + 1] = bI.y;
                                        ch[cB + 2] = bI.z;
                                        ch[cB + 3] = 1;
                                        ch[cB + 4] = bw.x;
                                        ch[cB + 5] = bw.y;
                                        ch[cB + 6] = bw.z;
                                        ch[cB + 7] = 1;
                                        ch[cB + 8] = bk.x;
                                        ch[cB + 9] = bk.y;
                                        ch[cB + 10] = bk.z;
                                        ch[cB + 11] = 1;
                                        ch[cB + 12] = cl.x;
                                        ch[cB + 13] = cl.y;
                                        ch[cB + 14] = cl.z;
                                        ch[cB + 15] = 1;
                                        a5 = cu[cy.a];
                                        j = cu[cy.b];
                                        dZ = cu[cy.c];
                                        b7 = cu[cy.d];
                                        b3[cB] = a5.x;
                                        b3[cB + 1] = a5.y;
                                        b3[cB + 2] = a5.z;
                                        b3[cB + 3] = 1;
                                        b3[cB + 4] = j.x;
                                        b3[cB + 5] = j.y;
                                        b3[cB + 6] = j.z;
                                        b3[cB + 7] = 1;
                                        b3[cB + 8] = dZ.x;
                                        b3[cB + 9] = dZ.y;
                                        b3[cB + 10] = dZ.z;
                                        b3[cB + 11] = 1;
                                        b3[cB + 12] = b7.x;
                                        b3[cB + 13] = b7.y;
                                        b3[cB + 14] = b7.z;
                                        b3[cB + 15] = 1;
                                        cB = cB + 16
                                    }
                                    if (cB > 0) {
                                        aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglSkinVertexABuffer);
                                        aP.bufferData(aP.ARRAY_BUFFER, ch, dC);
                                        aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglSkinVertexBBuffer);
                                        aP.bufferData(aP.ARRAY_BUFFER, b3, dC);
                                        aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglSkinIndicesBuffer);
                                        aP.bufferData(aP.ARRAY_BUFFER, bP, dC);
                                        aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglSkinWeightsBuffer);
                                        aP.bufferData(aP.ARRAY_BUFFER, bD, dC)
                                    }
                                }
                                if (bs && bY) {
                                    cD = 0;
                                    for (dv = c6.length; cD < dv; cD++) {
                                        cy = dF[c6[cD]];
                                        dH = cy.vertexColors;
                                        cw = cy.color;
                                        if (dH.length === 3 && bY === THREE.VertexColors) {
                                            dq = dH[0];
                                            cS = dH[1];
                                            cr = dH[2]
                                        } else {
                                            cr = cS = dq = cw
                                        }
                                        bl[dU] = dq.r;
                                        bl[dU + 1] = dq.g;
                                        bl[dU + 2] = dq.b;
                                        bl[dU + 3] = cS.r;
                                        bl[dU + 4] = cS.g;
                                        bl[dU + 5] = cS.b;
                                        bl[dU + 6] = cr.r;
                                        bl[dU + 7] = cr.g;
                                        bl[dU + 8] = cr.b;
                                        dU = dU + 9
                                    }
                                    cD = 0;
                                    for (dv = c5.length; cD < dv; cD++) {
                                        cy = dF[c5[cD]];
                                        dH = cy.vertexColors;
                                        cw = cy.color;
                                        if (dH.length === 4 && bY === THREE.VertexColors) {
                                            dq = dH[0];
                                            cS = dH[1];
                                            cr = dH[2];
                                            cR = dH[3]
                                        } else {
                                            cR = cr = cS = dq = cw
                                        }
                                        bl[dU] = dq.r;
                                        bl[dU + 1] = dq.g;
                                        bl[dU + 2] = dq.b;
                                        bl[dU + 3] = cS.r;
                                        bl[dU + 4] = cS.g;
                                        bl[dU + 5] = cS.b;
                                        bl[dU + 6] = cr.r;
                                        bl[dU + 7] = cr.g;
                                        bl[dU + 8] = cr.b;
                                        bl[dU + 9] = cR.r;
                                        bl[dU + 10] = cR.g;
                                        bl[dU + 11] = cR.b;
                                        dU = dU + 12
                                    }
                                    if (dU > 0) {
                                        aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglColorBuffer);
                                        aP.bufferData(aP.ARRAY_BUFFER, bl, dC)
                                    }
                                }
                                if (bE && bM.hasTangents) {
                                    cD = 0;
                                    for (dv = c6.length; cD < dv; cD++) {
                                        cy = dF[c6[cD]];
                                        b = cy.vertexTangents;
                                        cK = b[0];
                                        cp = b[1];
                                        ce = b[2];
                                        cs[c2] = cK.x;
                                        cs[c2 + 1] = cK.y;
                                        cs[c2 + 2] = cK.z;
                                        cs[c2 + 3] = cK.w;
                                        cs[c2 + 4] = cp.x;
                                        cs[c2 + 5] = cp.y;
                                        cs[c2 + 6] = cp.z;
                                        cs[c2 + 7] = cp.w;
                                        cs[c2 + 8] = ce.x;
                                        cs[c2 + 9] = ce.y;
                                        cs[c2 + 10] = ce.z;
                                        cs[c2 + 11] = ce.w;
                                        c2 = c2 + 12
                                    }
                                    cD = 0;
                                    for (dv = c5.length; cD < dv; cD++) {
                                        cy = dF[c5[cD]];
                                        b = cy.vertexTangents;
                                        cK = b[0];
                                        cp = b[1];
                                        ce = b[2];
                                        dL = b[3];
                                        cs[c2] = cK.x;
                                        cs[c2 + 1] = cK.y;
                                        cs[c2 + 2] = cK.z;
                                        cs[c2 + 3] = cK.w;
                                        cs[c2 + 4] = cp.x;
                                        cs[c2 + 5] = cp.y;
                                        cs[c2 + 6] = cp.z;
                                        cs[c2 + 7] = cp.w;
                                        cs[c2 + 8] = ce.x;
                                        cs[c2 + 9] = ce.y;
                                        cs[c2 + 10] = ce.z;
                                        cs[c2 + 11] = ce.w;
                                        cs[c2 + 12] = dL.x;
                                        cs[c2 + 13] = dL.y;
                                        cs[c2 + 14] = dL.z;
                                        cs[c2 + 15] = dL.w;
                                        c2 = c2 + 16
                                    }
                                    aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglTangentBuffer);
                                    aP.bufferData(aP.ARRAY_BUFFER, cs, dC)
                                }
                                if (bQ && c8) {
                                    cD = 0;
                                    for (dv = c6.length; cD < dv; cD++) {
                                        cy = dF[c6[cD]];
                                        bF = cy.vertexNormals;
                                        bU = cy.normal;
                                        if (bF.length === 3 && cY) {
                                            for (c7 = 0; c7 < 3; c7++) {
                                                dJ = bF[c7];
                                                bj[m] = dJ.x;
                                                bj[m + 1] = dJ.y;
                                                bj[m + 2] = dJ.z;
                                                m = m + 3
                                            }
                                        } else {
                                            for (c7 = 0; c7 < 3; c7++) {
                                                bj[m] = bU.x;
                                                bj[m + 1] = bU.y;
                                                bj[m + 2] = bU.z;
                                                m = m + 3
                                            }
                                        }
                                    }
                                    cD = 0;
                                    for (dv = c5.length; cD < dv; cD++) {
                                        cy = dF[c5[cD]];
                                        bF = cy.vertexNormals;
                                        bU = cy.normal;
                                        if (bF.length === 4 && cY) {
                                            for (c7 = 0; c7 < 4; c7++) {
                                                dJ = bF[c7];
                                                bj[m] = dJ.x;
                                                bj[m + 1] = dJ.y;
                                                bj[m + 2] = dJ.z;
                                                m = m + 3
                                            }
                                        } else {
                                            for (c7 = 0; c7 < 4; c7++) {
                                                bj[m] = bU.x;
                                                bj[m + 1] = bU.y;
                                                bj[m + 2] = bU.z;
                                                m = m + 3
                                            }
                                        }
                                    }
                                    aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglNormalBuffer);
                                    aP.bufferData(aP.ARRAY_BUFFER, bj, dC)
                                }
                                if (cV && a7 && cI) {
                                    cD = 0;
                                    for (dv = c6.length; cD < dv; cD++) {
                                        g = c6[cD];
                                        cy = dF[g];
                                        bt = a7[g];
                                        if (bt !== void 0) {
                                            for (c7 = 0; c7 < 3; c7++) {
                                                a2 = bt[c7];
                                                b1[dE] = a2.u;
                                                b1[dE + 1] = a2.v;
                                                dE = dE + 2
                                            }
                                        }
                                    }
                                    cD = 0;
                                    for (dv = c5.length; cD < dv; cD++) {
                                        g = c5[cD];
                                        cy = dF[g];
                                        bt = a7[g];
                                        if (bt !== void 0) {
                                            for (c7 = 0; c7 < 4; c7++) {
                                                a2 = bt[c7];
                                                b1[dE] = a2.u;
                                                b1[dE + 1] = a2.v;
                                                dE = dE + 2
                                            }
                                        }
                                    }
                                    if (dE > 0) {
                                        aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglUVBuffer);
                                        aP.bufferData(aP.ARRAY_BUFFER, b1, dC)
                                    }
                                }
                                if (cV && n && cI) {
                                    cD = 0;
                                    for (dv = c6.length; cD < dv; cD++) {
                                        g = c6[cD];
                                        cy = dF[g];
                                        bh = n[g];
                                        if (bh !== void 0) {
                                            for (c7 = 0; c7 < 3; c7++) {
                                                e = bh[c7];
                                                bN[cZ] = e.u;
                                                bN[cZ + 1] = e.v;
                                                cZ = cZ + 2
                                            }
                                        }
                                    }
                                    cD = 0;
                                    for (dv = c5.length; cD < dv; cD++) {
                                        g = c5[cD];
                                        cy = dF[g];
                                        bh = n[g];
                                        if (bh !== void 0) {
                                            for (c7 = 0; c7 < 4; c7++) {
                                                e = bh[c7];
                                                bN[cZ] = e.u;
                                                bN[cZ + 1] = e.v;
                                                cZ = cZ + 2
                                            }
                                        }
                                    }
                                    if (cZ > 0) {
                                        aP.bindBuffer(aP.ARRAY_BUFFER, di.__webglUV2Buffer);
                                        aP.bufferData(aP.ARRAY_BUFFER, bN, dC)
                                    }
                                }
                                if (dX) {
                                    cD = 0;
                                    for (dv = c6.length; cD < dv; cD++) {
                                        cy = dF[c6[cD]];
                                        aZ[bv] = a0;
                                        aZ[bv + 1] = a0 + 1;
                                        aZ[bv + 2] = a0 + 2;
                                        bv = bv + 3;
                                        cv[b0] = a0;
                                        cv[b0 + 1] = a0 + 1;
                                        cv[b0 + 2] = a0;
                                        cv[b0 + 3] = a0 + 2;
                                        cv[b0 + 4] = a0 + 1;
                                        cv[b0 + 5] = a0 + 2;
                                        b0 = b0 + 6;
                                        a0 = a0 + 3
                                    }
                                    cD = 0;
                                    for (dv = c5.length; cD < dv; cD++) {
                                        cy = dF[c5[cD]];
                                        aZ[bv] = a0;
                                        aZ[bv + 1] = a0 + 1;
                                        aZ[bv + 2] = a0 + 3;
                                        aZ[bv + 3] = a0 + 1;
                                        aZ[bv + 4] = a0 + 2;
                                        aZ[bv + 5] = a0 + 3;
                                        bv = bv + 6;
                                        cv[b0] = a0;
                                        cv[b0 + 1] = a0 + 1;
                                        cv[b0 + 2] = a0;
                                        cv[b0 + 3] = a0 + 3;
                                        cv[b0 + 4] = a0 + 1;
                                        cv[b0 + 5] = a0 + 2;
                                        cv[b0 + 6] = a0 + 2;
                                        cv[b0 + 7] = a0 + 3;
                                        b0 = b0 + 8;
                                        a0 = a0 + 4
                                    }
                                    aP.bindBuffer(aP.ELEMENT_ARRAY_BUFFER, di.__webglFaceBuffer);
                                    aP.bufferData(aP.ELEMENT_ARRAY_BUFFER, aZ, dC);
                                    aP.bindBuffer(aP.ELEMENT_ARRAY_BUFFER, di.__webglLineBuffer);
                                    aP.bufferData(aP.ELEMENT_ARRAY_BUFFER, cv, dC)
                                }
                                if (bm) {
                                    c7 = 0;
                                    for (bW = bm.length; c7 < bW; c7++) {
                                        cH = bm[c7];
                                        if (cH.__original.needsUpdate) {
                                            cF = 0;
                                            if (cH.size === 1) {
                                                if (cH.boundTo === void 0 || cH.boundTo === "vertices") {
                                                    cD = 0;
                                                    for (dv = c6.length; cD < dv; cD++) {
                                                        cy = dF[c6[cD]];
                                                        cH.array[cF] = cH.value[cy.a];
                                                        cH.array[cF + 1] = cH.value[cy.b];
                                                        cH.array[cF + 2] = cH.value[cy.c];
                                                        cF = cF + 3
                                                    }
                                                    cD = 0;
                                                    for (dv = c5.length; cD < dv; cD++) {
                                                        cy = dF[c5[cD]];
                                                        cH.array[cF] = cH.value[cy.a];
                                                        cH.array[cF + 1] = cH.value[cy.b];
                                                        cH.array[cF + 2] = cH.value[cy.c];
                                                        cH.array[cF + 3] = cH.value[cy.d];
                                                        cF = cF + 4
                                                    }
                                                } else {
                                                    if (cH.boundTo === "faces") {
                                                        cD = 0;
                                                        for (dv = c6.length; cD < dv; cD++) {
                                                            cX = cH.value[c6[cD]];
                                                            cH.array[cF] = cX;
                                                            cH.array[cF + 1] = cX;
                                                            cH.array[cF + 2] = cX;
                                                            cF = cF + 3
                                                        }
                                                        cD = 0;
                                                        for (dv = c5.length; cD < dv; cD++) {
                                                            cX = cH.value[c5[cD]];
                                                            cH.array[cF] = cX;
                                                            cH.array[cF + 1] = cX;
                                                            cH.array[cF + 2] = cX;
                                                            cH.array[cF + 3] = cX;
                                                            cF = cF + 4
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (cH.size === 2) {
                                                    if (cH.boundTo === void 0 || cH.boundTo === "vertices") {
                                                        cD = 0;
                                                        for (dv = c6.length; cD < dv; cD++) {
                                                            cy = dF[c6[cD]];
                                                            du = cH.value[cy.a];
                                                            dt = cH.value[cy.b];
                                                            dr = cH.value[cy.c];
                                                            cH.array[cF] = du.x;
                                                            cH.array[cF + 1] = du.y;
                                                            cH.array[cF + 2] = dt.x;
                                                            cH.array[cF + 3] = dt.y;
                                                            cH.array[cF + 4] = dr.x;
                                                            cH.array[cF + 5] = dr.y;
                                                            cF = cF + 6
                                                        }
                                                        cD = 0;
                                                        for (dv = c5.length; cD < dv; cD++) {
                                                            cy = dF[c5[cD]];
                                                            du = cH.value[cy.a];
                                                            dt = cH.value[cy.b];
                                                            dr = cH.value[cy.c];
                                                            c3 = cH.value[cy.d];
                                                            cH.array[cF] = du.x;
                                                            cH.array[cF + 1] = du.y;
                                                            cH.array[cF + 2] = dt.x;
                                                            cH.array[cF + 3] = dt.y;
                                                            cH.array[cF + 4] = dr.x;
                                                            cH.array[cF + 5] = dr.y;
                                                            cH.array[cF + 6] = c3.x;
                                                            cH.array[cF + 7] = c3.y;
                                                            cF = cF + 8
                                                        }
                                                    } else {
                                                        if (cH.boundTo === "faces") {
                                                            cD = 0;
                                                            for (dv = c6.length; cD < dv; cD++) {
                                                                dr = dt = du = cX = cH.value[c6[cD]];
                                                                cH.array[cF] = du.x;
                                                                cH.array[cF + 1] = du.y;
                                                                cH.array[cF + 2] = dt.x;
                                                                cH.array[cF + 3] = dt.y;
                                                                cH.array[cF + 4] = dr.x;
                                                                cH.array[cF + 5] = dr.y;
                                                                cF = cF + 6
                                                            }
                                                            cD = 0;
                                                            for (dv = c5.length; cD < dv; cD++) {
                                                                c3 = dr = dt = du = cX = cH.value[c5[cD]];
                                                                cH.array[cF] = du.x;
                                                                cH.array[cF + 1] = du.y;
                                                                cH.array[cF + 2] = dt.x;
                                                                cH.array[cF + 3] = dt.y;
                                                                cH.array[cF + 4] = dr.x;
                                                                cH.array[cF + 5] = dr.y;
                                                                cH.array[cF + 6] = c3.x;
                                                                cH.array[cF + 7] = c3.y;
                                                                cF = cF + 8
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (cH.size === 3) {
                                                        var df;
                                                        df = cH.type === "c" ? ["r", "g", "b"] : ["x", "y", "z"];
                                                        if (cH.boundTo === void 0 || cH.boundTo === "vertices") {
                                                            cD = 0;
                                                            for (dv = c6.length; cD < dv; cD++) {
                                                                cy = dF[c6[cD]];
                                                                du = cH.value[cy.a];
                                                                dt = cH.value[cy.b];
                                                                dr = cH.value[cy.c];
                                                                cH.array[cF] = du[df[0]];
                                                                cH.array[cF + 1] = du[df[1]];
                                                                cH.array[cF + 2] = du[df[2]];
                                                                cH.array[cF + 3] = dt[df[0]];
                                                                cH.array[cF + 4] = dt[df[1]];
                                                                cH.array[cF + 5] = dt[df[2]];
                                                                cH.array[cF + 6] = dr[df[0]];
                                                                cH.array[cF + 7] = dr[df[1]];
                                                                cH.array[cF + 8] = dr[df[2]];
                                                                cF = cF + 9
                                                            }
                                                            cD = 0;
                                                            for (dv = c5.length; cD < dv; cD++) {
                                                                cy = dF[c5[cD]];
                                                                du = cH.value[cy.a];
                                                                dt = cH.value[cy.b];
                                                                dr = cH.value[cy.c];
                                                                c3 = cH.value[cy.d];
                                                                cH.array[cF] = du[df[0]];
                                                                cH.array[cF + 1] = du[df[1]];
                                                                cH.array[cF + 2] = du[df[2]];
                                                                cH.array[cF + 3] = dt[df[0]];
                                                                cH.array[cF + 4] = dt[df[1]];
                                                                cH.array[cF + 5] = dt[df[2]];
                                                                cH.array[cF + 6] = dr[df[0]];
                                                                cH.array[cF + 7] = dr[df[1]];
                                                                cH.array[cF + 8] = dr[df[2]];
                                                                cH.array[cF + 9] = c3[df[0]];
                                                                cH.array[cF + 10] = c3[df[1]];
                                                                cH.array[cF + 11] = c3[df[2]];
                                                                cF = cF + 12
                                                            }
                                                        } else {
                                                            if (cH.boundTo === "faces") {
                                                                cD = 0;
                                                                for (dv = c6.length; cD < dv; cD++) {
                                                                    dr = dt = du = cX = cH.value[c6[cD]];
                                                                    cH.array[cF] = du[df[0]];
                                                                    cH.array[cF + 1] = du[df[1]];
                                                                    cH.array[cF + 2] = du[df[2]];
                                                                    cH.array[cF + 3] = dt[df[0]];
                                                                    cH.array[cF + 4] = dt[df[1]];
                                                                    cH.array[cF + 5] = dt[df[2]];
                                                                    cH.array[cF + 6] = dr[df[0]];
                                                                    cH.array[cF + 7] = dr[df[1]];
                                                                    cH.array[cF + 8] = dr[df[2]];
                                                                    cF = cF + 9
                                                                }
                                                                cD = 0;
                                                                for (dv = c5.length; cD < dv; cD++) {
                                                                    c3 = dr = dt = du = cX = cH.value[c5[cD]];
                                                                    cH.array[cF] = du[df[0]];
                                                                    cH.array[cF + 1] = du[df[1]];
                                                                    cH.array[cF + 2] = du[df[2]];
                                                                    cH.array[cF + 3] = dt[df[0]];
                                                                    cH.array[cF + 4] = dt[df[1]];
                                                                    cH.array[cF + 5] = dt[df[2]];
                                                                    cH.array[cF + 6] = dr[df[0]];
                                                                    cH.array[cF + 7] = dr[df[1]];
                                                                    cH.array[cF + 8] = dr[df[2]];
                                                                    cH.array[cF + 9] = c3[df[0]];
                                                                    cH.array[cF + 10] = c3[df[1]];
                                                                    cH.array[cF + 11] = c3[df[2]];
                                                                    cF = cF + 12
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if (cH.size === 4) {
                                                            if (cH.boundTo === void 0 || cH.boundTo === "vertices") {
                                                                cD = 0;
                                                                for (dv = c6.length; cD < dv; cD++) {
                                                                    cy = dF[c6[cD]];
                                                                    du = cH.value[cy.a];
                                                                    dt = cH.value[cy.b];
                                                                    dr = cH.value[cy.c];
                                                                    cH.array[cF] = du.x;
                                                                    cH.array[cF + 1] = du.y;
                                                                    cH.array[cF + 2] = du.z;
                                                                    cH.array[cF + 3] = du.w;
                                                                    cH.array[cF + 4] = dt.x;
                                                                    cH.array[cF + 5] = dt.y;
                                                                    cH.array[cF + 6] = dt.z;
                                                                    cH.array[cF + 7] = dt.w;
                                                                    cH.array[cF + 8] = dr.x;
                                                                    cH.array[cF + 9] = dr.y;
                                                                    cH.array[cF + 10] = dr.z;
                                                                    cH.array[cF + 11] = dr.w;
                                                                    cF = cF + 12
                                                                }
                                                                cD = 0;
                                                                for (dv = c5.length; cD < dv; cD++) {
                                                                    cy = dF[c5[cD]];
                                                                    du = cH.value[cy.a];
                                                                    dt = cH.value[cy.b];
                                                                    dr = cH.value[cy.c];
                                                                    c3 = cH.value[cy.d];
                                                                    cH.array[cF] = du.x;
                                                                    cH.array[cF + 1] = du.y;
                                                                    cH.array[cF + 2] = du.z;
                                                                    cH.array[cF + 3] = du.w;
                                                                    cH.array[cF + 4] = dt.x;
                                                                    cH.array[cF + 5] = dt.y;
                                                                    cH.array[cF + 6] = dt.z;
                                                                    cH.array[cF + 7] = dt.w;
                                                                    cH.array[cF + 8] = dr.x;
                                                                    cH.array[cF + 9] = dr.y;
                                                                    cH.array[cF + 10] = dr.z;
                                                                    cH.array[cF + 11] = dr.w;
                                                                    cH.array[cF + 12] = c3.x;
                                                                    cH.array[cF + 13] = c3.y;
                                                                    cH.array[cF + 14] = c3.z;
                                                                    cH.array[cF + 15] = c3.w;
                                                                    cF = cF + 16
                                                                }
                                                            } else {
                                                                if (cH.boundTo === "faces") {
                                                                    cD = 0;
                                                                    for (dv = c6.length; cD < dv; cD++) {
                                                                        dr = dt = du = cX = cH.value[c6[cD]];
                                                                        cH.array[cF] = du.x;
                                                                        cH.array[cF + 1] = du.y;
                                                                        cH.array[cF + 2] = du.z;
                                                                        cH.array[cF + 3] = du.w;
                                                                        cH.array[cF + 4] = dt.x;
                                                                        cH.array[cF + 5] = dt.y;
                                                                        cH.array[cF + 6] = dt.z;
                                                                        cH.array[cF + 7] = dt.w;
                                                                        cH.array[cF + 8] = dr.x;
                                                                        cH.array[cF + 9] = dr.y;
                                                                        cH.array[cF + 10] = dr.z;
                                                                        cH.array[cF + 11] = dr.w;
                                                                        cF = cF + 12
                                                                    }
                                                                    cD = 0;
                                                                    for (dv = c5.length; cD < dv; cD++) {
                                                                        c3 = dr = dt = du = cX = cH.value[c5[cD]];
                                                                        cH.array[cF] = du.x;
                                                                        cH.array[cF + 1] = du.y;
                                                                        cH.array[cF + 2] = du.z;
                                                                        cH.array[cF + 3] = du.w;
                                                                        cH.array[cF + 4] = dt.x;
                                                                        cH.array[cF + 5] = dt.y;
                                                                        cH.array[cF + 6] = dt.z;
                                                                        cH.array[cF + 7] = dt.w;
                                                                        cH.array[cF + 8] = dr.x;
                                                                        cH.array[cF + 9] = dr.y;
                                                                        cH.array[cF + 10] = dr.z;
                                                                        cH.array[cF + 11] = dr.w;
                                                                        cH.array[cF + 12] = c3.x;
                                                                        cH.array[cF + 13] = c3.y;
                                                                        cH.array[cF + 14] = c3.z;
                                                                        cH.array[cF + 15] = c3.w;
                                                                        cF = cF + 16
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            aP.bindBuffer(aP.ARRAY_BUFFER, cH.buffer);
                                            aP.bufferData(aP.ARRAY_BUFFER, cH.array, dC)
                                        }
                                    }
                                }
                                if (d4) {
                                    delete di.__inittedArrays;
                                    delete di.__colorArray;
                                    delete di.__normalArray;
                                    delete di.__tangentArray;
                                    delete di.__uvArray;
                                    delete di.__uv2Array;
                                    delete di.__faceArray;
                                    delete di.__vertexArray;
                                    delete di.__lineArray;
                                    delete di.__skinVertexAArray;
                                    delete di.__skinVertexBArray;
                                    delete di.__skinIndexArray;
                                    delete di.__skinWeightArray
                                }
                            }
                        }
                    }
                    c4.verticesNeedUpdate = false;
                    c4.morphTargetsNeedUpdate = false;
                    c4.elementsNeedUpdate = false;
                    c4.uvsNeedUpdate = false;
                    c4.normalsNeedUpdate = false;
                    c4.colorsNeedUpdate = false;
                    c4.tangetsNeedUpdate = false;
                    a9.attributes && aH(a9)
                }
            } else {
                if (dd instanceof THREE.Line) {
                    a9 = aU(dd, dy);
                    bR = a9.attributes && aI(a9);
                    if (c4.verticesNeedUpdate || c4.colorsNeedUpdate || bR) {
                        var cx = c4
                          , d1 = aP.DYNAMIC_DRAW
                          , bA = void 0
                          , bo = void 0
                          , bG = void 0
                          , br = void 0
                          , bu = void 0
                          , ct = cx.vertices
                          , ci = cx.colors
                          , a1 = ct.length
                          , d = ci.length
                          , bi = cx.__vertexArray
                          , a3 = cx.__colorArray
                          , dV = cx.colorsNeedUpdate
                          , dP = cx.__webglCustomAttributesList
                          , f = void 0
                          , b4 = void 0
                          , a6 = void 0
                          , dW = void 0
                          , Z = void 0
                          , dN = void 0;
                        if (cx.verticesNeedUpdate) {
                            for (bA = 0; bA < a1; bA++) {
                                bG = ct[bA];
                                br = bA * 3;
                                bi[br] = bG.x;
                                bi[br + 1] = bG.y;
                                bi[br + 2] = bG.z
                            }
                            aP.bindBuffer(aP.ARRAY_BUFFER, cx.__webglVertexBuffer);
                            aP.bufferData(aP.ARRAY_BUFFER, bi, d1)
                        }
                        if (dV) {
                            for (bo = 0; bo < d; bo++) {
                                bu = ci[bo];
                                br = bo * 3;
                                a3[br] = bu.r;
                                a3[br + 1] = bu.g;
                                a3[br + 2] = bu.b
                            }
                            aP.bindBuffer(aP.ARRAY_BUFFER, cx.__webglColorBuffer);
                            aP.bufferData(aP.ARRAY_BUFFER, a3, d1)
                        }
                        if (dP) {
                            f = 0;
                            for (b4 = dP.length; f < b4; f++) {
                                dN = dP[f];
                                if (dN.needsUpdate && (dN.boundTo === void 0 || dN.boundTo === "vertices")) {
                                    br = 0;
                                    dW = dN.value.length;
                                    if (dN.size === 1) {
                                        for (a6 = 0; a6 < dW; a6++) {
                                            dN.array[a6] = dN.value[a6]
                                        }
                                    } else {
                                        if (dN.size === 2) {
                                            for (a6 = 0; a6 < dW; a6++) {
                                                Z = dN.value[a6];
                                                dN.array[br] = Z.x;
                                                dN.array[br + 1] = Z.y;
                                                br = br + 2
                                            }
                                        } else {
                                            if (dN.size === 3) {
                                                if (dN.type === "c") {
                                                    for (a6 = 0; a6 < dW; a6++) {
                                                        Z = dN.value[a6];
                                                        dN.array[br] = Z.r;
                                                        dN.array[br + 1] = Z.g;
                                                        dN.array[br + 2] = Z.b;
                                                        br = br + 3
                                                    }
                                                } else {
                                                    for (a6 = 0; a6 < dW; a6++) {
                                                        Z = dN.value[a6];
                                                        dN.array[br] = Z.x;
                                                        dN.array[br + 1] = Z.y;
                                                        dN.array[br + 2] = Z.z;
                                                        br = br + 3
                                                    }
                                                }
                                            } else {
                                                if (dN.size === 4) {
                                                    for (a6 = 0; a6 < dW; a6++) {
                                                        Z = dN.value[a6];
                                                        dN.array[br] = Z.x;
                                                        dN.array[br + 1] = Z.y;
                                                        dN.array[br + 2] = Z.z;
                                                        dN.array[br + 3] = Z.w;
                                                        br = br + 4
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    aP.bindBuffer(aP.ARRAY_BUFFER, dN.buffer);
                                    aP.bufferData(aP.ARRAY_BUFFER, dN.array, d1)
                                }
                            }
                        }
                    }
                    c4.verticesNeedUpdate = false;
                    c4.colorsNeedUpdate = false;
                    a9.attributes && aH(a9)
                } else {
                    if (dd instanceof THREE.ParticleSystem) {
                        a9 = aU(dd, dy);
                        bR = a9.attributes && aI(a9);
                        (c4.verticesNeedUpdate || c4.colorsNeedUpdate || dd.sortParticles || bR) && aR(c4, aP.DYNAMIC_DRAW, dd);
                        c4.verticesNeedUpdate = false;
                        c4.colorsNeedUpdate = false;
                        a9.attributes && aH(a9)
                    }
                }
            }
        }
    }
    ;
    this.initMaterial = function(R, Q, O, N) {
        var K, J, I;
        R instanceof THREE.MeshDepthMaterial ? I = "depth" : R instanceof THREE.MeshNormalMaterial ? I = "normal" : R instanceof THREE.MeshBasicMaterial ? I = "basic" : R instanceof THREE.MeshLambertMaterial ? I = "lambert" : R instanceof THREE.MeshPhongMaterial ? I = "phong" : R instanceof THREE.LineBasicMaterial ? I = "basic" : R instanceof THREE.ParticleBasicMaterial && (I = "particle_basic");
        if (I) {
            var H = THREE.ShaderLib[I];
            R.uniforms = THREE.UniformsUtils.clone(H.uniforms);
            R.vertexShader = H.vertexShader;
            R.fragmentShader = H.fragmentShader
        }
        var B, D, A, w, y;
        B = w = y = H = 0;
        for (D = Q.length; B < D; B++) {
            A = Q[B];
            if (!A.onlyShadow) {
                A instanceof THREE.DirectionalLight && w++;
                A instanceof THREE.PointLight && y++;
                A instanceof THREE.SpotLight && H++
            }
        }
        if (y + H + w <= am) {
            D = w;
            A = y;
            w = H
        } else {
            D = Math.ceil(am * w / (y + w));
            w = A = am - D
        }
        var q = 0
          , H = 0;
        for (y = Q.length; H < y; H++) {
            B = Q[H];
            if (B.castShadow) {
                B instanceof THREE.SpotLight && q++;
                B instanceof THREE.DirectionalLight && !B.shadowCascade && q++
            }
        }
        var g;
        R: {
            y = R.fragmentShader;
            B = R.vertexShader;
            var H = R.uniforms, Q = R.attributes, O = {
                map: !!R.map,
                envMap: !!R.envMap,
                lightMap: !!R.lightMap,
                vertexColors: R.vertexColors,
                fog: O,
                useFog: R.fog,
                sizeAttenuation: R.sizeAttenuation,
                skinning: R.skinning,
                maxBones: 50,
                morphTargets: R.morphTargets,
                morphNormals: R.morphNormals,
                maxMorphTargets: this.maxMorphTargets,
                maxMorphNormals: this.maxMorphNormals,
                maxDirLights: D,
                maxPointLights: A,
                maxSpotLights: w,
                maxShadows: q,
                shadowMapEnabled: this.shadowMapEnabled && N.receiveShadow,
                shadowMapSoft: this.shadowMapSoft,
                shadowMapDebug: this.shadowMapDebug,
                shadowMapCascade: this.shadowMapCascade,
                alphaTest: R.alphaTest,
                metal: R.metal,
                perPixel: R.perPixel,
                wrapAround: R.wrapAround,
                doubleSided: N && N.doubleSided
            }, Z, N = [];
            if (I) {
                N.push(I)
            } else {
                N.push(y);
                N.push(B)
            }
            for (Z in O) {
                N.push(Z);
                N.push(O[Z])
            }
            I = N.join();
            Z = 0;
            for (N = an.length; Z < N; Z++) {
                if (an[Z].code === I) {
                    g = an[Z].program;
                    break R
                }
            }
            Z = aP.createProgram();
            N = ["precision " + ad + " float;", aq > 0 ? "#define VERTEX_TEXTURES" : "", ac.gammaInput ? "#define GAMMA_INPUT" : "", ac.gammaOutput ? "#define GAMMA_OUTPUT" : "", ac.physicallyBasedShading ? "#define PHYSICALLY_BASED_SHADING" : "", "#define MAX_DIR_LIGHTS " + O.maxDirLights, "#define MAX_POINT_LIGHTS " + O.maxPointLights, "#define MAX_SPOT_LIGHTS " + O.maxSpotLights, "#define MAX_SHADOWS " + O.maxShadows, "#define MAX_BONES " + O.maxBones, O.map ? "#define USE_MAP" : "", O.envMap ? "#define USE_ENVMAP" : "", O.lightMap ? "#define USE_LIGHTMAP" : "", O.vertexColors ? "#define USE_COLOR" : "", O.skinning ? "#define USE_SKINNING" : "", O.morphTargets ? "#define USE_MORPHTARGETS" : "", O.morphNormals ? "#define USE_MORPHNORMALS" : "", O.perPixel ? "#define PHONG_PER_PIXEL" : "", O.wrapAround ? "#define WRAP_AROUND" : "", O.doubleSided ? "#define DOUBLE_SIDED" : "", O.shadowMapEnabled ? "#define USE_SHADOWMAP" : "", O.shadowMapSoft ? "#define SHADOWMAP_SOFT" : "", O.shadowMapDebug ? "#define SHADOWMAP_DEBUG" : "", O.shadowMapCascade ? "#define SHADOWMAP_CASCADE" : "", O.sizeAttenuation ? "#define USE_SIZEATTENUATION" : "", "uniform mat4 objectMatrix;\nuniform mat4 modelViewMatrix;\nuniform mat4 projectionMatrix;\nuniform mat4 viewMatrix;\nuniform mat3 normalMatrix;\nuniform vec3 cameraPosition;\nattribute vec3 position;\nattribute vec3 normal;\nattribute vec2 uv;\nattribute vec2 uv2;\n#ifdef USE_COLOR\nattribute vec3 color;\n#endif\n#ifdef USE_MORPHTARGETS\nattribute vec3 morphTarget0;\nattribute vec3 morphTarget1;\nattribute vec3 morphTarget2;\nattribute vec3 morphTarget3;\n#ifdef USE_MORPHNORMALS\nattribute vec3 morphNormal0;\nattribute vec3 morphNormal1;\nattribute vec3 morphNormal2;\nattribute vec3 morphNormal3;\n#else\nattribute vec3 morphTarget4;\nattribute vec3 morphTarget5;\nattribute vec3 morphTarget6;\nattribute vec3 morphTarget7;\n#endif\n#endif\n#ifdef USE_SKINNING\nattribute vec4 skinVertexA;\nattribute vec4 skinVertexB;\nattribute vec4 skinIndex;\nattribute vec4 skinWeight;\n#endif\n"].join("\n");
            D = ["precision " + ad + " float;", "#define MAX_DIR_LIGHTS " + O.maxDirLights, "#define MAX_POINT_LIGHTS " + O.maxPointLights, "#define MAX_SPOT_LIGHTS " + O.maxSpotLights, "#define MAX_SHADOWS " + O.maxShadows, O.alphaTest ? "#define ALPHATEST " + O.alphaTest : "", ac.gammaInput ? "#define GAMMA_INPUT" : "", ac.gammaOutput ? "#define GAMMA_OUTPUT" : "", ac.physicallyBasedShading ? "#define PHYSICALLY_BASED_SHADING" : "", O.useFog && O.fog ? "#define USE_FOG" : "", O.useFog && O.fog instanceof THREE.FogExp2 ? "#define FOG_EXP2" : "", O.map ? "#define USE_MAP" : "", O.envMap ? "#define USE_ENVMAP" : "", O.lightMap ? "#define USE_LIGHTMAP" : "", O.vertexColors ? "#define USE_COLOR" : "", O.metal ? "#define METAL" : "", O.perPixel ? "#define PHONG_PER_PIXEL" : "", O.wrapAround ? "#define WRAP_AROUND" : "", O.doubleSided ? "#define DOUBLE_SIDED" : "", O.shadowMapEnabled ? "#define USE_SHADOWMAP" : "", O.shadowMapSoft ? "#define SHADOWMAP_SOFT" : "", O.shadowMapDebug ? "#define SHADOWMAP_DEBUG" : "", O.shadowMapCascade ? "#define SHADOWMAP_CASCADE" : "", "uniform mat4 viewMatrix;\nuniform vec3 cameraPosition;\n"].join("\n");
            aP.attachShader(Z, aE("fragment", D + y));
            aP.attachShader(Z, aE("vertex", N + B));
            aP.linkProgram(Z);
            aP.getProgramParameter(Z, aP.LINK_STATUS) || console.error("Could not initialise shader\nVALIDATE_STATUS: " + aP.getProgramParameter(Z, aP.VALIDATE_STATUS) + ", gl error [" + aP.getError() + "]");
            Z.uniforms = {};
            Z.attributes = {};
            var Y, N = ["viewMatrix", "modelViewMatrix", "projectionMatrix", "normalMatrix", "objectMatrix", "cameraPosition", "boneGlobalMatrices", "morphTargetInfluences"];
            for (Y in H) {
                N.push(Y)
            }
            Y = N;
            N = 0;
            for (H = Y.length; N < H; N++) {
                y = Y[N];
                Z.uniforms[y] = aP.getUniformLocation(Z, y)
            }
            N = ["position", "normal", "uv", "uv2", "tangent", "color", "skinVertexA", "skinVertexB", "skinIndex", "skinWeight"];
            for (Y = 0; Y < O.maxMorphTargets; Y++) {
                N.push("morphTarget" + Y)
            }
            for (Y = 0; Y < O.maxMorphNormals; Y++) {
                N.push("morphNormal" + Y)
            }
            for (g in Q) {
                N.push(g)
            }
            g = N;
            Y = 0;
            for (Q = g.length; Y < Q; Y++) {
                O = g[Y];
                Z.attributes[O] = aP.getAttribLocation(Z, O)
            }
            Z.id = an.length;
            an.push({
                program: Z,
                code: I
            });
            ac.info.memory.programs = an.length;
            g = Z
        }
        R.program = g;
        g = R.program.attributes;
        g.position >= 0 && aP.enableVertexAttribArray(g.position);
        g.color >= 0 && aP.enableVertexAttribArray(g.color);
        g.normal >= 0 && aP.enableVertexAttribArray(g.normal);
        g.tangent >= 0 && aP.enableVertexAttribArray(g.tangent);
        if (R.skinning && g.skinVertexA >= 0 && g.skinVertexB >= 0 && g.skinIndex >= 0 && g.skinWeight >= 0) {
            aP.enableVertexAttribArray(g.skinVertexA);
            aP.enableVertexAttribArray(g.skinVertexB);
            aP.enableVertexAttribArray(g.skinIndex);
            aP.enableVertexAttribArray(g.skinWeight)
        }
        if (R.attributes) {
            for (J in R.attributes) {
                g[J] !== void 0 && g[J] >= 0 && aP.enableVertexAttribArray(g[J])
            }
        }
        if (R.morphTargets) {
            R.numSupportedMorphTargets = 0;
            Z = "morphTarget";
            for (J = 0; J < this.maxMorphTargets; J++) {
                Y = Z + J;
                if (g[Y] >= 0) {
                    aP.enableVertexAttribArray(g[Y]);
                    R.numSupportedMorphTargets++
                }
            }
        }
        if (R.morphNormals) {
            R.numSupportedMorphNormals = 0;
            Z = "morphNormal";
            for (J = 0; J < this.maxMorphNormals; J++) {
                Y = Z + J;
                if (g[Y] >= 0) {
                    aP.enableVertexAttribArray(g[Y]);
                    R.numSupportedMorphNormals++
                }
            }
        }
        R.uniformsList = [];
        for (K in R.uniforms) {
            R.uniformsList.push([R.uniforms[K], K])
        }
    }
    ;
    this.setFaceCulling = function(d, c) {
        if (d) {
            !c || c === "ccw" ? aP.frontFace(aP.CCW) : aP.frontFace(aP.CW);
            d === "back" ? aP.cullFace(aP.BACK) : d === "front" ? aP.cullFace(aP.FRONT) : aP.cullFace(aP.FRONT_AND_BACK);
            aP.enable(aP.CULL_FACE)
        } else {
            aP.disable(aP.CULL_FACE)
        }
    }
    ;
    this.setObjectFaces = function(b) {
        if (F !== b.doubleSided) {
            b.doubleSided ? aP.disable(aP.CULL_FACE) : aP.enable(aP.CULL_FACE);
            F = b.doubleSided
        }
        if (ah !== b.flipSided) {
            b.flipSided ? aP.frontFace(aP.CW) : aP.frontFace(aP.CCW);
            ah = b.flipSided
        }
    }
    ;
    this.setDepthTest = function(b) {
        if (M !== b) {
            b ? aP.enable(aP.DEPTH_TEST) : aP.disable(aP.DEPTH_TEST);
            M = b
        }
    }
    ;
    this.setDepthWrite = function(b) {
        if (S !== b) {
            aP.depthMask(b);
            S = b
        }
    }
    ;
    this.setBlending = function(f, e, h, g) {
        if (f !== G) {
            switch (f) {
            case THREE.NoBlending:
                aP.disable(aP.BLEND);
                break;
            case THREE.AdditiveBlending:
                aP.enable(aP.BLEND);
                aP.blendEquation(aP.FUNC_ADD);
                aP.blendFunc(aP.SRC_ALPHA, aP.ONE);
                break;
            case THREE.SubtractiveBlending:
                aP.enable(aP.BLEND);
                aP.blendEquation(aP.FUNC_ADD);
                aP.blendFunc(aP.ZERO, aP.ONE_MINUS_SRC_COLOR);
                break;
            case THREE.MultiplyBlending:
                aP.enable(aP.BLEND);
                aP.blendEquation(aP.FUNC_ADD);
                aP.blendFunc(aP.ZERO, aP.SRC_COLOR);
                break;
            case THREE.CustomBlending:
                aP.enable(aP.BLEND);
                break;
            default:
                aP.enable(aP.BLEND);
                aP.blendEquationSeparate(aP.FUNC_ADD, aP.FUNC_ADD);
                aP.blendFuncSeparate(aP.SRC_ALPHA, aP.ONE_MINUS_SRC_ALPHA, aP.ONE, aP.ONE_MINUS_SRC_ALPHA)
            }
            G = f
        }
        if (f === THREE.CustomBlending) {
            if (e !== aj) {
                aP.blendEquation(ay(e));
                aj = e
            }
            if (h !== aD || g !== al) {
                aP.blendFunc(ay(h), ay(g));
                aD = h;
                al = g
            }
        } else {
            al = aD = aj = null
        }
    }
    ;
    this.setTexture = function(h, g) {
        if (h.needsUpdate) {
            if (!h.__webglInit) {
                h.__webglInit = true;
                h.__webglTexture = aP.createTexture();
                ac.info.memory.textures++
            }
            aP.activeTexture(aP.TEXTURE0 + g);
            aP.bindTexture(aP.TEXTURE_2D, h.__webglTexture);
            aP.pixelStorei(aP.UNPACK_PREMULTIPLY_ALPHA_WEBGL, h.premultiplyAlpha);
            var l = h.image
              , k = (l.width & l.width - 1) === 0 && (l.height & l.height - 1) === 0
              , j = ay(h.format)
              , i = ay(h.type);
            aw(aP.TEXTURE_2D, h, k);
            h instanceof THREE.DataTexture ? aP.texImage2D(aP.TEXTURE_2D, 0, j, l.width, l.height, 0, j, i, l.data) : aP.texImage2D(aP.TEXTURE_2D, 0, j, j, i, h.image);
            h.generateMipmaps && k && aP.generateMipmap(aP.TEXTURE_2D);
            h.needsUpdate = false;
            if (h.onUpdate) {
                h.onUpdate()
            }
        } else {
            aP.activeTexture(aP.TEXTURE0 + g);
            aP.bindTexture(aP.TEXTURE_2D, h.__webglTexture)
        }
    }
    ;
    this.setRenderTarget = function(j) {
        var g = j instanceof THREE.WebGLRenderTargetCube;
        if (j && !j.__webglFramebuffer) {
            if (j.depthBuffer === void 0) {
                j.depthBuffer = true
            }
            if (j.stencilBuffer === void 0) {
                j.stencilBuffer = true
            }
            j.__webglTexture = aP.createTexture();
            var p = (j.width & j.width - 1) === 0 && (j.height & j.height - 1) === 0
              , o = ay(j.format)
              , n = ay(j.type);
            if (g) {
                j.__webglFramebuffer = [];
                j.__webglRenderbuffer = [];
                aP.bindTexture(aP.TEXTURE_CUBE_MAP, j.__webglTexture);
                aw(aP.TEXTURE_CUBE_MAP, j, p);
                for (var m = 0; m < 6; m++) {
                    j.__webglFramebuffer[m] = aP.createFramebuffer();
                    j.__webglRenderbuffer[m] = aP.createRenderbuffer();
                    aP.texImage2D(aP.TEXTURE_CUBE_MAP_POSITIVE_X + m, 0, o, j.width, j.height, 0, o, n, null);
                    var l = j
                      , k = aP.TEXTURE_CUBE_MAP_POSITIVE_X + m;
                    aP.bindFramebuffer(aP.FRAMEBUFFER, j.__webglFramebuffer[m]);
                    aP.framebufferTexture2D(aP.FRAMEBUFFER, aP.COLOR_ATTACHMENT0, k, l.__webglTexture, 0);
                    ae(j.__webglRenderbuffer[m], j)
                }
                p && aP.generateMipmap(aP.TEXTURE_CUBE_MAP)
            } else {
                j.__webglFramebuffer = aP.createFramebuffer();
                j.__webglRenderbuffer = aP.createRenderbuffer();
                aP.bindTexture(aP.TEXTURE_2D, j.__webglTexture);
                aw(aP.TEXTURE_2D, j, p);
                aP.texImage2D(aP.TEXTURE_2D, 0, o, j.width, j.height, 0, o, n, null);
                o = aP.TEXTURE_2D;
                aP.bindFramebuffer(aP.FRAMEBUFFER, j.__webglFramebuffer);
                aP.framebufferTexture2D(aP.FRAMEBUFFER, aP.COLOR_ATTACHMENT0, o, j.__webglTexture, 0);
                ae(j.__webglRenderbuffer, j);
                p && aP.generateMipmap(aP.TEXTURE_2D)
            }
            g ? aP.bindTexture(aP.TEXTURE_CUBE_MAP, null) : aP.bindTexture(aP.TEXTURE_2D, null);
            aP.bindRenderbuffer(aP.RENDERBUFFER, null);
            aP.bindFramebuffer(aP.FRAMEBUFFER, null)
        }
        if (j) {
            g = g ? j.__webglFramebuffer[j.activeCubeFace] : j.__webglFramebuffer;
            p = j.width;
            j = j.height;
            n = o = 0
        } else {
            g = null;
            p = ab;
            j = aC;
            o = ai;
            n = C
        }
        if (g !== aA) {
            aP.bindFramebuffer(aP.FRAMEBUFFER, g);
            aP.viewport(o, n, p, j);
            aA = g
        }
        af = p;
        r = j
    }
}
;
THREE.WebGLRenderTarget = function(e, d, f) {
    this.width = e;
    this.height = d;
    f = f || {};
    this.wrapS = f.wrapS !== void 0 ? f.wrapS : THREE.ClampToEdgeWrapping;
    this.wrapT = f.wrapT !== void 0 ? f.wrapT : THREE.ClampToEdgeWrapping;
    this.magFilter = f.magFilter !== void 0 ? f.magFilter : THREE.LinearFilter;
    this.minFilter = f.minFilter !== void 0 ? f.minFilter : THREE.LinearMipMapLinearFilter;
    this.offset = new THREE.Vector2(0,0);
    this.repeat = new THREE.Vector2(1,1);
    this.format = f.format !== void 0 ? f.format : THREE.RGBAFormat;
    this.type = f.type !== void 0 ? f.type : THREE.UnsignedByteType;
    this.depthBuffer = f.depthBuffer !== void 0 ? f.depthBuffer : true;
    this.stencilBuffer = f.stencilBuffer !== void 0 ? f.stencilBuffer : true;
    this.generateMipmaps = true
}
;
THREE.WebGLRenderTarget.prototype.clone = function() {
    var b = new THREE.WebGLRenderTarget(this.width,this.height);
    b.wrapS = this.wrapS;
    b.wrapT = this.wrapT;
    b.magFilter = this.magFilter;
    b.minFilter = this.minFilter;
    b.offset.copy(this.offset);
    b.repeat.copy(this.repeat);
    b.format = this.format;
    b.type = this.type;
    b.depthBuffer = this.depthBuffer;
    b.stencilBuffer = this.stencilBuffer;
    return b
}
;
THREE.WebGLRenderTargetCube = function(e, d, f) {
    THREE.WebGLRenderTarget.call(this, e, d, f);
    this.activeCubeFace = 0
}
;
THREE.WebGLRenderTargetCube.prototype = new THREE.WebGLRenderTarget;
THREE.WebGLRenderTargetCube.prototype.constructor = THREE.WebGLRenderTargetCube;
THREE.RenderableVertex = function() {
    this.positionWorld = new THREE.Vector3;
    this.positionScreen = new THREE.Vector4;
    this.visible = true
}
;
THREE.RenderableVertex.prototype.copy = function(b) {
    this.positionWorld.copy(b.positionWorld);
    this.positionScreen.copy(b.positionScreen)
}
;
THREE.RenderableFace3 = function() {
    this.v1 = new THREE.RenderableVertex;
    this.v2 = new THREE.RenderableVertex;
    this.v3 = new THREE.RenderableVertex;
    this.centroidWorld = new THREE.Vector3;
    this.centroidScreen = new THREE.Vector3;
    this.normalWorld = new THREE.Vector3;
    this.vertexNormalsWorld = [new THREE.Vector3, new THREE.Vector3, new THREE.Vector3];
    this.faceMaterial = this.material = null;
    this.uvs = [[]];
    this.z = null
}
;
THREE.RenderableFace4 = function() {
    this.v1 = new THREE.RenderableVertex;
    this.v2 = new THREE.RenderableVertex;
    this.v3 = new THREE.RenderableVertex;
    this.v4 = new THREE.RenderableVertex;
    this.centroidWorld = new THREE.Vector3;
    this.centroidScreen = new THREE.Vector3;
    this.normalWorld = new THREE.Vector3;
    this.vertexNormalsWorld = [new THREE.Vector3, new THREE.Vector3, new THREE.Vector3, new THREE.Vector3];
    this.faceMaterial = this.material = null;
    this.uvs = [[]];
    this.z = null
}
;
THREE.RenderableObject = function() {
    this.z = this.object = null
}
;
THREE.RenderableLine = function() {
    this.z = null;
    this.v1 = new THREE.RenderableVertex;
    this.v2 = new THREE.RenderableVertex;
    this.material = null
}
;
THREE.GeometryUtils = {
    merge: function(N, M) {
        for (var L, K, J = N.vertices.length, I = M instanceof THREE.Mesh ? M.geometry : M, H = N.vertices, G = I.vertices, D = N.faces, E = I.faces, F = N.faceVertexUvs[0], C = I.faceVertexUvs[0], B = {}, z = 0; z < N.materials.length; z++) {
            B[N.materials[z].id] = z
        }
        if (M instanceof THREE.Mesh) {
            M.matrixAutoUpdate && M.updateMatrix();
            L = M.matrix;
            K = new THREE.Matrix4;
            K.extractRotation(L, M.scale)
        }
        for (var z = 0, t = G.length; z < t; z++) {
            var x = G[z].clone();
            L && L.multiplyVector3(x);
            H.push(x)
        }
        z = 0;
        for (t = E.length; z < t; z++) {
            var H = E[z], u, r, v = H.vertexNormals, g = H.vertexColors;
            H instanceof THREE.Face3 ? u = new THREE.Face3(H.a + J,H.b + J,H.c + J) : H instanceof THREE.Face4 && (u = new THREE.Face4(H.a + J,H.b + J,H.c + J,H.d + J));
            u.normal.copy(H.normal);
            K && K.multiplyVector3(u.normal);
            G = 0;
            for (x = v.length; G < x; G++) {
                r = v[G].clone();
                K && K.multiplyVector3(r);
                u.vertexNormals.push(r)
            }
            u.color.copy(H.color);
            G = 0;
            for (x = g.length; G < x; G++) {
                r = g[G];
                u.vertexColors.push(r.clone())
            }
            if (H.materialIndex !== void 0) {
                G = I.materials[H.materialIndex];
                x = G.id;
                g = B[x];
                if (g === void 0) {
                    g = N.materials.length;
                    B[x] = g;
                    N.materials.push(G)
                }
                u.materialIndex = g
            }
            u.centroid.copy(H.centroid);
            L && L.multiplyVector3(u.centroid);
            D.push(u)
        }
        z = 0;
        for (t = C.length; z < t; z++) {
            L = C[z];
            K = [];
            G = 0;
            for (x = L.length; G < x; G++) {
                K.push(new THREE.UV(L[G].u,L[G].v))
            }
            F.push(K)
        }
    },
    clone: function(j) {
        var g = new THREE.Geometry, p, o = j.vertices, n = j.faces, m = j.faceVertexUvs[0];
        if (j.materials) {
            g.materials = j.materials.slice()
        }
        j = 0;
        for (p = o.length; j < p; j++) {
            g.vertices.push(o[j].clone())
        }
        j = 0;
        for (p = n.length; j < p; j++) {
            g.faces.push(n[j].clone())
        }
        j = 0;
        for (p = m.length; j < p; j++) {
            for (var o = m[j], n = [], l = 0, k = o.length; l < k; l++) {
                n.push(new THREE.UV(o[l].u,o[l].v))
            }
            g.faceVertexUvs[0].push(n)
        }
        return g
    },
    randomPointInTriangle: function(j, g, p) {
        var o, n, m, l = new THREE.Vector3, k = THREE.GeometryUtils.__v1;
        o = THREE.GeometryUtils.random();
        n = THREE.GeometryUtils.random();
        if (o + n > 1) {
            o = 1 - o;
            n = 1 - n
        }
        m = 1 - o - n;
        l.copy(j);
        l.multiplyScalar(o);
        k.copy(g);
        k.multiplyScalar(n);
        l.addSelf(k);
        k.copy(p);
        k.multiplyScalar(m);
        l.addSelf(k);
        return l
    },
    randomPointInFace: function(i, g, n) {
        var m, l, k;
        if (i instanceof THREE.Face3) {
            m = g.vertices[i.a];
            l = g.vertices[i.b];
            k = g.vertices[i.c];
            return THREE.GeometryUtils.randomPointInTriangle(m, l, k)
        }
        if (i instanceof THREE.Face4) {
            m = g.vertices[i.a];
            l = g.vertices[i.b];
            k = g.vertices[i.c];
            var g = g.vertices[i.d], j;
            if (n) {
                if (i._area1 && i._area2) {
                    n = i._area1;
                    j = i._area2
                } else {
                    n = THREE.GeometryUtils.triangleArea(m, l, g);
                    j = THREE.GeometryUtils.triangleArea(l, k, g);
                    i._area1 = n;
                    i._area2 = j
                }
            } else {
                n = THREE.GeometryUtils.triangleArea(m, l, g);
                j = THREE.GeometryUtils.triangleArea(l, k, g)
            }
            return THREE.GeometryUtils.random() * (n + j) < n ? THREE.GeometryUtils.randomPointInTriangle(m, l, g) : THREE.GeometryUtils.randomPointInTriangle(l, k, g)
        }
    },
    randomPointsInGeometry: function(B, A) {
        function z(d) {
            function c(f, b) {
                if (b < f) {
                    return f
                }
                var a = f + Math.floor((b - f) / 2);
                return s[a] > d ? c(f, a - 1) : s[a] < d ? c(a + 1, b) : a
            }
            return c(0, s.length - 1)
        }
        var y, x, w = B.faces, v = B.vertices, u = w.length, r = 0, s = [], t, q, p, g;
        for (x = 0; x < u; x++) {
            y = w[x];
            if (y instanceof THREE.Face3) {
                t = v[y.a];
                q = v[y.b];
                p = v[y.c];
                y._area = THREE.GeometryUtils.triangleArea(t, q, p)
            } else {
                if (y instanceof THREE.Face4) {
                    t = v[y.a];
                    q = v[y.b];
                    p = v[y.c];
                    g = v[y.d];
                    y._area1 = THREE.GeometryUtils.triangleArea(t, q, g);
                    y._area2 = THREE.GeometryUtils.triangleArea(q, p, g);
                    y._area = y._area1 + y._area2
                }
            }
            r = r + y._area;
            s[x] = r
        }
        y = [];
        for (x = 0; x < A; x++) {
            v = THREE.GeometryUtils.random() * r;
            v = z(v);
            y[x] = THREE.GeometryUtils.randomPointInFace(w[v], B, true)
        }
        return y
    },
    triangleArea: function(g, f, j) {
        var i, h = THREE.GeometryUtils.__v1;
        h.sub(g, f);
        i = h.length();
        h.sub(g, j);
        g = h.length();
        h.sub(f, j);
        j = h.length();
        f = 0.5 * (i + g + j);
        return Math.sqrt(f * (f - i) * (f - g) * (f - j))
    },
    center: function(e) {
        e.computeBoundingBox();
        var d = e.boundingBox
          , f = new THREE.Vector3;
        f.add(d.min, d.max);
        f.multiplyScalar(-0.5);
        e.applyMatrix((new THREE.Matrix4).makeTranslation(f.x, f.y, f.z));
        e.computeBoundingBox();
        return f
    },
    normalizeUVs: function(h) {
        for (var h = h.faceVertexUvs[0], g = 0, l = h.length; g < l; g++) {
            for (var k = h[g], j = 0, i = k.length; j < i; j++) {
                if (k[j].u !== 1) {
                    k[j].u = k[j].u - Math.floor(k[j].u)
                }
                if (k[j].v !== 1) {
                    k[j].v = k[j].v - Math.floor(k[j].v)
                }
            }
        }
    },
    triangulateQuads: function(z) {
        var y, x, w, v, u = [], t = [], s = [];
        y = 0;
        for (x = z.faceUvs.length; y < x; y++) {
            t[y] = []
        }
        y = 0;
        for (x = z.faceVertexUvs.length; y < x; y++) {
            s[y] = []
        }
        y = 0;
        for (x = z.faces.length; y < x; y++) {
            w = z.faces[y];
            if (w instanceof THREE.Face4) {
                v = w.a;
                var p = w.b
                  , q = w.c
                  , r = w.d
                  , o = new THREE.Face3
                  , g = new THREE.Face3;
                o.color.copy(w.color);
                g.color.copy(w.color);
                o.materialIndex = w.materialIndex;
                g.materialIndex = w.materialIndex;
                o.a = v;
                o.b = p;
                o.c = r;
                g.a = p;
                g.b = q;
                g.c = r;
                if (w.vertexColors.length === 4) {
                    o.vertexColors[0] = w.vertexColors[0].clone();
                    o.vertexColors[1] = w.vertexColors[1].clone();
                    o.vertexColors[2] = w.vertexColors[3].clone();
                    g.vertexColors[0] = w.vertexColors[1].clone();
                    g.vertexColors[1] = w.vertexColors[2].clone();
                    g.vertexColors[2] = w.vertexColors[3].clone()
                }
                u.push(o, g);
                w = 0;
                for (v = z.faceVertexUvs.length; w < v; w++) {
                    if (z.faceVertexUvs[w].length) {
                        o = z.faceVertexUvs[w][y];
                        p = o[1];
                        q = o[2];
                        r = o[3];
                        o = [o[0].clone(), p.clone(), r.clone()];
                        p = [p.clone(), q.clone(), r.clone()];
                        s[w].push(o, p)
                    }
                }
                w = 0;
                for (v = z.faceUvs.length; w < v; w++) {
                    if (z.faceUvs[w].length) {
                        p = z.faceUvs[w][y];
                        t[w].push(p, p)
                    }
                }
            } else {
                u.push(w);
                w = 0;
                for (v = z.faceUvs.length; w < v; w++) {
                    t[w].push(z.faceUvs[w])
                }
                w = 0;
                for (v = z.faceVertexUvs.length; w < v; w++) {
                    s[w].push(z.faceVertexUvs[w])
                }
            }
        }
        z.faces = u;
        z.faceUvs = t;
        z.faceVertexUvs = s;
        z.computeCentroids();
        z.computeFaceNormals();
        z.computeVertexNormals();
        z.hasTangents && z.computeTangents()
    },
    explode: function(t) {
        for (var s = [], r = 0, q = t.faces.length; r < q; r++) {
            var p = s.length
              , o = t.faces[r];
            if (o instanceof THREE.Face4) {
                var n = o.a
                  , m = o.b
                  , g = o.c
                  , n = t.vertices[n]
                  , m = t.vertices[m]
                  , g = t.vertices[g]
                  , j = t.vertices[o.d];
                s.push(n.clone());
                s.push(m.clone());
                s.push(g.clone());
                s.push(j.clone());
                o.a = p;
                o.b = p + 1;
                o.c = p + 2;
                o.d = p + 3
            } else {
                n = o.a;
                m = o.b;
                g = o.c;
                n = t.vertices[n];
                m = t.vertices[m];
                g = t.vertices[g];
                s.push(n.clone());
                s.push(m.clone());
                s.push(g.clone());
                o.a = p;
                o.b = p + 1;
                o.c = p + 2
            }
        }
        t.vertices = s;
        delete t.__tmpVertices
    },
    tessellate: function(T, S) {
        var R, Q, P, O, N, M, J, K, L, I, G, F, x, E, C, t, D, g, v, r = [], z = [];
        R = 0;
        for (Q = T.faceVertexUvs.length; R < Q; R++) {
            z[R] = []
        }
        R = 0;
        for (Q = T.faces.length; R < Q; R++) {
            P = T.faces[R];
            if (P instanceof THREE.Face3) {
                O = P.a;
                N = P.b;
                M = P.c;
                K = T.vertices[O];
                L = T.vertices[N];
                I = T.vertices[M];
                F = K.distanceTo(L);
                x = L.distanceTo(I);
                G = K.distanceTo(I);
                if (F > S || x > S || G > S) {
                    J = T.vertices.length;
                    g = P.clone();
                    v = P.clone();
                    if (F >= x && F >= G) {
                        K = K.clone();
                        K.lerpSelf(L, 0.5);
                        g.a = O;
                        g.b = J;
                        g.c = M;
                        v.a = J;
                        v.b = N;
                        v.c = M;
                        if (P.vertexNormals.length === 3) {
                            O = P.vertexNormals[0].clone();
                            O.lerpSelf(P.vertexNormals[1], 0.5);
                            g.vertexNormals[1].copy(O);
                            v.vertexNormals[0].copy(O)
                        }
                        if (P.vertexColors.length === 3) {
                            O = P.vertexColors[0].clone();
                            O.lerpSelf(P.vertexColors[1], 0.5);
                            g.vertexColors[1].copy(O);
                            v.vertexColors[0].copy(O)
                        }
                        P = 0
                    } else {
                        if (x >= F && x >= G) {
                            K = L.clone();
                            K.lerpSelf(I, 0.5);
                            g.a = O;
                            g.b = N;
                            g.c = J;
                            v.a = J;
                            v.b = M;
                            v.c = O;
                            if (P.vertexNormals.length === 3) {
                                O = P.vertexNormals[1].clone();
                                O.lerpSelf(P.vertexNormals[2], 0.5);
                                g.vertexNormals[2].copy(O);
                                v.vertexNormals[0].copy(O);
                                v.vertexNormals[1].copy(P.vertexNormals[2]);
                                v.vertexNormals[2].copy(P.vertexNormals[0])
                            }
                            if (P.vertexColors.length === 3) {
                                O = P.vertexColors[1].clone();
                                O.lerpSelf(P.vertexColors[2], 0.5);
                                g.vertexColors[2].copy(O);
                                v.vertexColors[0].copy(O);
                                v.vertexColors[1].copy(P.vertexColors[2]);
                                v.vertexColors[2].copy(P.vertexColors[0])
                            }
                            P = 1
                        } else {
                            K = K.clone();
                            K.lerpSelf(I, 0.5);
                            g.a = O;
                            g.b = N;
                            g.c = J;
                            v.a = J;
                            v.b = N;
                            v.c = M;
                            if (P.vertexNormals.length === 3) {
                                O = P.vertexNormals[0].clone();
                                O.lerpSelf(P.vertexNormals[2], 0.5);
                                g.vertexNormals[2].copy(O);
                                v.vertexNormals[0].copy(O)
                            }
                            if (P.vertexColors.length === 3) {
                                O = P.vertexColors[0].clone();
                                O.lerpSelf(P.vertexColors[2], 0.5);
                                g.vertexColors[2].copy(O);
                                v.vertexColors[0].copy(O)
                            }
                            P = 2
                        }
                    }
                    r.push(g, v);
                    T.vertices.push(K);
                    O = 0;
                    for (N = T.faceVertexUvs.length; O < N; O++) {
                        if (T.faceVertexUvs[O].length) {
                            K = T.faceVertexUvs[O][R];
                            v = K[0];
                            M = K[1];
                            g = K[2];
                            if (P === 0) {
                                L = v.clone();
                                L.lerpSelf(M, 0.5);
                                K = [v.clone(), L.clone(), g.clone()];
                                M = [L.clone(), M.clone(), g.clone()]
                            } else {
                                if (P === 1) {
                                    L = M.clone();
                                    L.lerpSelf(g, 0.5);
                                    K = [v.clone(), M.clone(), L.clone()];
                                    M = [L.clone(), g.clone(), v.clone()]
                                } else {
                                    L = v.clone();
                                    L.lerpSelf(g, 0.5);
                                    K = [v.clone(), M.clone(), L.clone()];
                                    M = [L.clone(), M.clone(), g.clone()]
                                }
                            }
                            z[O].push(K, M)
                        }
                    }
                } else {
                    r.push(P);
                    O = 0;
                    for (N = T.faceVertexUvs.length; O < N; O++) {
                        z[O].push(T.faceVertexUvs[O][R])
                    }
                }
            } else {
                O = P.a;
                N = P.b;
                M = P.c;
                J = P.d;
                K = T.vertices[O];
                L = T.vertices[N];
                I = T.vertices[M];
                G = T.vertices[J];
                F = K.distanceTo(L);
                x = L.distanceTo(I);
                E = I.distanceTo(G);
                C = K.distanceTo(G);
                if (F > S || x > S || E > S || C > S) {
                    t = T.vertices.length;
                    D = T.vertices.length + 1;
                    g = P.clone();
                    v = P.clone();
                    if (F >= x && F >= E && F >= C || E >= x && E >= F && E >= C) {
                        F = K.clone();
                        F.lerpSelf(L, 0.5);
                        L = I.clone();
                        L.lerpSelf(G, 0.5);
                        g.a = O;
                        g.b = t;
                        g.c = D;
                        g.d = J;
                        v.a = t;
                        v.b = N;
                        v.c = M;
                        v.d = D;
                        if (P.vertexNormals.length === 4) {
                            O = P.vertexNormals[0].clone();
                            O.lerpSelf(P.vertexNormals[1], 0.5);
                            N = P.vertexNormals[2].clone();
                            N.lerpSelf(P.vertexNormals[3], 0.5);
                            g.vertexNormals[1].copy(O);
                            g.vertexNormals[2].copy(N);
                            v.vertexNormals[0].copy(O);
                            v.vertexNormals[3].copy(N)
                        }
                        if (P.vertexColors.length === 4) {
                            O = P.vertexColors[0].clone();
                            O.lerpSelf(P.vertexColors[1], 0.5);
                            N = P.vertexColors[2].clone();
                            N.lerpSelf(P.vertexColors[3], 0.5);
                            g.vertexColors[1].copy(O);
                            g.vertexColors[2].copy(N);
                            v.vertexColors[0].copy(O);
                            v.vertexColors[3].copy(N)
                        }
                        P = 0
                    } else {
                        F = L.clone();
                        F.lerpSelf(I, 0.5);
                        L = G.clone();
                        L.lerpSelf(K, 0.5);
                        g.a = O;
                        g.b = N;
                        g.c = t;
                        g.d = D;
                        v.a = D;
                        v.b = t;
                        v.c = M;
                        v.d = J;
                        if (P.vertexNormals.length === 4) {
                            O = P.vertexNormals[1].clone();
                            O.lerpSelf(P.vertexNormals[2], 0.5);
                            N = P.vertexNormals[3].clone();
                            N.lerpSelf(P.vertexNormals[0], 0.5);
                            g.vertexNormals[2].copy(O);
                            g.vertexNormals[3].copy(N);
                            v.vertexNormals[0].copy(N);
                            v.vertexNormals[1].copy(O)
                        }
                        if (P.vertexColors.length === 4) {
                            O = P.vertexColors[1].clone();
                            O.lerpSelf(P.vertexColors[2], 0.5);
                            N = P.vertexColors[3].clone();
                            N.lerpSelf(P.vertexColors[0], 0.5);
                            g.vertexColors[2].copy(O);
                            g.vertexColors[3].copy(N);
                            v.vertexColors[0].copy(N);
                            v.vertexColors[1].copy(O)
                        }
                        P = 1
                    }
                    r.push(g, v);
                    T.vertices.push(F, L);
                    O = 0;
                    for (N = T.faceVertexUvs.length; O < N; O++) {
                        if (T.faceVertexUvs[O].length) {
                            K = T.faceVertexUvs[O][R];
                            v = K[0];
                            M = K[1];
                            g = K[2];
                            K = K[3];
                            if (P === 0) {
                                L = v.clone();
                                L.lerpSelf(M, 0.5);
                                I = g.clone();
                                I.lerpSelf(K, 0.5);
                                v = [v.clone(), L.clone(), I.clone(), K.clone()];
                                M = [L.clone(), M.clone(), g.clone(), I.clone()]
                            } else {
                                L = M.clone();
                                L.lerpSelf(g, 0.5);
                                I = K.clone();
                                I.lerpSelf(v, 0.5);
                                v = [v.clone(), M.clone(), L.clone(), I.clone()];
                                M = [I.clone(), L.clone(), g.clone(), K.clone()]
                            }
                            z[O].push(v, M)
                        }
                    }
                } else {
                    r.push(P);
                    O = 0;
                    for (N = T.faceVertexUvs.length; O < N; O++) {
                        z[O].push(T.faceVertexUvs[O][R])
                    }
                }
            }
        }
        T.faces = r;
        T.faceVertexUvs = z
    }
};
THREE.GeometryUtils.random = THREE.Math.random16;
THREE.GeometryUtils.__v1 = new THREE.Vector3;
THREE.ImageUtils = {
    crossOrigin: "anonymous",
    loadTexture: function(g, f, j) {
        var i = new Image
          , h = new THREE.Texture(i,f);
        i.onload = function() {
            h.needsUpdate = true;
            j && j(this)
        }
        ;
        i.crossOrigin = this.crossOrigin;
        i.src = g;
        return h
    },
    loadTextureCube: function(h, g, l) {
        var k, j = [], i = new THREE.Texture(j,g), g = j.loadCount = 0;
        for (k = h.length; g < k; ++g) {
            j[g] = new Image;
            j[g].onload = function() {
                j.loadCount = j.loadCount + 1;
                if (j.loadCount === 6) {
                    i.needsUpdate = true
                }
                l && l(this)
            }
            ;
            j[g].crossOrigin = this.crossOrigin;
            j[g].src = h[g]
        }
        return i
    },
    getNormalMap: function(L, K) {
        var J = function(d) {
            var c = Math.sqrt(d[0] * d[0] + d[1] * d[1] + d[2] * d[2]);
            return [d[0] / c, d[1] / c, d[2] / c]
        }
          , K = K | 1
          , I = L.width
          , H = L.height
          , G = document.createElement("canvas");
        G.width = I;
        G.height = H;
        var F = G.getContext("2d");
        F.drawImage(L, 0, 0);
        for (var E = F.getImageData(0, 0, I, H).data, B = F.createImageData(I, H), C = B.data, D = 0; D < I; D++) {
            for (var z = 0; z < H; z++) {
                var y = z - 1 < 0 ? 0 : z - 1
                  , x = z + 1 > H - 1 ? H - 1 : z + 1
                  , r = D - 1 < 0 ? 0 : D - 1
                  , v = D + 1 > I - 1 ? I - 1 : D + 1
                  , t = []
                  , g = [0, 0, E[(z * I + D) * 4] / 255 * K];
                t.push([-1, 0, E[(z * I + r) * 4] / 255 * K]);
                t.push([-1, -1, E[(y * I + r) * 4] / 255 * K]);
                t.push([0, -1, E[(y * I + D) * 4] / 255 * K]);
                t.push([1, -1, E[(y * I + v) * 4] / 255 * K]);
                t.push([1, 0, E[(z * I + v) * 4] / 255 * K]);
                t.push([1, 1, E[(x * I + v) * 4] / 255 * K]);
                t.push([0, 1, E[(x * I + D) * 4] / 255 * K]);
                t.push([-1, 1, E[(x * I + r) * 4] / 255 * K]);
                y = [];
                r = t.length;
                for (x = 0; x < r; x++) {
                    var v = t[x]
                      , u = t[(x + 1) % r]
                      , v = [v[0] - g[0], v[1] - g[1], v[2] - g[2]]
                      , u = [u[0] - g[0], u[1] - g[1], u[2] - g[2]];
                    y.push(J([v[1] * u[2] - v[2] * u[1], v[2] * u[0] - v[0] * u[2], v[0] * u[1] - v[1] * u[0]]))
                }
                t = [0, 0, 0];
                for (x = 0; x < y.length; x++) {
                    t[0] = t[0] + y[x][0];
                    t[1] = t[1] + y[x][1];
                    t[2] = t[2] + y[x][2]
                }
                t[0] = t[0] / y.length;
                t[1] = t[1] / y.length;
                t[2] = t[2] / y.length;
                g = (z * I + D) * 4;
                C[g] = (t[0] + 1) / 2 * 255 | 0;
                C[g + 1] = (t[1] + 0.5) * 255 | 0;
                C[g + 2] = t[2] * 255 | 0;
                C[g + 3] = 255
            }
        }
        F.putImageData(B, 0, 0);
        return G
    },
    generateDataTexture: function(j, g, p) {
        for (var o = j * g, n = new Uint8Array(3 * o), m = Math.floor(p.r * 255), l = Math.floor(p.g * 255), p = Math.floor(p.b * 255), k = 0; k < o; k++) {
            n[k * 3] = m;
            n[k * 3 + 1] = l;
            n[k * 3 + 2] = p
        }
        j = new THREE.DataTexture(n,j,g,THREE.RGBFormat);
        j.needsUpdate = true;
        return j
    }
};
THREE.SceneUtils = {
    showHierarchy: function(d, c) {
        THREE.SceneUtils.traverseHierarchy(d, function(b) {
            b.visible = c
        })
    },
    traverseHierarchy: function(g, f) {
        var j, i, h = g.children.length;
        for (i = 0; i < h; i++) {
            j = g.children[i];
            f(j);
            THREE.SceneUtils.traverseHierarchy(j, f)
        }
    },
    createMultiMaterialObject: function(h, g) {
        var l, k = g.length, j = new THREE.Object3D;
        for (l = 0; l < k; l++) {
            var i = new THREE.Mesh(h,g[l]);
            j.add(i)
        }
        return j
    },
    cloneObject: function(f) {
        var e;
        if (f instanceof THREE.Mesh) {
            e = new THREE.Mesh(f.geometry,f.material)
        } else {
            if (f instanceof THREE.Line) {
                e = new THREE.Line(f.geometry,f.material,f.type)
            } else {
                if (f instanceof THREE.Ribbon) {
                    e = new THREE.Ribbon(f.geometry,f.material)
                } else {
                    if (f instanceof THREE.ParticleSystem) {
                        e = new THREE.ParticleSystem(f.geometry,f.material);
                        e.sortParticles = f.sortParticles
                    } else {
                        if (f instanceof THREE.Particle) {
                            e = new THREE.Particle(f.material)
                        } else {
                            if (f instanceof THREE.Sprite) {
                                e = new THREE.Sprite({});
                                e.color.copy(f.color);
                                e.map = f.map;
                                e.blending = f.blending;
                                e.useScreenCoordinates = f.useScreenCoordinates;
                                e.mergeWith3D = f.mergeWith3D;
                                e.affectedByDistance = f.affectedByDistance;
                                e.scaleByViewport = f.scaleByViewport;
                                e.alignment = f.alignment;
                                e.rotation3d.copy(f.rotation3d);
                                e.rotation = f.rotation;
                                e.opacity = f.opacity;
                                e.uvOffset.copy(f.uvOffset);
                                e.uvScale.copy(f.uvScale)
                            } else {
                                f instanceof THREE.LOD ? e = new THREE.LOD : f instanceof THREE.Object3D && (e = new THREE.Object3D)
                            }
                        }
                    }
                }
            }
        }
        e.name = f.name;
        e.parent = f.parent;
        e.up.copy(f.up);
        e.position.copy(f.position);
        e.rotation instanceof THREE.Vector3 && e.rotation.copy(f.rotation);
        e.eulerOrder = f.eulerOrder;
        e.scale.copy(f.scale);
        e.dynamic = f.dynamic;
        e.doubleSided = f.doubleSided;
        e.flipSided = f.flipSided;
        e.renderDepth = f.renderDepth;
        e.rotationAutoUpdate = f.rotationAutoUpdate;
        e.matrix.copy(f.matrix);
        e.matrixWorld.copy(f.matrixWorld);
        e.matrixRotationWorld.copy(f.matrixRotationWorld);
        e.matrixAutoUpdate = f.matrixAutoUpdate;
        e.matrixWorldNeedsUpdate = f.matrixWorldNeedsUpdate;
        e.quaternion.copy(f.quaternion);
        e.useQuaternion = f.useQuaternion;
        e.boundRadius = f.boundRadius;
        e.boundRadiusScale = f.boundRadiusScale;
        e.visible = f.visible;
        e.castShadow = f.castShadow;
        e.receiveShadow = f.receiveShadow;
        e.frustumCulled = f.frustumCulled;
        for (var h = 0; h < f.children.length; h++) {
            var g = THREE.SceneUtils.cloneObject(f.children[h]);
            e.children[h] = g;
            g.parent = e
        }
        if (f instanceof THREE.LOD) {
            for (h = 0; h < f.LODs.length; h++) {
                e.LODs[h] = {
                    visibleAtDistance: f.LODs[h].visibleAtDistance,
                    object3D: e.children[h]
                }
            }
        }
        return e
    },
    detach: function(e, d, f) {
        e.applyMatrix(d.matrixWorld);
        d.remove(e);
        f.add(e)
    },
    attach: function(f, e, h) {
        var g = new THREE.Matrix4;
        g.getInverse(h.matrixWorld);
        f.applyMatrix(g);
        e.remove(f);
        h.add(f)
    }
};
THREE.WebGLRenderer && (THREE.ShaderUtils = {
    lib: {
        fresnel: {
            uniforms: {
                mRefractionRatio: {
                    type: "f",
                    value: 1.02
                },
                mFresnelBias: {
                    type: "f",
                    value: 0.1
                },
                mFresnelPower: {
                    type: "f",
                    value: 2
                },
                mFresnelScale: {
                    type: "f",
                    value: 1
                },
                tCube: {
                    type: "t",
                    value: 1,
                    texture: null
                }
            },
            fragmentShader: "uniform samplerCube tCube;\nvarying vec3 vReflect;\nvarying vec3 vRefract[3];\nvarying float vReflectionFactor;\nvoid main() {\nvec4 reflectedColor = textureCube( tCube, vec3( -vReflect.x, vReflect.yz ) );\nvec4 refractedColor = vec4( 1.0, 1.0, 1.0, 1.0 );\nrefractedColor.r = textureCube( tCube, vec3( -vRefract[0].x, vRefract[0].yz ) ).r;\nrefractedColor.g = textureCube( tCube, vec3( -vRefract[1].x, vRefract[1].yz ) ).g;\nrefractedColor.b = textureCube( tCube, vec3( -vRefract[2].x, vRefract[2].yz ) ).b;\nrefractedColor.a = 1.0;\ngl_FragColor = mix( refractedColor, reflectedColor, clamp( vReflectionFactor, 0.0, 1.0 ) );\n}",
            vertexShader: "uniform float mRefractionRatio;\nuniform float mFresnelBias;\nuniform float mFresnelScale;\nuniform float mFresnelPower;\nvarying vec3 vReflect;\nvarying vec3 vRefract[3];\nvarying float vReflectionFactor;\nvoid main() {\nvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\nvec4 mPosition = objectMatrix * vec4( position, 1.0 );\nvec3 nWorld = normalize ( mat3( objectMatrix[0].xyz, objectMatrix[1].xyz, objectMatrix[2].xyz ) * normal );\nvec3 I = mPosition.xyz - cameraPosition;\nvReflect = reflect( I, nWorld );\nvRefract[0] = refract( normalize( I ), nWorld, mRefractionRatio );\nvRefract[1] = refract( normalize( I ), nWorld, mRefractionRatio * 0.99 );\nvRefract[2] = refract( normalize( I ), nWorld, mRefractionRatio * 0.98 );\nvReflectionFactor = mFresnelBias + mFresnelScale * pow( 1.0 + dot( normalize( I ), nWorld ), mFresnelPower );\ngl_Position = projectionMatrix * mvPosition;\n}"
        },
        normal: {
            uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib.fog, THREE.UniformsLib.lights, THREE.UniformsLib.shadowmap, {
                enableAO: {
                    type: "i",
                    value: 0
                },
                enableDiffuse: {
                    type: "i",
                    value: 0
                },
                enableSpecular: {
                    type: "i",
                    value: 0
                },
                enableReflection: {
                    type: "i",
                    value: 0
                },
                tDiffuse: {
                    type: "t",
                    value: 0,
                    texture: null
                },
                tCube: {
                    type: "t",
                    value: 1,
                    texture: null
                },
                tNormal: {
                    type: "t",
                    value: 2,
                    texture: null
                },
                tSpecular: {
                    type: "t",
                    value: 3,
                    texture: null
                },
                tAO: {
                    type: "t",
                    value: 4,
                    texture: null
                },
                tDisplacement: {
                    type: "t",
                    value: 5,
                    texture: null
                },
                uNormalScale: {
                    type: "f",
                    value: 1
                },
                uDisplacementBias: {
                    type: "f",
                    value: 0
                },
                uDisplacementScale: {
                    type: "f",
                    value: 1
                },
                uDiffuseColor: {
                    type: "c",
                    value: new THREE.Color(16777215)
                },
                uSpecularColor: {
                    type: "c",
                    value: new THREE.Color(1118481)
                },
                uAmbientColor: {
                    type: "c",
                    value: new THREE.Color(16777215)
                },
                uShininess: {
                    type: "f",
                    value: 30
                },
                uOpacity: {
                    type: "f",
                    value: 1
                },
                uReflectivity: {
                    type: "f",
                    value: 0.5
                },
                uOffset: {
                    type: "v2",
                    value: new THREE.Vector2(0,0)
                },
                uRepeat: {
                    type: "v2",
                    value: new THREE.Vector2(1,1)
                },
                wrapRGB: {
                    type: "v3",
                    value: new THREE.Vector3(1,1,1)
                }
            }]),
            fragmentShader: ["uniform vec3 uAmbientColor;\nuniform vec3 uDiffuseColor;\nuniform vec3 uSpecularColor;\nuniform float uShininess;\nuniform float uOpacity;\nuniform bool enableDiffuse;\nuniform bool enableSpecular;\nuniform bool enableAO;\nuniform bool enableReflection;\nuniform sampler2D tDiffuse;\nuniform sampler2D tNormal;\nuniform sampler2D tSpecular;\nuniform sampler2D tAO;\nuniform samplerCube tCube;\nuniform float uNormalScale;\nuniform float uReflectivity;\nvarying vec3 vTangent;\nvarying vec3 vBinormal;\nvarying vec3 vNormal;\nvarying vec2 vUv;\nuniform vec3 ambientLightColor;\n#if MAX_DIR_LIGHTS > 0\nuniform vec3 directionalLightColor[ MAX_DIR_LIGHTS ];\nuniform vec3 directionalLightDirection[ MAX_DIR_LIGHTS ];\n#endif\n#if MAX_POINT_LIGHTS > 0\nuniform vec3 pointLightColor[ MAX_POINT_LIGHTS ];\nvarying vec4 vPointLight[ MAX_POINT_LIGHTS ];\n#endif\n#ifdef WRAP_AROUND\nuniform vec3 wrapRGB;\n#endif\nvarying vec3 vViewPosition;", THREE.ShaderChunk.shadowmap_pars_fragment, THREE.ShaderChunk.fog_pars_fragment, "void main() {\ngl_FragColor = vec4( vec3( 1.0 ), uOpacity );\nvec3 specularTex = vec3( 1.0 );\nvec3 normalTex = texture2D( tNormal, vUv ).xyz * 2.0 - 1.0;\nnormalTex.xy *= uNormalScale;\nnormalTex = normalize( normalTex );\nif( enableDiffuse ) {\n#ifdef GAMMA_INPUT\nvec4 texelColor = texture2D( tDiffuse, vUv );\ntexelColor.xyz *= texelColor.xyz;\ngl_FragColor = gl_FragColor * texelColor;\n#else\ngl_FragColor = gl_FragColor * texture2D( tDiffuse, vUv );\n#endif\n}\nif( enableAO ) {\n#ifdef GAMMA_INPUT\nvec4 aoColor = texture2D( tAO, vUv );\naoColor.xyz *= aoColor.xyz;\ngl_FragColor.xyz = gl_FragColor.xyz * aoColor.xyz;\n#else\ngl_FragColor.xyz = gl_FragColor.xyz * texture2D( tAO, vUv ).xyz;\n#endif\n}\nif( enableSpecular )\nspecularTex = texture2D( tSpecular, vUv ).xyz;\nmat3 tsb = mat3( normalize( vTangent ), normalize( vBinormal ), normalize( vNormal ) );\nvec3 finalNormal = tsb * normalTex;\nvec3 normal = normalize( finalNormal );\nvec3 viewPosition = normalize( vViewPosition );\n#if MAX_POINT_LIGHTS > 0\nvec3 pointDiffuse = vec3( 0.0 );\nvec3 pointSpecular = vec3( 0.0 );\nfor ( int i = 0; i < MAX_POINT_LIGHTS; i ++ ) {\nvec3 pointVector = normalize( vPointLight[ i ].xyz );\nfloat pointDistance = vPointLight[ i ].w;\n#ifdef WRAP_AROUND\nfloat pointDiffuseWeightFull = max( dot( normal, pointVector ), 0.0 );\nfloat pointDiffuseWeightHalf = max( 0.5 * dot( normal, pointVector ) + 0.5, 0.0 );\nvec3 pointDiffuseWeight = mix( vec3 ( pointDiffuseWeightFull ), vec3( pointDiffuseWeightHalf ), wrapRGB );\n#else\nfloat pointDiffuseWeight = max( dot( normal, pointVector ), 0.0 );\n#endif\npointDiffuse += pointDistance * pointLightColor[ i ] * uDiffuseColor * pointDiffuseWeight;\nvec3 pointHalfVector = normalize( pointVector + viewPosition );\nfloat pointDotNormalHalf = max( dot( normal, pointHalfVector ), 0.0 );\nfloat pointSpecularWeight = specularTex.r * max( pow( pointDotNormalHalf, uShininess ), 0.0 );\n#ifdef PHYSICALLY_BASED_SHADING\nfloat specularNormalization = ( uShininess + 2.0001 ) / 8.0;\nvec3 schlick = uSpecularColor + vec3( 1.0 - uSpecularColor ) * pow( 1.0 - dot( pointVector, pointHalfVector ), 5.0 );\npointSpecular += schlick * pointLightColor[ i ] * pointSpecularWeight * pointDiffuseWeight * pointDistance * specularNormalization;\n#else\npointSpecular += pointDistance * pointLightColor[ i ] * uSpecularColor * pointSpecularWeight * pointDiffuseWeight;\n#endif\n}\n#endif\n#if MAX_DIR_LIGHTS > 0\nvec3 dirDiffuse = vec3( 0.0 );\nvec3 dirSpecular = vec3( 0.0 );\nfor( int i = 0; i < MAX_DIR_LIGHTS; i++ ) {\nvec4 lDirection = viewMatrix * vec4( directionalLightDirection[ i ], 0.0 );\nvec3 dirVector = normalize( lDirection.xyz );\n#ifdef WRAP_AROUND\nfloat directionalLightWeightingFull = max( dot( normal, dirVector ), 0.0 );\nfloat directionalLightWeightingHalf = max( 0.5 * dot( normal, dirVector ) + 0.5, 0.0 );\nvec3 dirDiffuseWeight = mix( vec3( directionalLightWeightingFull ), vec3( directionalLightWeightingHalf ), wrapRGB );\n#else\nfloat dirDiffuseWeight = max( dot( normal, dirVector ), 0.0 );\n#endif\ndirDiffuse += directionalLightColor[ i ] * uDiffuseColor * dirDiffuseWeight;\nvec3 dirHalfVector = normalize( dirVector + viewPosition );\nfloat dirDotNormalHalf = max( dot( normal, dirHalfVector ), 0.0 );\nfloat dirSpecularWeight = specularTex.r * max( pow( dirDotNormalHalf, uShininess ), 0.0 );\n#ifdef PHYSICALLY_BASED_SHADING\nfloat specularNormalization = ( uShininess + 2.0001 ) / 8.0;\nvec3 schlick = uSpecularColor + vec3( 1.0 - uSpecularColor ) * pow( 1.0 - dot( dirVector, dirHalfVector ), 5.0 );\ndirSpecular += schlick * directionalLightColor[ i ] * dirSpecularWeight * dirDiffuseWeight * specularNormalization;\n#else\ndirSpecular += directionalLightColor[ i ] * uSpecularColor * dirSpecularWeight * dirDiffuseWeight;\n#endif\n}\n#endif\nvec3 totalDiffuse = vec3( 0.0 );\nvec3 totalSpecular = vec3( 0.0 );\n#if MAX_DIR_LIGHTS > 0\ntotalDiffuse += dirDiffuse;\ntotalSpecular += dirSpecular;\n#endif\n#if MAX_POINT_LIGHTS > 0\ntotalDiffuse += pointDiffuse;\ntotalSpecular += pointSpecular;\n#endif\ngl_FragColor.xyz = gl_FragColor.xyz * ( totalDiffuse + ambientLightColor * uAmbientColor) + totalSpecular;\nif ( enableReflection ) {\nvec3 wPos = cameraPosition - vViewPosition;\nvec3 vReflect = reflect( normalize( wPos ), normal );\nvec4 cubeColor = textureCube( tCube, vec3( -vReflect.x, vReflect.yz ) );\n#ifdef GAMMA_INPUT\ncubeColor.xyz *= cubeColor.xyz;\n#endif\ngl_FragColor.xyz = mix( gl_FragColor.xyz, cubeColor.xyz, specularTex.r * uReflectivity );\n}", THREE.ShaderChunk.shadowmap_fragment, THREE.ShaderChunk.linear_to_gamma_fragment, THREE.ShaderChunk.fog_fragment, "}"].join("\n"),
            vertexShader: ["attribute vec4 tangent;\nuniform vec2 uOffset;\nuniform vec2 uRepeat;\n#ifdef VERTEX_TEXTURES\nuniform sampler2D tDisplacement;\nuniform float uDisplacementScale;\nuniform float uDisplacementBias;\n#endif\nvarying vec3 vTangent;\nvarying vec3 vBinormal;\nvarying vec3 vNormal;\nvarying vec2 vUv;\n#if MAX_POINT_LIGHTS > 0\nuniform vec3 pointLightPosition[ MAX_POINT_LIGHTS ];\nuniform float pointLightDistance[ MAX_POINT_LIGHTS ];\nvarying vec4 vPointLight[ MAX_POINT_LIGHTS ];\n#endif\nvarying vec3 vViewPosition;", THREE.ShaderChunk.shadowmap_pars_vertex, "void main() {\nvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\nvViewPosition = -mvPosition.xyz;\nvNormal = normalMatrix * normal;\nvTangent = normalMatrix * tangent.xyz;\nvBinormal = cross( vNormal, vTangent ) * tangent.w;\nvUv = uv * uRepeat + uOffset;\n#if MAX_POINT_LIGHTS > 0\nfor( int i = 0; i < MAX_POINT_LIGHTS; i++ ) {\nvec4 lPosition = viewMatrix * vec4( pointLightPosition[ i ], 1.0 );\nvec3 lVector = lPosition.xyz - mvPosition.xyz;\nfloat lDistance = 1.0;\nif ( pointLightDistance[ i ] > 0.0 )\nlDistance = 1.0 - min( ( length( lVector ) / pointLightDistance[ i ] ), 1.0 );\nlVector = normalize( lVector );\nvPointLight[ i ] = vec4( lVector, lDistance );\n}\n#endif\n#ifdef VERTEX_TEXTURES\nvec3 dv = texture2D( tDisplacement, uv ).xyz;\nfloat df = uDisplacementScale * dv.x + uDisplacementBias;\nvec4 displacedPosition = vec4( normalize( vNormal.xyz ) * df, 0.0 ) + mvPosition;\ngl_Position = projectionMatrix * displacedPosition;\n#else\ngl_Position = projectionMatrix * mvPosition;\n#endif", THREE.ShaderChunk.shadowmap_vertex, "}"].join("\n")
        },
        cube: {
            uniforms: {
                tCube: {
                    type: "t",
                    value: 1,
                    texture: null
                },
                tFlip: {
                    type: "f",
                    value: -1
                }
            },
            vertexShader: "varying vec3 vViewPosition;\nvoid main() {\nvec4 mPosition = objectMatrix * vec4( position, 1.0 );\nvViewPosition = cameraPosition - mPosition.xyz;\ngl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n}",
            fragmentShader: "uniform samplerCube tCube;\nuniform float tFlip;\nvarying vec3 vViewPosition;\nvoid main() {\nvec3 wPos = cameraPosition - vViewPosition;\ngl_FragColor = textureCube( tCube, vec3( tFlip * wPos.x, wPos.yz ) );\n}"
        }
    }
});
THREE.BufferGeometry = function() {
    this.id = THREE.GeometryCount++;
    this.vertexColorArray = this.vertexUvArray = this.vertexNormalArray = this.vertexPositionArray = this.vertexIndexArray = this.vertexColorBuffer = this.vertexUvBuffer = this.vertexNormalBuffer = this.vertexPositionBuffer = this.vertexIndexBuffer = null;
    this.dynamic = false;
    this.boundingSphere = this.boundingBox = null;
    this.morphTargets = []
}
;
THREE.BufferGeometry.prototype = {
    constructor: THREE.BufferGeometry,
    computeBoundingBox: function() {},
    computeBoundingSphere: function() {}
};
THREE.CubeGeometry = function(N, M, L, K, J, I, H, G) {
    function D(af, ae, ad, ab, aa, Y, X, V) {
        var W, U = K || 1, T = J || 1, Q = aa / 2, ac = Y / 2, P = E.vertices.length;
        if (af === "x" && ae === "y" || af === "y" && ae === "x") {
            W = "z"
        } else {
            if (af === "x" && ae === "z" || af === "z" && ae === "x") {
                W = "y";
                T = I || 1
            } else {
                if (af === "z" && ae === "y" || af === "y" && ae === "z") {
                    W = "x";
                    U = I || 1
                }
            }
        }
        var O = U + 1
          , k = T + 1
          , S = aa / U
          , e = Y / T
          , f = new THREE.Vector3;
        f[W] = X > 0 ? 1 : -1;
        for (aa = 0; aa < k; aa++) {
            for (Y = 0; Y < O; Y++) {
                var d = new THREE.Vector3;
                d[af] = (Y * S - Q) * ad;
                d[ae] = (aa * e - ac) * ab;
                d[W] = X;
                E.vertices.push(d)
            }
        }
        for (aa = 0; aa < T; aa++) {
            for (Y = 0; Y < U; Y++) {
                af = new THREE.Face4(Y + O * aa + P,Y + O * (aa + 1) + P,Y + 1 + O * (aa + 1) + P,Y + 1 + O * aa + P);
                af.normal.copy(f);
                af.vertexNormals.push(f.clone(), f.clone(), f.clone(), f.clone());
                af.materialIndex = V;
                E.faces.push(af);
                E.faceVertexUvs[0].push([new THREE.UV(Y / U,aa / T), new THREE.UV(Y / U,(aa + 1) / T), new THREE.UV((Y + 1) / U,(aa + 1) / T), new THREE.UV((Y + 1) / U,aa / T)])
            }
        }
    }
    THREE.Geometry.call(this);
    var E = this, F = N / 2, C = M / 2, B = L / 2, z, t, x, u, r, v;
    if (H !== void 0) {
        if (H instanceof Array) {
            this.materials = H
        } else {
            this.materials = [];
            for (z = 0; z < 6; z++) {
                this.materials.push(H)
            }
        }
        z = 0;
        u = 1;
        t = 2;
        r = 3;
        x = 4;
        v = 5
    } else {
        this.materials = []
    }
    this.sides = {
        px: true,
        nx: true,
        py: true,
        ny: true,
        pz: true,
        nz: true
    };
    if (G != void 0) {
        for (var g in G) {
            this.sides[g] !== void 0 && (this.sides[g] = G[g])
        }
    }
    this.sides.px && D("z", "y", -1, -1, L, M, F, z);
    this.sides.nx && D("z", "y", 1, -1, L, M, -F, u);
    this.sides.py && D("x", "z", 1, 1, N, L, C, t);
    this.sides.ny && D("x", "z", 1, -1, N, L, -C, r);
    this.sides.pz && D("x", "y", 1, -1, N, M, B, x);
    this.sides.nz && D("x", "y", -1, -1, N, M, -B, v);
    this.computeCentroids();
    this.mergeVertices()
}
;
THREE.CubeGeometry.prototype = new THREE.Geometry;
THREE.CubeGeometry.prototype.constructor = THREE.CubeGeometry;
THREE.CylinderGeometry = function(X, W, V, U, T, S) {
    THREE.Geometry.call(this);
    var X = X !== void 0 ? X : 20, W = W !== void 0 ? W : 20, V = V !== void 0 ? V : 100, R = V / 2, U = U || 8, T = T || 1, Q, M, O = [], P = [];
    for (M = 0; M <= T; M++) {
        var L = []
          , J = []
          , I = M / T
          , C = I * (W - X) + X;
        for (Q = 0; Q <= U; Q++) {
            var G = Q / U
              , E = new THREE.Vector3;
            E.x = C * Math.sin(G * Math.PI * 2);
            E.y = -I * V + R;
            E.z = C * Math.cos(G * Math.PI * 2);
            this.vertices.push(E);
            L.push(this.vertices.length - 1);
            J.push(new THREE.UV(G,I))
        }
        O.push(L);
        P.push(J)
    }
    V = (W - X) / V;
    for (Q = 0; Q < U; Q++) {
        if (X !== 0) {
            L = this.vertices[O[0][Q]].clone();
            J = this.vertices[O[0][Q + 1]].clone()
        } else {
            L = this.vertices[O[1][Q]].clone();
            J = this.vertices[O[1][Q + 1]].clone()
        }
        L.setY(Math.sqrt(L.x * L.x + L.z * L.z) * V).normalize();
        J.setY(Math.sqrt(J.x * J.x + J.z * J.z) * V).normalize();
        for (M = 0; M < T; M++) {
            var I = O[M][Q]
              , C = O[M + 1][Q]
              , G = O[M + 1][Q + 1]
              , E = O[M][Q + 1]
              , x = L.clone()
              , F = L.clone()
              , t = J.clone()
              , z = J.clone()
              , v = P[M][Q].clone()
              , D = P[M + 1][Q].clone()
              , r = P[M + 1][Q + 1].clone()
              , g = P[M][Q + 1].clone();
            this.faces.push(new THREE.Face4(I,C,G,E,[x, F, t, z]));
            this.faceVertexUvs[0].push([v, D, r, g])
        }
    }
    if (!S && X > 0) {
        this.vertices.push(new THREE.Vector3(0,R,0));
        for (Q = 0; Q < U; Q++) {
            I = O[0][Q];
            C = O[0][Q + 1];
            G = this.vertices.length - 1;
            x = new THREE.Vector3(0,1,0);
            F = new THREE.Vector3(0,1,0);
            t = new THREE.Vector3(0,1,0);
            v = P[0][Q].clone();
            D = P[0][Q + 1].clone();
            r = new THREE.UV(D.u,0);
            this.faces.push(new THREE.Face3(I,C,G,[x, F, t]));
            this.faceVertexUvs[0].push([v, D, r])
        }
    }
    if (!S && W > 0) {
        this.vertices.push(new THREE.Vector3(0,-R,0));
        for (Q = 0; Q < U; Q++) {
            I = O[M][Q + 1];
            C = O[M][Q];
            G = this.vertices.length - 1;
            x = new THREE.Vector3(0,-1,0);
            F = new THREE.Vector3(0,-1,0);
            t = new THREE.Vector3(0,-1,0);
            v = P[M][Q + 1].clone();
            D = P[M][Q].clone();
            r = new THREE.UV(D.u,1);
            this.faces.push(new THREE.Face3(I,C,G,[x, F, t]));
            this.faceVertexUvs[0].push([v, D, r])
        }
    }
    this.computeCentroids();
    this.computeFaceNormals()
}
;
THREE.CylinderGeometry.prototype = new THREE.Geometry;
THREE.CylinderGeometry.prototype.constructor = THREE.CylinderGeometry;
THREE.PlaneGeometry = function(v, u, t, s) {
    THREE.Geometry.call(this);
    for (var r = v / 2, q = u / 2, t = t || 1, s = s || 1, p = t + 1, o = s + 1, g = v / t, m = u / s, n = new THREE.Vector3(0,1,0), v = 0; v < o; v++) {
        for (u = 0; u < p; u++) {
            this.vertices.push(new THREE.Vector3(u * g - r,0,v * m - q))
        }
    }
    for (v = 0; v < s; v++) {
        for (u = 0; u < t; u++) {
            r = new THREE.Face4(u + p * v,u + p * (v + 1),u + 1 + p * (v + 1),u + 1 + p * v);
            r.normal.copy(n);
            r.vertexNormals.push(n.clone(), n.clone(), n.clone(), n.clone());
            this.faces.push(r);
            this.faceVertexUvs[0].push([new THREE.UV(u / t,v / s), new THREE.UV(u / t,(v + 1) / s), new THREE.UV((u + 1) / t,(v + 1) / s), new THREE.UV((u + 1) / t,v / s)])
        }
    }
    this.computeCentroids()
}
;
THREE.PlaneGeometry.prototype = new THREE.Geometry;
THREE.PlaneGeometry.prototype.constructor = THREE.PlaneGeometry;
THREE.SphereGeometry = function(L, K, J, I, H, G, F) {
    THREE.Geometry.call(this);
    var L = L || 50, I = I !== void 0 ? I : 0, H = H !== void 0 ? H : Math.PI * 2, G = G !== void 0 ? G : 0, F = F !== void 0 ? F : Math.PI, K = Math.max(3, Math.floor(K) || 8), J = Math.max(2, Math.floor(J) || 6), E, B, C = [], D = [];
    for (B = 0; B <= J; B++) {
        var z = []
          , y = [];
        for (E = 0; E <= K; E++) {
            var x = E / K
              , r = B / J
              , v = new THREE.Vector3;
            v.x = -L * Math.cos(I + x * H) * Math.sin(G + r * F);
            v.y = L * Math.cos(G + r * F);
            v.z = L * Math.sin(I + x * H) * Math.sin(G + r * F);
            this.vertices.push(v);
            z.push(this.vertices.length - 1);
            y.push(new THREE.UV(x,r))
        }
        C.push(z);
        D.push(y)
    }
    for (B = 0; B < J; B++) {
        for (E = 0; E < K; E++) {
            var I = C[B][E + 1]
              , H = C[B][E]
              , G = C[B + 1][E]
              , F = C[B + 1][E + 1]
              , z = this.vertices[I].clone().normalize()
              , y = this.vertices[H].clone().normalize()
              , x = this.vertices[G].clone().normalize()
              , r = this.vertices[F].clone().normalize()
              , v = D[B][E + 1].clone()
              , t = D[B][E].clone()
              , g = D[B + 1][E].clone()
              , u = D[B + 1][E + 1].clone();
            if (Math.abs(this.vertices[I].y) == L) {
                this.faces.push(new THREE.Face3(I,G,F,[z, x, r]));
                this.faceVertexUvs[0].push([v, g, u])
            } else {
                if (Math.abs(this.vertices[G].y) == L) {
                    this.faces.push(new THREE.Face3(I,H,G,[z, y, x]));
                    this.faceVertexUvs[0].push([v, t, g])
                } else {
                    this.faces.push(new THREE.Face4(I,H,G,F,[z, y, x, r]));
                    this.faceVertexUvs[0].push([v, t, g, u])
                }
            }
        }
    }
    this.computeCentroids();
    this.computeFaceNormals();
    this.boundingSphere = {
        radius: L
    }
}
;
THREE.SphereGeometry.prototype = new THREE.Geometry;
THREE.SphereGeometry.prototype.constructor = THREE.SphereGeometry;
THREE.PolyhedronGeometry = function(x, w, v, u) {
    function t(e) {
        var d = e.normalize().clone();
        d.index = n.vertices.push(d) - 1;
        var f = Math.atan2(e.z, -e.x) / 2 / Math.PI + 0.5
          , e = Math.atan2(-e.y, Math.sqrt(e.x * e.x + e.z * e.z)) / Math.PI + 0.5;
        d.uv = new THREE.UV(f,e);
        return d
    }
    function s(f, e, i, h) {
        if (h < 1) {
            h = new THREE.Face3(f.index,e.index,i.index,[f.clone(), e.clone(), i.clone()]);
            h.centroid.addSelf(f).addSelf(e).addSelf(i).divideScalar(3);
            h.normal = h.centroid.clone().normalize();
            n.faces.push(h);
            h = Math.atan2(h.centroid.z, -h.centroid.x);
            n.faceVertexUvs[0].push([q(f.uv, f, h), q(e.uv, e, h), q(i.uv, i, h)])
        } else {
            h = h - 1;
            s(f, r(f, e), r(f, i), h);
            s(r(f, e), e, r(e, i), h);
            s(r(f, i), r(e, i), i, h);
            s(r(f, e), r(e, i), r(f, i), h)
        }
    }
    function r(e, d) {
        g[e.index] || (g[e.index] = []);
        g[d.index] || (g[d.index] = []);
        var f = g[e.index][d.index];
        f === void 0 && (g[e.index][d.index] = g[d.index][e.index] = f = t((new THREE.Vector3).add(e, d).divideScalar(2)));
        return f
    }
    function q(e, d, f) {
        f < 0 && e.u === 1 && (e = new THREE.UV(e.u - 1,e.v));
        d.x === 0 && d.z === 0 && (e = new THREE.UV(f / 2 / Math.PI + 0.5,e.v));
        return e
    }
    THREE.Geometry.call(this);
    for (var v = v || 1, u = u || 0, n = this, o = 0, p = x.length; o < p; o++) {
        t(new THREE.Vector3(x[o][0],x[o][1],x[o][2]))
    }
    for (var g = [], x = this.vertices, o = 0, p = w.length; o < p; o++) {
        s(x[w[o][0]], x[w[o][1]], x[w[o][2]], u)
    }
    this.mergeVertices();
    o = 0;
    for (p = this.vertices.length; o < p; o++) {
        this.vertices[o].multiplyScalar(v)
    }
    this.computeCentroids();
    this.boundingSphere = {
        radius: v
    }
}
;
THREE.PolyhedronGeometry.prototype = new THREE.Geometry;
THREE.PolyhedronGeometry.prototype.constructor = THREE.PolyhedronGeometry;
THREE.IcosahedronGeometry = function(e, d) {
    var f = (1 + Math.sqrt(5)) / 2;
    THREE.PolyhedronGeometry.call(this, [[-1, f, 0], [1, f, 0], [-1, -f, 0], [1, -f, 0], [0, -1, f], [0, 1, f], [0, -1, -f], [0, 1, -f], [f, 0, -1], [f, 0, 1], [-f, 0, -1], [-f, 0, 1]], [[0, 11, 5], [0, 5, 1], [0, 1, 7], [0, 7, 10], [0, 10, 11], [1, 5, 9], [5, 11, 4], [11, 10, 2], [10, 7, 6], [7, 1, 8], [3, 9, 4], [3, 4, 2], [3, 2, 6], [3, 6, 8], [3, 8, 9], [4, 9, 5], [2, 4, 11], [6, 2, 10], [8, 6, 7], [9, 8, 1]], e, d)
}
;
THREE.IcosahedronGeometry.prototype = new THREE.Geometry;
THREE.IcosahedronGeometry.prototype.constructor = THREE.IcosahedronGeometry;
export {THREE};

#
# note: '"use strict";', 'import' and 'export' keywork are not supported by minifier
#
java -jar /tmp/yuicompressor-2.4.8.jar --type js ./GLmol.js         -o ./GLmol.min.js
java -jar /tmp/yuicompressor-2.4.8.jar --type js ./jQuery-1.7.js    -o ./jQuery-1.7.min.js
java -jar /tmp/yuicompressor-2.4.8.jar --type js ./Three49custom.js -o ./Three49custom.min.js

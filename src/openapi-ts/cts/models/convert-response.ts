/* tslint:disable */
/* eslint-disable */
/**
 * CTS API - overwitten methods
 * API specification to support \"CTS - REST API\" operations
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * 
 * @export
 * @interface ConvertResponse
 */
export interface ConvertResponse {
    /**
     * identifier source of the conversion
     * @type {string}
     * @memberof ConvertResponse
     */
    'fromIdentifier'?: string;
    /**
     * identifier destination of the conversion
     * @type {string}
     * @memberof ConvertResponse
     */
    'toIdentifier'?: string;
    /**
     * the queried identifier for the conversion
     * @type {string}
     * @memberof ConvertResponse
     */
    'searchTerm'?: string;
    /**
     * 
     * @type {Array<string>}
     * @memberof ConvertResponse
     */
    'results'?: Array<string>;
}


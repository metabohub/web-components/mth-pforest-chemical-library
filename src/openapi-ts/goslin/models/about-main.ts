/* tslint:disable */
/* eslint-disable */
/**
 * Goslin REST-API documentation
 * REST-API of Goslin, \"Grammar Of Succinct LIpid Nomenclature\" tool, which translates lipid names into a standardized name according to lipid shorthand nomenclature and provides related information.
 *
 * The version of the OpenAPI document: 2.1.0
 * Contact: Faustine.SOUC@uca.fr
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * 
 * @export
 * @interface AboutMain
 */
export interface AboutMain {
    /**
     * Name of the API
     * @type {string}
     * @memberof AboutMain
     */
    'name'?: string;
    /**
     * Short description of the microservice
     * @type {string}
     * @memberof AboutMain
     */
    'short_description'?: string;
    /**
     * List of keywords following an ontology
     * @type {Array<string>}
     * @memberof AboutMain
     */
    'keywords'?: Array<string>;
}


/* tslint:disable */
/* eslint-disable */
/**
 * Goslin REST-API documentation
 * REST-API of Goslin, \"Grammar Of Succinct LIpid Nomenclature\" tool, which translates lipid names into a standardized name according to lipid shorthand nomenclature and provides related information.
 *
 * The version of the OpenAPI document: 2.1.0
 * Contact: Faustine.SOUC@uca.fr
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * 
 * @export
 * @interface SucessfulParsingLipidmapsDataValue
 */
export interface SucessfulParsingLipidmapsDataValue {
    /**
     * The InCHI key corresponding to the LipidMaps ID
     * @type {string}
     * @memberof SucessfulParsingLipidmapsDataValue
     */
    'inchi_key'?: string;
    /**
     * The SwissLipids ID corresponding to the LipidMaps ID if it is present
     * @type {string}
     * @memberof SucessfulParsingLipidmapsDataValue
     */
    'swisslipids_id'?: string;
}


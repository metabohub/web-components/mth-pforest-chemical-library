const enum CompoundDescriptionTypeEnum {
    InChI = 'InChI',
    MolText = 'MolText',
    MolFile = 'MolFile',
  }

export { CompoundDescriptionTypeEnum }
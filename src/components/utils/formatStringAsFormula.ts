function formatStringAsFormula(formula: string): string {
    return formula.replace(/\d+/g, "<sub>$&</sub>");
}

export { formatStringAsFormula }
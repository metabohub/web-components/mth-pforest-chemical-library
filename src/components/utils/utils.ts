function makeid(length): string {
    const crypto = window.crypto;
    const array = new Uint32Array(length);
    crypto.getRandomValues(array);
    return array.join("");
}

export { makeid };
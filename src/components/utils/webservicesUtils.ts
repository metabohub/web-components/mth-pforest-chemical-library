enum Status {
    Init, // request has not been sent (yet!)
    Loading, // request pending, processed on server side
    Processing, // processing response on client side
    Success, // request is 2XX OK, show success
    Error, // request is either 4XX or 5XX, show error
}

export { Status }
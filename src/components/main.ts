import type { App } from 'vue';
// import components
import {
    // GLmol wrapper
    MthPfGlmolWrapper, //
    MoleculeDisplayOption, //
    // in-house
    MthPfDownloadSdfFileButton, //
    SdfProperty, //
    MthPfDisplayFormattedFormula, //
    // CDK
    MthPfCdkAbout, // about
    MthPfCdkConversion, // conversion
    ConversionFormatEnum, // conversion
    MthPfCdkDepiction, // depiction
    DepictionFormatEnum, // depiction
    HydrogenDisplayEnum, // depiction
    MthPfCdkProperties, // properties
    CompoundDescriptionTypeEnum, // conversion, properties
    // CTS
    MthPfCtsConvert, //
    CompoundIdentifierType, //
    CtsConvertResponse, //
    // Goslin
    MthPfGoslinAbout, //
    MthPfGoslinLipidNameValidation, //
} from "@/components";

// import methods
import {
    formatStringAsFormula,//
} from "@/components/utils";

// import input / emited objects
import { CdkPropertiesResponse } from '@/openapi-ts/cdk';
import {
    ValidateusingGET200Response,
    ValidateusingGET200ResponseLipidListInner,
} from '@/openapi-ts/goslin';

const MetabohubPeakForestChemicalLibrary = {
    install: (app: App) => {
        // chemical components
        app.component('MthPfGlmolWrapper', MthPfGlmolWrapper);
        app.component('MthPfDownloadSdfFileButton', MthPfDownloadSdfFileButton);
        app.component('MthPfDisplayFormattedFormula', MthPfDisplayFormattedFormula);
        // CDK
        app.component('MthPfCdkAbout', MthPfCdkAbout);
        app.component('MthPfCdkConversion', MthPfCdkConversion);
        app.component('MthPfCdkDepiction', MthPfCdkDepiction);
        app.component('MthPfCdkProperties', MthPfCdkProperties);
        // CTS
        app.component('MthPfCtsConvert', MthPfCtsConvert);
        // Goslin
        app.component('MthPfGoslinAbout', MthPfGoslinAbout);
        app.component('MthPfGoslinLipidNameValidation', MthPfGoslinLipidNameValidation);
    },
};

export {
    // all components
    MetabohubPeakForestChemicalLibrary,
    // methods
    formatStringAsFormula,
    // TS code
    MoleculeDisplayOption, // for 'GLmol Wrapper'
    type SdfProperty, // for 'Download SDF File Button'
    // CDK
    ConversionFormatEnum, //
    CompoundDescriptionTypeEnum, //
    DepictionFormatEnum, //
    HydrogenDisplayEnum, //
    type CdkPropertiesResponse as CdkPropertiesResponse, //
    // CTS
    type CompoundIdentifierType, //
    type CtsConvertResponse, //
    // Goslin
    type ValidateusingGET200Response as GoslinSuccessResponse, // for 'Goslin Lipid Name Validation'
    type ValidateusingGET200ResponseLipidListInner as GoslinLipidList,
};

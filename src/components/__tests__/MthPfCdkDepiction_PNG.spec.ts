// import for tests
import { mount } from '@vue/test-utils';
import { afterAll, expect, test, vitest } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCdkDepiction, { CompoundDescriptionTypeEnum, DepictionFormatEnum } from '@/components/MthPfCdkDepiction.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
    plugins: [vuetify]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const serverOK = setupServer(
    rest.post('*/cdk/*', (req, res, ctx) => {
        return res(
            // ctx.status(200)
            ctx.body(new Blob())
        )
    }),
);

const serverKO1 = setupServer(
    rest.post('*/cdk/*', (req, res, ctx) => {
        return res(ctx.status(500), ctx.body(''))
    }),
);

const serverKO2 = setupServer(
    rest.post('*/cdk/*', (req, res, ctx) => {
        return res(ctx.status(500), ctx.body(''))
    }),
);

afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    serverOK.close();
    serverKO1.close();
    serverKO2.close();
})

// vitest.setup.ts
class FormDataMock {
    append: (name: string, value: string | Blob, fileName?: string) => void = vitest.fn();
    delete: (name: string) => void = vitest.fn();
    get: (name: string) => FormDataEntryValue | null = vitest.fn();
    getAll: (name: string) => FormDataEntryValue[] = vitest.fn();
    has: (name: string) => boolean = vitest.fn();
    set: (name: string, value: string | Blob, fileName?: string) => void = vitest.fn();
    forEach: (callbackfn: (value: FormDataEntryValue, key: string, parent: FormData) => void, thisArg?: any) => void = vitest.fn();
}

// @ts-ignore
global.FormData = FormDataMock;

Object.defineProperty(window.URL, 'createObjectURL', {
    value: (data) => { return data.type; }
});

test('mount component - send query "PNG" OK', async () => {
    // mock
    serverOK.listen()
    // mount 
    const wrapper = mount(MthPfCdkDepiction, { global, props: { depictionFormat: DepictionFormatEnum.PNG } });
    // set
    wrapper.vm.localCompoundDescription = "xxx";
    wrapper.vm.localCompoundDescriptionType = CompoundDescriptionTypeEnum.InChI;
    // supermock
    wrapper.vm.getFormData = function () {
        return new FormDataMock();
    }
    wrapper.vm.hasBodyFormData = function () {
        return true;
    }
    // wait
    await sleep(10);
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(10);
    // test new values
    expect(wrapper.vm.isLocked).toBe(false);
    expect(wrapper.vm.localCompoundDescription).toBe("xxx");
    expect(wrapper.vm.status).toBe(Status.Success);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-success');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(false);
    expect(wrapper.find(".hint-success").exists()).toBe(true);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(false);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-rotate-right");
    // close
    serverOK.close();
});

test('mount component - send query "PNG" | fail void', async () => {
    // mock
    serverKO1.listen()
    // mount 
    const wrapper = mount(MthPfCdkDepiction, { global, props: { depictionFormat: DepictionFormatEnum.PNG } });
    // set
    wrapper.vm.localCompoundDescription = "xxx";
    wrapper.vm.localCompoundDescriptionType = CompoundDescriptionTypeEnum.InChI;
    // supermock
    wrapper.vm.getFormData = function () {
        return new FormDataMock();
    }
    wrapper.vm.hasBodyFormData = function () {
        return false;
    }
    // wait
    await sleep(10);
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(10);
    // test new values
    expect(wrapper.vm.isLocked).toBe(false);
    expect(wrapper.vm.localCompoundDescription).toBe("xxx");
    expect(wrapper.vm.status).toBe(Status.Init);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-info');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(true);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(false);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
    // close
    serverKO1.close();
});

test('mount component - send query "PNG" | fail 500', async () => {
    // mock
    serverKO2.listen()
    // mount 
    const wrapper = mount(MthPfCdkDepiction, { global, props: { depictionFormat: DepictionFormatEnum.PNG } });
    // set
    wrapper.vm.localCompoundDescription = "xxx";
    wrapper.vm.localCompoundDescriptionType = CompoundDescriptionTypeEnum.InChI;
    // supermock
    wrapper.vm.getFormData = function () {
        return new FormDataMock();
    }
    wrapper.vm.hasBodyFormData = function () {
        return true;
    }
    // wait
    await sleep(10);
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(200);
    // test new values
    expect(wrapper.vm.isLocked).toBe(false);
    expect(wrapper.vm.localCompoundDescription).toBe("xxx");
    expect(wrapper.vm.status).toBe(Status.Error);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-error');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(false);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(true);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-rotate-right");
    // close
    serverKO2.close();
});
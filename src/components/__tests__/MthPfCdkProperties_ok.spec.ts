// import for tests
import { mount } from '@vue/test-utils';
import { describe, expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCdkProperties, { CompoundDescriptionTypeEnum } from '@/components/MthPfCdkProperties.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}


describe('ok', () => {

  test('mount component - mock OK', async () => {
    // mock 
    vi.mock('@/openapi-ts/cdk', async () => {
      const actual = await vi.importActual("@/openapi-ts/cdk")
      const successResponse = {
        status: 200,
        data: {
          "state": "Success",
          "formula": "C8H10N4O2",
          "monoisotopic_mass": 194.08037556,
          "average_mass": 194.19091739584468,
          "canonical_smiles": "O=C1C2=C(N=CN2C)N(C(=O)N1C)C",
          "stereo_smiles": "CN1C=NC2=C1C(N(C)C(N2C)=O)=O",
          "inchi": "InChI=1S/C8H10N4O2/c1-10-4-9-6-5(10)7(13)12(3)8(14)11(6)2/h4H,1-3H3",
          "inchikey": "RYYVLZVUVIJVGH-UHFFFAOYSA-N",
          "logP": -0.10858213188126853,
          "CDK_version": "2.9"
        }
      };
      const Configuration = vi.fn()
      const CdkApi = vi.fn()
      CdkApi.prototype.computeProperties = vi.fn()// 
        .mockResolvedValue(successResponse)
      return {
        ...(actual as object),//
        Configuration, //
        CdkApi,//
      }
    }); 
    // mount
    expect(MthPfCdkProperties).toBeTruthy()
    const wrapper = mount(MthPfCdkProperties, { global });
    // Check initial values
    expect(wrapper.vm.conf.basePath).toBe('https://metabocloud.mesocentre.uca.fr/cdk');
    expect(wrapper.vm.isLocked).toBe(false);
    expect(wrapper.vm.localCompoundDescription).toBe(undefined);
    expect(wrapper.vm.status).toBe(Status.Init);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong> click on the.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-info');
    // test rendering
    expect(wrapper.find(".metabohub-peakforest-cdk-properties").exists()).toBe(true);
    expect(wrapper.find("div").text()).toContain("Info click on the");
    expect(wrapper.find("div").text()).toContain("Compound's description (InChI)");
    // expect(wrapper.find("div").text()).toContain("Enter a lipid normalized name"); // placeholder
    expect(wrapper.find(".hint-info").exists()).toBe(true);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(false);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
    // test set values
    wrapper.vm.isLocked = false;
    wrapper.vm.localCompoundDescription = "~~caffeine~~";
    await sleep(50);
    expect(wrapper.vm.localCompoundDescription).toBe('~~caffeine~~');
    expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.InChI);
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(100);
    // test new values
    expect(wrapper.vm.isLocked).toBe(true);
    expect(wrapper.vm.localCompoundDescription).toBe('~~caffeine~~');
    expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.InChI);
    expect(wrapper.vm.status).toBe(Status.Success);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-success');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(false);
    expect(wrapper.find(".hint-success").exists()).toBe(true);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(false);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-rotate-right");
    // test 'reset' click
    await wrapper.vm.resetCall();
    await sleep(10);
    // test new values
    expect(wrapper.vm.isLocked).toBe(false);
    expect(wrapper.vm.localCompoundDescription).toBe(undefined);
    expect(wrapper.vm.status).toBe(Status.Init);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-info');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(true);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(false);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
    // TEST MOL TEXT
    wrapper.vm.localCompoundDescription = "~~caffeine~~";
    wrapper.vm.localCompoundDescriptionType = CompoundDescriptionTypeEnum.MolText;
    await wrapper.vm.submitToMicroservice();
    await sleep(100);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    await wrapper.vm.resetCall();
    await sleep(10);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
    // TEST MOL FILE
    const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
    wrapper.vm.localCompoundDescription = undefined;
    await wrapper.setProps({ compoundDescriptionFile: f1 });
    await wrapper.setProps({ compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile });
    await sleep(10);
    // manual debug
    wrapper.vm.localCompoundDescriptionFiles = [f1];
    await wrapper.vm.fileChanged();
    await wrapper.vm.submitToMicroservice();
    await sleep(100);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    await wrapper.vm.resetCall();
    await sleep(10);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
    // TEST SDF FILE
    const f2 = new File([`toto1`], "f1.sdf", { type: "text/plain", });
    wrapper.vm.localCompoundDescription = undefined;
    await wrapper.setProps({ compoundDescriptionFile: f2 });
    await wrapper.setProps({ compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile });
    await wrapper.vm.fileChanged();
    await wrapper.vm.submitToMicroservice();
    await sleep(100);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    await wrapper.vm.resetCall();
    await sleep(10);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);

  });
})


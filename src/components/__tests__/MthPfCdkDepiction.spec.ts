// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi, vitest } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCdkDepiction, { CompoundDescriptionTypeEnum, DepictionFormatEnum, HydrogenDisplayEnum } from '@/components/MthPfCdkDepiction.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

test('mount component - send query "undefined"', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: "<?xml version='1.0' encoding='UTF-8'?>...."
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.depictMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
});

test('mount component - option - compoundDescription', async () => {
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global, props: { compoundDescription: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescription).toBe('set by parent');
  // set props
  await wrapper.setProps({ compoundDescription: 'changed!' });
  expect(wrapper.vm.localCompoundDescription).toBe('changed!');
  // set props
  await wrapper.setProps({ 'compoundDescription': 're-changed!' });
  expect(wrapper.vm.localCompoundDescription).toBe('re-changed!');
});

test('mount component - option - compoundDescriptionType', async () => {
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global, props: { compoundDescriptionType: CompoundDescriptionTypeEnum.InChI } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.InChI);
  // set props
  await wrapper.setProps({ compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile });
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.MolFile);
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-cdk-depiction").exists()).toBe(true);
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  // set props
  await wrapper.setProps({ 'compoundDescriptionType': CompoundDescriptionTypeEnum.MolText });
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.MolText);
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-cdk-depiction").exists()).toBe(true);
  expect(wrapper.find(".hint-info").exists()).toBe(true);
});

test('mount component - option - compoundDescriptionFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global, props: { compoundDescriptionFile: undefined } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // set props
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
  // set props
  await wrapper.setProps({ 'compoundDescriptionFile': f2 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f2.mol");
});

test('mount component - option - compoundDescriptionFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global, props: { compoundDescriptionFile: f1 } });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
});

test('mount component - function - fileChanged', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkDepiction,
    {
      global,
      props: {
        compoundDescriptionFile: undefined,
        compoundDescriptionType: CompoundDescriptionTypeEnum.InChI, // wrong to avoid mock request to webservice!
      }
    }
  );
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // CALL METHOD - 1
  await wrapper.vm.fileChanged();
  await sleep(10);
  // set value
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
  // CALL METHOD - 2
  await wrapper.vm.fileChanged();
  await sleep(10);
});

test('mount component - function - getFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.sdf", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkDepiction,
    {
      global,
      props: {
        compoundDescriptionFile: undefined,
        compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile,
      }
    });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // CALL METHOD - 1 
  expect(wrapper.vm.getFile("mol")).toBe(undefined);
  await sleep(10);
  // CALL METHOD - 2 
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.getFile(".mol")?.name).toBe("f1.mol");
  expect(wrapper.vm.getFile(".sdf")).toBe(undefined);
  await sleep(10);
  // CALL METHOD - 3 
  await wrapper.setProps({ compoundDescriptionFile: f2 });
  expect(wrapper.vm.getFile(".mol")).toBe(undefined);
  expect(wrapper.vm.getFile(".sdf")?.name).toBe("f2.sdf");
  await sleep(10);
});

/////////////////////////////////////////////////
// ERRORS

test('mount component - service light error', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: {
        "state": "Failure",
      }
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.depictMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
});

test('mount component - Reject ', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const errorResponse = {
      status: 200,
      data: {}
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.depictMolecule = vi.fn()// 
      .mockRejectedValue(errorResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  const wrapper = mount(MthPfCdkDepiction, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
});

test('mount component - send query "empty"', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: null
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.depictMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  const wrapper = mount(MthPfCdkDepiction, { global, props: { depictionFormat: DepictionFormatEnum.SVG } });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(100);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");

});

test('mount component - autoSubmit param.', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', () => {
    const actual = vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: "<?xml version='1.0' encoding='UTF-8'?>..."
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.depictMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  expect(MthPfCdkDepiction).toBeTruthy()
  const wrapper = mount(MthPfCdkDepiction, { global, props: { compoundDescription: "xxx", autoSubmit: false } });
  // Check initial data
  expect(wrapper.vm.localAutoSubmit).toBe(false);
  await wrapper.vm.autoSubmitAction();
  await sleep(50);
  expect(wrapper.emitted()['cdk-depiction-response']).toBe(undefined)
  // set props
  await wrapper.setProps({ autoSubmit: true });
  expect(wrapper.vm.localAutoSubmit).toBe(true);
  // try autosumit false
  await wrapper.vm.autoSubmitAction();
  await sleep(50);
  expect(wrapper.vm.status).toBe(Status.Success)
  expect(wrapper.emitted()['cdk-depiction-response'].length).toBe(1)
  expect(wrapper.emitted()['cdk-depiction-response'][0]).toStrictEqual(['<?xml version=\'1.0\' encoding=\'UTF-8\'?--------']);
  // set props
  await wrapper.setProps({ 'auto-submit': false });
  expect(wrapper.vm.localAutoSubmit).toBe(false);
});

test('mount component - autoSubmit param.', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', () => {
    const actual = vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: "<?xml version='1.0' encoding='UTF-8'?--------"
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.depictMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  expect(MthPfCdkDepiction).toBeTruthy()
  const wrapper = mount(MthPfCdkDepiction, { global, props: { compoundDescription: "xxx", hideInputOnSuccess: false } });
  // Check initial data
  expect(wrapper.vm.localHideInputOnSuccess).toBe(false);
  // set props
  await wrapper.setProps({ hideInputOnSuccess: true });
  expect(wrapper.vm.localHideInputOnSuccess).toBe(true);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.html()).toContain("Info"); // ;
  expect(wrapper.find('span.hint-info').html()).toContain("Info"); // ;
  // expect(wrapper.findComponent('span.hint-info').exists()).toBe(true);
  await wrapper.vm.submitToMicroservice();
  await sleep(50);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
  expect(wrapper.html()).toContain("Success"); // ;
  // expect(wrapper.find('span.hint-info'))  .toBe(undefined); // ;
  expect(wrapper.findComponent('span.hint-info').exists()).toBe(false);
  // set props
  await wrapper.setProps({ 'hide-input-on-success': false });
  expect(wrapper.vm.localHideInputOnSuccess).toBe(false);
  await sleep(50);
  // recheck
  // expect(wrapper.find('span.hint-info').html()).toContain("Info"); // ;
  //expect(wrapper.findComponent('span.hint-info').exists()).toBe(true);
});

test('mount component - function - resetCallOverwrite', async () => {
  // mount 
  const wrapper = mount(
    MthPfCdkDepiction,
    {
      global,
      // props: { compoundDescription: 'xxx' }
    });
  // Check initial data  
  expect(wrapper.vm.blob).toBe(undefined);
  expect(wrapper.vm.imageSrc).toBe(undefined);
  // Set initial data  
  wrapper.vm.blob = new Blob();
  wrapper.vm.imageSrc = 'yyy';
  expect(wrapper.vm.blob).not.toBe(undefined);
  expect(wrapper.vm.imageSrc).not.toBe(undefined);
  // CALL
  wrapper.vm.resetCallOverwrite()
  // test call
  expect(wrapper.vm.blob).toBe(undefined);
  expect(wrapper.vm.imageSrc).toBe(undefined);
});

const data = [];
// vitest.setup.ts
class FormDataMock {
  append: (name: string, value: string | Blob, fileName?: string) => void = function (n, v) { data[n] = v; };//void = vitest.fn();
  delete: (name: string) => void = vitest.fn();
  get: (name: string) => FormDataEntryValue | null = vitest.fn();
  getAll: (name: string) => FormDataEntryValue[] = function (k) { return data[k] }// vitest.fn();
  has: (name: string) => boolean = function (k) { return k in data };//vitest.fn();
  set: (name: string, value: string | Blob, fileName?: string) => void = vitest.fn();
  forEach: (callbackfn: (value: FormDataEntryValue, key: string, parent: FormData) => void, thisArg?: any) => void = vitest.fn();
}

test('mount component - functions - getBodyFormData / hasBodyFormData', async () => {
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.sdf", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkDepiction,
    {
      global,
      props: { compoundDescription: undefined, compoundDescriptionType: CompoundDescriptionTypeEnum.InChI }
    });
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  const t0 = wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(wrapper.vm.hasBodyFormData(t0)).toBe(false);
  // call 1 
  await wrapper.setProps({ 'compound-description': 'xxx' });
  const t1 = wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe('xxx');
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File)).toBe(undefined);
  expect(wrapper.vm.hasBodyFormData(t1)).toBe(true);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 2
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolText });
  const t2 = wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe('xxx');
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File)).toBe(undefined);
  expect(wrapper.vm.hasBodyFormData(t2)).toBe(true);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 3
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolFile });
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  const t3 = wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File).name).toBe('f1.mol');
  expect((data['sdfFile'] as File)).toBe(undefined);
  expect(wrapper.vm.hasBodyFormData(t3)).toBe(true);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 4
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolFile });
  await wrapper.setProps({ compoundDescriptionFile: f2 });
  const t4 = wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File).name).toBe('f2.sdf');
  expect(wrapper.vm.hasBodyFormData(t4)).toBe(true);
});

test('mount component - option - depictionFormat', async () => {
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global, props: { depictionFormat: DepictionFormatEnum.PNG } });
  // Check initial data
  expect(wrapper.vm.localDepictionFormat).toBe(DepictionFormatEnum.PNG);
  // set props
  await wrapper.setProps({ depictionFormat: DepictionFormatEnum.SVG });
  expect(wrapper.vm.localDepictionFormat).toBe(DepictionFormatEnum.SVG);
  // set props
  await wrapper.setProps({ 'depictionFormat': DepictionFormatEnum.PNG });
  expect(wrapper.vm.localDepictionFormat).toBe(DepictionFormatEnum.PNG);
});

test('mount component - method - getHydrogenDisplay', async () => {
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global });
  // Check initial data
  expect(wrapper.vm.optHydrogenDisplay).toBe(undefined);
  expect(wrapper.vm.getHydrogenDisplay()).toBe(undefined);
  // set props
  await wrapper.setProps({ optHydrogenDisplay: HydrogenDisplayEnum.CDK_DEFAULT });
  expect(wrapper.vm.getHydrogenDisplay()).toBe('cdk-default');
  // set props 
  await wrapper.setProps({ optHydrogenDisplay: HydrogenDisplayEnum.MINIMAL });
  expect(wrapper.vm.getHydrogenDisplay()).toBe('minimal');
  // set props 
  await wrapper.setProps({ optHydrogenDisplay: HydrogenDisplayEnum.NON_CHIRAL });
  expect(wrapper.vm.getHydrogenDisplay()).toBe('non-chiral');
});

test('mount component - method - getGetParameter', async () => {
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global });
  // Check  
  expect(wrapper.vm.getGetParameter('key', undefined)).toBe('');
  expect(wrapper.vm.getGetParameter('key', 'value')).toBe('key=value');
});

test('mount component - method - processError', async () => {
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global });
  // Check
  expect(wrapper.vm.status).toBe(Status.Init);
  // run
  wrapper.vm.processError('xXx_error_xXx')
  // check
  expect(wrapper.vm.status).toBe(Status.Error);
  expect(wrapper.vm.currentHint).toBe("<strong>Error!</strong> The webservice returned this error: xXx_error_xXx.");
  expect(wrapper.emitted()['cdk-depiction-response'][0]).toStrictEqual([undefined]);
  // check 2
  wrapper.vm.currentHint = 'xxx';
  wrapper.vm.processError('HTTP error')
  expect(wrapper.vm.currentHint).toBe("xxx");
});

let file_content = "";
Object.defineProperty(window.URL, 'createObjectURL', {
  value: (data) => { file_content = data; return data.type; }
});

test('mount component - method - getGetParameter', async () => {
  // init
  const blob = new Blob([`MOL\n\n> <K1>\nV1\n\n> <K2>\nV2a; \nV2b; \n\n$$$$\n`], { type: 'text/plain' });
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global });
  // Check  - undef
  wrapper.vm.downloadFile();
  expect(file_content).toStrictEqual('');
  // update
  wrapper.vm.blob = blob;
  wrapper.vm.downloadFile();
  // check - ok
  expect(file_content).toStrictEqual(blob);
  // // test safari
  // Object.defineProperty(window.navigator, 'userAgent', { value: "Safari" });
  // // check - ok
  // expect(file_content).toStrictEqual(blob);
});

test('mount component - method - processBlob', async () => {
  // init
  const blob = new Blob([`MOL\n\n> <K1>\nV1\n\n> <K2>\nV2a; \nV2b; \n\n$$$$\n`], { type: 'text/plain' });
  // mount 
  const wrapper = mount(MthPfCdkDepiction, { global });
  // check before
  expect(wrapper.vm.status).toStrictEqual(Status.Init);
  // test and check: OK
  const content1 = wrapper.vm.processBlob(blob);
  expect(wrapper.vm.blob).toStrictEqual(blob);
  expect(wrapper.vm.status).toStrictEqual(Status.Success);
  expect(content1).toStrictEqual("text/plain");
  // test and check: KO
  const content2 = wrapper.vm.processBlob(undefined);
  expect(wrapper.vm.blob).toStrictEqual(undefined);
  expect(wrapper.vm.status).toStrictEqual(Status.Error);
  expect(content2).toStrictEqual(undefined);
});
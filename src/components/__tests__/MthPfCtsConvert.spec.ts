// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, afterAll } from "vitest";
import vuetify from '@/plugins/vuetify';
import { MetabohubPeakForestBasalLibrary, } from '@metabohub/peakforest-basal-library';
// core test
import MthPfCtsConvert, { CompoundIdentifierType, } from '@/components/MthPfCtsConvert.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';
const server_reject = setupServer(
  rest.get('*/convert/*', (req, res, ctx) => {
    return res(
      // ctx.status(200)
      ctx.json([])
    )
  }),
);
const server_OK = setupServer(
  rest.get('**/convert/**', (req, res, ctx) => {
    return res(
      ctx.json([{
        fromIdentifier: "InChIKey",
        results: ["3-[(2S)-1-methyl-2-pyrrolidinyl]pyridine", "3-[(2S)-1-methylpyrrolidin-2-yl]pyridine"],
        searchTerm: "SNICXCGAKADSCV-JTQLQIEISA-N",
        toIdentifier: "Chemical Name"
      }])
    )
  }),
);

test('mount component - send query "undefined"', async () => {
  // mount 
  const wrapper = mount(MthPfCtsConvert, {
    global,
    props: {
      compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
      compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
    }
  });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
});

test('mount component - option - compoundDescription', async () => {
  // mock
  server_OK.listen();
  // mount 
  const wrapper = mount(MthPfCtsConvert, {
    global, props: {
      compoundDescription: "set by parent",
      compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
      compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
    }
  });
  // Check initial data
  expect(wrapper.vm.localCompoundDescription).toBe('set by parent');
  // set props
  await wrapper.setProps({ compoundDescription: 'changed!' });
  expect(wrapper.vm.localCompoundDescription).toBe('changed!');
  // set props
  await wrapper.setProps({ 'compoundDescription': 're-changed!' });
  expect(wrapper.vm.localCompoundDescription).toBe('re-changed!');
  // unmock
  server_OK.close();
});

test('mount component - option - compoundInputIdentifier/compoundOutputIdentifier', async () => {
  // mock
  server_OK.listen();
  // mount 
  const wrapper = mount(MthPfCtsConvert, {
    global, props: {
      compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
      compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
    }
  });
  // Check initial data
  expect(wrapper.vm.compoundInputIdentifier).toBe('InChIKey');
  // set props
  await wrapper.setProps({ compoundInputIdentifier: 'Chemical Name' });
  expect(wrapper.vm.compoundInputIdentifier).toBe('Chemical Name');
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-cts-convert").exists()).toBe(true);
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  // set props
  await wrapper.setProps({ 'compoundInputIdentifier': 'ChEBI' });
  expect(wrapper.vm.compoundInputIdentifier).toBe('ChEBI');
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-cts-convert").exists()).toBe(true);
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  // unmock
  server_OK.close();
});

test('mount component - Reject ', async () => {
  // mock
  server_reject.listen();
  // mount
  const wrapper = mount(MthPfCtsConvert, {
    global,
    props: {
      compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
      compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
    }
  });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  server_reject.close();
});

test('mount component - send query "empty"', async () => {
  // mock
  server_OK.listen();
  // mount
  const wrapper = mount(MthPfCtsConvert, {
    global,
    props: {
      compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
      compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
    }
  });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  //close
  server_OK.close();
});

test('mount component - function - resetCallOverwrite', async () => {
  server_OK.listen();
  // mount
  const wrapper = mount(
    MthPfCtsConvert,
    {
      global,
      props: {
        compoundDescription: 'xxx',
        compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
        compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
      }
    });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescription).toBe('xxx');
  wrapper.vm.localCompoundDescription = 'yyy';
  expect(wrapper.vm.localCompoundDescription).toBe('yyy');
  // CALL
  wrapper.vm.resetCallOverwrite()// implemented!
  // test call
  expect(wrapper.vm.localCompoundDescription).toBe('xxx');
  //close
  server_OK.close();
});

// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCdkConversion, { ConversionFormatEnum } from '@/components/MthPfCdkConversion.vue';
import { Status } from '../utils/webservicesUtils';
import { MetabohubPeakForestBasalLibrary } from '@metabohub/peakforest-basal-library';

// avoid vuetify error
const global = {
    plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

test('mount component - send query "undef"', async () => {
    // mock
    vi.mock('@/openapi-ts/cdk', () => {
        const actual = vi.importActual("@/openapi-ts/cdk") 
        const Configuration = vi.fn()
        const CdkApi = vi.fn()
        CdkApi.prototype.convertMolecule = vi.fn()// 
        .mockRejectedValue("xxx")
        return {
            ...(actual as object),//
            Configuration, //
            CdkApi,//
        }
    });
    // mount 
    const wrapper = mount(MthPfCdkConversion, { global, props: { depictionFormat: ConversionFormatEnum.InChIKey } });
    // set
    wrapper.vm.localCompoundDescription = "xxx";
    await sleep(10);
    expect(wrapper.vm.localCompoundDescription).toBe("xxx");
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(10);
    // test new values
    expect(wrapper.vm.isLocked).toBe(false);
    expect(wrapper.vm.localCompoundDescription).toBe("xxx");
    expect(wrapper.vm.status).toBe(Status.Error);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-error');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(false);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(true);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-rotate-right");
});
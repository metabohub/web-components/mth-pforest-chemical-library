// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfInchiGeneration, { CompoundDescriptionTypeEnum } from '@/components/MthPfInchiGeneration.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
    plugins: [vuetify]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

test('mount component - 404', async () => {
    // mock
    vi.mock('@/openapi-ts/inchi', async () => {
        const actual = await vi.importActual("@/openapi-ts/inchi")
        const response = {
            status: 404,
            data: { "x": "y" },
        };
        const Configuration = vi.fn()
        const InChIApi = vi.fn()
        InChIApi.prototype.generateInChIs = vi.fn()// 
            .mockResolvedValue(response)
        return {
            ...(actual as object), //
            Configuration, //
            InChIApi,//
        }
    });
    // mount
    expect(MthPfInchiGeneration).toBeTruthy()
    const wrapper = mount(MthPfInchiGeneration, { global, props: { compoundDescription: "~~caffeine~~" } });
    // test 'submit' click!
    await sleep(100);
    await wrapper.vm.submitToMicroservice()
    await sleep(100);
    // test new values 
    expect(wrapper.vm.status).toBe(Status.Error);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-error');
});

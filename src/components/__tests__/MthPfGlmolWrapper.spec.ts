import { mount } from '@vue/test-utils';
import { expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfGlmolWrapper from '../MthPfGlmolWrapper.vue';
import { MoleculeDisplayOption } from '../MthPfGlmolWrapper.vue';
import { Status } from '../utils';

const global = {
    plugins: [vuetify]
}
const crypto = require('crypto');
Object.defineProperty(window, "crypto", {
    value: {
        getRandomValues: arr => crypto.randomBytes(arr.length)
    }
});

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

test('mount component', async () => {
    expect(MthPfGlmolWrapper).toBeTruthy()
});

test('test basic', async () => {
    const wrapper = mount(MthPfGlmolWrapper,
        {
            global,
            props: {
                molFile: `xxx`, // mandatory!
            }
        }
    );
    // Test if the component is rendered correctly 
    expect(wrapper.find('.molGL').exists()).toBe(true);
    expect(wrapper.find('textarea').exists()).toBe(true);
    expect(wrapper.find('textarea').html()).toBe('<textarea data-v-57ff1abd="" id="glmol_000_src" style="display: none;"></textarea>');
    // expect(wrapper.find('.molGL').isVisible()).toBe(false);
    // expect(wrapper.find('textarea').isVisible()).toBe(false);
    // eraly init
    expect(wrapper.find('.progress-loader').exists()).toBe(true);
    expect(wrapper.find('.molGL canvas').exists()).toBe(false);
    // test defaults values
    expect(wrapper.vm.optWidth).toBe(350);
    expect(wrapper.vm.optHeight).toBe(200);
    expect(wrapper.vm.optShowMenuChangeStyle).toBe(true);
    expect(wrapper.vm.optDefaultStyle).toBe('ballsAndSticks');
    expect(wrapper.vm.optShowSaveBtn).toBe(true);
    expect(wrapper.vm.optBackgroundColor).toBe('black');
    // end loading
    await (sleep(100))
    expect(wrapper.find('.progress-loader').exists()).toBe(false);
});

test('test change - mofFile', async () => {
    const wrapper = mount(MthPfGlmolWrapper,
        {
            global,
            props: {
                molFile: `xxx`, // mandatory!
            }
        }
    );
    //  Tests
    expect(wrapper.vm.inputMolFile).toBe('xxx');
    await wrapper.setProps({ 'molFile': 'changed!' });
    expect(wrapper.vm.inputMolFile).toBe('changed!');
    wrapper.vm.inputMolFile = "yyy";
    expect(wrapper.vm.inputMolFile).toBe('yyy');
});

test('test change - optWidth / optHeight', async () => {
    const wrapper = mount(MthPfGlmolWrapper,
        {
            global,
            props: {
                molFile: `xxx`, // mandatory!
                optWidth: 333,
                optHeight: 666,
            }
        }
    );
    //  Tests
    expect(wrapper.vm.optDivWidth).toBe(333);
    expect(wrapper.vm.optDivHeight).toBe(666);
    await wrapper.setProps({ 'optWidth': 444 });
    await wrapper.setProps({ 'optHeight': 555 });
    expect(wrapper.vm.optDivWidth).toBe(444);
    expect(wrapper.vm.optDivHeight).toBe(555);
});

test('test change - optShowMenuChangeStyle / optDefaultStyle / optBackgroundColor / displayOption', async () => {
    const wrapper = mount(MthPfGlmolWrapper,
        {
            global,
            props: {
                molFile: `xxx`, // mandatory!
                optShowMenuChangeStyle: false,
                optDefaultStyle: 'sticks',
                optBackgroundColor: 'red',
            }
        }
    );
    //  Tests
    expect(wrapper.vm.optStyleShow).toBe(false);
    expect(wrapper.vm.optStyle).toBe('sticks');
    expect(wrapper.vm.optBgClr).toBe('red');
    await wrapper.setProps({ 'optShowMenuChangeStyle': true });
    await wrapper.setProps({ 'optDefaultStyle': 'spheres' });
    await wrapper.setProps({ 'optBackgroundColor': 'blue' });
    expect(wrapper.vm.optStyleShow).toBe(true);
    expect(wrapper.vm.optStyle).toBe('spheres');
    expect(wrapper.vm.optBgClr).toBe('blue');
    // displayOption
    wrapper.vm.displayOption = { title: 'string', value: MoleculeDisplayOption.Lines };
    sleep(100);
    //    expect(wrapper.vm.optStyle).toBe('lines');
});

test('test change - optShowSaveBtn', async () => {
    const wrapper = mount(MthPfGlmolWrapper,
        {
            global,
            props: {
                molFile: `xxx`, // mandatory!
                optShowSaveBtn: false,
            }
        }
    );
    //  Tests
    expect(wrapper.vm.optSaveShow).toBe(false);
    await wrapper.setProps({ 'optShowSaveBtn': true });
    expect(wrapper.vm.optSaveShow).toBe(true);
    // click
    const button = wrapper.find('.v-btn');
    await button.trigger('click');
    await wrapper.vm.saveImage();
    // how to test it?
});

test('test change - loadingValue / status ', async () => {
    const wrapper = mount(MthPfGlmolWrapper,
        {
            global,
            props: {
                molFile: `xxx`, // mandatory!
                optShowSaveBtn: false,
            }
        }
    );
    //  Tests
    expect(wrapper.vm.status).toBe(Status.Init);
    expect(wrapper.vm.loadingValue).toBe(0);
    wrapper.vm.loadingValue = 100;
    await wrapper.vm.updateProgress();
    expect(wrapper.vm.loadingValue).not.toBe(0);
    // how to test it?
});

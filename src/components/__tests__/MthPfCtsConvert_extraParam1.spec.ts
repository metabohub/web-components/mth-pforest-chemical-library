// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, } from "vitest";
import vuetify from '@/plugins/vuetify';
import { MetabohubPeakForestBasalLibrary, } from '@metabohub/peakforest-basal-library';
// core test
import MthPfCtsConvert, { CompoundIdentifierType, } from '@/components/MthPfCtsConvert.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';
const server_OK = setupServer(
  rest.get('*/convert/*', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json([{
        fromIdentifier: "InChIKey",
        results: ["3-[(2S)-1-methyl-2-pyrrolidinyl]pyridine", "3-[(2S)-1-methylpyrrolidin-2-yl]pyridine"],
        searchTerm: "SNICXCGAKADSCV-JTQLQIEISA-N",
        toIdentifier: "Chemical Name"
      }])
    )
  }),
);

test('mount component - hideInputOnSuccess  param.', async () => {
  server_OK.listen();
  // mount
  expect(MthPfCtsConvert).toBeTruthy()
  const wrapper = mount(MthPfCtsConvert, {
    global, props: {
      compoundDescription: "SNICXCGAKADSCV-JTQLQIEISA-N",
      hideInputOnSuccess: false,
      compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
      compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
    }
  });
  // Check initial data
  expect(wrapper.vm.localHideInputOnSuccess).toBe(false);
  // set props
  await wrapper.setProps({ hideInputOnSuccess: true });
  expect(wrapper.vm.localHideInputOnSuccess).toBe(true);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/); // not yet submitted 
  expect(wrapper.html()).toContain("Info");
  expect(wrapper.find('span.hint-info').html()).toContain("Info"); // ;
  await wrapper.vm.submitToMicroservice(); // REAL SUBMIT
  await sleep(250);
  expect(wrapper.vm.status).toBe(Status.Success);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
  expect(wrapper.html()).toContain("Success");
  expect(wrapper.findComponent('span.hint-info').exists()).toBe(false);
  // set props
  await wrapper.setProps({ 'hide-input-on-success': false });
  expect(wrapper.vm.localHideInputOnSuccess).toBe(false);
  // unmock
  server_OK.close();
});

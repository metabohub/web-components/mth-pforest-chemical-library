// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi, vitest } from "vitest";
import vuetify from '@/plugins/vuetify';
import { MetabohubPeakForestBasalLibrary, PropertyTypeEnum } from '@metabohub/peakforest-basal-library';
// core test
import MthPfCdkConversion, { CompoundDescriptionTypeEnum, ConversionFormatEnum } from '@/components/MthPfCdkConversion.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

test('mount component - send query "undefined"', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: "inchikey=xxxxx"
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.convertMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
});

test('mount component - option - compoundDescription', async () => {
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global, props: { compoundDescription: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescription).toBe('set by parent');
  // set props
  await wrapper.setProps({ compoundDescription: 'changed!' });
  expect(wrapper.vm.localCompoundDescription).toBe('changed!');
  // set props
  await wrapper.setProps({ 'compoundDescription': 're-changed!' });
  expect(wrapper.vm.localCompoundDescription).toBe('re-changed!');
});

test('mount component - option - compoundDescriptionType', async () => {
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global, props: { compoundDescriptionType: CompoundDescriptionTypeEnum.InChI } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.InChI);
  // set props
  await wrapper.setProps({ compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile });
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.MolFile);
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-cdk-conversion").exists()).toBe(true);
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  // set props
  await wrapper.setProps({ 'compoundDescriptionType': CompoundDescriptionTypeEnum.MolText });
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.MolText);
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-cdk-conversion").exists()).toBe(true);
  expect(wrapper.find(".hint-info").exists()).toBe(true);
});

test('mount component - option - compoundDescriptionFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global, props: { compoundDescriptionFile: undefined } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // set props
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
  // set props
  await wrapper.setProps({ 'compoundDescriptionFile': f2 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f2.mol");
});

test('mount component - option - compoundDescriptionFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global, props: { compoundDescriptionFile: f1 } });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
});

test('mount component - function - fileChanged', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkConversion,
    {
      global,
      props: {
        compoundDescriptionFile: undefined,
        compoundDescriptionType: CompoundDescriptionTypeEnum.InChI, // wrong to avoid mock request to webservice!
      }
    }
  );
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // CALL METHOD - 1
  await wrapper.vm.fileChanged();
  await sleep(10);
  // set value
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
  // CALL METHOD - 2
  await wrapper.vm.fileChanged();
  await sleep(10);
});

test('mount component - function - getFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.sdf", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkConversion,
    {
      global,
      props: {
        compoundDescriptionFile: undefined,
        compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile,
      }
    });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // CALL METHOD - 1 
  expect(wrapper.vm.getFile("mol")).toBe(undefined);
  await sleep(10);
  // CALL METHOD - 2 
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.getFile(".mol")?.name).toBe("f1.mol");
  expect(wrapper.vm.getFile(".sdf")).toBe(undefined);
  await sleep(10);
  // CALL METHOD - 3 
  await wrapper.setProps({ compoundDescriptionFile: f2 });
  expect(wrapper.vm.getFile(".mol")).toBe(undefined);
  expect(wrapper.vm.getFile(".sdf")?.name).toBe("f2.sdf");
  await sleep(10);
});

/////////////////////////////////////////////////
// ERRORS

test('mount component - service light error', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: {
        "state": "Failure",
      }
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.convertMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  vi.unmock('@/openapi-ts/cdk');
});

test('mount component - Reject ', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const errorResponse = {
      status: 200,
      data: {}
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.convertMolecule = vi.fn()// 
      .mockRejectedValue(errorResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  const wrapper = mount(MthPfCdkConversion, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  vi.unmock('@/openapi-ts/cdk');
});

test('mount component - send query "empty"', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: {}
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.convertMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  const wrapper = mount(MthPfCdkConversion, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  vi.unmock('@/openapi-ts/cdk');
});

test('mount component - autoSubmit param.', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: "xxxx"
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.convertMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  expect(MthPfCdkConversion).toBeTruthy()
  const wrapper = mount(MthPfCdkConversion, { global, props: { compoundDescription: "xxx", autoSubmit: false } });
  // Check initial data
  expect(wrapper.vm.localAutoSubmit).toBe(false);
  await wrapper.vm.autoSubmitAction();
  await sleep(50);
  expect(wrapper.emitted()['cdk-conversion-response']).toBe(undefined)
  // set props
  await wrapper.setProps({ autoSubmit: true });
  expect(wrapper.vm.localAutoSubmit).toBe(true);
  // try autosumit false
  await wrapper.vm.autoSubmitAction();
  await sleep(50);
  expect(wrapper.vm.status).toBe(Status.Success)
  expect(wrapper.emitted()['cdk-conversion-response'].length).toBe(1)
  expect(wrapper.emitted()['cdk-conversion-response'][0]).toStrictEqual(['xxx'])
  // set props
  await wrapper.setProps({ 'auto-submit': false });
  expect(wrapper.vm.localAutoSubmit).toBe(false);
});

test('mount component - autoSubmit param.', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: "xxx"
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.convertMolecule = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  expect(MthPfCdkConversion).toBeTruthy()
  const wrapper = mount(MthPfCdkConversion, { global, props: { compoundDescription: "xxx", hideInputOnSuccess: false } });
  // Check initial data
  expect(wrapper.vm.localHideInputOnSuccess).toBe(false);
  // set props
  await wrapper.setProps({ hideInputOnSuccess: true });
  expect(wrapper.vm.localHideInputOnSuccess).toBe(true);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.html()).toContain("Info"); // ;
  expect(wrapper.find('span.hint-info').html()).toContain("Info"); // ;
  // expect(wrapper.findComponent('span.hint-info').exists()).toBe(true);
  await wrapper.vm.submitToMicroservice();
  await sleep(50);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
  expect(wrapper.html()).toContain("Success"); // ;
  // expect(wrapper.find('span.hint-info'))  .toBe(undefined); // ;
  expect(wrapper.findComponent('span.hint-info').exists()).toBe(false);
  // set props
  await wrapper.setProps({ 'hide-input-on-success': false });
  expect(wrapper.vm.localHideInputOnSuccess).toBe(false);
  await sleep(50);
  // recheck
  // expect(wrapper.find('span.hint-info').html()).toContain("Info"); // ;
  //expect(wrapper.findComponent('span.hint-info').exists()).toBe(true);
});

test('mount component - function - resetCallOverwrite', async () => {
  // mount 
  const wrapper = mount(
    MthPfCdkConversion,
    {
      global,
      props: { compoundDescription: 'xxx' }
    });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescription).toBe('xxx');
  wrapper.vm.localCompoundDescription = 'yyy';
  expect(wrapper.vm.localCompoundDescription).toBe('yyy');
  // CALL
  wrapper.vm.resetCallOverwrite()// not implemented
  // test call
  expect(wrapper.vm.localCompoundDescription).toBe('yyy');
});

const data = [];
// vitest.setup.ts
class FormDataMock {
  append: (name: string, value: string | Blob, fileName?: string) => void = function (n, v) { data[n] = v; };//void = vitest.fn();
  delete: (name: string) => void = vitest.fn();
  get: (name: string) => FormDataEntryValue | null = vitest.fn();
  getAll: (name: string) => FormDataEntryValue[] = vitest.fn();
  has: (name: string) => boolean = vitest.fn();
  set: (name: string, value: string | Blob, fileName?: string) => void = vitest.fn();
  forEach: (callbackfn: (value: FormDataEntryValue, key: string, parent: FormData) => void, thisArg?: any) => void = vitest.fn();
}

test('mount component - function - getBodyFormData', async () => {
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.sdf", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkConversion,
    {
      global,
      props: { compoundDescription: 'xxx', compoundDescriptionType: CompoundDescriptionTypeEnum.InChI }
    });
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 1 
  wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe('xxx');
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File)).toBe(undefined);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 2
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolText });
  wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe('xxx');
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File)).toBe(undefined);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 3
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolFile });
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File).name).toBe('f1.mol');
  expect((data['sdfFile'] as File)).toBe(undefined);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 3
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolFile });
  await wrapper.setProps({ compoundDescriptionFile: f2 });
  wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File).name).toBe('f2.sdf');
});

test('mount component - option - conversionFormat', async () => {
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global, props: { conversionFormat: ConversionFormatEnum.MOL } });
  // Check initial data 
  expect(wrapper.vm.localConversionFormat).toBe(ConversionFormatEnum.MOL);
  expect(wrapper.vm.outputType).toBe(PropertyTypeEnum.Textarea);
  // set
  await wrapper.setProps({ conversionFormat: ConversionFormatEnum.InChI });
  // check update
  expect(wrapper.vm.localConversionFormat).toBe(ConversionFormatEnum.InChI);
  expect(wrapper.vm.outputType).toBe(PropertyTypeEnum.TextField);
});

let file_content = "";
Object.defineProperty(window.URL, 'createObjectURL', {
  value: (data) => { file_content = data; return data.type; }
});
test('mount component - method - getGetParameter', async () => {
  // init
  const blob = new Blob([`MOL\n\n> <K1>\nV1\n\n> <K2>\nV2a; \nV2b; \n\n$$$$\n`], { type: 'text/plain' });
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global });
  // Check  - undef
  wrapper.vm.downloadFile();
  expect(file_content).toStrictEqual('');
  // update
  wrapper.vm.blob = blob;
  wrapper.vm.downloadFile();
  // check - ok
  expect(file_content).toStrictEqual(blob);
  // // test safari
  // Object.defineProperty(window.navigator, 'userAgent', { value: "Safari" });
  // // check - ok
  // expect(file_content).toStrictEqual(blob);
});

test('mount component - method - getOutFormat', async () => {
  // mount 
  const wrapper = mount(MthPfCdkConversion, { global, props: { conversionFormat: ConversionFormatEnum.MOL } });
  // Check initial data 
  expect(wrapper.vm.localConversionFormat).toBe(ConversionFormatEnum.MOL);
  expect(wrapper.vm.getOutFormat()).toBe('mol');
  // set
  await wrapper.setProps({ conversionFormat: ConversionFormatEnum.InChI });
  // check update
  expect(wrapper.vm.localConversionFormat).toBe(ConversionFormatEnum.InChI);
  expect(wrapper.vm.getOutFormat()).toBe('inchi');
  // set
  await wrapper.setProps({ conversionFormat: ConversionFormatEnum.InChIKey });
  // check update
  expect(wrapper.vm.localConversionFormat).toBe(ConversionFormatEnum.InChIKey);
  expect(wrapper.vm.getOutFormat()).toBe('inchikey');
  // set
  await wrapper.setProps({ conversionFormat: ConversionFormatEnum.SDF });
  // check update
  expect(wrapper.vm.localConversionFormat).toBe(ConversionFormatEnum.SDF);
  expect(wrapper.vm.getOutFormat()).toBe('sdf');
});
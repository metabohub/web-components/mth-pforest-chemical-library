// import for tests
import { mount } from '@vue/test-utils';
import { describe, expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfInchiGeneration, { CompoundDescriptionTypeEnum } from '@/components/MthPfInchiGeneration.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
    plugins: [vuetify]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

describe('error mode', () => {
    test('mount component - mock empty json', async () => {
        // mock
        vi.mock('@/openapi-ts/inchi', async () => {
            const actual = await vi.importActual("@/openapi-ts/inchi")
            const successResponse = {
                status: 200,
                data: {}
            };
            const Configuration = vi.fn()
            const InChIApi = vi.fn()
            InChIApi.prototype.generateInChIs = vi.fn()// 
                .mockResolvedValue(successResponse)
            return {
                ...(actual as object),//
                Configuration, //
                InChIApi: InChIApi,//
            }
        });
        // mount
        expect(MthPfInchiGeneration).toBeTruthy()
        const wrapper = mount(MthPfInchiGeneration, { global, props: { compoundDescription: "~~caffeine~~", compoundDescriptionType: CompoundDescriptionTypeEnum.MolText, } });
        // fake init
        wrapper.vm.localCompoundDescription = '~~caffeine~~';
        // test 'submit' click!
        await sleep(50);
        await wrapper.vm.submitToMicroservice()
        await sleep(50);
        // test new values
        expect(wrapper.vm.isLocked).toBe(true);
        expect(wrapper.vm.localCompoundDescription).toBe('~~caffeine~~');
        expect(wrapper.vm.status).toBe(Status.Error);
        expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
        expect(wrapper.vm.currentHintClasses).toBe('hint-error');
        // unmock
        vi.unmock('@/openapi-ts/inchi');
    });

});

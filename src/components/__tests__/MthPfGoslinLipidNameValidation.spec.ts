// import for tests
import { mount } from '@vue/test-utils';
import { afterAll, expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfGoslinLipidNameValidation from '@/components/MthPfGoslinLipidNameValidation.vue';
import { Status } from '../utils/webservicesUtils';

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const serverOK = setupServer(
  rest.get(/\/goslin\/validate.*/, (req, res, ctx) => {
    return res(
      ctx.json({
        "nb_total_not_parsed": 0,
        "nb_not_parsed_uncompleted": 0,
        "nb_not_parsed_failures": 0,
        "lipid_list": [
          {
            "state": "Success",
            "normalized_name": "PC 13:0",
            "original_name": "PC 13:0",
            "grammar": "Shorthand2020",
            "lipidmaps_category": "GP",
            "lipidmaps_class": "Glycerophosphocholines [GP01]",
            "class_name": "PC",
            "extended_class": "PC",
            "lipid_level": "SPECIES",
            "mass": 467.26480451000003,
            "sum_formula": "C21H42NO8P",
            "lipidmaps_data": "Standardized name not link with any entry in the LipidMaps Structure Database"
          }
        ],
        "nb_success": 1,
        "nb_total_lipids": 1
      }),
    )
  }),
);

const serverKO_empty_json = setupServer(
  rest.get(/\/goslin\/validate.*/, (req, res, ctx) => {
    return res(
      ctx.json({}),
    )
  }),
);

const serverKO_thow_error = setupServer(
  rest.get(/\/goslin\/validate.*/, (req, res, ctx) => {
    return res(ctx.status(500), ctx.json({}))
  }),
);

const serverKO_no_json = setupServer(
  rest.get(/\/goslin\/validate.*/, (req, res, ctx) => {
    return res(ctx.status(200))
  }),
);

const serverKO_not_found = setupServer(
  rest.get(/\/goslin\/validate.*/, (req, res, ctx) => {
    return res(
      ctx.json({
        "nb_total_not_parsed": 1,
        "nb_not_parsed_uncompleted": 0,
        "nb_not_parsed_failures": 1,
        "lipid_list": [
          {
            "state": "Failure",
            "original_name": "--return error--",
            "messages": "Lipid not found"
          }
        ],
        "nb_success": 0,
        "nb_total_lipids": 1
      }),
    )
  }),
);

afterAll(() => {
  // Clean up after all tests are done, preventing this
  // interception layer from affecting irrelevant tests.
  serverOK.close();
  serverKO_empty_json.close();
  serverKO_thow_error.close();
  serverKO_no_json.close();
  serverKO_not_found.close();
})

// avoid vuetify error
const global = {
  plugins: [vuetify]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

test('mount component - mock OK', async () => {
  // mock
  serverOK.listen()
  // mount
  expect(MthPfGoslinLipidNameValidation).toBeTruthy()
  const wrapper = mount(MthPfGoslinLipidNameValidation, { global });
  // Check initial values
  expect(wrapper.vm.conf.basePath).toBe('https://metabocloud.mesocentre.uca.fr/goslin');
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localLipidNormalizedName).toBe('');
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong> click on the.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-goslin-lipid-name-validation").exists()).toBe(true);
  expect(wrapper.find("div").text()).toContain("Info click on the");
  expect(wrapper.find("div").text()).toContain("Lipid's normalized name");
  // expect(wrapper.find("div").text()).toContain("Enter a lipid normalized name"); // placeholder
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // test set values
  wrapper.vm.isLocked = false;
  wrapper.vm.localLipidNormalizedName = "PC 13:0";
  await sleep(10);
  expect(wrapper.vm.localLipidNormalizedName).toBe('PC 13:0');
  // test 'submit' click!
  await wrapper.vm.submitToGoslinMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(true);
  expect(wrapper.vm.localLipidNormalizedName).toBe('PC 13:0');
  expect(wrapper.vm.status).toBe(Status.Success);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-success');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(false);
  expect(wrapper.find(".hint-success").exists()).toBe(true);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-rotate-right");
  // test 'reset' click
  await wrapper.vm.resetCall();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localLipidNormalizedName).toBe('');
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  serverOK.close();
});

test('mount component - send query "null"', async () => {
  // mock
  serverOK.listen()
  // mount
  const wrapper = mount(MthPfGoslinLipidNameValidation, { global });
  // set
  wrapper.vm.localLipidNormalizedName = null;
  await sleep(10);
  expect(wrapper.vm.localLipidNormalizedName).toBe(null);
  // test 'submit' click!
  await wrapper.vm.submitToGoslinMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localLipidNormalizedName).toBe(null);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  serverOK.close();
});

test('mount component - send query "empty"', async () => {
  // mock
  serverOK.listen()
  // mount
  const wrapper = mount(MthPfGoslinLipidNameValidation, { global });
  // set
  wrapper.vm.localLipidNormalizedName = null;
  await sleep(10);
  expect(wrapper.vm.localLipidNormalizedName).toBe(null);
  // test 'submit' click!
  await wrapper.vm.submitToGoslinMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localLipidNormalizedName).toBe(null);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  serverOK.close();
});

test('mount component - mock empty json', async () => {
  // mock
  serverKO_empty_json.listen()
  // mount
  expect(MthPfGoslinLipidNameValidation).toBeTruthy()
  const wrapper = mount(MthPfGoslinLipidNameValidation, { global });
  // fake init
  wrapper.vm.localLipidNormalizedName = 'PC 14:0';
  // test 'submit' click!
  await wrapper.vm.submitToGoslinMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(true);
  expect(wrapper.vm.localLipidNormalizedName).toBe('PC 14:0');
  expect(wrapper.vm.status).toBe(Status.Success);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // unmock
  serverKO_empty_json.close();
});

test('mount component - mock throw error', async () => {
  // mock
  serverKO_thow_error.listen()
  // mount
  expect(MthPfGoslinLipidNameValidation).toBeTruthy()
  const wrapper = mount(MthPfGoslinLipidNameValidation, { global });
  // fake init
  wrapper.vm.localLipidNormalizedName = 'PC 14:0';
  // test 'submit' click!
  await wrapper.vm.submitToGoslinMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(true);
  expect(wrapper.vm.localLipidNormalizedName).toBe('PC 14:0');
  expect(wrapper.vm.status).toBe(Status.Error);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-error');
  // unmock
  serverKO_thow_error.close();
});

test('mount component - mock return no json', async () => {
  // mock
  serverKO_no_json.listen()
  // mount
  expect(MthPfGoslinLipidNameValidation).toBeTruthy()
  const wrapper = mount(MthPfGoslinLipidNameValidation, { global });
  // fake init
  wrapper.vm.localLipidNormalizedName = 'PC 14:0';
  // test 'submit' click!
  await wrapper.vm.submitToGoslinMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(true);
  expect(wrapper.vm.localLipidNormalizedName).toBe('PC 14:0');
  expect(wrapper.vm.status).toBe(Status.Error);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-error');
  // unmock
  serverKO_no_json.close();
});

test('mount component - mock return not found', async () => {
  // mock
  serverKO_not_found.listen()
  // mount
  expect(MthPfGoslinLipidNameValidation).toBeTruthy()
  const wrapper = mount(MthPfGoslinLipidNameValidation, { global });
  // fake init
  wrapper.vm.localLipidNormalizedName = 'PC 14:0';
  // test 'submit' click!
  await wrapper.vm.submitToGoslinMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(true);
  expect(wrapper.vm.localLipidNormalizedName).toBe('PC 14:0');
  expect(wrapper.vm.status).toBe(Status.Success);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Warning!<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-warning');
  // unmock
  serverKO_not_found.close();
});

test('mount component - option - lipidNormalizedName', async () => {
  // mount 
  const wrapper = mount(MthPfGoslinLipidNameValidation, { global, props: { lipidNormalizedName: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.localLipidNormalizedName).toBe('set by parent');
  // set props
  await wrapper.setProps({ lipidNormalizedName: 'changed!' });
  expect(wrapper.vm.localLipidNormalizedName).toBe('changed!');
  // set props
  await wrapper.setProps({ 'lipidNormalizedName': 're-changed!' });
  expect(wrapper.vm.localLipidNormalizedName).toBe('re-changed!');
});
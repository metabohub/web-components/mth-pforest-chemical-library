// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi, vitest } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCdkProperties, { CompoundDescriptionTypeEnum } from '@/components/MthPfCdkProperties.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

test('mount component - send query "undefined"', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: {
        "state": "Success",
        "formula": "C8H10N4O2",
        "monoisotopic_mass": 194.08037556,
        "average_mass": 194.19091739584468,
        "canonical_smiles": "O=C1C2=C(N=CN2C)N(C(=O)N1C)C",
        "stereo_smiles": "CN1C=NC2=C1C(N(C)C(N2C)=O)=O",
        "inchi": "InChI=1S/C8H10N4O2/c1-10-4-9-6-5(10)7(13)12(3)8(14)11(6)2/h4H,1-3H3",
        "inchikey": "RYYVLZVUVIJVGH-UHFFFAOYSA-N",
        "logP": -0.10858213188126853,
        "CDK_version": "2.9"
      }
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.computeProperties = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount 
  const wrapper = mount(MthPfCdkProperties, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
});

test('mount component - option - compoundDescription', async () => {
  // mount 
  const wrapper = mount(MthPfCdkProperties, { global, props: { compoundDescription: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescription).toBe('set by parent');
  // set props
  await wrapper.setProps({ compoundDescription: 'changed!' });
  expect(wrapper.vm.localCompoundDescription).toBe('changed!');
  // set props
  await wrapper.setProps({ 'compoundDescription': 're-changed!' });
  expect(wrapper.vm.localCompoundDescription).toBe('re-changed!');
});

test('mount component - option - compoundDescriptionType', async () => {
  // mount 
  const wrapper = mount(MthPfCdkProperties, { global, props: { compoundDescriptionType: CompoundDescriptionTypeEnum.InChI } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.InChI);
  // set props
  await wrapper.setProps({ compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile });
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.MolFile);
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-cdk-properties").exists()).toBe(true);
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  // set props
  await wrapper.setProps({ 'compoundDescriptionType': CompoundDescriptionTypeEnum.MolText });
  expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.MolText);
  // test rendering
  expect(wrapper.find(".metabohub-peakforest-cdk-properties").exists()).toBe(true);
  expect(wrapper.find(".hint-info").exists()).toBe(true);
});

test('mount component - option - compoundDescriptionFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(MthPfCdkProperties, { global, props: { compoundDescriptionFile: undefined } });
  // Check initial data
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // set props
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
  // set props
  await wrapper.setProps({ 'compoundDescriptionFile': f2 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f2.mol");
});

test('mount component - option - compoundDescriptionFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(MthPfCdkProperties, { global, props: { compoundDescriptionFile: f1 } });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
});

test('mount component - function - fileChanged', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkProperties,
    {
      global,
      props: {
        compoundDescriptionFile: undefined,
        compoundDescriptionType: CompoundDescriptionTypeEnum.InChI, // wrong to avoid mock request to webservice!
      }
    }
  );
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // CALL METHOD - 1
  await wrapper.vm.fileChanged();
  await sleep(10);
  // set value
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.localCompoundDescriptionFile?.name).toBe("f1.mol");
  // CALL METHOD - 2
  await wrapper.vm.fileChanged();
  await sleep(10);
});

test('mount component - function - getFile', async () => {
  // init 
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.sdf", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkProperties,
    {
      global,
      props: {
        compoundDescriptionFile: undefined,
        compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile,
      }
    });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescriptionFile).toBe(undefined);
  // CALL METHOD - 1 
  expect(wrapper.vm.getFile("mol")).toBe(undefined);
  await sleep(10);
  // CALL METHOD - 2 
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  expect(wrapper.vm.getFile(".mol")?.name).toBe("f1.mol");
  expect(wrapper.vm.getFile(".sdf")).toBe(undefined);
  await sleep(10);
  // CALL METHOD - 3 
  await wrapper.setProps({ compoundDescriptionFile: f2 });
  expect(wrapper.vm.getFile(".mol")).toBe(undefined);
  expect(wrapper.vm.getFile(".sdf")?.name).toBe("f2.sdf");
  await sleep(10);
});

/////////////////////////////////////////////////
// ERRORS

test('mount component - service light error', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: {
        "state": "Failure",
      }
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.computeProperties = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount 
  const wrapper = mount(MthPfCdkProperties, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  vi.unmock('@/openapi-ts/cdk');
});

test('mount component - Reject ', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const errorResponse = {
      status: 200,
      data: {}
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.computeProperties = vi.fn()// 
      .mockRejectedValue(errorResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  const wrapper = mount(MthPfCdkProperties, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  vi.unmock('@/openapi-ts/cdk');
});

test('mount component - send query "empty"', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: {}
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.computeProperties = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  const wrapper = mount(MthPfCdkProperties, { global });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  vi.unmock('@/openapi-ts/cdk');
});

test('mount component - autoSubmit param.', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: {
        "state": "Success",
        "formula": "C8H10N4O2",
        "monoisotopic_mass": 194.08037556,
        "average_mass": 194.19091739584468,
        "canonical_smiles": "O=C1C2=C(N=CN2C)N(C(=O)N1C)C",
        "stereo_smiles": "CN1C=NC2=C1C(N(C)C(N2C)=O)=O",
        "inchi": "InChI=1S/C8H10N4O2/c1-10-4-9-6-5(10)7(13)12(3)8(14)11(6)2/h4H,1-3H3",
        "inchikey": "RYYVLZVUVIJVGH-UHFFFAOYSA-N",
        "logP": -0.10858213188126853,
        "CDK_version": "2.9"
      }
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.computeProperties = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  expect(MthPfCdkProperties).toBeTruthy()
  const wrapper = mount(MthPfCdkProperties, { global, props: { compoundDescription: "xxx", autoSubmit: false } });
  // Check initial data
  expect(wrapper.vm.localAutoSubmit).toBe(false);
  await wrapper.vm.autoSubmitAction();
  await sleep(50);
  expect(wrapper.emitted()['cdk-properties-response']).toBe(undefined)
  // set props
  await wrapper.setProps({ autoSubmit: true });
  expect(wrapper.vm.localAutoSubmit).toBe(true);
  // try autosumit false
  await wrapper.vm.autoSubmitAction();
  await sleep(50);
  expect(wrapper.vm.status).toBe(Status.Success)
  expect(wrapper.emitted()['cdk-properties-response'].length).toBe(1)
  expect(wrapper.emitted()['cdk-properties-response'][0]).toHaveProperty('[0].state', 'Success')
  // set props
  await wrapper.setProps({ 'auto-submit': false });
  expect(wrapper.vm.localAutoSubmit).toBe(false);
});

test('mount component - autoSubmit param.', async () => {
  // mock
  vi.mock('@/openapi-ts/cdk', async () => {
    const actual = await vi.importActual("@/openapi-ts/cdk")
    const successResponse = {
      status: 200,
      data: {
        "state": "Success",
        "formula": "C8H10N4O2",
        "monoisotopic_mass": 194.08037556,
        "average_mass": 194.19091739584468,
        "canonical_smiles": "O=C1C2=C(N=CN2C)N(C(=O)N1C)C",
        "stereo_smiles": "CN1C=NC2=C1C(N(C)C(N2C)=O)=O",
        "inchi": "InChI=1S/C8H10N4O2/c1-10-4-9-6-5(10)7(13)12(3)8(14)11(6)2/h4H,1-3H3",
        "inchikey": "RYYVLZVUVIJVGH-UHFFFAOYSA-N",
        "logP": -0.10858213188126853,
        "CDK_version": "2.9"
      }
    };
    const Configuration = vi.fn()
    const CdkApi = vi.fn()
    CdkApi.prototype.computeProperties = vi.fn()// 
      .mockResolvedValue(successResponse)
    return {
      ...(actual as object),//
      Configuration, //
      CdkApi,//
    }
  });
  // mount
  expect(MthPfCdkProperties).toBeTruthy()
  const wrapper = mount(MthPfCdkProperties, { global, props: { compoundDescription: "xxx", hideInputOnSuccess: false } });
  // Check initial data
  expect(wrapper.vm.localHideInputOnSuccess).toBe(false);
  // set props
  await wrapper.setProps({ hideInputOnSuccess: true });
  expect(wrapper.vm.localHideInputOnSuccess).toBe(true);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.html()).toContain("Info"); // ;
  expect(wrapper.find('span.hint-info').html()).toContain("Info"); // ;
  // expect(wrapper.findComponent('span.hint-info').exists()).toBe(true);
  await wrapper.vm.submitToMicroservice();
  await sleep(50);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
  expect(wrapper.html()).toContain("Success"); // ;
  // expect(wrapper.find('span.hint-info'))  .toBe(undefined); // ;
  expect(wrapper.findComponent('span.hint-info').exists()).toBe(false);
  // set props
  await wrapper.setProps({ 'hide-input-on-success': false });
  expect(wrapper.vm.localHideInputOnSuccess).toBe(false);
  await sleep(50);
  // recheck
  // expect(wrapper.find('span.hint-info').html()).toContain("Info"); // ;
  //expect(wrapper.findComponent('span.hint-info').exists()).toBe(true);
});

test('mount component - function - resetCallOverwrite', async () => {
  // mount 
  const wrapper = mount(
    MthPfCdkProperties,
    {
      global,
      props: { compoundDescription: 'xxx' }
    });
  // Check initial data 
  expect(wrapper.vm.localCompoundDescription).toBe('xxx');
  wrapper.vm.localCompoundDescription = 'yyy';
  expect(wrapper.vm.localCompoundDescription).toBe('yyy');
  // CALL
  wrapper.vm.resetCallOverwrite()
  // test call
  expect(wrapper.vm.localCompoundDescription).toBe('xxx');
});

const data = [];
// vitest.setup.ts
class FormDataMock {
  append: (name: string, value: string | Blob, fileName?: string) => void = function (n, v) { data[n] = v; };//void = vitest.fn();
  delete: (name: string) => void = vitest.fn();
  get: (name: string) => FormDataEntryValue | null = vitest.fn();
  getAll: (name: string) => FormDataEntryValue[] = vitest.fn();
  has: (name: string) => boolean = vitest.fn();
  set: (name: string, value: string | Blob, fileName?: string) => void = vitest.fn();
  forEach: (callbackfn: (value: FormDataEntryValue, key: string, parent: FormData) => void, thisArg?: any) => void = vitest.fn();
}

test('mount component - function - getBodyFormData', async () => {
  const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
  const f2 = new File([`toto2`], "f2.sdf", { type: "text/plain", });
  // mount 
  const wrapper = mount(
    MthPfCdkProperties,
    {
      global,
      props: { compoundDescription: 'xxx', compoundDescriptionType: CompoundDescriptionTypeEnum.InChI }
    });
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 1 
  wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe('xxx');
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File)).toBe(undefined);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 2
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolText });
  wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe('xxx');
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File)).toBe(undefined);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 3
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolFile });
  await wrapper.setProps({ compoundDescriptionFile: f1 });
  wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File).name).toBe('f1.mol');
  expect((data['sdfFile'] as File)).toBe(undefined);
  // reset
  data['inchi'] = undefined
  data['molText'] = undefined
  data['molFile'] = undefined
  data['sdfFile'] = undefined
  // call 3
  await wrapper.setProps({ 'compound-description-type': CompoundDescriptionTypeEnum.MolFile });
  await wrapper.setProps({ compoundDescriptionFile: f2 });
  wrapper.vm.updateBodyFormData(new FormDataMock());
  expect(data['inchi']).toBe(undefined);
  expect(data['molText']).toBe(undefined);
  expect((data['molFile'] as File)).toBe(undefined);
  expect((data['sdfFile'] as File).name).toBe('f2.sdf');
});
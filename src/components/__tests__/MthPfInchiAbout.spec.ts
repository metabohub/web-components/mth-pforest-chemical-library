// import for tests
import { mount } from '@vue/test-utils';
import { afterAll, expect, test, } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfInchiAbout from '@/components/MthPfInchiAbout.vue';
import { Status } from '../utils/webservicesUtils';

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const serverOK = setupServer(
  rest.get('/inchi/about', (req, res, ctx) => {
    return res(
      ctx.json({
        main: { name: "mock inchi api" },
        version: { version: "1.2.3", short_commit_sha1: '1472583' },
      }),
    )
  }),
);

const serverKO_empty_json = setupServer(
  rest.get('/inchi/about', (req, res, ctx) => {
    return res(
      ctx.json({}),
    )
  }),
);

const serverKO_thow_error = setupServer(
  rest.get('/inchi/about', (req, res, ctx) => {
    return res(ctx.status(500), ctx.json({}))
  }),
);

const serverKO_no_json = setupServer(
  rest.get('/inchi/about', (req, res, ctx) => {
    return res(ctx.status(200))
  }),
);

afterAll(() => {
  // Clean up after all tests are done, preventing this
  // interception layer from affecting irrelevant tests.
  serverOK.close();
  serverKO_empty_json.close();
  serverKO_thow_error.close();
  serverKO_no_json.close();
})

// avoid vuetify error
const global = {
  plugins: [vuetify]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

test('component', () => {
  // mount
  expect(MthPfInchiAbout).toBeTruthy()
});

test('mount component - mock OK', async () => {
  // mock
  serverOK.listen()
  // mount
  expect(MthPfInchiAbout).toBeTruthy()
  const wrapper = mount(MthPfInchiAbout, { global });
  // Check initial values
  expect(wrapper.vm.conf.basePath).toBe('https://metabocloud.mesocentre.uca.fr/inchi');
  expect(wrapper.vm.apiName).toBe('');
  expect(wrapper.vm.apiVersion).toBe('');
  expect(wrapper.vm.apiShortSha1).toBe('');
  expect(wrapper.vm.status).toBe(Status.Init);
  // Check if data loaded
  expect(wrapper.find("div").exists()).toBe(true);
  expect(wrapper.find("div").text()).toContain("loading...");
  // pres-rendering
  expect(wrapper.find("div.inchi-api-loading").exists()).toBe(true);
  expect(wrapper.find("div.inchi-api-success").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-error").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-unknown").exists()).toBe(false);
  // test set values
  wrapper.vm.status = Status.Success;
  wrapper.vm.apiName = "update 1";
  wrapper.vm.apiVersion = "update 2";
  wrapper.vm.apiShortSha1 = "update 3";
  // check set values
  expect(wrapper.vm.status).toBe(Status.Success);
  expect(wrapper.vm.apiName).toBe('update 1');
  expect(wrapper.vm.apiVersion).toBe('update 2');
  expect(wrapper.vm.apiShortSha1).toBe('update 3');
  // sleep
  await sleep(10);
  // test mock
  expect(wrapper.vm.apiName).toBe('mock inchi api');
  expect(wrapper.vm.apiVersion).toBe('1.2.3');
  expect(wrapper.vm.apiShortSha1).toBe('1472583');
  // check rendering
  expect(wrapper.find("div").text()).toContain("mock inchi api v1.2.3 (1472583)");
  expect(wrapper.find("div").text()).not.toContain("Error: could not get data from InChI microservice");
  expect(wrapper.find("div.inchi-api-loading").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-success").exists()).toBe(true);
  expect(wrapper.find("div.inchi-api-error").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-unknown").exists()).toBe(false);
  // unmock
  serverOK.close();
  // set manual error
  wrapper.vm.status = Status.Error;
  wrapper.vm.errorMessage = "--mock-error--";
  await sleep(1);
  expect(wrapper.find("div").text()).toContain("--mock-error--");
  // post error rendering
  expect(wrapper.find("div.inchi-api-loading").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-success").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-error").exists()).toBe(true);
  expect(wrapper.find("div.inchi-api-unknown").exists()).toBe(false);
  // test not supported status
  wrapper.vm.status = Status.Processing;
  await sleep(1);
  expect(wrapper.find("div.inchi-api-loading").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-success").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-error").exists()).toBe(false);
  expect(wrapper.find("div.inchi-api-unknown").exists()).toBe(true);
});

test('mount component - mock empty json', async () => {
  // mock
  serverKO_empty_json.listen()
  // mount
  const wrapper = mount(MthPfInchiAbout, { global });
  // sleep
  await sleep(10);
  // test mock
  expect(wrapper.vm.apiName).toBe('');
  expect(wrapper.vm.apiVersion).toBe('');
  expect(wrapper.vm.apiShortSha1).toBe('');
  // unmock
  serverKO_empty_json.close();
});

test('mount component - mock throw error', async () => {
  // mock
  serverKO_thow_error.listen()
  // mount
  const wrapper = mount(MthPfInchiAbout, { global });
  // sleep
  await sleep(10);
  // test mock
  expect(wrapper.vm.apiName).toBe('');
  expect(wrapper.vm.apiVersion).toBe('');
  expect(wrapper.vm.apiShortSha1).toBe('');
  // unmock
  serverKO_thow_error.close();
});

test('mount component - mock return no json', async () => {
  // mock
  serverKO_no_json.listen()
  // mount
  const wrapper = mount(MthPfInchiAbout, { global });
  // sleep
  await sleep(10);
  // test mock
  expect(wrapper.vm.apiName).toBe('');
  expect(wrapper.vm.apiVersion).toBe('');
  expect(wrapper.vm.apiShortSha1).toBe('');
  // check rendering
  expect(wrapper.find("div").text()).toContain("Error: could not get data from InChI microservice");
  // unmock
  serverKO_no_json.close();
});

import { mount } from '@vue/test-utils';
import { expect, test } from "vitest";

import HighchartsVue from 'highcharts-vue';

// core test
import MthPfHighchartsPeaklistsViewer, { HighlightType, Peaklist, PeakListType, YAxisType } from '../MthPfHighchartsPeaklistsViewer.vue';

const global = {
    plugins: [HighchartsVue]
}

// DATA FOR TESTS 
import { peaklist1, peaklist2, peaklist3, peaklist4 } from '../../assets/resources/peaklists';
// peakslists arrays
const peaklistsMass = [] as Peaklist[];
peaklistsMass.push(peaklist1);
peaklistsMass.push(peaklist2);
const peaklistsNmr = [] as Peaklist[];
peaklistsNmr.push(peaklist3);
peaklistsNmr.push(peaklist4);

// TESTS

test('mount component', async () => {
    expect(MthPfHighchartsPeaklistsViewer).toBeTruthy()
});

test('test attributes default values', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test setted
    expect(wrapper.vm.chartTitle).toBe('title');
    expect(wrapper.vm.chartPeaklists.length).toBe(2);
    // test not setted
    expect(wrapper.vm.chartSubTitle).toBe('Select area to zoom');
    expect(wrapper.vm.chartAxisMinY).toBe(null);
    expect(wrapper.vm.chartAxisMaxY).toBe(null);
    expect(wrapper.vm.chartAxisMinX).toBe(null);
    expect(wrapper.vm.chartAxisMaxX).toBe(null);
    expect(wrapper.vm.chartYAxisLabel).toBe(YAxisType.Relative);
    expect(wrapper.vm.chartDataLabelType).toBe(HighlightType.None);
    expect(wrapper.vm.chartDataLabelNb).toBe(0);
    expect(wrapper.vm.chartHeight).toBe('350px');
    expect(wrapper.vm.chartWidth).toBe('500px');
});

test('test attribute "viewer-title"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartTitle).toBe('title');
    expect(wrapper.vm.chartOptions.title.text).toBe('title');
    await wrapper.setProps({ viewerTitle: 'title changed!' });
    expect(wrapper.vm.chartTitle).toBe('title changed!');
    expect(wrapper.vm.chartOptions.title.text).toBe('title changed!');
});

test('test attribute "viewer-peaklists"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.viewerPeaklists[0].name).toBe('D-Glucose; LC-ESI-TOF; MS; POSITIVE; <em>PFs000XXX</em>');
    await wrapper.setProps({ viewerPeaklists: peaklistsNmr });
    expect(wrapper.vm.viewerPeaklists[0].name).toBe('D-Glucose; 7.0; NOESY-1D (noesygppr1d) - 500MHz; <em>PFs000XXX</em>');
});

test('test attribute "viewer-sub-title"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
            'viewer-sub-title': 'subtitle',
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartSubTitle).toBe('subtitle');
    expect(wrapper.vm.chartOptions.subtitle.text).toBe('subtitle');
    await wrapper.setProps({ viewerSubTitle: 'subtitle changed!' });
    expect(wrapper.vm.chartSubTitle).toBe('subtitle changed!');
    expect(wrapper.vm.chartOptions.subtitle.text).toBe('subtitle changed!');
});

test('test attribute "y-axis-min"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
            'y-axis-min': 42,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartAxisMinY).toBe(42);
    await wrapper.setProps({ yAxisMin: 666 });
    expect(wrapper.vm.chartAxisMinY).toBe(666);
});

test('test attribute "y-axis-max"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
            'y-axis-max': 42,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartAxisMaxY).toBe(42);
    await wrapper.setProps({ yAxisMax: 666 });
    expect(wrapper.vm.chartAxisMaxY).toBe(666);
});

test('test attribute "x-axis-min"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
            'x-axis-min': 42,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartAxisMinX).toBe(42);
    await wrapper.setProps({ xAxisMin: 666 });
    expect(wrapper.vm.chartAxisMinX).toBe(666);
});

test('test attribute "x-axis-max"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
            'x-axis-max': 42,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartAxisMaxX).toBe(42);
    await wrapper.setProps({ xAxisMax: 666 });
    expect(wrapper.vm.chartAxisMaxX).toBe(666);
});

test('test attribute "x-axis-max"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
            'y-axis-label': YAxisType.Relative,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartYAxisLabel).toBe(YAxisType.Relative);
    await wrapper.setProps({ yAxisLabel: YAxisType.Absolute });
    expect(wrapper.vm.chartYAxisLabel).toBe(YAxisType.Absolute);
});

test('test attribute "viewer-height"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsNmr,
            // to test
            'viewer-height': '42%',
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartHeight).toBe('42%');
    expect(wrapper.find("div.metabohub-peakforest-highcharts-peaklists-viewer").attributes('style')).toBe('height: 42%; width: 500px;');
    await wrapper.setProps({ viewerHeight: '666px' });
    expect(wrapper.vm.chartHeight).toBe('666px');
    expect(wrapper.find("div.metabohub-peakforest-highcharts-peaklists-viewer").attributes('style')).toBe('height: 666px; width: 500px;');
});

test('test attribute "viewer-width"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsNmr,
            // to test
            'viewer-width': '42%',
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartWidth).toBe('42%');
    expect(wrapper.find("div.metabohub-peakforest-highcharts-peaklists-viewer").attributes('style')).toBe('height: 350px; width: 42%;');
    await wrapper.setProps({ viewerWidth: '666px' });
    expect(wrapper.vm.chartWidth).toBe('666px');
    expect(wrapper.find("div.metabohub-peakforest-highcharts-peaklists-viewer").attributes('style')).toBe('height: 350px; width: 666px;');
});

test('test attribute "highlight-type"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
            'highlight-type': HighlightType.Top,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartDataLabelType).toBe(HighlightType.Top);
    await wrapper.setProps({ highlightType: HighlightType.GreaterThan });
    expect(wrapper.vm.chartDataLabelType).toBe(HighlightType.GreaterThan);
});

test('test attribute "highlight-number"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
            // to test
            'highlight-number': 42,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartDataLabelNb).toBe(42);
    await wrapper.setProps({ highlightNumber: 666 });
    expect(wrapper.vm.chartDataLabelNb).toBe(666);
});

// test methods

test('test method "chartOptions.xAxis.labels.formatter"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test
    expect(wrapper.vm.chartOptions.xAxis.labels.formatter({ value: 42 })).toBe('42');
    expect(wrapper.vm.chartOptions.xAxis.labels.formatter({ value: -666 })).toBe('666');
});

test('test method "checkPeaklistsType"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // init
    expect(wrapper.vm.chartErrorMessage).toBe('');
    // Case 'no spectra'
    wrapper.vm.chartPeaklists = [];
    await wrapper.vm.checkPeaklistsType();
    expect(wrapper.vm.chartErrorMessage).toBe('no peaklist to display (either MS and NMR).');
    // Case 'mix MS and NMR'
    wrapper.vm.chartPeaklists = [peaklist1, peaklist3];
    await wrapper.vm.checkPeaklistsType();
    expect(wrapper.vm.chartErrorMessage).toBe('mixed peaklist type (mix of MS and NMR peaklists).');
});

test('test method "updateSeries"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // init
    expect(wrapper.vm.chartPeaklists.length).toBe(2);
    expect(wrapper.vm.chartOptions.series.length).toBe(4);
    // Case 'too many spectra'
    wrapper.vm.chartPeaklists = [peaklist1, peaklist1, peaklist1, peaklist1, peaklist1, peaklist1, peaklist1, peaklist1, peaklist1, peaklist1, peaklist1,];
    await wrapper.vm.updateSeries();
    expect(wrapper.vm.chartPeaklists.length).toBe(11);
    expect(wrapper.vm.chartOptions.series.length).toBe(20);
});

test('test method "updateAxisLabel"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // test 1
    wrapper.vm.chartPeaklistsType = PeakListType.Mass;
    wrapper.vm.chartYAxisLabel = YAxisType.Absolute;
    await wrapper.vm.updateAxisLabel();
    expect(wrapper.vm.chartTooltipXLabel).toBe('m/z');
    expect(wrapper.vm.chartTooltipXUnit).toBe('');
    expect(wrapper.vm.chartOptions.xAxis.title.text).toBe('m/z');
    expect(wrapper.vm.chartTooltipYLabel).toBe('Abs. Intensity');
    expect(wrapper.vm.chartTooltipYUnit).toBe('');
    expect(wrapper.vm.chartOptions.yAxis.title.text).toBe('Absolute Intensity');
    // test 2 
    wrapper.vm.chartPeaklistsType = PeakListType.Nmr1d;
    wrapper.vm.chartYAxisLabel = YAxisType.Relative;
    await wrapper.vm.updateAxisLabel();
    expect(wrapper.vm.chartTooltipXLabel).toBe('chemical shift');
    expect(wrapper.vm.chartTooltipXUnit).toBe(' (ppm)');
    expect(wrapper.vm.chartOptions.xAxis.title.text).toBe('chemical shift (ppm)');
    expect(wrapper.vm.chartTooltipYLabel).toBe('Rel. Intensity');
    expect(wrapper.vm.chartTooltipYUnit).toBe('(%)');
    expect(wrapper.vm.chartOptions.yAxis.title.text).toBe('Relative Intensity (%)');
    // // test 3 
    // wrapper.vm.chartPeaklistsType = null as PeakListType ;
    // wrapper.vm.chartYAxisLabel = null as YAxisType;
    // await wrapper.vm.updateAxisLabel();
    // expect(wrapper.vm.chartTooltipXLabel).toBe('chemical shift');
    // expect(wrapper.vm.chartTooltipXUnit).toBe(' (ppm)');
    // expect(wrapper.vm.chartOptions.xAxis.title.text).toBe('chemical shift (ppm)');
    // expect(wrapper.vm.chartTooltipYLabel).toBe('Rel. Intensity');
    // expect(wrapper.vm.chartTooltipYUnit).toBe('(%)');
    // expect(wrapper.vm.chartOptions.yAxis.title.text).toBe('Relative Intensity (%)');
});

test('test method "chartOptions.xAxis.labels.formatter"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // fake test
    await wrapper.vm.updateTooltip();
    // real update
    wrapper.vm.chartOptions.tooltip['color'] = '#7cb5ec';
    wrapper.vm.chartOptions.tooltip['series'] = { name: 'xxx', color: '#7cb5ec' };
    // case mass:     // { mz: 204.05606, i: 1.21, c: "C5 13C H12 Na O6", a: "[(M+Na)]+ (13C)", },
    wrapper.vm.chartOptions.tooltip['x'] = 204.05606;
    wrapper.vm.chartOptions.tooltip['y'] = 1.21;
    // mock4test
    await wrapper.vm.updateAxisRanges();
    expect(wrapper.vm.chartOptions.tooltip.formatter()).toBe('<b>xxx</b><br/>m/z: 204.05606;<br/>Rel. Intensity: 1.21(%);<br />Composition: C5 13C H12 Na O6<br />Attribution: [(M+Na)]+ (13C)');
    // case no attr / compo
    wrapper.vm.chartOptions.tooltip['color'] = "#434348";
    wrapper.vm.chartOptions.tooltip['x'] = 220.09;
    expect(wrapper.vm.chartOptions.tooltip.formatter()).toBe('<b>xxx</b><br/>m/z: 220.09;<br/>Rel. Intensity: 1.21(%);');
    // case NMR
    await wrapper.setProps({ viewerPeaklists: peaklistsNmr });
    await wrapper.vm.updateTooltip();
    // real update
    wrapper.vm.chartOptions.tooltip['color'] = '#7cb5ec';
    wrapper.vm.chartOptions.tooltip['series'] = { name: 'xxx', color: '#7cb5ec' };
    // case nmr: { ppm: 5.24, i: 1.57, a: 'H1a' },
    wrapper.vm.chartOptions.tooltip['x'] = -5.24;
    wrapper.vm.chartOptions.tooltip['y'] = 1.23;
    expect(wrapper.vm.chartOptions.tooltip.formatter()).toBe('<b>xxx</b><br/>chemical shift: 5.24 (ppm);<br/>Rel. Intensity: 1.23(%);<br />Annotation: H1a');
    // case no annot.
    wrapper.vm.chartOptions.tooltip['color'] = '#434348';
    wrapper.vm.chartOptions.tooltip['series'] = { name: 'xxx', color: '#434348' };
    wrapper.vm.chartOptions.tooltip['x'] = -3.5;
    expect(wrapper.vm.chartOptions.tooltip.formatter()).toBe('<b>xxx</b><br/>chemical shift: 3.5 (ppm);<br/>Rel. Intensity: 1.23(%);');
    // case hide
    wrapper.vm.chartOptions.tooltip['series'] = { name: 'xxx - HIDE', color: '#7cb5ec' };
    expect(wrapper.vm.chartOptions.tooltip.formatter()).toBe(false);
});

test('test method " chartOptions.plotOptions.series.dataLabels "', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // // test chartDataType undefinied
    // wrapper.vm.chartDataLabelType = undefined;
    // await wrapper.vm.updateDataLabel();
    // await wrapper.vm.updateAxisRanges();
    // expect(wrapper.vm.chartOptions.plotOptions.series.dataLabels.formatter()).toBe(undefined);
    // test chartDataType None
    wrapper.vm.chartDataLabelType = HighlightType.None;
    await wrapper.vm.updateDataLabel();
    await wrapper.vm.updateAxisRanges();
    expect(wrapper.vm.chartOptions.plotOptions.series.dataLabels.formatter()).toBe(undefined);
    // test Top 3 - not all - ok
    wrapper.vm.chartDataLabelType = HighlightType.Top;
    wrapper.vm.chartDataLabelNb = 3;
    await wrapper.vm.updateDataLabel();
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['color'] = '#7cb5ec';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['series'] = {};
    wrapper.vm.chartOptions.plotOptions.series.dataLabels.series['color'] = '#7cb5ec';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels.series['name'] = 'xxx';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['x'] = 42.0;
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['y'] = 999;// OK
    await wrapper.vm.updateAxisRanges(); // fake waiter
    expect(wrapper.vm.chartOptions.plotOptions.series.dataLabels.formatter()).toBe('<span style="color: #7cb5ec">42 </span>');
    // test Top 3 - not all - ko
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['y'] = 0.1;// KO
    expect(wrapper.vm.chartOptions.plotOptions.series.dataLabels.formatter()).toBe(null);
    // test Top 200 - all (ok)
    wrapper.vm.chartDataLabelNb = 200;
    await wrapper.vm.updateDataLabel();
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['color'] = '#7cb5ec';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['series'] = {};
    wrapper.vm.chartOptions.plotOptions.series.dataLabels.series['color'] = '#7cb5ec';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels.series['name'] = 'xxx';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['x'] = 42.0;
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['y'] = 0.1;// KO for Top 3, OK for Top 200
    expect(wrapper.vm.chartOptions.plotOptions.series.dataLabels.formatter()).toBe('<span style="color: #7cb5ec">42 </span>');
    // test GT 3 - ok
    wrapper.vm.chartDataLabelType = HighlightType.GreaterThan;
    wrapper.vm.chartDataLabelNb = 3;
    await wrapper.vm.updateDataLabel();
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['color'] = '#7cb5ec';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['series'] = {};
    wrapper.vm.chartOptions.plotOptions.series.dataLabels.series['color'] = '#7cb5ec';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels.series['name'] = 'xxx';
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['x'] = 42.0;
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['y'] = 4;// OK for GT 3
    expect(wrapper.vm.chartOptions.plotOptions.series.dataLabels.formatter()).toBe('<span style="color: #7cb5ec">42 </span>');
    // test GT 3 - ko
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['y'] = 2;// KO for GT 3
    expect(wrapper.vm.chartOptions.plotOptions.series.dataLabels.formatter()).toBe(null);
    // test HIDDEN
    wrapper.vm.chartOptions.plotOptions.series.dataLabels['y'] = 4;// OK for GT 3
    wrapper.vm.chartOptions.plotOptions.series.dataLabels.series['name'] = 'xxx - HIDE';// KO for hidden
    expect(wrapper.vm.chartOptions.plotOptions.series.dataLabels.formatter()).toBe(null);
});

test('test method " this.chartOptions.plotOptions.series.events "', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            // mandatory
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // mock
    const e = {
        'target': {
            'color': '#7cb5ec',
        }
    };
    // run test
    await wrapper.vm.updateEvents(); // call
    await wrapper.vm.updateAxisRanges(); // fake waiter
    // test before
    expect(wrapper.vm.chartOptions.series[0].hidden).toBe(false);
    // action hide + test
    await wrapper.vm.chartOptions.plotOptions.series.events.legendItemClick(e); // real test
    expect(wrapper.vm.chartOptions.series[0].hidden).toBe(true);
    // action unhide + test
    await wrapper.vm.chartOptions.plotOptions.series.events.legendItemClick(e); // real test
    expect(wrapper.vm.chartOptions.series[0].hidden).toBe(false);
});

test('test method "loadHighcharts"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        setup() {
            return {
                chartHeight: '',
                chartWidth: '',
                chartErrorMessage: '',
                chartPeaklists: [],
            }
        },
        created() { },
        // computed: { loadHighcharts() { return false; } }
    } as any);
    // test mock
    expect(wrapper.vm.loadHighcharts).toBe(false);
});

test('test method "getSize"', async () => {
    const wrapper = mount(MthPfHighchartsPeaklistsViewer, {
        global,
        props: {
            'viewer-title': 'title',
            'viewer-peaklists': peaklistsMass,
        },
        computed: { loadHighcharts() { return false; } }
    } as any);
    // tests ok
    expect(wrapper.vm.getSize('567px')).toBe('567px');
    expect(wrapper.vm.getSize('66%')).toBe('66%');
    // tests ko
    expect(wrapper.vm.getSize(undefined)).toBe('');
    expect(wrapper.vm.getSize('toto')).toBe('');
});
// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCdkProperties from '@/components/MthPfCdkProperties.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
    plugins: [vuetify]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}


test('mount component - mock return no json', async () => {
    // mock
    vi.mock('@/openapi-ts/cdk/api/cdk-api.ts', () => {
        const response = {};
        const actual = vi.importActual("@/openapi-ts/cdk")
        const Configuration = vi.fn()
        const CdkApi = vi.fn()
        CdkApi.prototype.computeProperties = vi.fn()// 
            .mockResolvedValue(response)
        return {
            ...(actual as object),//
            Configuration, //
            CdkApi,//
        }
    });
    // mount
    expect(MthPfCdkProperties).toBeTruthy()
    const wrapper = mount(MthPfCdkProperties, { global });
    // fake init
    wrapper.vm.localCompoundDescription = '~~caffeine~~';
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(10);
    // test new values
    expect(wrapper.vm.isLocked).toBe(true);
    expect(wrapper.vm.localCompoundDescription).toBe('~~caffeine~~');
    expect(wrapper.vm.status).toBe(Status.Error);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-error');
    // unmock
    vi.unmock('@/openapi-ts/cdk/api/cdk-api.ts');
});

// test('mount component - mock throw error', async () => {
//     // mock
//     vi.mock('@/openapi-ts/cdk', async () => {
//         const actual = await vi.importActual("@/openapi-ts/cdk")
//         const successResponse = {
//             status: 404
//         };
//         const Configuration = vi.fn()
//         const CdkApi = vi.fn()
//         CdkApi.prototype.computeProperties = vi.fn()// 
//             .mockResolvedValue(successResponse)
//         return {
//             ...(actual as object),//
//             Configuration, //
//             CdkApi,//
//         }
//     });
//     // mount
//     expect(MthPfCdkProperties).toBeTruthy()
//     const wrapper = mount(MthPfCdkProperties, { global });
//     // fake init
//     wrapper.vm.localCompoundDescription = '~~caffeine~~';
//     // test 'submit' click!
//     await wrapper.vm.submitToCdkMicroservice()
//     await sleep(10);
//     // test new values
//     expect(wrapper.vm.isLocked).toBe(true);
//     expect(wrapper.vm.localCompoundDescription).toBe('~~caffeine~~');
//     expect(wrapper.vm.status).toBe(Status.Error);
//     expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
//     expect(wrapper.vm.currentHintClasses).toBe('hint-error');
//     // unmock
//     vi.unmock('@/openapi-ts/cdk');
// });

// test('mount component - mock return no json', async () => {
//     // mock
//     vi.mock('@/openapi-ts/cdk', async () => {
//         const actual = await vi.importActual("@/openapi-ts/cdk")
//         const successResponse = {};
//         const Configuration = vi.fn()
//         const CdkApi = vi.fn()
//         CdkApi.prototype.computeProperties = vi.fn()// 
//             .mockResolvedValue(successResponse)
//         return {
//             ...(actual as object),//
//             Configuration, //
//             CdkApi,//
//         }
//     });
//     // mount
//     expect(MthPfCdkProperties).toBeTruthy()
//     const wrapper = mount(MthPfCdkProperties, { global });
//     // fake init
//     wrapper.vm.localCompoundDescription = '~~caffeine~~';
//     // test 'submit' click!
//     await wrapper.vm.submitToCdkMicroservice();
//     await sleep(10);
//     // test new values
//     expect(wrapper.vm.isLocked).toBe(true);
//     expect(wrapper.vm.localCompoundDescription).toBe('~~caffeine~~');
//     expect(wrapper.vm.status).toBe(Status.Error);
//     expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
//     expect(wrapper.vm.currentHintClasses).toBe('hint-error');
//     // unmock
//     vi.unmock('@/openapi-ts/cdk');
// });

// test('mount component - send query "empty"', async () => {
//     // mock
//     vi.mock('@/openapi-ts/cdk', async () => {
//         const actual = await vi.importActual("@/openapi-ts/cdk")
//         const successResponse = {
//             status: 200,
//             data: {}
//         };
//         const Configuration = vi.fn()
//         const CdkApi = vi.fn()
//         CdkApi.prototype.computeProperties = vi.fn()// 
//             .mockResolvedValue(successResponse)
//         return {
//             ...(actual as object),//
//             Configuration, //
//             CdkApi,//
//         }
//     });
//     // mount
//     const wrapper = mount(MthPfCdkProperties, { global });
//     // set
//     wrapper.vm.localCompoundDescription = undefined;
//     await sleep(10);
//     expect(wrapper.vm.localCompoundDescription).toBe(undefined);
//     // test 'submit' click!
//     await wrapper.vm.submitToCdkMicroservice();
//     await sleep(10);
//     // test new values
//     expect(wrapper.vm.isLocked).toBe(false);
//     expect(wrapper.vm.localCompoundDescription).toBe(undefined);
//     expect(wrapper.vm.status).toBe(Status.Init);
//     expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
//     expect(wrapper.vm.currentHintClasses).toBe('hint-info');
//     // test rendering
//     expect(wrapper.find(".hint-info").exists()).toBe(true);
//     expect(wrapper.find(".hint-success").exists()).toBe(false);
//     expect(wrapper.find(".hint-warning").exists()).toBe(false);
//     expect(wrapper.find(".hint-error").exists()).toBe(false);
//     expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
//     // unmock
//     vi.unmock('@/openapi-ts/cdk');
// });

// test('mount component - Reject ', async () => {
//     // mock
//     vi.mock('@/openapi-ts/cdk', async () => {
//         const actual = await vi.importActual("@/openapi-ts/cdk")
//         const errorResponse = {
//             status: 200,
//             data: {}
//         };
//         const Configuration = vi.fn()
//         const CdkApi = vi.fn()
//         CdkApi.prototype.computeProperties = vi.fn()// 
//             .mockRejectedValue(errorResponse)
//         return {
//             ...(actual as object),//
//             Configuration, //
//             CdkApi,//
//         }
//     });
//     // mount
//     const wrapper = mount(MthPfCdkProperties, { global });
//     // set
//     wrapper.vm.localCompoundDescription = undefined;
//     await sleep(10);
//     expect(wrapper.vm.localCompoundDescription).toBe(undefined);
//     // test 'submit' click!
//     await wrapper.vm.submitToCdkMicroservice();
//     await sleep(10);
//     // test new values
//     expect(wrapper.vm.isLocked).toBe(false);
//     expect(wrapper.vm.localCompoundDescription).toBe(undefined);
//     expect(wrapper.vm.status).toBe(Status.Init);
//     expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
//     expect(wrapper.vm.currentHintClasses).toBe('hint-info');
//     // test rendering
//     expect(wrapper.find(".hint-info").exists()).toBe(true);
//     expect(wrapper.find(".hint-success").exists()).toBe(false);
//     expect(wrapper.find(".hint-warning").exists()).toBe(false);
//     expect(wrapper.find(".hint-error").exists()).toBe(false);
//     expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
// });


// test('mount component - service light error', async () => {
//     // mock
//     vi.mock('@/openapi-ts/cdk', async () => {
//       const actual = await vi.importActual("@/openapi-ts/cdk")
//       const successResponse = {
//         status: 200,
//         data: {
//           "state": "Failure", 
//         }
//       };
//       const Configuration = vi.fn()
//       const CdkApi = vi.fn()
//       CdkApi.prototype.computeProperties = vi.fn()// 
//         .mockResolvedValue(successResponse)
//       return {
//         ...(actual as object),//
//         Configuration, //
//         CdkApi,//
//       }
//     });
//     // mount 
//     const wrapper = mount(MthPfCdkProperties, { global });
//     // set
//     wrapper.vm.localCompoundDescription = undefined;
//     await sleep(10);
//     expect(wrapper.vm.localCompoundDescription).toBe(undefined);
//     // test 'submit' click!
//     await wrapper.vm.submitToCdkMicroservice();
//     await sleep(10);
//     // test new values
//     expect(wrapper.vm.isLocked).toBe(false);
//     expect(wrapper.vm.localCompoundDescription).toBe(undefined);
//     expect(wrapper.vm.status).toBe(Status.Init);
//     expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
//     expect(wrapper.vm.currentHintClasses).toBe('hint-info');
//     // test rendering
//     expect(wrapper.find(".hint-info").exists()).toBe(true);
//     expect(wrapper.find(".hint-success").exists()).toBe(false);
//     expect(wrapper.find(".hint-warning").exists()).toBe(false);
//     expect(wrapper.find(".hint-error").exists()).toBe(false);
//     expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
//         // unmock
//         vi.unmock('@/openapi-ts/cdk');
//   });
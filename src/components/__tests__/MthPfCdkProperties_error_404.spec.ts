// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCdkProperties from '@/components/MthPfCdkProperties.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
    plugins: [vuetify]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

test('mount component - 404', async () => {
    // mock
    vi.mock('@/openapi-ts/cdk', async () => {
        const actual = await vi.importActual("@/openapi-ts/cdk")
        const response = {
            status: 404,
            data: {
                "state": "Success",
                "formula": "C8H10N4O2",
                "monoisotopic_mass": 194.08037556,
                "average_mass": 194.19091739584468,
                "canonical_smiles": "O=C1C2=C(N=CN2C)N(C(=O)N1C)C",
                "stereo_smiles": "CN1C=NC2=C1C(N(C)C(N2C)=O)=O",
                "inchi": "InChI=1S/C8H10N4O2/c1-10-4-9-6-5(10)7(13)12(3)8(14)11(6)2/h4H,1-3H3",
                "inchikey": "RYYVLZVUVIJVGH-UHFFFAOYSA-N",
                "logP": -0.10858213188126853,
                "CDK_version": "2.9"
            },
        };
        const Configuration = vi.fn()
        const CdkApi = vi.fn()
        CdkApi.prototype.computeProperties = vi.fn()// 
            .mockResolvedValue(response)
        return {
            ...(actual as object),//
            Configuration, //
            CdkApi,//
        }
    });
    // mount
    expect(MthPfCdkProperties).toBeTruthy()
    const wrapper = mount(MthPfCdkProperties, { global, props: { compoundDescription: "~~caffeine~~" } });
    // test 'submit' click!
    await sleep(100);
    await wrapper.vm.submitToMicroservice()
    await sleep(10);
    // test new values 
    expect(wrapper.vm.status).toBe(Status.Error);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-error');
});

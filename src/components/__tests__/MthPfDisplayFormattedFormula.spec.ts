import { mount } from '@vue/test-utils';
import { expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';
import { MetabohubPeakForestBasalLibrary } from '@metabohub/peakforest-basal-library';
import { formatStringAsFormula } from '@/components/utils'

// core test
import MthPfDisplayFormattedFormula from '@/components/MthPfDisplayFormattedFormula.vue';

const global = {
    plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

test('mount component', async () => {
    expect(MthPfDisplayFormattedFormula).toBeTruthy()
    const wrapper = mount(MthPfDisplayFormattedFormula, { global });
    // Check initial data
    expect(wrapper.vm.value).toBe('');
});

test('mount component - option - formulaValue', async () => {
    // mount
    const wrapper = mount(MthPfDisplayFormattedFormula, { global, props: { formulaValue: "set by parent" } });
    // Check initial data
    expect(wrapper.vm.value).toBe('set by parent');
    // set props
    await wrapper.setProps({ formulaValue: 'changed!' });
    expect(wrapper.vm.value).toBe('changed!');
});

test('mount component - initial value', () => {
    // mount
    const wrapper = mount(MthPfDisplayFormattedFormula, { global });
    // Check initial data
    expect(wrapper.vm.value).toBe('');
});

test('computed - transform', () => {
    // mount
    const wrapper = mount(MthPfDisplayFormattedFormula, { global });
    // Set value and check transformed value
    wrapper.vm.value = 'test';
    expect(wrapper.vm.transform).toBe('test');
    // Check formula transformation
    wrapper.vm.value = 'CH4';
    expect(wrapper.vm.transform).toBe('CH<sub>4</sub>');
    // test NULL value
    wrapper.vm.value = null;
    expect(wrapper.vm.transform).toBe('');
});

test('test core method = formatStringAsFormula', () => {
    expect(formatStringAsFormula("")).toBe('');
    expect(formatStringAsFormula("CH4")).toBe('CH<sub>4</sub>');
});
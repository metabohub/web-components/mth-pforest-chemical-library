// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCtsConvert, { CompoundIdentifierType } from '@/components/MthPfCtsConvert.vue';
import { Status } from '../utils/webservicesUtils';
import { MetabohubPeakForestBasalLibrary } from '@metabohub/peakforest-basal-library';

// avoid vuetify error
const global = {
    plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

test('mount component - send query "void"', async () => {
    // mock
    vi.mock('@/openapi-ts/cts', () => {
        const actual = vi.importActual("@/openapi-ts/cdk")
        const successResponse = {
            status: 200,
            data: "" 
        };
        const Configuration = vi.fn()
        const CtsSearchApi = vi.fn()
        CtsSearchApi.prototype.convert = vi.fn()// 
          .mockResolvedValue(successResponse)
        return {
          ...(actual as object),//
          Configuration, //
          CtsSearchApi,//
        }
    });
    // mount 
    const wrapper = mount(MthPfCtsConvert, {
        global, props: {
          compoundDescription: "RYYVLZVUVIJVGH-UHFFFAOYSA-N", 
          compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
          compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
        }
      });
    // set
    wrapper.vm.localCompoundDescription = "RYYVLZVUVIJVGH-UHFFFAOYSA-N";   
    await sleep(10);
    expect(wrapper.vm.localCompoundDescription).toBe("RYYVLZVUVIJVGH-UHFFFAOYSA-N");
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(10);
    // test new values
    expect(wrapper.vm.isLocked).toBe(true);
    expect(wrapper.vm.localCompoundDescription).toBe("RYYVLZVUVIJVGH-UHFFFAOYSA-N");
    expect(wrapper.vm.status).toBe(Status.Error);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-error');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(false);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(true);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-rotate-right");
});
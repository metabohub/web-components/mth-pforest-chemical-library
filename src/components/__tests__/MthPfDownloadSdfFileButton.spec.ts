import { mount } from '@vue/test-utils';
import { expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfDownloadSdfFileButton from '../MthPfDownloadSdfFileButton.vue';

const global = {
    plugins: [vuetify]
}

test('mount component', async () => {
    expect(MthPfDownloadSdfFileButton).toBeTruthy()
});

test('test button', () => {
    const wrapper = mount(MthPfDownloadSdfFileButton, { global });
    // Test if the button text is rendered correctly
    const button = wrapper.find('.btn');
    expect(button.exists()).toBe(true);
    expect(button.text()).toBe('Save as SDF file');
});

test('test button - overwrite slot', () => {
    const wrapper = mount(MthPfDownloadSdfFileButton, {
        global, slots: {
            default: "Test Button"
        }
    });
    // Test if the button text is rendered correctly
    const button = wrapper.find('.btn');
    expect(button.exists()).toBe(true);
    expect(button.text()).toBe('Test Button');
});

let file_content = "";
let file_url = "";
Object.defineProperty(window.URL, 'createObjectURL', {
    value: (data) => { file_content = data }
});
Object.defineProperty(window.URL, 'revokeObjectURL', {
    value: (url) => { file_url = url }
});

test('test click on the button download file', async () => {
    // init
    expect(file_content).toBe('');
    expect(file_url).toBe('');
    // mount
    const wrapper = mount(MthPfDownloadSdfFileButton, //
        {
            global,
            props: {
                molFile: "MOL",
                extraData: [
                    { key: "K1", value: "V1", },
                    { key: "K2", value: ["V2a", "V2b"], }
                ]
            }
        });
    // Test if the button text is rendered correctly
    const button = wrapper.find('.btn');
    await button.trigger('click');
    // check
    const blob = new Blob([`MOL\n\n> <K1>\nV1\n\n> <K2>\nV2a; \nV2b; \n\n$$$$\n`], { type: 'text/plain' });
    expect(file_content).toStrictEqual(blob);
    expect(file_url).toBe(undefined);
});

test('mount component - option - mol-file', async () => {
    // mount 
    const wrapper = mount(MthPfDownloadSdfFileButton, { global, props: { "mol-file": "set by parent" } });
    // Check initial data
    expect(wrapper.vm.molData).toBe('set by parent');
    // set props
    await wrapper.setProps({ 'molFile': 'changed!' });
    expect(wrapper.vm.molData).toBe('changed!');
});

test('mount component - option - extra-data', async () => {
    // mount 
    const wrapper = mount(MthPfDownloadSdfFileButton, { global, props: { "extra-data": [{ _key: "key set by parent", _value: "value set by parent" }] } });
    // Check initial data
    expect(wrapper.vm.sdfData).toStrictEqual([{ _key: "key set by parent", _value: "value set by parent" }]);
    // set props
    await wrapper.setProps({ 'extraData': [{ key: "key changed by parent", value: "value changed by parent" }] });
    expect(wrapper.vm.sdfData).toStrictEqual([{ key: "key changed by parent", value: "value changed by parent" }]);
});

test('mount component - option - file-name', async () => {
    // mount 
    const wrapper = mount(MthPfDownloadSdfFileButton, { global, props: { "file-name": "set by parent" } });
    // Check initial data
    expect(wrapper.vm.name).toBe('set by parent');
    // set props
    await wrapper.setProps({ 'fileName': 'changed!' });
    expect(wrapper.vm.name).toBe('changed!');
});
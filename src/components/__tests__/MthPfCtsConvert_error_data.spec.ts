// import for tests
import { mount } from '@vue/test-utils';
import { expect, test,   } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCtsConvert, { CompoundIdentifierType } from '@/components/MthPfCtsConvert.vue';
import { Status } from '../utils/webservicesUtils';
import { MetabohubPeakForestBasalLibrary } from '@metabohub/peakforest-basal-library';

// avoid vuetify error
const global = {
    plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';
const server = setupServer(
  rest.get('*/convert/*', (req, res, ctx) => {
    return res(
        ctx.status(500)
    )
  }),
);

test('mount component - send query "undef"', async () => {
    // mock
    server.listen();
    // mount 
    const wrapper = mount(MthPfCtsConvert, {
        global, props: {
            compoundDescription: "xxx",
            compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
            compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
        }
    });
    // set
    wrapper.vm.localCompoundDescription = "xxx";
    await sleep(10);
    expect(wrapper.vm.localCompoundDescription).toBe("xxx");
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(10);
    // test new values
    expect(wrapper.vm.isLocked).toBe(true);
    expect(wrapper.vm.localCompoundDescription).toBe("xxx");
    expect(wrapper.vm.status).toBe(Status.Error);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-error');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(false);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(true);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-rotate-right");
    // 
    server.close();
});
// import for tests
import { mount } from '@vue/test-utils';
import { describe, expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfInchiGeneration, { CompoundDescriptionTypeEnum } from '@/components/MthPfInchiGeneration.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}


describe('ok', () => {

  test('mount component - mock OK', async () => {
    // mock 
    vi.mock('@/openapi-ts/inchi', async () => {
      const actual = await vi.importActual("@/openapi-ts/inchi")
      const successResponse = {
        status: 200,
        data: {
          "inchi": "InChI=1S/C5H9NO2/c7-5(8)4-2-1-3-6-4/h4,6H,1-3H2,(H,7,8)/t4-/m0/s1",
          "inchiKey": "ONIBWKKTOPOVIA-BYPYZUCNSA-N",
          "softwareVersion": "1.06"
        }
      };
      const Configuration = vi.fn()
      const InChIApi = vi.fn()
      InChIApi.prototype.generateInChIs = vi.fn()// 
        .mockResolvedValue(successResponse)
      return {
        ...(actual as object),//
        Configuration, //
        InChIApi: InChIApi,//
      } 
    });
    // mount
    expect(MthPfInchiGeneration).toBeTruthy()
    const wrapper = mount(MthPfInchiGeneration, { global, compoundDescription: "~~caffeine~~", compoundDescriptionType: CompoundDescriptionTypeEnum.MolText, });
    // Check initial values
    expect(wrapper.vm.conf.basePath).toBe('https://metabocloud.mesocentre.uca.fr/inchi');
    expect(wrapper.vm.isLocked).toBe(false);
    expect(wrapper.vm.localCompoundDescription).toBe(undefined);
    expect(wrapper.vm.status).toBe(Status.Init);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong> click on the.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-info');
    // test rendering
    expect(wrapper.find(".metabohub-peakforest-inchi-generation").exists()).toBe(true);  
    expect(wrapper.vm.showInput).toBe(true);
    expect(wrapper.vm.isTextField).toBe(true);
    expect(wrapper.find("div").text()).toContain("Info click on the"); 
    expect(wrapper.find("div").text()).toContain("Compound's description (MOL / SDF)");
    expect(wrapper.find(".hint-info").exists()).toBe(true);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(false);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
    // test set values
    wrapper.vm.isLocked = false;
    wrapper.vm.localCompoundDescription = "~~caffeine~~";
    await sleep(50);
    expect(wrapper.vm.localCompoundDescription).toBe('~~caffeine~~');
    expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.MolText);
    // test 'submit' click!
    await wrapper.vm.submitToMicroservice();
    await sleep(100);
    // test new values
    expect(wrapper.vm.isLocked).toBe(true);
    expect(wrapper.vm.localCompoundDescription).toBe('~~caffeine~~');
    expect(wrapper.vm.localCompoundDescriptionType).toBe(CompoundDescriptionTypeEnum.MolText);
    expect(wrapper.vm.status).toBe(Status.Success);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-success');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(false);
    expect(wrapper.find(".hint-success").exists()).toBe(true);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(false);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-rotate-right");
    // test 'reset' click
    await wrapper.vm.resetCall();
    await sleep(10);
    // test new values
    expect(wrapper.vm.isLocked).toBe(false);
    expect(wrapper.vm.localCompoundDescription).toBe(undefined);
    expect(wrapper.vm.status).toBe(Status.Init);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-info');
    // test rendering
    expect(wrapper.find(".hint-info").exists()).toBe(true);
    expect(wrapper.find(".hint-success").exists()).toBe(false);
    expect(wrapper.find(".hint-warning").exists()).toBe(false);
    expect(wrapper.find(".hint-error").exists()).toBe(false);
    expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
    // TEST MOL TEXT
    wrapper.vm.localCompoundDescription = "~~caffeine~~";
    wrapper.vm.localCompoundDescriptionType = CompoundDescriptionTypeEnum.MolText;
    await wrapper.vm.submitToMicroservice();
    await sleep(100);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    await wrapper.vm.resetCall();
    await sleep(10);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
    // TEST MOL FILE
    const f1 = new File([`toto1`], "f1.mol", { type: "text/plain", });
    wrapper.vm.localCompoundDescription = undefined;
    await wrapper.setProps({ compoundDescriptionFile: f1 });
    await wrapper.setProps({ compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile });
    await sleep(10);
    // manual debug
    wrapper.vm.localCompoundDescriptionFiles = [f1];
    await wrapper.vm.fileChanged();
    await wrapper.vm.submitToMicroservice();
    await sleep(100);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    await wrapper.vm.resetCall();
    await sleep(10);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
    // TEST SDF FILE
    const f2 = new File([`toto1`], "f1.sdf", { type: "text/plain", });
    wrapper.vm.localCompoundDescription = undefined;
    await wrapper.setProps({ compoundDescriptionFile: f2 });
    await wrapper.setProps({ compoundDescriptionType: CompoundDescriptionTypeEnum.MolFile });
    await wrapper.vm.fileChanged();
    await wrapper.vm.submitToMicroservice();
    await sleep(100);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Success!<\/strong>.*/);
    await wrapper.vm.resetCall();
    await sleep(10);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);

  });
})


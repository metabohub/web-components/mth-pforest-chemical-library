// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, } from "vitest";
import vuetify from '@/plugins/vuetify';
import { MetabohubPeakForestBasalLibrary, } from '@metabohub/peakforest-basal-library';
// core test
import MthPfCtsConvert, { CompoundIdentifierType, } from '@/components/MthPfCtsConvert.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';
const server = setupServer(
  rest.get('*/convert/*', (req, res, ctx) => {
    return res(
      // ctx.status(200)
      ctx.json([{
        fromIdentifier: "InChIKey",
        results: [],
        searchTerm: "SNICXCGAKADSCV-JTQLQIEISA-N",
        toIdentifier: "Chemical Name"
      }])
    )
  }),
);
/////////////////////////////////////////////////
// ERRORS

test('mount component - service light error | no data sent', async () => {
  // mock
  server.listen();
  // mount 
  const wrapper = mount(MthPfCtsConvert, {
    global,
    props: {
      // compoundDescription: "xxx",
      compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
      compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
    }
  });
  // set
  wrapper.vm.localCompoundDescription = undefined;
  await sleep(10);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  // test 'submit' click!
  await wrapper.vm.submitToMicroservice();
  await sleep(10);
  // test new values
  expect(wrapper.vm.isLocked).toBe(false);
  expect(wrapper.vm.localCompoundDescription).toBe(undefined);
  expect(wrapper.vm.status).toBe(Status.Init);
  expect(wrapper.vm.currentHint).toMatch(/^<strong>Info<\/strong>.*/);
  expect(wrapper.vm.currentHintClasses).toBe('hint-info');
  // test rendering
  expect(wrapper.find(".hint-info").exists()).toBe(true);
  expect(wrapper.find(".hint-success").exists()).toBe(false);
  expect(wrapper.find(".hint-warning").exists()).toBe(false);
  expect(wrapper.find(".hint-error").exists()).toBe(false);
  expect(wrapper.find("div.v-input__append").find("i.fas").classes()).toContain("fa-magnifying-glass");
  // unmock
  server.close();
});

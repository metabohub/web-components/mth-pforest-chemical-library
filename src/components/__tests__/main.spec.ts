import { createApp } from 'vue';
import { MetabohubPeakForestChemicalLibrary } from '@/components/main';
import { describe, it } from 'vitest';

// test action
describe('MetabohubPeakForestChemicalLibrary', () => {
    it('should register MthPfDisplayProperty component', () => {
        const app = createApp({});
        MetabohubPeakForestChemicalLibrary.install(app);
    });
});
// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, vi } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfCdkProperties from '@/components/MthPfCdkProperties.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
    plugins: [vuetify]
}

function sleep(milliseconds: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, milliseconds);
    });
}

test('mount component - 404', async () => {
    // mock
    vi.mock('@/openapi-ts/cdk', async () => {
        const actual = await vi.importActual("@/openapi-ts/cdk")
        const response = {
            status: 200,
            data: {
            },
        };
        const Configuration = vi.fn()
        const CdkApi = vi.fn()
        CdkApi.prototype.computeProperties = vi.fn()// 
            .mockResolvedValue(response)
        return {
            ...(actual as object),//
            Configuration, //
            CdkApi,//
        }
    });
    // mount
    expect(MthPfCdkProperties).toBeTruthy()
    const wrapper = mount(MthPfCdkProperties, { global, props: { compoundDescription: "~~caffeine~~" } });
    // test 'submit' click!
    await sleep(100);
    await wrapper.vm.submitToMicroservice()
    await sleep(10);
    // test new values 
    expect(wrapper.vm.status).toBe(Status.Error);
    expect(wrapper.vm.currentHint).toMatch(/^<strong>Error!<\/strong>.*/);
    expect(wrapper.vm.currentHintClasses).toBe('hint-error');
});

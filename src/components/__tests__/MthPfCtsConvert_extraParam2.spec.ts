// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, } from "vitest";
import vuetify from '@/plugins/vuetify';
import { MetabohubPeakForestBasalLibrary, } from '@metabohub/peakforest-basal-library';
// core test
import MthPfCtsConvert, { CompoundIdentifierType, } from '@/components/MthPfCtsConvert.vue';
import { Status } from '../utils/webservicesUtils';

// avoid vuetify error
const global = {
  plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';
const server_OK = setupServer(
  rest.get('*/convert/*', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json([{
        fromIdentifier: "InChIKey",
        results: ["3-[(2S)-1-methyl-2-pyrrolidinyl]pyridine", "3-[(2S)-1-methylpyrrolidin-2-yl]pyridine"],
        searchTerm: "SNICXCGAKADSCV-JTQLQIEISA-N",
        toIdentifier: "Chemical Name"
      }])
    )
  }),
);

test('mount component - autoSubmit  param.', async () => {
  server_OK.listen();
  // mount
  expect(MthPfCtsConvert).toBeTruthy()
  const wrapper = mount(MthPfCtsConvert, {
    global, props: {
      compoundDescription: "SNICXCGAKADSCV-JTQLQIEISA-N",
      autoSubmit: false,
      compoundInputIdentifier: 'InChIKey' as CompoundIdentifierType,
      compoundOutputIdentifier: 'Chemical Name' as CompoundIdentifierType,
    }
  });
  // Check initial data
  expect(wrapper.vm.localAutoSubmit).toBe(false);
  await wrapper.vm.autoSubmitAction();
  await sleep(50);
  expect(wrapper.emitted()['cts-convert-response']).toBe(undefined)
  // set props
  await wrapper.setProps({ autoSubmit: true });
  expect(wrapper.vm.localAutoSubmit).toBe(true);
  // try autosumit false
  await wrapper.vm.autoSubmitAction();
  await sleep(50);
  expect(wrapper.vm.status).toBe(Status.Success)
  expect(wrapper.emitted()['cts-convert-response'].length).toBe(1)
  expect(wrapper.emitted()['cts-convert-response'][0]).toStrictEqual([{
    fromIdentifier: "InChIKey",
    results: ["3-[(2S)-1-methyl-2-pyrrolidinyl]pyridine", "3-[(2S)-1-methylpyrrolidin-2-yl]pyridine"],
    searchTerm: "SNICXCGAKADSCV-JTQLQIEISA-N",
    toIdentifier: "Chemical Name"
  }])
  // set props
  await wrapper.setProps({ 'auto-submit': false });
  // unmock
  server_OK.close();
});

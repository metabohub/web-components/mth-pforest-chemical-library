// import for tests
import { mount } from '@vue/test-utils';
import { expect, test, } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import AMthPfProcessing from '@/components/AMthPfProcessing.vue';

// avoid vuetify error
const global = {
  plugins: [vuetify]
}

test('component', () => {
  // mount
  expect(AMthPfProcessing).toBeTruthy()
});

test('mount component - mock OK', async () => {
  // mount
  expect(AMthPfProcessing).toBeTruthy()
  const wrapper = mount(AMthPfProcessing, { global });
  try {
    wrapper.vm.submitToMicroservice();
  } catch (e) {
    expect((e as Error).message).toBe('ABSTRACT METHOD - IMPLEMENT ME!');
  }
  wrapper.vm.resetCallOverwrite(); // ⇒  do nothing
});



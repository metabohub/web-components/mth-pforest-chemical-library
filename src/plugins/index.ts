/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import { loadFonts } from './webfontloader'
import vuetify from './vuetify'
import router from '../router'

// in-house lib.
import { MetabohubPeakForestBasalLibrary } from '@metabohub/peakforest-basal-library';
import '@metabohub/peakforest-basal-library/dist/src/components/main.css';

// from 'highcharts'
import Highcharts from 'highcharts';
import exportingInit from 'highcharts/modules/exporting';
exportingInit(Highcharts);

// Types
import type { App } from 'vue'

export function registerPlugins(app: App) {
  loadFonts()
  app
    .use(vuetify)//
    .use(MetabohubPeakForestBasalLibrary) // 
    .use(router)//
}

// Composables
import { createWebHashHistory, createRouter } from 'vue-router'

// in-house views
import About from "@/views/About.vue";
import HowToInstall from "@/views/HowToInstall.vue";

// tutorials
import TutoMthPfCdkAbout from "@/views/TutoMthPfCdkAbout.vue";
import TutoMthPfCdkProperties from "@/views/TutoMthPfCdkProperties.vue";
import TutoMthPfCdkConvert from "@/views/TutoMthPfCdkConvert.vue";
import TutoMthPfCdkDepiction from "@/views/TutoMthPfCdkDepiction.vue";
import TutoMthPfInChIAbout from "@/views/TutoMthPfInChIAbout.vue";
import TutoMthPfInChIGeneration from "@/views/TutoMthPfInChIGeneration.vue";
import TutoMthPfGoslinAbout from "@/views/TutoMthPfGoslinAbout.vue";
import TutoMthPfGoslinLipidNameValidation from '@/views/TutoMthPfGoslinLipidNameValidation.vue';
import TutoMthPfDisplayFormattedFormula from '@/views/TutoMthPfDisplayFormattedFormula.vue';
import TutoMthPfDownloadSdfFileButton from '@/views/TutoMthPfDownloadSdfFileButton.vue';
import TutoMthPfHighchartsPeaklistsViewer from '@/views/TutoMthPfHighchartsPeaklistsViewer.vue';
import TutoMthPfGlmolWrapper from '@/views/TutoMthPfGlmolWrapper.vue';
import TutoMthPfCtsConvert from '@/views/TutoMthPfCtsConvert.vue';

// define routes
const routes = [
  // default page
  {
    path: '/',
    redirect: '/how-to-install'
  },
  // main views
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/how-to-install",
    name: "How to Install",
    component: HowToInstall,
  },
  // tutorial views
  {
    path: "/tuto-cdk-about",
    name: "TutoMthPfCdkAbout",
    component: TutoMthPfCdkAbout,
  },
  {
    path: "/tuto-cdk-properties",
    name: "TutoMthPfCdkProperties",
    component: TutoMthPfCdkProperties,
  },
  {
    path: "/tuto-cdk-convert",
    name: "TutoMthPfCdkConvert",
    component: TutoMthPfCdkConvert,
  },
  {
    path: "/tuto-cdk-depiction",
    name: "TutoMthPfCdkDepiction",
    component: TutoMthPfCdkDepiction,
  },
  // inchi
  {
    path: "/tuto-inchi-about",
    name: "TutoMthPfInChIAbout",
    component: TutoMthPfInChIAbout,
  },
  {
    path: "/tuto-inchi-generation",
    name: "TutoMthPfInChIGeneration",
    component: TutoMthPfInChIGeneration,
  },
  // goslin
  {
    path: "/tuto-goslin-about",
    name: "TutoMthPfGoslinAbout",
    component: TutoMthPfGoslinAbout,
  },
  {
    path: "/tuto-goslin-lipid-name-validation",
    name: "TutoMthPfGoslinLipidNameValidation",
    component: TutoMthPfGoslinLipidNameValidation,
  },
  {
    path: "/tuto-formatted-formula",
    name: "TutoMthPfDisplayFormattedFormula",
    component: TutoMthPfDisplayFormattedFormula,
  },
  {
    path: "/tuto-download-sdf-file-button",
    name: "TutoMthPfDownloadSdfFileButton",
    component: TutoMthPfDownloadSdfFileButton,
  },
  {
    path: "/tuto-highcharts-peaklists-viewer",
    name: "TutoMthPfHighchartsPeaklistsViewer",
    component: TutoMthPfHighchartsPeaklistsViewer,
  },
  {
    path: "/tuto-glmol-wrapper",
    name: "TutoMthPfGlmolWrapper",
    component: TutoMthPfGlmolWrapper,
  },
  {
    path: "/tuto-cts-convert",
    name: "TutoMthPfCtsConvert",
    component: TutoMthPfCtsConvert,
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
})

export default router;

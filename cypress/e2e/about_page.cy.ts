


describe('The Home Page', () => {
  it('successfully loads | from nav', () => {
    // connect
    cy.visit('http://localhost:3000/');
    cy.get('[href="#/about"]').click();
    // test header / left menu / footer
    cy.contains("TODO: about page");
    // test URL
    cy.url().should('eq', 'http://localhost:3000/#/about');
  });
});
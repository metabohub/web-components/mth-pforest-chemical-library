import MthPfGoslinAbout from '@/components/MthPfGoslinAbout.vue'

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('<MthPfGoslinAbout />', () => {

  it('renders', () => {
    cy.intercept('GET', '/goslin/about', { fixture: 'about_goslin.json' });
    cy.mount(MthPfGoslinAbout, { global });
    cy.get('span').should('not.be.empty');
    cy.get('.metabohub-peakforest-goslin-about > div > :nth-child(1)').contains('Goslin - RESTful API - MOCK')
    cy.get('.metabohub-peakforest-goslin-about > div > :nth-child(2)').contains('v12.34.56')
    cy.get('.metabohub-peakforest-goslin-about > div > :nth-child(3)').contains('0123456')
  });

})

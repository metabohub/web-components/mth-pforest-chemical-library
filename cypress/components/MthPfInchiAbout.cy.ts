import MthPfInchiAbout from '@/components/MthPfInchiAbout.vue'

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('<MthPfInchiAbout />', () => {

  it('renders', () => {
    cy.intercept('GET', '/inchi/about', { fixture: 'about_inchi.json' });
    cy.mount(MthPfInchiAbout, { global });
    cy.get('span').should('not.be.empty');
    cy.get('.metabohub-peakforest-inchi-about > div > :nth-child(1)').contains('InChI - RESTful API - MOCK');
    cy.get('.metabohub-peakforest-inchi-about > div > :nth-child(2)').contains('v12.34.56');
    cy.get('.metabohub-peakforest-inchi-about > div > :nth-child(3)').contains('0123456');
  });

})

import MthPfCdkProperties from '@/components/MthPfCdkProperties.vue';

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('MthPfCdkProperties', () => {
  it('should display the input field', () => {
    cy.mount(MthPfCdkProperties, { global });
    cy.get('.metabohub-peakforest-cdk-properties .field').should('be.visible');
  });

  it('should get compound properties', () => {
    // https://metabocloud.mesocentre.uca.fr/cdk/properties
    cy.intercept('POST', '/cdk/properties', { fixture: 'properties_cdk.json', delay: 500, });
    cy.mount(MthPfCdkProperties, { global });
    cy.get('.metabohub-peakforest-cdk-properties .field input').type('mock');
    cy.get('.metabohub-peakforest-cdk-properties .field .v-icon.fa-magnifying-glass').click()//
      .then(() => {
        cy.get('.metabohub-peakforest-cdk-properties .field .v-progress-circular').should('be.visible');
        cy.get('.metabohub-peakforest-cdk-properties .hint-info').should('be.visible');
      });
  });

  it('should reset the input field', async () => {
    cy.intercept('POST', '/cdk/properties', { fixture: 'properties_cdk.json' });
    cy.mount(MthPfCdkProperties, { global });
    cy.get('.metabohub-peakforest-cdk-properties .field input').type('mock');
    // reset
    cy.get('.metabohub-peakforest-cdk-properties .field .v-icon.fa-magnifying-glass').click()//
      .then(() => {
        cy.get('.metabohub-peakforest-cdk-properties .field .v-icon').should('have.class', 'fa-rotate-right');
        cy.get('.metabohub-peakforest-cdk-properties .field .v-icon.fa-rotate-right').click();
        cy.get('.metabohub-peakforest-cdk-properties .field input').should('have.value', '');
      });
  });
});
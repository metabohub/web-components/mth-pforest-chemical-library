import MthPfGlmolWrapper from '@/components/MthPfGlmolWrapper.vue'
import caffeineMolFile from "@/assets/resources/caffeine.mol?raw";

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('<MthPfGlmolWrapper />', () => {

  it('renders', () => {
    cy.mount(MthPfGlmolWrapper,
      { global, props: { 'mol-file': caffeineMolFile } },

    );
    cy.get('small').should('have.text', "Powered thanks GLmol!");
    cy.get('.v-field').should('exist');
    cy.get('.v-btn').should('exist');
  });

  it('click download button', () => {
    // test
    cy.mount(MthPfGlmolWrapper, { global, props: { 'mol-file': caffeineMolFile } });
    // download
    cy.get('.v-btn').click();
    // read
    cy.readFile('cypress/downloads/download.png').should('contain', `PNG\r\n`)
  });

  it('no dnl btn', () => {
    cy.mount(MthPfGlmolWrapper,
      { global, props: { 'mol-file': caffeineMolFile, optShowSaveBtn: false } },

    );
    cy.get('small').should('have.text', "Powered thanks GLmol!");
    cy.get('.v-field').should('exist');
    cy.get('.v-btn').should('not.exist');
  });

  it('no dnl switch style', () => {
    cy.mount(MthPfGlmolWrapper,
      { global, props: { 'mol-file': caffeineMolFile, optShowMenuChangeStyle: false } },

    );
    cy.get('small').should('have.text', "Powered thanks GLmol!");
    cy.get('.v-field').should('not.exist');
    cy.get('.v-btn').should('exist');
  });

});

import MthPfCtsConvert from '@/components/MthPfCtsConvert.vue';

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('MthPfCtsConvert', () => {
  it('should display the input field', () => {
    cy.mount(MthPfCtsConvert, { global });
    cy.get('.metabohub-peakforest-cts-convert .field').should('be.visible');
  });

  it('should get compound properties', () => {
    // https://cts.fiehnlab.ucdavis.edu/services/rest/convert
    cy.intercept('POST', '//rest/convert', { fixture: 'convert_cts.json', delay: 500, });
    cy.mount(MthPfCtsConvert, { global });
    cy.get('.metabohub-peakforest-cts-convert .field input').type('mock');
    cy.get('.metabohub-peakforest-cts-convert .field .v-icon.fa-magnifying-glass').click()//
      .then(() => {
        cy.get('.metabohub-peakforest-cts-convert .field .v-progress-circular').should('be.visible');
        cy.get('.metabohub-peakforest-cts-convert .hint-info').should('be.visible');
      });
  });

  it('should reset the input field', async () => {
    cy.intercept('POST', '//rest/convert', { fixture: 'convert_cts.json' });
    cy.mount(MthPfCtsConvert, { global });
    cy.get('.metabohub-peakforest-cts-convert .field input').type('mock');
    // reset
    cy.get('.metabohub-peakforest-cts-convert .field .v-icon.fa-magnifying-glass').click()//
      .then(() => {
        cy.get('.metabohub-peakforest-cts-convert .field .v-icon').should('have.class', 'fa-rotate-right');
        cy.get('.metabohub-peakforest-cts-convert .field .v-icon.fa-rotate-right').click();
        cy.get('.metabohub-peakforest-cts-convert .field input').should('have.value', '');
      });
  });
});
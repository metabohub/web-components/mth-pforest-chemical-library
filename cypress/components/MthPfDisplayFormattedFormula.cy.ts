import MthPfDisplayFormattedFormula from '@/components/MthPfDisplayFormattedFormula.vue';

import vuetify from '@/plugins/vuetify';
import { MetabohubPeakForestBasalLibrary } from '@metabohub/peakforest-basal-library';
import '@metabohub/peakforest-basal-library/dist/src/components/main.css';

const global = {
  plugins: [vuetify, MetabohubPeakForestBasalLibrary]
}

describe('MthPfDisplayFormattedFormula', () => {
  it('test displays the component', () => {
    cy.mount(MthPfDisplayFormattedFormula, { global });
    cy.get('.metabohub-peakforest-display-formatted-formula').should('be.visible');
  });
});

describe('MthPfDisplayFormattedFormula - test content', () => {
  it('test display the formatted formula', () => {
    cy.mount(MthPfDisplayFormattedFormula, { global, props: { formulaValue: "C2H5OH" } });
    cy.get('.v-field__input > span').should('have.html', "C<sub>2</sub>H<sub>5</sub>OH");
  });

  it('test copyToClipboard', async () => {
    // overwrite navigator clipboard method
    let clipboardContents = "";
    Object.defineProperty(navigator, "clipboard", {
      value: {
        writeText: async (text: string) => {
          clipboardContents = text;
        },
        readText: () => clipboardContents,
      },
    });
    // mount component
    cy.mount(MthPfDisplayFormattedFormula, { global, props: { formulaValue: "data to copy in clipboard" } })
    // test rendering
    cy.get('span').should('have.text', "data to copy in clipboard");
    cy.get(".v-input__append > .fas").should('have.class', 'fa-clipboard');
    // test clipboard before
    expect(clipboardContents).to.eq("");
    expect(window.navigator.clipboard.readText()).to.eq("");
    // copy action
    cy.get('.fas').click();
    // test rendering
    cy.get(".v-input__append > .fas").should('have.class', 'fa-circle-check fa-bounce');
    // test clipboard after
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("data to copy in clipboard");
      expect(window.navigator.clipboard.readText()).to.eq("data to copy in clipboard");
    })
    // test after more than 1800 ms
    cy.wait(3000).then(() => {
      cy.get(".v-input__append > .fas").should('have.class', 'fa-clipboard');
    });
  })

});
import MthPfInchiGeneration from '@/components/MthPfInchiGeneration.vue';

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('MthPfInchiGeneration', () => {
  it('should display the input field', () => {
    cy.mount(MthPfInchiGeneration, { global });
    cy.get('.metabohub-peakforest-inchi-generation .field').should('be.visible');
  });

  it('should get compound generate', () => {
    // https://metabocloud.mesocentre.uca.fr/inchi/generate
    cy.intercept('POST', '/inchi/generate', { fixture: 'generate_inchi.json', delay: 500, });
    cy.mount(MthPfInchiGeneration, { global });
    cy.get('.metabohub-peakforest-inchi-generation .field textarea').type('mock');
    cy.get('.metabohub-peakforest-inchi-generation .field .v-icon.fa-magnifying-glass').click()//
      .then(() => {
        cy.get('.metabohub-peakforest-inchi-generation .field .v-progress-circular').should('be.visible');
        cy.get('.metabohub-peakforest-inchi-generation .hint-info').should('be.visible');
      });
  });

  it('should reset the input field', async () => {
    cy.intercept('POST', '/inchi/generate', { fixture: 'generate_inchi.json' });
    cy.mount(MthPfInchiGeneration, { global });
    cy.get('.metabohub-peakforest-inchi-generation .field input').type('mock');
    // reset
    cy.get('.metabohub-peakforest-inchi-generation .field .v-icon.fa-magnifying-glass').click()//
      .then(() => {
        cy.get('.metabohub-peakforest-inchi-generation .field .v-icon').should('have.class', 'fa-rotate-right');
        cy.get('.metabohub-peakforest-inchi-generation .field .v-icon.fa-rotate-right').click();
        cy.get('.metabohub-peakforest-inchi-generation .field input').should('have.value', '');
      });
  });
});
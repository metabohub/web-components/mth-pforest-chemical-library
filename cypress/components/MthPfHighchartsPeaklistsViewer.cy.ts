import MthPfHighchartsPeaklistsViewer, { Peaklist } from '@/components/MthPfHighchartsPeaklistsViewer.vue'

import HighchartsVue from 'highcharts-vue';
const global = {
  plugins: [HighchartsVue]
}

// DATA FOR TESTS 
import { peaklist1, peaklist2, peaklist3, peaklist4 } from '@/assets/resources/peaklists';
// peakslists arrays
const peaklistsMass = [] as Peaklist[];
peaklistsMass.push(peaklist1);
peaklistsMass.push(peaklist2);
const peaklistsNmr = [] as Peaklist[];
peaklistsNmr.push(peaklist3);
peaklistsNmr.push(peaklist4);

describe('<MthPfHighchartsPeaklistsViewer />', () => {

  it('renders - show MS, default size', () => {
    cy.mount(MthPfHighchartsPeaklistsViewer, {
      global, props: {
        // mandatory
        'viewer-title': 'title',
        'viewer-peaklists': peaklistsMass,
      },
    });
    cy.get('div.hightcharts-viewer').should('not.be.empty');
    cy.get('div.metabohub-peakforest-highcharts-peaklists-viewer')
      .and('have.css', 'width')
      .should('include', '500px');
    cy.get('div.metabohub-peakforest-highcharts-peaklists-viewer')
      .and('have.css', 'height')
      .should('include', '350px');
    cy.get('small').contains('Powered thanks Highcharts!');
  });

  it('renders - show NMR, custum size', () => {
    cy.mount(MthPfHighchartsPeaklistsViewer, {
      global, props: {
        // mandatory
        'viewer-title': 'title',
        'viewer-peaklists': peaklistsNmr,
        // to test
        'viewer-height': '600px',
        'viewer-width': '100%',
      },
    });
    cy.get('div.metabohub-peakforest-highcharts-peaklists-viewer')
      .and('have.css', 'width')
      .should('include', 'px');// can't be 100%; convert percent in pixels
    cy.get('div.metabohub-peakforest-highcharts-peaklists-viewer')
      .and('have.css', 'height')
      .should('include', '600px');
  });

})

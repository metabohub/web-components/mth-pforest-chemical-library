import MthPfCdkAbout from '@/components/MthPfCdkAbout.vue'

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('<MthPfCdkAbout />', () => {

  it('renders', () => {
    cy.intercept('GET', '/cdk/about', { fixture: 'about_cdk.json' });
    cy.mount(MthPfCdkAbout, { global });
    cy.get('span').should('not.be.empty');
    cy.get('.metabohub-peakforest-cdk-about > div > :nth-child(1)').contains('CDK - RESTful API - MOCK')
    cy.get('.metabohub-peakforest-cdk-about > div > :nth-child(2)').contains('v12.34.56')
    cy.get('.metabohub-peakforest-cdk-about > div > :nth-child(3)').contains('0123456')
  });

})

import MthPfGoslinLipidNameValidation from '@/components/MthPfGoslinLipidNameValidation.vue';

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('MthPfGoslinLipidNameValidation', () => {
  it('should display the input field', () => {
    cy.mount(MthPfGoslinLipidNameValidation, { global });
    cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field').should('be.visible');
  });

  it('should validate the lipid name', () => {
    // https://metabocloud.mesocentre.uca.fr/goslin/validate?lipids_names=mock
    cy.intercept('GET', '/goslin/validate/?lipids_names=mock', { fixture: 'validate_goslin.json' });
    cy.mount(MthPfGoslinLipidNameValidation, { global });
    cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field input').type('mock');
    cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field .v-icon.fa-magnifying-glass').click();
    cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field .v-progress-circular').should('be.visible');
    // cy.wait('@goslinMicroservice');
    cy.get('.metabohub-peakforest-goslin-lipid-name-validation .hint-info').should('be.visible');
    // cy.wait(500).then(() => {
    //   cy.get('.metabohub-peakforest-goslin-lipid-name-validation .hint-info').should('not.be.visible');
    //   cy.get('.metabohub-peakforest-goslin-lipid-name-validation .hint-success').should('be.visible');
    // });
  });

  it('should reset the input field', async () => {
    cy.intercept('GET', '/goslin/validate/?lipids_names=mock', { fixture: 'validate_goslin.json' });
    cy.mount(MthPfGoslinLipidNameValidation, { global });
    cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field input').type('mock');
    
    //
    cy.wait(//
      cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field .v-icon.fa-magnifying-glass').click()//
    )//
      .then(() => {
        cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field .v-icon').should('have.class', 'fa-rotate-right');
        cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field .v-icon.fa-rotate-right').click();
        cy.get('.metabohub-peakforest-goslin-lipid-name-validation .field input').should('have.value', '');
      });
  });
});
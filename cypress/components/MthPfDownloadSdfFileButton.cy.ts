import MthPfDownloadSdfFileButton from '@/components/MthPfDownloadSdfFileButton.vue'

import vuetify from '@/plugins/vuetify';

const global = {
  plugins: [vuetify]
}

describe('<MthPfDownloadSdfFileButton />', () => {

  it('renders', () => {
    cy.mount(MthPfDownloadSdfFileButton, { global });
    cy.get('.v-btn').should('have.text', "Save as SDF file");
  });

  it('set slot', () => {
    cy.mount(MthPfDownloadSdfFileButton, {
      global, slots: {
        default: "Test Button"
      }
    });
    cy.get('.v-btn').should('have.text', "Test Button");
  });

  it('click download button', () => {
    // test
    cy.mount(MthPfDownloadSdfFileButton, { global, props: { molFile: "MOL", extraData: [{ key: "K", value: "V", }] } });
    // download
    cy.get('.v-btn').click();
    // read
    cy.readFile('cypress/downloads/sdf_file.sdf').should('contain', `MOL\n\n> <K>\nV\n\n$$$$\n`)
  });

  it('click download button / set file name', () => {
    // test
    cy.mount(MthPfDownloadSdfFileButton, { global, props: { molFile: "MOL", fileName: "test_xxx" } });
    // download
    cy.get('.v-btn').click();
    // read
    cy.readFile('cypress/downloads/test_xxx.sdf').should('contain', `MOL\n\n$$$$\n`)
  });

});

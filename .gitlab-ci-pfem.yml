# -----------------------------------------------------------------------------
# parent files
include:
  - remote: 'https://unh-pfem-gitlab.ara.inrae.fr/pfem-public/pfem-gitlab-tools/-/raw/dev/cicd-templates/jobs/gitlab-ci-npm-jobs.yml'

# -----------------------------------------------------------------------------
# maven env.
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  # DEV
  SSH_HOST_DEV: "unh-pfemlindev.ara.inrae.fr"
  PATH_APP_DEV: "/var/www/html/mth-pforest-chemical-lib"
  # reports
  FOLDER_REPORT_COVERAGE: "/var/www/html/code_report_$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"

# -----------------------------------------------------------------------------
# cache
cache:
  key: ${CI_COMMIT_REF_SLUG}
  policy: pull-push
  paths:
  - node_modules/
  - src/assets/json/git-version.json
  - artifacts/
  - public/assets/
  - lib/
  - metabohub-*.tgz
  - coverage/

# -----------------------------------------------------------------------------
# stages
stages:
  - build
  - tests
  - deploy_and_publish
  - reports

# -----------------------------------------------------------------------------
## JOB - TESTS

# test if linter is OK
execute linting:
  stage: tests
  needs: ["build standalone - dev"]
  dependencies: ["build standalone - dev"]
  extends:
   - .tests_execute_linting

# tests - unit and coverage
tests - unit:
  stage: tests
  needs: ["build standalone - dev"]
  dependencies: ["build standalone - dev"]
  extends:
    - .tests_unit

# test - e2e (pages)
tests - e2e, pages:
  stage: tests
  needs: ["build standalone - dev"]
  dependencies: ["build standalone - dev"]
  extends:
    - .tests_e2e_pages

# test - e2e (components)
tests - e2e, components:
  stage: tests
  needs: ["build standalone - dev"]
  dependencies: ["build standalone - dev"]
  extends:
    - .tests_e2e_components

# -----------------------------------------------------------------------------
## JOB - BUILD

# build standalone - prod:
#   stage: build
#   tags:
#     - docker
#   only:
#     refs:
#       - master
#       - tags
#   before_script:
#     - echo "==================================";
#     - echo "[info] clean old node_modules cache";
#     - rm -rf node_modules
#     - echo "[info] clean generated sources";
#     - rm -rf $CI_PROJECT_DIR/dist
#     - echo "[info] init node modules ";
#     - npm install
#   script:
#     - echo "==================================";
#     - echo "[info] vuejs build app (prod)";
#     - nodejs git-version.js
#     - npm run build:prod
#   after_script:
#     - echo "==================================";
#     - echo "[info] copy to artifacts ";
#     - test -d $CI_PROJECT_DIR/artifacts || mkdir $CI_PROJECT_DIR/artifacts/
#     - cp -r $CI_PROJECT_DIR/dist/mth-pforest-chemical-* $CI_PROJECT_DIR/artifacts/

build standalone - dev:
  stage: build
  extends:
    - .tag_docker
    - .except_master
    - .build_standalone_dev_job

build library - dev:
  stage: build
  extends:
    - .tag_docker
    - .build_lib_dev_before_script
  script:
    - echo "==================================";
    - echo "[info] vuejs build library";
    - npm run build:lib
    - npm pack
  after_script:
    - echo "==================================";
    - echo "[info] copy to lib ";
    - test -d $CI_PROJECT_DIR/lib || mkdir $CI_PROJECT_DIR/lib/
    - cp -r $CI_PROJECT_DIR/dist/* $CI_PROJECT_DIR/lib/
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME-lib
    expire_in: 1 day
    paths:
      - lib

# -----------------------------------------------------------------------------
## JOBS - DEPLOY

# deploy prod:
#   stage: deploy
#   tags:
#     - docker
#   image: "pfem/ssh-utils"
#   only:
#     refs:
#       - tags
#   before_script:
#     - echo "==================================";
#     - echo "[info] init SSH key";
#     - mkdir -p ~/.ssh
#     - eval $(ssh-agent -s)
#     - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
#     - ssh-add <(echo "$SSH_PRIVATE_KEY" | base64 -d)
#   script:
#     - echo "==================================";
#     - echo "[info] remove old app on $SSH_HOST_PROD ";
#     - ssh $SSH_OPTS $SSH_USER@$SSH_HOST_PROD "rm -rf $PATH_APP_PROD/* "
#     - echo "[info] update app on $SSH_HOST_PROD ";
#     - scp -r $CI_PROJECT_DIR/artifacts/mth-pforest-chemical-library/* $SSH_USER@$SSH_HOST_PROD:$PATH_APP_PROD/

deploy standalone - dev:
  stage: deploy_and_publish
  needs: ["build standalone - dev"]
  extends:
    - .tag_docker
    - .before_script_prepare_ssh
    - .except_master
  script:
    - echo "==================================";
    - echo "[info] remove old app on $SSH_HOST_DEV ";
    - ssh $SSH_OPTS $SSH_USER@$SSH_HOST_DEV "rm -rf $PATH_APP_DEV/* "
    - echo "[info] update app on $SSH_HOST_DEV ";
    - scp -r $CI_PROJECT_DIR/artifacts/* $SSH_USER@$SSH_HOST_DEV:$PATH_APP_DEV/

# -----------------------------------------------------------------------------
## JOB - PUBLISH

publish library - dev:
  stage: deploy_and_publish
  needs: ["build library - dev"]
  dependencies: ["build library - dev"]
  extends:
    - .tag_docker
    - .publish_lib_dev_before_script
  script:
    - echo "==================================";
    - echo "[info] vuejs build app (dev)";
    - npm publish

# -----------------------------------------------------------------------------
## JOBS - STAGE "reports"

# jobs - code report / coverage
publish report - code coverage:
  stage: reports
  extends:
    - .publish_report_code_coverage

# [end]
